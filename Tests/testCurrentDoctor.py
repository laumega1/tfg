from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import time

class UnitTestCurrentDoctor(unittest.TestCase):
    def setUp(self):
        global driver
        driver = webdriver.Chrome()
        driver.get("http://localhost:4200/#/login")

        # It is necesary to login each time beacause Selenium not save localstorage
        emailInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[1]/span/input')
        passwordInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[2]/span/input')
        button = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[3]/button')
        if emailInput is not None and passwordInput is not None:
            emailInput.send_keys("lauramelerogarrigos@gmail.com")
            passwordInput.send_keys("11223344")
            button.click()

    #--------------------------------------------------------------
    # testEditCurrentDoctorAdmin()
    #--------------------------------------------------------------
    # Unit test that modifies our status as doctors. You will start
    # the process as an administrator and change your status to
    # non-administrator
    #--------------------------------------------------------------    
    def testEditCurrentDoctorAdmin(self):
        driver.implicitly_wait(2)
        gotToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        gotToSettingsBtn.click()
        
        editPatientBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/p-panel/div/div[2]/div/div/div/div/div[2]/button[2]')
        editPatientBtn.click()
        time.sleep(1)

        checkBox = driver.find_element(By.XPATH, '//*[@id="isAdminCheckbox"]/div/div[2]')
        checkBox.click()

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/app-modal-doctors/p-dialog/div/div/div[2]/form/div[6]/button')
        button.click()
        
        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/app-modal-doctors/p-dialog/div/div/div[2]/p-messages/div/div')
            print("La prueba ha sido un éxito, se ha modificado el estado del doctor correctamente. Pasa de isAdmin = true a isAdmin = false")
        except:
            print("La prueba ha fallado, no se ha podido cambiar el estado del doctor")

        checkBox = driver.find_element(By.XPATH, '//*[@id="isAdminCheckbox"]/div/div[2]')
        checkBox.click()

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/app-modal-doctors/p-dialog/div/div/div[2]/form/div[6]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/app-modal-doctors/p-dialog/div/div/div[2]/p-messages/div/div')
            print("La prueba ha sido un éxito, no se ha cambiado el estado del doctor porque no es administrador")
        except:
            print("La prueba ha fallado, no se ha podido cambiar el estado del doctor")

    #--------------------------------------------------------------
    # testEditCurrentDoctorPasswordError()
    #--------------------------------------------------------------
    # Unit test developed to modify the password of the doctor, in
    # this case, we want both passwords to be incorrect to ensure
    # that there is no error
    #--------------------------------------------------------------
    def testEditCurrentDoctorPasswordError(self):
        driver.implicitly_wait(2)
        goToCalendarBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        goToCalendarBtn.click()
        
        editPatientBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/p-panel/div/div[2]/div/div/div/div/div[2]/button[2]')
        editPatientBtn.click()
        time.sleep(1)

        password = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/app-modal-doctors/p-dialog/div/div/div[2]/form/div[3]/div[1]/input')
        password.clear()
        password.send_keys("1234")

        passwordRepeat = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/app-modal-doctors/p-dialog/div/div/div[2]/form/div[3]/div[2]/input')
        passwordRepeat.clear()
        passwordRepeat.send_keys("123456")

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/app-modal-doctors/p-dialog/div/div/div[2]/form/div[6]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/app-modal-doctors/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-error" in typeError:
                print("La prueba ha sido un éxito, se ha generado un error al ser las 2 contraseñas diferentes")
            else:
                print("La prueba ha fallado, no se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido cambiar el estado del doctor")

    #--------------------------------------------------------------
    # testEditCurrentDoctorPasswordCorrect()
    #--------------------------------------------------------------
    # Unit test developed to modify the doctor's password, in this
    # case, we want both passwords to be correct so that it can be
    # modified
    #--------------------------------------------------------------
    def testEditCurrentDoctorPasswordCorrect(self):
        driver.implicitly_wait(2)
        gotToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        gotToSettingsBtn.click()
        
        editPatientBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/p-panel/div/div[2]/div/div/div/div/div[2]/button[2]')
        editPatientBtn.click()
        time.sleep(1)

        password = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/app-modal-doctors/p-dialog/div/div/div[2]/form/div[3]/div[1]/input')
        password.clear()
        password.send_keys("11223344")

        passwordRepeat = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/app-modal-doctors/p-dialog/div/div/div[2]/form/div[3]/div[2]/input')
        passwordRepeat.clear()
        passwordRepeat.send_keys("11223344")

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/app-modal-doctors/p-dialog/div/div/div[2]/form/div[6]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctor-resume/app-modal-doctors/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, se ha cambiado la contraseña correctamente")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido cambiar la contraseña")
                
    def tearDown(self):
        driver.quit()

if __name__ == "__main__":
    unittest.main()
