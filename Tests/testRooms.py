from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import time

class UnitTestRooms(unittest.TestCase):
    def setUp(self):
        global driver
        driver = webdriver.Chrome()
        driver.get("http://localhost:4200/#/login")

        # It is necesary to login each time beacause Selenium not save localstorage
        emailInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[1]/span/input')
        passwordInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[2]/span/input')
        button = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[3]/button')
        if emailInput is not None and passwordInput is not None:
            emailInput.send_keys("lauramelerogarrigos@gmail.com")
            passwordInput.send_keys("11223344")
            button.click()

    #--------------------------------------------------------------
    # testEditRoomWithDuplicateCoordinator()
    #--------------------------------------------------------------
    # Unit test to try to modify a room by introducing a MAC for
    # the coordinator different from the current one but that
    # already exists in the database. This should generate an error
    # message
    #--------------------------------------------------------------
    def testEditRoomWithDuplicateCoordinator(self):
        driver.implicitly_wait(2)
        goToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        goToSettingsBtn.click()
        
        editRoomBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-10-content"]/div/div[2]/div/div[3]/div/div[2]/button')
        editRoomBtn.click()
        time.sleep(1)

        macCoordinator = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[2]/div[2]/input')
        macCoordinator.clear()
        macCoordinator.send_keys("00:ab:01:a3:20:y1")

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[4]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-error" in typeError:
                print("La prueba ha sido un éxito, no se ha modificado la sala ya que la MAC del coordinador está repetida")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de éxito")
        except:
            print("La prueba ha fallado, no ha aparecido ningún mensaje en cuanto a la sala")

    #--------------------------------------------------------------
    # testEditRoomWithDuplicateIpApplication()
    #--------------------------------------------------------------
    # Unit test to try to modify a room by entering an IP for the
    # application different from the current one but that already
    # exists in the database. This should generate an error message
    #--------------------------------------------------------------
    def testEditRoomWithDuplicateIpApplication(self):
        driver.implicitly_wait(2)
        goToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        goToSettingsBtn.click()
        
        editRoomBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-10-content"]/div/div[2]/div/div[3]/div/div[2]/button')
        editRoomBtn.click()
        time.sleep(1)

        ipApplication = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[3]/div[1]/input')
        ipApplication.clear()
        ipApplication.send_keys("192.168.56.82")

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[4]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-error" in typeError:
                print("La prueba ha sido un éxito, no se ha modificado la sala ya que la IP de la aplicación está repetida")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de éxito")
        except:
            print("La prueba ha fallado, no ha aparecido ningún mensaje en cuanto a la sala")

    #--------------------------------------------------------------
    # testEditRoomWithDuplicateMACApplication()
    #--------------------------------------------------------------
    # Unit test to try to modify a room by entering an MAC for the
    # application different from the current one but that already
    # exists in the database. This should generate an error message
    #--------------------------------------------------------------
    def testEditRoomWithDuplicateMACApplication(self):
        driver.implicitly_wait(2)
        goToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        goToSettingsBtn.click()
        
        editRoomBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-10-content"]/div/div[2]/div/div[3]/div/div[2]/button')
        editRoomBtn.click()
        time.sleep(1)

        macApplication = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[3]/div[2]/input')
        macApplication.clear()
        macApplication.send_keys("fe::32:5a:2e38:60")

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[4]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-error" in typeError:
                print("La prueba ha sido un éxito, no se ha modificado la sala ya que la MAC de la aplicación está repetida")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de éxito")
        except:
            print("La prueba ha fallado, no ha aparecido ningún mensaje en cuanto a la sala")

    #--------------------------------------------------------------
    # testEditRoom()
    #--------------------------------------------------------------
    # Unit test to try to modify a room correctly to check if data
    # is saved
    #--------------------------------------------------------------
    def testEditRoom(self):
        driver.implicitly_wait(2)
        goToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        goToSettingsBtn.click()
        
        editRoomBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-10-content"]/div/div[2]/div/div[3]/div/div[2]/button')
        editRoomBtn.click()
        time.sleep(1)

        name = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[1]/div/input')
        name.clear()
        name.send_keys("Sala videollamadas planta baja")

        drowpdown = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[2]/div[1]/p-dropdown/div/div[2]')
        drowpdown.click()
        time.sleep(1)

        dropdownSelectedOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[3]')
        dropdownSelectedOption.click()
        
        ipApplication = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[3]/div[1]/input')
        ipApplication.clear()
        ipApplication.send_keys("192.168.56.63")

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[4]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, la sala se ha mofificado correctamente")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido actualizar la sala")

    #--------------------------------------------------------------
    # testNewRoom()
    #--------------------------------------------------------------
    # Unit test to try to create a room correctly to check if data
    # is saved
    #--------------------------------------------------------------
    def testNewRoom(self):
        driver.implicitly_wait(2)
        goToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        goToSettingsBtn.click()
        
        createRoomBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-10-content"]/div/div[1]/div[2]/button[2]')
        createRoomBtn.click()
        time.sleep(1)

        name = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[1]/div/input')
        name.clear()
        name.send_keys("Sala videollamadas Selenium")

        drowpdown = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[2]/div[1]/p-dropdown/div/div[2]')
        drowpdown.click()
        time.sleep(1)

        dropdownSelectedOption = driver.find_element(By.XPATH, '//html/body/div/div[2]/ul/p-dropdownitem[2]')
        dropdownSelectedOption.click()

        macCoordinator = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[2]/div[2]/input')
        macCoordinator.clear()
        macCoordinator.send_keys("69:ab:f4:3b:2c:e4")
        
        ipApplication = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[3]/div[1]/input')
        ipApplication.clear()
        ipApplication.send_keys("192.168.54.23")

        macApplication = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[3]/div[2]/input')
        macApplication.clear()
        macApplication.send_keys("fe:54:2b:00:b5:3c")

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/form/div[4]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-rooms-list/app-modal-rooms/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, la sala se ha creado correctamente")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido crear la sala")
                
    def tearDown(self):
        driver.quit()

if __name__ == "__main__":
    unittest.main()
