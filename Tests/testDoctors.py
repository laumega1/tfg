from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import time

class UnitTestDoctors(unittest.TestCase):
    def setUp(self):
        global driver
        driver = webdriver.Chrome()
        driver.get("http://localhost:4200/#/login")

        # It is necesary to login each time beacause Selenium not save localstorage
        emailInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[1]/span/input')
        passwordInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[2]/span/input')
        button = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[3]/button')
        if emailInput is not None and passwordInput is not None:
            emailInput.send_keys("lauramelerogarrigos@gmail.com")
            passwordInput.send_keys("11223344")
            button.click()

    #--------------------------------------------------------------
    # testEditDoctor()
    #--------------------------------------------------------------
    # Unit test developed to modify doctor's information such as
    # name, surname, password, admin permissions and basic health
    # zones
    #--------------------------------------------------------------
    def testEditDoctor(self):
        driver.implicitly_wait(2)
        gotToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        gotToSettingsBtn.click()
        
        editPatientBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-7-content"]/div/div[2]/div/div[3]/div/div[2]/button')
        editPatientBtn.click()
        time.sleep(1)

        name = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[1]/div[1]/input')
        name.clear()
        name.send_keys("Juanito")

        surname = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[1]/div[2]/input')
        surname.clear()
        surname.send_keys("Arman Manzanillo")

        password = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[3]/div[1]/input')
        password.clear()
        password.send_keys("11223344")

        passwordRepeat = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[3]/div[2]/input')
        passwordRepeat.clear()
        passwordRepeat.send_keys("11223344")

        checkBox = driver.find_element(By.XPATH, '//*[@id="isAdminCheckbox"]/div/div[2]')
        checkBox.click()

        dropdown = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[5]/p/p-dropdown/div/div[2]')
        dropdown.click()
        time.sleep(1)

        dropdownSelectedOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[1]')
        dropdownSelectedOption.click()       
        
        deleteZBS = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[5]/p/p-chip/div/span')
        deleteZBS.click()

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[6]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, se ha modificado el médico")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido modificar el médico")

    #--------------------------------------------------------------
    # testNewDoctor()
    #--------------------------------------------------------------
    # Unit test developed to create a new doctor
    #--------------------------------------------------------------
    def testNewDoctor(self):
        driver.implicitly_wait(2)
        gotToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        gotToSettingsBtn.click()
        
        createPatientBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-7-content"]/div/div[1]/div[2]/button[2]')
        createPatientBtn.click()
        time.sleep(1)

        name = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[1]/div[1]/input')
        name.send_keys("Dr. Selenium")

        surname = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[1]/div[2]/input')
        surname.send_keys("Software Test")

        email = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[2]/input')
        email.send_keys("selenium@test.com")

        password = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[3]/div[1]/input')
        password.send_keys("1234")

        passwordRepeat = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[3]/div[2]/input')
        passwordRepeat.send_keys("1234")

        checkBox = driver.find_element(By.XPATH, '//*[@id="isAdminCheckbox"]/div/div[2]')
        checkBox.click()

        dropdown = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[5]/p/p-dropdown/div/div[2]')
        dropdown.click()
        time.sleep(1)

        dropdownSelectedOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[1]')
        dropdownSelectedOption.click()

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/form/div[6]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[1]/app-doctors-list/app-modal-doctors/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, se ha creado el médico")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido crear el médico")
                
    def tearDown(self):
        driver.quit()

if __name__ == "__main__":
    unittest.main()
