from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import time

class UnitTestCenters(unittest.TestCase):
    def setUp(self):
        global driver
        driver = webdriver.Chrome()
        driver.get("http://localhost:4200/#/login")

        # It is necesary to login each time beacause Selenium not save localstorage
        emailInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[1]/span/input')
        passwordInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[2]/span/input')
        button = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[3]/button')
        if emailInput is not None and passwordInput is not None:
            emailInput.send_keys("lauramelerogarrigos@gmail.com")
            passwordInput.send_keys("11223344")
            button.click()

    #--------------------------------------------------------------
    # testEditCenter()
    #--------------------------------------------------------------
    # Unit test to try to edit a center
    #--------------------------------------------------------------
    def testEditCenter(self):
        driver.implicitly_wait(2)
        gotToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        gotToSettingsBtn.click()
        
        editBHZBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-9-content"]/div/div[2]/div/div[3]/div/div[2]/button')
        editBHZBtn.click()
        time.sleep(1)

        name = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-centers-list/app-modal-centers/p-dialog/div/div/div[2]/form/div[1]/div[1]/input')
        name.clear()
        name.send_keys("Hospital comarcal de Vinaròs")

        phone = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-centers-list/app-modal-centers/p-dialog/div/div/div[2]/form/div[1]/div[2]/input')
        phone.clear()
        phone.send_keys("795663846")

        zbsBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-centers-list/app-modal-centers/p-dialog/div/div/div[2]/form/div[2]/div/p-dropdown/div/div[2]')
        zbsBtn.click()
        time.sleep(1)

        zbsSelectedOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[2]')
        zbsSelectedOption.click()

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-centers-list/app-modal-centers/p-dialog/div/div/div[2]/form/div[4]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-centers-list/app-modal-centers/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, se ha modificado el centro de forma correcta")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido modificar el centro")

    #--------------------------------------------------------------
    # testNewCenter()
    #--------------------------------------------------------------
    # Unit test developed to create a new center
    #--------------------------------------------------------------
    def testNewCenter(self):
        driver.implicitly_wait(2)
        gotToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        gotToSettingsBtn.click()
        
        createBHZBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-9-content"]/div/div[1]/div[2]/button[2]')
        createBHZBtn.click()
        time.sleep(1)

        name = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-centers-list/app-modal-centers/p-dialog/div/div/div[2]/form/div[1]/div[1]/input')
        name.clear()
        name.send_keys("Centro de salud del Grau")

        phone = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-centers-list/app-modal-centers/p-dialog/div/div/div[2]/form/div[1]/div[2]/input')
        phone.clear()
        phone.send_keys("622084958")

        zbsBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-centers-list/app-modal-centers/p-dialog/div/div/div[2]/form/div[2]/div/p-dropdown/div/div[2]')
        zbsBtn.click()
        time.sleep(1)

        zbsSelectedOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[2]')
        zbsSelectedOption.click()

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-centers-list/app-modal-centers/p-dialog/div/div/div[2]/form/div[3]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-centers-list/app-modal-centers/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, se ha creado el centro de forma correcta")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido crear el centro")
                
    def tearDown(self):
        driver.quit()

if __name__ == "__main__":
    unittest.main()
