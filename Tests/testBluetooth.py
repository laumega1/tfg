import bluetooth

totalTrys = 10

#-------------------------------------------------------------------------

def foundAllDevices():
    nearbyDevices = bluetooth.discover_devices(duration=8, lookup_names=True, flush_cache=True, lookup_class=False)
    print("Found {} devices.".format(len(nearbyDevices)))

    for addr,name in nearbyDevices:
        print(" {} - {}".format(addr,name))

#-------------------------------------------------------------------------
        
while(totalTrys != 0):
    foundAllDevices()
    totalTrys = totalTrys-1;