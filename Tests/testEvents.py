from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import time

class UnitTestEvents(unittest.TestCase):
    def setUp(self):
        global driver
        driver = webdriver.Chrome()
        driver.get("http://localhost:4200/#/login")

        # It is necesary to login each time beacause Selenium not save localstorage
        emailInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[1]/span/input')
        passwordInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[2]/span/input')
        button = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[3]/button')
        if emailInput is not None and passwordInput is not None:
            emailInput.send_keys("lauramelerogarrigos@gmail.com")
            passwordInput.send_keys("11223344")
            button.click()

    #--------------------------------------------------------------
    # testNewEventFromHome()
    #--------------------------------------------------------------
    # Unit test that checks if an event is added correctly from the
    # start menu or not
    #--------------------------------------------------------------    
    def testNewEventFromHome(self):
        driver.implicitly_wait(2)
        addEventBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-3-titlebar"]/div/button[2]')
        addEventBtn.click()
        
        dropdownPatientBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[1]/app-upcomming-events/app-modal-events/p-dialog/div/div/div[2]/form/div[1]/p-dropdown/div/div[2]')
        dropdownPatientBtn.click()

        selectOptionDropdown = driver.find_element(By.XPATH, '//*[@id="pr_id_15_list"]/p-dropdownitem[2]')
        selectOptionDropdown.click()

        descriptionInput = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[1]/app-upcomming-events/app-modal-events/p-dialog/div/div/div[2]/form/div[3]/span/input')
        descriptionInput.send_keys("Nuevo evento desde Selenium en la página de inicio")

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[1]/app-upcomming-events/app-modal-events/p-dialog/div/div/div[2]/form/div[6]/button')
        button.click()
        
        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[1]/app-upcomming-events/app-modal-events/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, se ha creado el evento desde el home")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha creado el evento desde la página del home")

    #--------------------------------------------------------------
    # testNewEventFromCalendar()
    #--------------------------------------------------------------
    # Unit test that checks if an event is added correctly from
    # the calendar or not
    #-------------------------------------------------------------- 
    def testNewEventFromCalendar(self):
        driver.implicitly_wait(2)
        goToCalendarBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[3]')
        goToCalendarBtn.click()
        
        addEventBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/div[1]/div[1]/button[2]')
        addEventBtn.click()
        
        dropdownPatientBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/app-modal-events/p-dialog/div/div/div[2]/form/div[1]/p-dropdown/div/div[2]')
        dropdownPatientBtn.click()

        selectOptionDropdown = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[1]')
        selectOptionDropdown.click()

        descriptionInput = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/app-modal-events/p-dialog/div/div/div[2]/form/div[3]/span/input')
        descriptionInput.send_keys("Nuevo evento desde Selenium en la página del calendario")

        init = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/app-modal-events/p-dialog/div/div/div[2]/form/div[5]/div[1]/p-calendar')
        init.click()
        date = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/app-modal-events/p-dialog/div/div/div[2]/form/div[5]/div[1]/p-calendar/span/div/div[1]/div/div[2]/table/tbody/tr[3]/td[3]')
        date.click()

        end = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/app-modal-events/p-dialog/div/div/div[2]/form/div[5]/div[2]/p-calendar')
        end.click()
        hour = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/app-modal-events/p-dialog/div/div/div[2]/form/div[5]/div[2]/p-calendar/span/div/div[2]/div[1]/button[1]')
        hour.click()
        date = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/app-modal-events/p-dialog/div/div/div[2]/form/div[5]/div[2]/p-calendar/span/div/div[1]/div/div[2]/table/tbody/tr[3]/td[3]')
        date.click()

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/app-modal-events/p-dialog/div/div/div[2]/form/div[6]/button')
        button.click()
        
        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/app-modal-events/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, se ha creado el evento desde la página del calendario")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha creado el evento desde la página del calendario")

    #--------------------------------------------------------------
    # testEditEvent()
    #--------------------------------------------------------------
    # Unit test that checks if an event can be edited correctly, in
    # this case the event corresponds to the sixth day of the first
    # week of the month.
    # NOTE: THERE MUST BE AN EVENT CREATED. THE cell CAN BE CHANGE
    #--------------------------------------------------------------
    def testEditEvent(self):
        driver.implicitly_wait(2)
        goToCalendarBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[3]')
        goToCalendarBtn.click()

        cell = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/div[2]/mwl-calendar-month-view/div/div/div[1]/div/mwl-calendar-month-cell[6]')
        cell.click()

        editBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/div[2]/mwl-calendar-month-view/div/div/div[1]/mwl-calendar-open-day-events/div/div/mwl-calendar-event-actions/span/a[2]')
        editBtn.click()

        descriptionInput = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/app-modal-events/p-dialog/div/div/div[2]/form/div[3]/span/input')
        descriptionInput.clear()
        descriptionInput.send_keys("Evento modificado desde Selenium")
        
        time.sleep(2)
        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/app-modal-events/p-dialog/div/div/div[2]/form/div[6]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/app-modal-events/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, el elemento se ha modificado correctamente")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha modificado el elemento")

    #--------------------------------------------------------------
    # testDeleteEvent()
    #--------------------------------------------------------------
    # From the beginning, the day is changed to the third of the
    # concurrent week, an event is added and later it is eliminated.
    # NOTE: IT IS POSSIBLE THAT WHEN CHANGING THE DAY, YOU HAVE TO
    # MODIFY THE changeMonth, dayAdded AND editBtn PARTS.
    #--------------------------------------------------------------
    def testDeleteEvent(self):
        driver.implicitly_wait(2)
        dayBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-3-content"]/div/div[1]/div[3]')
        dayBtn.click()
        
        addEventBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-3-titlebar"]/div/button[2]')
        addEventBtn.click()
        
        dropdownPatientBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[1]/app-upcomming-events/app-modal-events/p-dialog/div/div/div[2]/form/div[1]/p-dropdown/div/div[2]')
        dropdownPatientBtn.click()

        selectOptionDropdown = driver.find_element(By.XPATH, '//*[@id="pr_id_15_list"]/p-dropdownitem[2]')
        selectOptionDropdown.click()

        descriptionInput = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[1]/app-upcomming-events/app-modal-events/p-dialog/div/div/div[2]/form/div[3]/span/input')
        descriptionInput.send_keys("Nuevo evento para ser eliminado desde Selenium")

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[1]/app-upcomming-events/app-modal-events/p-dialog/div/div/div[2]/form/div[6]/button')
        button.click()
        
        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[1]/app-upcomming-events/app-modal-events/p-dialog/div/div/div[2]/p-messages/div/div')
            print("Se ha creado el evento para ser eliminado con éxito")

            closeModalBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[1]/app-upcomming-events/app-modal-events/p-dialog/div/div/div[1]/div/button')
            closeModalBtn.click()
        except:
            print("No se ha creado el evento para ser eliminado")

        goToCalendarBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[3]')
        goToCalendarBtn.click()

        #changeMonth = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/div[1]/div[1]/div/button[1]')
        #changeMonth.click()

        dayAdded = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/div[2]/mwl-calendar-month-view/div/div/div[2]/div/mwl-calendar-month-cell[3]')
        dayAdded.click()

        editBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/div[2]/mwl-calendar-month-view/div/div/div[2]/mwl-calendar-open-day-events/div/div/mwl-calendar-event-actions/span/a[1]')
        editBtn.click()

        confirmBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/p-confirmdialog/div/div/div[3]/button[2]')
        confirmBtn.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-calendar/app-full-calendar/p-toast/div')
            print("La prueba ha sido un éxito, se ha borrado correctamente")
        except:
            print("La prueba ha fallado, no se ha borrado el evento")
        
                
    def tearDown(self):
        driver.quit()

if __name__ == "__main__":
    unittest.main()

