from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import time

class UnitTestBHZ(unittest.TestCase):
    def setUp(self):
        global driver
        driver = webdriver.Chrome()
        driver.get("http://localhost:4200/#/login")

        # It is necesary to login each time beacause Selenium not save localstorage
        emailInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[1]/span/input')
        passwordInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[2]/span/input')
        button = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[3]/button')
        if emailInput is not None and passwordInput is not None:
            emailInput.send_keys("lauramelerogarrigos@gmail.com")
            passwordInput.send_keys("11223344")
            button.click()

    #--------------------------------------------------------------
    # testEditBHZ()
    #--------------------------------------------------------------
    # Unit test to try to modify a basic health zone
    #--------------------------------------------------------------
    def testEditBHZ(self):
        driver.implicitly_wait(2)
        goToCalendarBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        goToCalendarBtn.click()
        
        editBHZBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-8-content"]/div/div[2]/div/div[3]/div/div[2]/button')
        editBHZBtn.click()
        time.sleep(1)

        name = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-basic-health-zones-list/app-modal-basic-health-zones/p-dialog/div/div/div[2]/form/div[1]/div/input')
        name.clear()
        name.send_keys("Vinaròs")

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-basic-health-zones-list/app-modal-basic-health-zones/p-dialog/div/div/div[2]/form/div[3]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-basic-health-zones-list/app-modal-basic-health-zones/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, se ha modificado la zona básica de salud")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido modificar la zona básica de salud")

    #--------------------------------------------------------------
    # testNewBHZ()
    #--------------------------------------------------------------
    # Unit test developed to create a new basic health zone
    #--------------------------------------------------------------
    def testNewBHZ(self):
        driver.implicitly_wait(2)
        goToCalendarBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        goToCalendarBtn.click()
        
        createBHZBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-8-content"]/div/div[1]/div[2]/button[2]')
        createBHZBtn.click()
        time.sleep(1)

        name = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-basic-health-zones-list/app-modal-basic-health-zones/p-dialog/div/div/div[2]/form/div[1]/div/input')
        name.clear()
        name.send_keys("La Plana")

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-basic-health-zones-list/app-modal-basic-health-zones/p-dialog/div/div/div[2]/form/div[2]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[2]/app-basic-health-zones-list/app-modal-basic-health-zones/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, se ha modificado la zona básica de salud")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido modificar la zona básica de salud")
                
    def tearDown(self):
        driver.quit()

if __name__ == "__main__":
    unittest.main()
