from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import time

class UnitTestPatients(unittest.TestCase):
    def setUp(self):
        global driver
        driver = webdriver.Chrome()
        driver.get("http://localhost:4200/#/login")

        # It is necesary to login each time beacause Selenium not save localstorage
        emailInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[1]/span/input')
        passwordInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[2]/span/input')
        button = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[3]/button')
        if emailInput is not None and passwordInput is not None:
            emailInput.send_keys("lauramelerogarrigos@gmail.com")
            passwordInput.send_keys("11223344")
            button.click()

    #--------------------------------------------------------------
    # testNewPatient()
    #--------------------------------------------------------------
    # Unit test that creates a new patient. The process is carried
    # out from the home page and it is not necessary to do it from
    # the patient page since the component is exactly the same
    #--------------------------------------------------------------    
    def testNewPatient(self):
        driver.implicitly_wait(5)
        addPatientBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/p-panel/div/div[2]/div/div[1]/div[2]/button[2]')
        addPatientBtn.click()

        namePatient = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[1]/div[1]/input')
        namePatient.send_keys("Selenium")

        surnamePatient = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[1]/div[2]/input')
        surnamePatient.send_keys("Programa Test")

        agePatient = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[2]/div[1]/input')
        agePatient.send_keys("18")

        sipPatient = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[2]/div[2]/input')
        sipPatient.send_keys("111111111")

        dropdownBhzBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[3]/div[1]/p-dropdown/div/div[2]')
        dropdownBhzBtn.click()

        selectedBhzOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[1]')
        selectedBhzOption.click()
        time.sleep(1)

        dropdownCenterBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[3]/div[2]/p-dropdown/div/div[2]')
        dropdownCenterBtn.click()

        selectedCenterOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[1]/li')
        selectedCenterOption.click()
        time.sleep(1)

        dropdownDoctorBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[4]/div/p-dropdown/div/div[2]')
        dropdownDoctorBtn.click()

        selectedDoctorOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[1]/li')
        selectedDoctorOption.click()

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[5]/button')
        button.click()
        
        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, se ha creado un paciente correctamente")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha creado el paciente")

    #--------------------------------------------------------------
    # testEditPatient()
    #--------------------------------------------------------------
    # Unit test that modifies a patient. The process is carried out
    # from the home page and it is not necessary to do it from the
    # patient page since the component is exactly the same.
    # NOTE: IT THIS CASE, WE CAN TRY TO EDIT THE USER WITH NAME
    # Pepita
    #--------------------------------------------------------------  
    def testEditPatient(self):
        driver.implicitly_wait(5)

        editPatientBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/p-panel/div/div[2]/div/div[2]/div[3]/div/div[2]/button[1]')
        editPatientBtn.click()

        surnamePatient = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[1]/div[2]/input')
        surnamePatient.clear()
        surnamePatient.send_keys("Munuera Martinez")

        agePatient = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[2]/div[1]/input')
        agePatient.clear()
        agePatient.send_keys("96")

        dropdownBhzBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[3]/div[1]/p-dropdown/div/div[2]')
        dropdownBhzBtn.click()

        selectedBhzOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[2]')
        selectedBhzOption.click()
        time.sleep(1);

        dropdownCenterBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[3]/div[2]/p-dropdown/div/div[2]')
        dropdownCenterBtn.click()

        selectedCenterOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[1]/li')
        selectedCenterOption.click()
        time.sleep(1)

        dropdownDoctorBtn = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[4]/div/p-dropdown/div/div[2]')
        dropdownDoctorBtn.click()

        selectedDoctorOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[1]/li')
        selectedDoctorOption.click()

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/form/div[5]/button')
        button.click()
        
        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-home/div/div[3]/app-patients-list/app-modal-patients-list/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, el paciente se ha editado correctamente")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido editar el paciente")
                
    def tearDown(self):
        driver.quit()

if __name__ == "__main__":
    unittest.main()
