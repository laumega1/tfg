from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest


class UnitTestLogin(unittest.TestCase):
    def setUp(self):
        global driver
        driver = webdriver.Chrome()
        driver.get("http://localhost:4200/#/login")

    #--------------------------------------------------------------
    # testErrorEmail()
    #--------------------------------------------------------------
    # Unit test that checks if an error is generated when entering
    # the wrong username and/or password
    #--------------------------------------------------------------
    def testErrorEmail(self):
        driver.implicitly_wait(2)
        emailInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[1]/span/input')
        passwordInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[2]/span/input')
        button = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[3]/button')
        
        if emailInput is not None and passwordInput is not None:
            emailInput.send_keys("emailIncorrecto@gmail.com")
            passwordInput.send_keys("1234")
            button.click()

            try:
                error = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/p-messages/div/div')
                typeError = error.get_attribute("class")
                if "p-message-error" in typeError:
                    print("La prueba de error ha sido un éxito, ha dado error")
                else:
                    print("La prueba de error ha fallado, no se ha mostrado el mensaje de error")
            except:
                print("La prueba de error ha fallado, se ha iniciado sesión")

    #--------------------------------------------------------------
    # testCorrectEmail()
    #--------------------------------------------------------------
    # Unit test that checks if you log in when you enter a correct
    # password and username
    #--------------------------------------------------------------
    def testCorrectEmail(self):
        driver.implicitly_wait(2)
        emailInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[1]/span/input')
        passwordInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[2]/span/input')
        button = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[3]/button')
        if emailInput is not None and passwordInput is not None:
            emailInput.send_keys("lauramelerogarrigos@gmail.com")
            passwordInput.send_keys("11223344")
            button.click()

            try:
                error = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/p-messages/div/div')
                print("La prueba de éxito ha fallado, ha dado error")
            except:
                print("La prueba de éxito ha sido un éxito, ha iniciado sesión")
                
    def tearDown(self):
        driver.quit()

if __name__ == "__main__":
    unittest.main()

