from bluepy.btle import Scanner, DefaultDelegate

#----------------------------------------------------------------------------
#-------------------------------VARIABLES------------------------------------
#----------------------------------------------------------------------------

weightID = "5c:ca:d3:6b:4f:12"
weightDescription = "Manufacturer"

#----------------------------------------------------------------------------
#---------------------------------CLASSES------------------------------------
#----------------------------------------------------------------------------

class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)
        
    def handleDiscovery(self, dev, isNewDev, isNewData):
        if isNewDev:
            print("Discovered device", dev.addr)
        elif isNewData:
            print("Received new data from", dev.addr)

#----------------------------------------------------------------------------
#----------------------------FUNCTIONS WEIGHT--------------------------------
#----------------------------------------------------------------------------

#--------------------------------------------------------------
# string --> getWeightFromManufacture() --> float
#--------------------------------------------------------------
# Given a string of bytes (in hexadecimal format)
# corresponding to the frame of the scale, obtains the weight
# and the units
#--------------------------------------------------------------
def getWeightFromManufacture(value):
    weight = getWeightNumber(value)
    
    units = getWeightUnits(value)
    
    print("Peso final de: ", weight, units)
    
    return weight

#--------------------------------------------------------------
# string --> getWeightNumber() --> float
#--------------------------------------------------------------
# Given a string of bytes (in hexadecimal format)
# corresponding to the frame of the scale, obtains the bytes
# corresponding to the weight (4 and 5) and add the decimal
# part
#--------------------------------------------------------------
def getWeightNumber(value):
    weightBytes = value[10:12]+value[8:10]
            
    number = int(weightBytes, 16)
    
    weight = putWeightDecimalPoint(number, len(str(number))-1)
    
    return weight

#--------------------------------------------------------------
# string --> getWeightUnits() --> string
#--------------------------------------------------------------
# Given a string of bytes (in hexadecimal format)
# corresponding to the frame of the scale, obtains the byte
# corresponding to message properties (3). Bits 3 and 4
# corresponding to the selected unity are analyzed.
#--------------------------------------------------------------
def getWeightUnits(value):
    unitsBinary = parseHexaToBinary(value[6:8], 8)[3:5]
    
    if unitsBinary == "00":
        return "KG"
    elif unitsBinary == "01":
        return "Jin"
    elif unitsBinary == "10":
        return "LB"
    elif unitsBinary == "11":
        return "ST:LB"

#--------------------------------------------------------------
# int, int --> putWeightDecimalPoint() --> float
#--------------------------------------------------------------
# Given the number obtained by the balance and knowing how
# many decimals there are; the point is added to convert it to
# a decimal number.
# Ex. 115, 2 --> putWeightDecimalPoint() --> 11.5
#--------------------------------------------------------------
def putWeightDecimalPoint(value, position):
    newValue = float(str(value)[0:position]+"."+str(value)[position:position+1])
    
    return newValue

#--------------------------------------------------------------
# string --> getWeightDecimalPoint() --> int
#--------------------------------------------------------------
# Given a string of bytes (in hexadecimal format)
# corresponding to the frame of the scale, obtains the byte
# corresponding to message properties (3). Bits 1 and 2
# corresponding to the decimal point are analyzed.
# NOTE: I DON'T KNOW THE USE OF THIS
#--------------------------------------------------------------
def getWeightDecimalPoint(value):
    decimalPointBinary = parseHexaToBinary(value[6:8], 8)[1:3]
    
    if decimalPointBinary == "00":
        return 2
    elif decimalPointBinary == "01":
        return 0
    elif decimalPointBinary == "10":
        return 1

#----------------------------------------------------------------------------
#----------------------------FUNCTIONS UTILITY--------------------------------
#----------------------------------------------------------------------------

#--------------------------------------------------------------
# string, int --> parseHexaToBinary() --> string
#--------------------------------------------------------------
# Given a few bytes in hexadecimal format, the binary frame is
# obtained. As an intermediate point, if is necessary, the
# frame is padded with the necessary bits to complete the
# number of bytes.
#--------------------------------------------------------------
def parseHexaToBinary(valueBytes, totalBites):
    valueInt = int(valueBytes, 16)
    
    valueBinary = str(bin(valueInt))[2:]
    valueBinary = fillIncompleteByte(valueBinary, totalBites)

    return valueBinary

#--------------------------------------------------------------
# string, int --> fillIncompleteByte() --> string
#--------------------------------------------------------------
# Fills binary frames of bytes with the necessary bits to be
# able to be treated correctly.
#--------------------------------------------------------------
def fillIncompleteByte(byte, totalBites):
    necesaryBites = totalBites - len(byte)
    
    for i in range(0, necesaryBites):
        byte = "0"+byte

    return byte

#----------------------------------------------------------------------------
#-------------------------------MAIN CODE------------------------------------
#----------------------------------------------------------------------------

def main():
    scanner = Scanner().withDelegate(ScanDelegate())
    devices = scanner.scan(10.0)

    for dev in devices:
        #print("Device ",dev.addr,"(",dev.addrType,"), RSSSI=",dev.rssi," Db")
        for(adtype, desc, value) in dev.getScanData():
            #print(" ",desc," = ",value)
            if dev.addr == weightID and desc == weightDescription:
                getWeightFromManufacture(value)
                
main()