from selenium import webdriver
from selenium.webdriver.common.by import By
import unittest
import time

class UnitTestDevices(unittest.TestCase):
    def setUp(self):
        global driver
        driver = webdriver.Chrome()
        driver.get("http://localhost:4200/#/login")

        # It is necesary to login each time beacause Selenium not save localstorage
        emailInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[1]/span/input')
        passwordInput = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[2]/span/input')
        button = driver.find_element(By.XPATH, '//*[@id="login"]/div[1]/p-card/div/div/div/form/div[3]/button')
        if emailInput is not None and passwordInput is not None:
            emailInput.send_keys("lauramelerogarrigos@gmail.com")
            passwordInput.send_keys("11223344")
            button.click()

    #--------------------------------------------------------------
    # testEditRoomWithDuplicateMAC()
    #--------------------------------------------------------------
    # Unit test to try to modify a room by introducing a MAC different
    # from the current one but that already exists in the database.
    # This should generate an error message
    #--------------------------------------------------------------
    def testEditRoomWithDuplicateMAC(self):
        driver.implicitly_wait(2)
        goToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        goToSettingsBtn.click()
        
        editDeviceBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-11-content"]/div/div[2]/div/div[5]/div[1]/div[2]/button')
        editDeviceBtn.click()
        time.sleep(1)

        mac = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/form/div[2]/div[1]/input')
        mac.clear()
        mac.send_keys("64:69:4e:a3:b6:77")

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/form/div[4]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-error" in typeError:
                print("La prueba ha sido un éxito, no se ha modificado el dispositivo ya que la MAC está repetida")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de éxito")
        except:
            print("La prueba ha fallado, no ha aparecido ningún mensaje en cuanto al dispositivo")

    #--------------------------------------------------------------
    # testEditRoomWithDuplicateCoordinatorDevice()
    #--------------------------------------------------------------
    # Unit test to try to modify a room by entering an IP for the
    # application different from the current one but that already
    # exists in the database. This should generate an error message
    #--------------------------------------------------------------
    def testEditRoomWithDuplicateIpApplication(self):
        driver.implicitly_wait(2)
        goToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        goToSettingsBtn.click()
        
        editDeviceBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-11-content"]/div/div[2]/div/div[5]/div[1]/div[2]/button')
        editDeviceBtn.click()
        time.sleep(1)

        coordinatorDropdown = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/form/div[1]/div/p-dropdown/div/div[2]')
        coordinatorDropdown.click()
        time.sleep(1)

        coordinatorSelectedOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[1]')
        coordinatorSelectedOption.click()
        
        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/form/div[4]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-error" in typeError:
                print("La prueba ha sido un éxito, no se ha modificado el dispositivo ya que ya hay uno de este tipo asignado a ese coordinador")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de éxito")
        except:
            print("La prueba ha fallado, no ha aparecido ningún mensaje en cuanto al dispositivo")

    #--------------------------------------------------------------
    # testEditDevice()
    #--------------------------------------------------------------
    # Unit test to try to modify a device correctly
    #--------------------------------------------------------------
    def testEditDevice(self):
        driver.implicitly_wait(2)
        goToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        goToSettingsBtn.click()
        
        editDeviceBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-11-content"]/div/div[2]/div/div[5]/div[1]/div[2]/button')
        editDeviceBtn.click()
        time.sleep(1)

        mac = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/form/div[2]/div[1]/input')
        mac.clear()
        mac.send_keys("ca:ca:cc:cc:ac:ac")

        typeDropdown = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/form/div[2]/div[2]/p-dropdown/div/div[2]')
        typeDropdown.click()
        time.sleep(1)

        typeSelectedOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[1]')
        typeSelectedOption.click()

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/form/div[4]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, el dispositivo se ha mofificado correctamente")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido actualizar el dispositivo")

    #--------------------------------------------------------------
    # testNewDevice()
    #--------------------------------------------------------------
    # Unit test to try to create a device correctly
    #--------------------------------------------------------------
    def testNewDevice(self):
        driver.implicitly_wait(2)
        goToSettingsBtn = driver.find_element(By.XPATH, '//*[@id="leftMenu"]/a[5]')
        goToSettingsBtn.click()
        
        editDeviceBtn = driver.find_element(By.XPATH, '//*[@id="p-panel-11-content"]/div/div[1]/div[2]/button[2]')
        editDeviceBtn.click()
        time.sleep(1)

        mac = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/form/div[2]/div[1]/input')
        mac.clear()
        mac.send_keys("se:le:ni:um:te:st")

        coordinatorDropdown = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/form/div[1]/div/p-dropdown/div/div[2]')
        coordinatorDropdown.click()
        time.sleep(1)

        coordinatorSelectedOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[4]')
        coordinatorSelectedOption.click()

        typeDropdown = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/form/div[2]/div[2]/p-dropdown/div/div[2]')
        typeDropdown.click()
        time.sleep(1)

        typeSelectedOption = driver.find_element(By.XPATH, '/html/body/div/div[2]/ul/p-dropdownitem[1]')
        typeSelectedOption.click()

        button = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/form/div[4]/button')
        button.click()

        try:
            error = driver.find_element(By.XPATH, '/html/body/app-root/div/div[2]/app-doctors/div/div[3]/app-devices-list/app-modal-devices/p-dialog/div/div/div[2]/p-messages/div/div')
            typeError = error.get_attribute("class")
            if "p-message-success" in typeError:
                print("La prueba ha sido un éxito, el dispositivo se ha creado correctamente")
            else:
                print("La prueba ha fallado, se ha mostrado un mensaje de error")
        except:
            print("La prueba ha fallado, no se ha podido crear el dispositivo")
                
    def tearDown(self):
        driver.quit()

if __name__ == "__main__":
    unittest.main()
