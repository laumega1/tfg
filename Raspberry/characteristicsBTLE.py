from bluepy.btle import Peripheral, DefaultDelegate
from binascii import hexlify

class CharacteristicsDelegate(DefaultDelegate):
    #--------------------------------------------------------------
    # int, obj, obj --> __init__()
    #--------------------------------------------------------------
    # Constructor of the class
    #--------------------------------------------------------------  
    def __init__(self, item, item2):
        DefaultDelegate.__init__(self)
        self.__item = item
        self.__item2 = item2

    #--------------------------------------------------------------
    # string, binary --> handleNotification()
    #--------------------------------------------------------------
    # Asynchronous function that is called when there is a new
    # notification of the feature that we have susbscribe to.
    # If the conditions are correctyly, we accept the
    # plot to start its treatment and stop the search.
    #--------------------------------------------------------------  
    def handleNotification(self, cHandle, data):
        if self.__item.isDataFrameCorrrectly(hexlify(data)):
            self.__item2.stopSearchNotifications()
            self.__item.processData(str(hexlify(data)))

class CharacteristicsBTLE:
    #--------------------------------------------------------------
    # string, string, string, obj --> __init__()
    #--------------------------------------------------------------
    # Constructor of the class
    #--------------------------------------------------------------  
    def __init__(self, idToScan, idService, idCharacteristic, item):
        self.__idToScan = idToScan
        self.__idService = idService

        self.__idCharacteristic = idCharacteristic
        self.__item = item

        self.__searchingValue = True

    #--------------------------------------------------------------
    # foundValueToScan()
    #--------------------------------------------------------------
    # When this function is called, it tries to establish a
    # connection with a specific device given its MAC. If the system 
    # don't get it, ask if you want to try again.
    # If the connection is succes, the delegate object is assigned to
    # it. FInally it obtains a service given its identifier and
    # connects to one if its features. To start receiving
    # notifications it is important to previously write about a
    # feature to ask the device to send the values. FInally, it
    # waits for notifications in a loop until it is stopped.
    #--------------------------------------------------------------    
    def foundValueToScan(self):
        try:
            peripheral = Peripheral(self.__idToScan)
        except:
            self.__item.notificateError()
            return

        peripheral.setDelegate(CharacteristicsDelegate(self.__item, self))

        service = peripheral.getServiceByUUID(self.__idService)
        characteristic = service.getCharacteristics(self.__idCharacteristic)[0]

        peripheral.writeCharacteristic(characteristic.valHandle+1, b"\x01\x00")

        while self.__searchingValue:
            if peripheral.waitForNotifications(1.0):
                continue

    #--------------------------------------------------------------
    # stopSearchNotifications()
    #--------------------------------------------------------------
    # Change the state of a variable used to control even when he
    # is looking for the plot.
    #--------------------------------------------------------------    
    def stopSearchNotifications(self):
        self.__searchingValue = False
