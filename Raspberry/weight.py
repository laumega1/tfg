
import sys
import os

from utilities import parseHexaToBinary, putDecimalPoint
from constants import ORDERS_TOPIC_RESULT

from scanBTLE import ScanBTLE

class Weight:
    #--------------------------------------------------------------
    # string, string, MeasurementInstruction, sendMqttData() 
    # --> __init__()
    #--------------------------------------------------------------
    # Constructor of the class
    #--------------------------------------------------------------  
    def __init__(self, weightID, weightDescription, message, sendMqttDataFunction):
        self.__weightID = weightID
        self.__weightDescription = weightDescription
        self.__message = message
        self.__sendMqttDataFunction = sendMqttDataFunction

    #--------------------------------------------------------------
    # getWeight()
    #--------------------------------------------------------------
    # The whole process begins to obtain the final weight. The
    # process begins by instantiating the scanBTLE class and asking
    # it to fetch the requested device frame
    #--------------------------------------------------------------     
    def getWeight(self):
        scannedValue = ScanBTLE(self.__weightID, self.__weightDescription, self)
        scannedValue.foundValueToScan()

    #--------------------------------------------------------------
    # string --> processData()
    #--------------------------------------------------------------
    # Function that is called from the scanBTLE file after obtaining
    # the data frame. She will calculate the weight based on the
    # data frame and check if the result is correct.
    #--------------------------------------------------------------        
    def processData(self, value):
        weight, units = self.__getWeightFromManufacture(value)

        weight, units = self.__checkIfUnitsIsCorrect(weight, units)

        self.__sendResultData(weight, units)

    #--------------------------------------------------------------
    # float, string --> __sendResultData()
    #--------------------------------------------------------------
    # It create the topic to send the response and add the measure
    # result and the current measurement units.
    # It calls to the main function to send the result.
    #--------------------------------------------------------------
    def __sendResultData(self, weight, units):
        topic = ORDERS_TOPIC_RESULT+self.__message["message"]["data"]["macPatientDevice"]+"-sip="+self.__message["message"]["data"]["sip"]

        self.__message["message"]["data"]["measurementResult"][0]["value"] = weight
        self.__message["message"]["data"]["measurementResult"][0]["units"] = units

        self.__sendMqttDataFunction(self.__message, topic)

    #--------------------------------------------------------------
    # notificateError()
    #--------------------------------------------------------------
    # If any of the process crash or can't get information, send
    # error message so that if they want they can try again
    # It calls to the main function to send the result.
    #--------------------------------------------------------------
    def notificateError(self):
        topic = ORDERS_TOPIC_RESULT+self.__message["message"]["data"]["macPatientDevice"]+"-sip="+self.__message["message"]["data"]["sip"]

        self.__message["message"]["error"] = "Error, vuelva a intentarlo en unos segundos"

        self.__sendMqttDataFunction(self.__message, topic)
        os.execv(sys.executable, ['python3'] + [sys.argv[0]])
        
    #--------------------------------------------------------------
    # string --> __getWeightFromManufacture() --> float, string
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the scale, obtains the weight
    # and the units
    #--------------------------------------------------------------
    def __getWeightFromManufacture(self, value):
        weight = self.__getWeightNumber(value)
        
        units = self.__getWeightUnits(value)
        
        print("Peso final de: ", weight, units)
        
        return weight, units

    #--------------------------------------------------------------
    # string --> __getWeightNumber() --> float
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the scale, obtains the bytes
    # corresponding to the weight (4 and 5) and add the decimal
    # part
    #--------------------------------------------------------------
    def __getWeightNumber(self, value):
        weightBytes = value[10:12]+value[8:10]
                
        number = int(weightBytes, 16)
        
        weight = putDecimalPoint(number, len(str(number))-1)
        
        return weight

    #--------------------------------------------------------------
    # string --> __getWeightUnits() --> string
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the scale, obtains the byte
    # corresponding to message properties (3). Bits 3 and 4
    # corresponding to the selected unity are analyzed.
    #--------------------------------------------------------------
    def __getWeightUnits(self, value):
        unitsBinary = parseHexaToBinary(value[6:8], 8)[3:5]
        
        if unitsBinary == "00":
            return "Kg"
        elif unitsBinary == "01":
            return "Jin"
        elif unitsBinary == "10":
            return "Lb"
        elif unitsBinary == "11":
            return "St:Lb"

    #--------------------------------------------------------------
    # string --> __getWeightDecimalPoint() --> int
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the scale, obtains the byte
    # corresponding to message properties (3). Bits 1 and 2
    # corresponding to the decimal point are analyzed.
    # NOTE: I DON'T KNOW THE USE OF THIS
    #--------------------------------------------------------------
    def __getWeightDecimalPoint(self, value):
        decimalPointBinary = parseHexaToBinary(value[6:8], 8)[1:3]
        
        if decimalPointBinary == "00":
            return 2
        elif decimalPointBinary == "01":
            return 0
        elif decimalPointBinary == "10":
            return 1

    #--------------------------------------------------------------
    # float, string --> __checkIfUnitsIsCorrect() --> float, string
    #--------------------------------------------------------------
    # If weight units are not kg, convert given unit to kg
    #--------------------------------------------------------------
    def __checkIfUnitsIsCorrect(self, value, units):
        if units == "Jin":
            return self.__parseJinToKg(value), units
        elif units == "Lb":
            return self.__parseLbToKg(value), units
        elif units == "St:Lb":
            return self.__parseStToKg(value), units
        else:
            return value, units
    
    #--------------------------------------------------------------
    # float --> __parseJinToKg() --> float
    #--------------------------------------------------------------
    # Convert the Jin to Kg. It is important, to unify
    # the data in BBDD
    #--------------------------------------------------------------
    def __parseJinToKg(self, value):
        return float(value * 0.5)

    #--------------------------------------------------------------
    # float --> __parseLbToKg() --> float
    #--------------------------------------------------------------
    # Convert the Lb to Kg. It is important, to unify
    # the data in BBDD
    #--------------------------------------------------------------
    def __parseLbToKg(self, value):
        return float(value * 0.45359237)

    #--------------------------------------------------------------
    # float --> __parseStToKg() --> float
    #--------------------------------------------------------------
    # Convert the St:Lb to Kg. It is important, to unify
    # the data in BBDD
    #--------------------------------------------------------------
    def __parseStToKg(self, value):
        return float(value * 6.3503)
