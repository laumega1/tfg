END_POINT = "a2f5gk0ywdk3tg-ats.iot.us-east-1.amazonaws.com"
PATH_TO_CERTIFICATE = "./certificates/PoCAR.cert.pem"
PATH_TO_PRIVATE_KEY = "./certificates/PoCAR.private.key"
PATH_TO_AMAZON_ROOT_CA = "./certificates/root-CA.crt"

ORDERS_TOPIC = "PoCAR/consultMeasuresNotification/coordinatorMAC="
ORDERS_TOPIC_RESULT = "PoCAR/consultMeasuresNotificationResult/macDevice="