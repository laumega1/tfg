from bluepy.btle import Scanner, DefaultDelegate

class ScanDelegate(DefaultDelegate):
    def __init__(self):
        DefaultDelegate.__init__(self)

class ScanBTLE:
    #--------------------------------------------------------------
    # string, string, obj --> __init__()
    #--------------------------------------------------------------
    # Constructor of the class
    #--------------------------------------------------------------  
    def __init__(self, idToScan, descriptionToScan, item):
        self.__idToScan = idToScan
        self.__descriptionToScan = descriptionToScan
        self.__item = item

    #--------------------------------------------------------------
    # foundValueToScan()
    #--------------------------------------------------------------
    # Starts the proccess of finding the data frame corresponds to
    # a MAC and a description. When it finds it, the treatment of
    # said values begins. If it does not find it, a dialog appears
    # asking if you want to try again
    #--------------------------------------------------------------    
    def foundValueToScan(self):
        scanner = Scanner().withDelegate(ScanDelegate())
        devices = scanner.scan(10.0, passive=True)

        deviceDetected = False

        for dev in devices:
            for(adtype, desc, value) in dev.getScanData():
                if dev.addr == self.__idToScan and desc == self.__descriptionToScan:
                    deviceDetected = True
                    self.__item.processData(value)
        
        if deviceDetected == False:
            self.__item.notificateError()