import sys
import os

from characteristicsBTLE import CharacteristicsBTLE

from constants import ORDERS_TOPIC_RESULT

class PulseOximeter:
    #--------------------------------------------------------------
    # string, string, string, MeasurementInstruction, sendMqttData()
    # --> __init__()
    #--------------------------------------------------------------
    # Constructor of the class
    #--------------------------------------------------------------  
    def __init__(self, pulseOximeterID, pulseOximeterService, pulseOximeterCharacteristic, message, sendMqttDataFunction):
        self.__pulseOximeterID = pulseOximeterID
        self.__pulseOximeterService = pulseOximeterService
        self.__pulseOximeterCharacteristic = pulseOximeterCharacteristic
        self.__message = message
        self.__sendMqttDataFunction = sendMqttDataFunction

    #--------------------------------------------------------------
    # getPulseOximeter()
    #--------------------------------------------------------------
    # The whole process begins to obtain the final beats per minute
    # Tand the percent oxygen saturation.The process begins by 
    # instantiating the characteristicsBTLE class and asking it to 
    # fetch the requested device frame
    #--------------------------------------------------------------     
    def getPulseOximeter(self):
        scannedValue = CharacteristicsBTLE(self.__pulseOximeterID, self.__pulseOximeterService, self.__pulseOximeterCharacteristic, self)
        scannedValue.foundValueToScan()

    #--------------------------------------------------------------
    # string --> isDataFrameCorrrectly() --> T/F
    #--------------------------------------------------------------
    # It is checked that the conditions are met to consider the
    # frame valid, in this case, that the frame has certain size,
    # that the first byte is different from aa and the last one
    # equal to 00.
    #--------------------------------------------------------------   
    def isDataFrameCorrrectly(self, data):
        if len(data) >= 40:
            if data[0:2] != b'aa':
                if data[-2:] == b'00':
                    return True
        else:
            return False
            
    #--------------------------------------------------------------
    # string --> processData()
    #--------------------------------------------------------------
    # Function that is called from the characteristicsBTLE file 
    # after obtaining the data frame. She will calculate the
    # beats per minute and the percet of oxygen saturation on the 
    # data frame and check if the result is correct.
    #--------------------------------------------------------------        
    def processData(self, value):
        oxygen, beats = self.__getPulseOximeterFromDataFrame(value)

        self.__sendResultData(oxygen, beats)

    #--------------------------------------------------------------
    # float, string --> __sendResultData()
    #--------------------------------------------------------------
    # It create the topic to send the response and add the three
    # device measures.
    # It calls to the main function to send the result.
    #--------------------------------------------------------------
    def __sendResultData(self, oxygen, beats):
        topic = ORDERS_TOPIC_RESULT+self.__message["message"]["data"]["macPatientDevice"]+"-sip="+self.__message["message"]["data"]["sip"]

        self.__message["message"]["data"]["measurementResult"][0]["value"] = oxygen
        self.__message["message"]["data"]["measurementResult"][1]["value"] = beats

        self.__sendMqttDataFunction(self.__message, topic)

    #--------------------------------------------------------------
    # notificateError()
    #--------------------------------------------------------------
    # If any of the process crash or can't get information, send
    # error message so that if they want they can try again
    # It calls to the main function to send the result.
    #--------------------------------------------------------------
    def notificateError(self):
        topic = ORDERS_TOPIC_RESULT+self.__message["message"]["data"]["macPatientDevice"]+"-sip="+self.__message["message"]["data"]["sip"]

        self.__message["message"]["error"] = "Error, vuelva a intentarlo en unos segundos"

        self.__sendMqttDataFunction(self.__message, topic)
        os.execv(sys.executable, ['python3'] + [sys.argv[0]])
        
    #--------------------------------------------------------------
    # string --> __getPulseOximeterFromDataFrame() --> float
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame, obtains the pulse oxymeter values
    #--------------------------------------------------------------
    def __getPulseOximeterFromDataFrame(self, value):
        oxygen = self.__getOxygenNumber(value)
        
        beats = self.__getBeatsNumber(value)
        
        print("Pulsioxímetro final: ", oxygen, " - ", beats)
        
        return oxygen, beats

    #--------------------------------------------------------------
    # string --> __getOxygenNumber() --> int
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the pulse oximeter, obtains the 
    # byte corresponding to the oxygen saturation percent (16)
    #--------------------------------------------------------------
    def __getOxygenNumber(self, value):
        oxygenBytes = value[34:36]
                
        oxygen = int(oxygenBytes, 16)
        
        return oxygen

    #--------------------------------------------------------------
    # string --> __getBeatsNumber() --> int
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the pulse oxymeter, obtains the 
    # byte corresponding to the beats per minute (17)
    #--------------------------------------------------------------
    def __getBeatsNumber(self, value):
        beatsBytes = value[36:38]
                
        beats = int(beatsBytes, 16)
        
        return beats