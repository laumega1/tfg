import sys
import os

from utilities import putDecimalPoint
from constants import ORDERS_TOPIC_RESULT

from characteristicsBTLE import CharacteristicsBTLE

class Temperature:
    #--------------------------------------------------------------
    # string, string, string, MeasurementInstruction, sendMqttData() 
    # --> __init__()
    #--------------------------------------------------------------
    # Constructor of the class
    #--------------------------------------------------------------  
    def __init__(self, temperatureID, temperatureService, temperatureCharacteristic, message, sendMqttDataFunction):
        self.__temperatureID = temperatureID
        self.__temperatureService = temperatureService
        self.__temperatureCharacteristic = temperatureCharacteristic
        self.__message = message
        self.__sendMqttDataFunction = sendMqttDataFunction

    #--------------------------------------------------------------
    # getTemperature()
    #--------------------------------------------------------------
    # The whole process begins to obtain the final temperature. The
    # process begins by instantiating the characteristicsBTLE class 
    # and asking it to fetch the requested device frame
    #--------------------------------------------------------------     
    def getTemperature(self):
        scannedValue = CharacteristicsBTLE(self.__temperatureID, self.__temperatureService, self.__temperatureCharacteristic, self)
        scannedValue.foundValueToScan()

    #--------------------------------------------------------------
    # string --> isDataFrameCorrrectly() --> T/F
    #--------------------------------------------------------------
    # It is checked that the conditions are met to consider the
    # frame valid, in this case, that the frame has certain size
    #--------------------------------------------------------------   
    def isDataFrameCorrrectly(self, data):
        if len(data) >= 28:
            return True
        else:
            return False

    #--------------------------------------------------------------
    # string --> processData()
    #--------------------------------------------------------------
    # Function that is called from the characteristicsBTLE file 
    # after obtaining the data frame. She will calculate the
    # temperature and the measure type based on the data frame and 
    # check if the result is correct.
    #--------------------------------------------------------------        
    def processData(self, value):
        temperature, units = self.__getTemperatureFromDataFrame(value)

        if temperature != None and units != None:
            temperature, units = self.__checkIfUnitsIsCorrect(temperature, units)

            self.__sendResultData(temperature, units)
        else:
            self.notificateError()

    #--------------------------------------------------------------
    # float, string --> __sendResultData()
    #--------------------------------------------------------------
    # It create the topic to send the response and add the measure
    # result and the current measurement units.
    # It calls to the main function to send the result.
    #--------------------------------------------------------------
    def __sendResultData(self, temperature, units):
        topic = ORDERS_TOPIC_RESULT+self.__message["message"]["data"]["macPatientDevice"]+"-sip="+self.__message["message"]["data"]["sip"]

        self.__message["message"]["data"]["measurementResult"][0]["value"] = temperature
        self.__message["message"]["data"]["measurementResult"][0]["units"] = units

        self.__sendMqttDataFunction(self.__message, topic)

    #--------------------------------------------------------------
    # notificateError()
    #--------------------------------------------------------------
    # If any of the process crash or can't get information, send
    # error message so that if they want they can try again
    # It calls to the main function to send the result.
    #--------------------------------------------------------------
    def notificateError(self):
        topic = ORDERS_TOPIC_RESULT+self.__message["message"]["data"]["macPatientDevice"]+"-sip="+self.__message["message"]["data"]["sip"]

        self.__message["message"]["error"] = "Error, vuelva a intentarlo en unos segundos"

        self.__sendMqttDataFunction(self.__message, topic)
        os.execv(sys.executable, ['python3'] + [sys.argv[0]])
        
    #--------------------------------------------------------------
    # string --> __getTemperatureFromDataFrame() --> float
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the temperature, obtains the 
    # value and the units
    #--------------------------------------------------------------
    def __getTemperatureFromDataFrame(self, value):
        typeString = self.__getTypeResult(value)

        if typeString != "error":
            number = self.__getTemperatureNumber(value)

            temperature = putDecimalPoint(number, 2)

            unit, position = self.__getExtraData(value)
        else:
            temperature, unit, position = None
        
        print("Temperatura final: ", temperature, unit, " - tomada en la:", position)
        
        return temperature, unit

    #--------------------------------------------------------------
    # string --> __getTemperatureNumber() --> int
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the temperature, obtains the bytes
    # corresponding to the temperature (6 and 7)
    #--------------------------------------------------------------
    def __getTemperatureNumber(self, value):
        temperatureBytes = value[14:18]
                
        temperature = int(temperatureBytes, 16)
        
        return temperature

    #--------------------------------------------------------------
    # string --> __getTypeResult() --> string
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the temperature, obtains the byte
    # corresponding to the type to check if the result is correctly,
    # is uncorrectly or is a old measure (4)
    #--------------------------------------------------------------
    def __getTypeResult(self, value):
        typeBytes = value[10:12]
                
        if typeBytes == "ff":
            return "current"
        elif typeBytes == "aa":
            return "historica"
        else:
            return "error"

    #--------------------------------------------------------------
    # string --> __getExtraData() --> string, string
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the temperature, obtains the 
    # byte corresponding to the COM (5) and return the unit of
    # measure an the place where the measurement was taken
    #--------------------------------------------------------------
    def __getExtraData(self, value):
        extraBytes = value[12:14]
                
        if extraBytes == "00":
            return "ºC", "Oreja"
        elif extraBytes == "01":
            return "ºF", "Oreja"
        elif extraBytes == "02":
            return "ºC", "Cabeza"
        else:
            return "ºF", "Cabeza"

    #--------------------------------------------------------------
    # float, string --> __checkIfUnitsIsCorrect() --> float, string
    #--------------------------------------------------------------
    # If weight units are not kg, convert given unit to kg
    #--------------------------------------------------------------
    def __checkIfUnitsIsCorrect(self, value, units):
        if units == "ºF":
            return self.__parseFahrenheitToCelsius(value), units
        else:
            return value, units

    #--------------------------------------------------------------
    # float --> __parseFahrenheitToCelsius() --> float
    #--------------------------------------------------------------
    # Convert the celsius to fahrenheit. It is important, to unify
    # the data in BBDD
    #--------------------------------------------------------------
    def __parseFahrenheitToCelsius(self, value):
        return float((value * 9/5) + 32)
