from weight import Weight
from bloodPreassure import BloodPreassure
from temperature import Temperature
from pulseOximeter import PulseOximeter

from constants import ORDERS_TOPIC_RESULT
class AnaliceOrder:
    #--------------------------------------------------------------
    # string, string, string, string, string, MeasurementInstruction, 
    # sendMqttData(), changeCheckingNewOrders() --> __init__()
    #--------------------------------------------------------------
    # Constructor of the class
    #--------------------------------------------------------------  
    def __init__(self, id, service, characteristic, description, type, message, checkingFunction, sendMqttDataFunction):
        self.__id = id
        self.__service = service
        self.__characteristic = characteristic
        self.__description = description
        self.__type = type
        self.__message = message

        self.__checkingFunction = checkingFunction
        self.__sendMqttDataFunction = sendMqttDataFunction

    #--------------------------------------------------------------
    # checkOrder()
    #--------------------------------------------------------------
    # See what type of sensor should be
    # analyzed to take the measurements
    #-------------------------------------------------------------- 
    def checkOrder(self):
        try:
            if self.__type == "weight":
                self.__getWeightValue()
            elif self.__type == "bloodPreassure":
                self.__getBloodPreassureValues()
            elif self.__type == "temperature":
                self.__getTemperatureValue()
            elif self.__type == "pulseOximeter":
                self.__getPulseoximeter()
        except:
            self.__sendMessageError()

    #--------------------------------------------------------------
    # __sendMessageError()
    #--------------------------------------------------------------
    # If the order is not correctly identify, sends an MQTT error
    # message
    #-------------------------------------------------------------- 
    def __sendMessageError(self):
        topic = ORDERS_TOPIC_RESULT+self.__message["message"]["data"]["macPatientDevice"]+"-sip="+self.__message["message"]["data"]["sip"]
        
        self.__message["message"]["error"] = "No se ha podido detectar el dispositivo"
        
        self.__sendMqttDataFunction(topic, self.__message)

    #--------------------------------------------------------------
    # __getWeightValue()
    #--------------------------------------------------------------
    # Creates an instante of the Weight class and calls the function
    # that starts the measurement taking procces. When process ends,
    # the scan continues
    #--------------------------------------------------------------        
    def __getWeightValue(self):
        weight = Weight(self.__id, self.__description, self.__message, self.__sendMqttDataFunction)
        weight.getWeight()
        
        self.__checkingFunction(False)

    #--------------------------------------------------------------
    # __getBloodPreassureValues()
    #--------------------------------------------------------------
    # Creates an instante of the Blood Preassure class and calls the 
    # function that starts the measurement taking procces. When process 
    # ends, the scan continues
    #--------------------------------------------------------------        
    def __getBloodPreassureValues(self):
        bloodPreassure = BloodPreassure(self.__id, self.__service, self.__characteristic, self.__message, self.__sendMqttDataFunction)
        bloodPreassure.getBloodPreassure()
        
        self.__checkingFunction(False)

    #--------------------------------------------------------------
    # __getTemperatureValue()
    #--------------------------------------------------------------
    # Creates an instante of the Temperature class and calls the 
    # function that starts the measurement taking procces. When 
    # process ends, the scan continues
    #--------------------------------------------------------------        
    def __getTemperatureValue(self):
        temperature = Temperature(self.__id, self.__service, self.__characteristic, self.__message, self.__sendMqttDataFunction)
        temperature.getTemperature()
        
        self.__checkingFunction(False)

    #--------------------------------------------------------------
    # __getPulseoximeter()
    #--------------------------------------------------------------
    # Creates an instante of the Pulseoximeter class and calls the 
    # function that starts the measurement taking procces. When 
    # process ends, the scan continues
    #--------------------------------------------------------------        
    def __getPulseoximeter(self):
        pulseOximeter = PulseOximeter(self.__id, self.__service, self.__characteristic, self.__message, self.__sendMqttDataFunction)
        pulseOximeter.getPulseOximeter()
        
        self.__checkingFunction(False)