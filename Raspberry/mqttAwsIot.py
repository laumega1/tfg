from awscrt import io, mqtt
from awsiot import mqtt_connection_builder

import json

class MqttAwsIotCore:
    #--------------------------------------------------------------
    # string, string, string, string, string --> __init__()
    #--------------------------------------------------------------
    # Constructor of the class
    #--------------------------------------------------------------  
    def __init__(self, endpoint, clientID, pathToCertificate, pathToPrivateKey, pathToAmazonRootCa):
        self.__endpoint = endpoint
        self.__clientID = clientID
        self.__pathToCertificate= pathToCertificate
        self.__pathToPrivateKey = pathToPrivateKey
        self.__pathToAmazonRootCa = pathToAmazonRootCa

        self.__mqttConnection = self.__spinUpResources()

    #--------------------------------------------------------------
    # __spinUpResources() --> Connection
    #--------------------------------------------------------------
    # Prepare the class for the MQTT connection with AWS IoT Core,
    # giving it as parameters (amos others) the different 
    # certificates. It returns the MQTT connection
    #--------------------------------------------------------------
    def __spinUpResources(self):
        eventLoopGroup = io.EventLoopGroup(1)
        hostResolver = io.DefaultHostResolver(eventLoopGroup)
        clientBootstrap = io.ClientBootstrap(eventLoopGroup, hostResolver)

        mqttConnection = mqtt_connection_builder.mtls_from_path(
            endpoint = self.__endpoint,
            cert_filepath = self.__pathToCertificate,
            pri_key_filepath = self.__pathToPrivateKey,
            clientBootstrap = clientBootstrap,
            ca_filepath = self.__pathToAmazonRootCa,
            client_id = self.__clientID,
            clean_session = False,
            keep_alive_secs = 6
        )

        return mqttConnection

    #--------------------------------------------------------------
    # mqttConnect()
    #--------------------------------------------------------------
    # Try to establish MQTT connection to AWS IoT Core
    #--------------------------------------------------------------
    def mqttConnect(self):
        print("Connecting to ", self.__endpoint, " with client ID: ", self.__clientID)

        try:
            connectFuture = self.__mqttConnection.connect()
            connectFuture.result()

            print("Connected")
        except:
            print("Can't connected")

    #--------------------------------------------------------------
    # mqttDisconnect()
    #--------------------------------------------------------------
    # Try to cancell MQTT connection to AWS IoT Core
    #--------------------------------------------------------------
    def mqttDisconnect(self):
        print("Disconnecting to ", self.__endpoint, " with client ID: ", self.__clientID)

        try:
            disconnectFuture = self.__mqttConnection.disconnect()
            disconnectFuture.result()

            print("Disconnected!")
        except:
            print("Can't disconnected")

    #--------------------------------------------------------------
    # JSON, string, binary --> publishMessage()
    #--------------------------------------------------------------
    # Given the message to send and the topic to send it to, it 
    # publishesh it. By default, the QoS value is 1, that is it
    # ensures that the message arrives.
    #--------------------------------------------------------------
    def publishMessage(self, message, topic, qos = mqtt.QoS.AT_LEAST_ONCE):
        self.__mqttConnection.publish(topic, message, qos)

        print("Published: ", message, " to the topic: ", topic)

    #--------------------------------------------------------------
    # string, analiceResult() --> subscribeMessage()
    #--------------------------------------------------------------
    # Given the topic to subscribe and the function to call when
    # messages arrive, create a subscription to that MQTT topic.
    # The function that is passed is necessary to treat the result
    # in a specific way.
    #--------------------------------------------------------------
    def subscribeMessage(self, topic, analiceResultFunction, qos = mqtt.QoS.AT_LEAST_ONCE):
        self.__analiceResultFunction = analiceResultFunction

        self.__mqttConnection.subscribe(topic, qos, self.callback)

        print("Subscribed to: ", topic)

    #--------------------------------------------------------------
    # string, string, dictionary --> callback()
    #--------------------------------------------------------------
    # Callback which is called when a message is received from any
    # topic
    #--------------------------------------------------------------
    def callback(self, topic, payload, **kwargs):
        self.__analiceResultFunction(json.loads(payload))