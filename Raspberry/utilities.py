#--------------------------------------------------------------
# string, int --> parseHexaToBinary() --> string
#--------------------------------------------------------------
# Given a few bytes in hexadecimal format, the binary frame is
# obtained. As an intermediate point, if is necessary, the
# frame is padded with the necessary bits to complete the
# number of bytes.
#--------------------------------------------------------------
def parseHexaToBinary(valueBytes, totalBites):
    valueInt = int(valueBytes, 16)

    valueBinary = str(bin(valueInt))[2:]
    valueBinary = fillIncompleteByte(valueBinary, totalBites)

    return valueBinary

#--------------------------------------------------------------
# string, int --> fillIncompleteByte() --> string
#--------------------------------------------------------------
# Fills binary frames of bytes with the necessary bits to be
# able to be treated correctly.
#--------------------------------------------------------------
def fillIncompleteByte(byte, totalBites):
    necesaryBites = totalBites - len(byte)

    for i in range(0, necesaryBites):
        byte = "0"+byte

    return byte

#--------------------------------------------------------------
# int, int --> __putDecimalPoint() --> float
#--------------------------------------------------------------
# Given the number obtained and knowing how
# many decimals there are; the point is added to convert it to
# a decimal number.
# Ex. 115, 2 --> __putDecimalPoint() --> 11.5
#--------------------------------------------------------------
def putDecimalPoint(value, position):
    newValue = float(str(value)[0:position]+"."+str(value)[position:position+1])
    
    return newValue
