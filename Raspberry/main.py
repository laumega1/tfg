from mqttAwsIot import MqttAwsIotCore

from getmac import get_mac_address
from time import sleep

from constants import END_POINT, PATH_TO_CERTIFICATE, PATH_TO_PRIVATE_KEY, PATH_TO_AMAZON_ROOT_CA, ORDERS_TOPIC

from analiceOrder import AnaliceOrder

import json

#----------------------------------------------------------------------------
#--------------------------------VARIABLES-----------------------------------
#----------------------------------------------------------------------------

checkingNewOrders = False

messageSubscriber = None

awsIot = None

#----------------------------------------------------------------------------
#--------------------------------FUNCTIONS-----------------------------------
#----------------------------------------------------------------------------

#--------------------------------------------------------------
# startMqttAwsIot()
#--------------------------------------------------------------
# Start the connection with aws iot and subscribe to a topic
# to delay order of measuring
#-------------------------------------------------------------- 
def startMqttAwsIot():
    global awsIot

    awsIot = MqttAwsIotCore(END_POINT, get_mac_address(), PATH_TO_CERTIFICATE, PATH_TO_PRIVATE_KEY, PATH_TO_AMAZON_ROOT_CA)

    awsIot.mqttConnect()

    awsIot.subscribeMessage(ORDERS_TOPIC+get_mac_address(), analiceResult)

#--------------------------------------------------------------
# string --> analiceResult()
#--------------------------------------------------------------
# Check if the message arribes is correctly formated to be 
# order to take a meassurement.
# If is correct stop the loop to make an action
#-------------------------------------------------------------- 
def analiceResult(message):
    global messageSubscriber

    print("nuevo valor encontrado")

    #print(message)

    try:
        if message["status"] == 200:
            changeCheckingNewOrders(True)
            messageSubscriber = message
                
    except:
        print("No message found")

#--------------------------------------------------------------
# MeasurementInstruction, string --> changeCheckingNewOrders()
#--------------------------------------------------------------
# Receive a message to publish in a MQTT topic on AWS IoT Core
#-------------------------------------------------------------- 
def sendMqttData(message, topic):
    print(message)

    global awsIot

    awsIot.publishMessage(json.dumps(message), topic)

#--------------------------------------------------------------
# bool --> changeCheckingNewOrders()
#--------------------------------------------------------------
# Change the state of the global variable used to start take
# a measure. THis function is necessari to change the state
# when a measure is end to take.
#-------------------------------------------------------------- 
def changeCheckingNewOrders(isChecking):
    global checkingNewOrders

    checkingNewOrders = isChecking

#----------------------------------------------------------------------------
#-------------------------------MAIN CODE------------------------------------
#----------------------------------------------------------------------------

def main():
    startMqttAwsIot()

    while True:
        if checkingNewOrders:
            order = AnaliceOrder(messageSubscriber["message"]["data"]["device"]["mac"], messageSubscriber["message"]["data"]["device"]["service"], messageSubscriber["message"]["data"]["device"]["characteristic"], messageSubscriber["message"]["data"]["device"]["description"], messageSubscriber["message"]["data"]["typeBBDD"], messageSubscriber, changeCheckingNewOrders, sendMqttData)
            order.checkOrder()
        else:
            sleep(1)

if __name__ == "__main__":
    main()