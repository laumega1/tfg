import sys
import os

from characteristicsBTLE import CharacteristicsBTLE

from constants import ORDERS_TOPIC_RESULT

class BloodPreassure:
    #--------------------------------------------------------------
    # string, string, string, MeasurementInstruction, 
    # sendMqttData() --> __init__()
    #--------------------------------------------------------------
    # Constructor of the class
    #--------------------------------------------------------------  
    def __init__(self, bloodPreassureID, bloodPreassureService, bloodPreassureCharacteristic, message, sendMqttDataFunction):
        self.__bloodPreassureID = bloodPreassureID
        self.__bloodPreassureService = bloodPreassureService
        self.__bloodPreassureCharacteristic = bloodPreassureCharacteristic
        self.__message = message
        self.__sendMqttDataFunction = sendMqttDataFunction

    #--------------------------------------------------------------
    # getBloodPreassure()
    #--------------------------------------------------------------
    # The whole process begins to obtain the final blood preassure. 
    # The process begins by instantiating the characteristicsBTLE 
    # class and asking it to fetch the requested device frame
    #--------------------------------------------------------------     
    def getBloodPreassure(self):
        scannedValue = CharacteristicsBTLE(self.__bloodPreassureID, self.__bloodPreassureService, self.__bloodPreassureCharacteristic, self)
        scannedValue.foundValueToScan()

    #--------------------------------------------------------------
    # string --> isDataFrameCorrrectly() --> T/F
    #--------------------------------------------------------------
    # It is checked that the conditions are met to consider the
    # frame valid, in this case, that the frame has certain size
    #--------------------------------------------------------------   
    def isDataFrameCorrrectly(self, data):
        if len(data) >= 24:
            return True
        else:
            return False
            
    #--------------------------------------------------------------
    # string --> processData()
    #--------------------------------------------------------------
    # Function that is called from the characteristicsBTLE file 
    # after obtaining the data frame. She will calculate the
    # systolic, diastolic and the rate based on the data frame and 
    # check if the result is correct.
    #--------------------------------------------------------------        
    def processData(self, value):
        systolic, diastolic, rate = self.__getBloodPreassureFromDataFrame(value)

        self.__sendResultData(systolic, diastolic, rate)

    #--------------------------------------------------------------
    # int, int, int --> __sendResultData()
    #--------------------------------------------------------------
    # It create the topic to send the response and add the three
    # device measures.
    # It calls to the main function to send the result.
    #--------------------------------------------------------------
    def __sendResultData(self, systolic, diastolic, rate):
        topic = ORDERS_TOPIC_RESULT+self.__message["message"]["data"]["macPatientDevice"]+"-sip="+self.__message["message"]["data"]["sip"]

        self.__message["message"]["data"]["measurementResult"][0]["value"] = systolic
        self.__message["message"]["data"]["measurementResult"][1]["value"] = diastolic
        self.__message["message"]["data"]["measurementResult"][2]["value"] = rate

        self.__sendMqttDataFunction(self.__message, topic)

    #--------------------------------------------------------------
    # notificateError()
    #--------------------------------------------------------------
    # If any of the process crash or can't get information, send
    # error message so that if they want they can try again
    # It calls to the main function to send the result.
    #--------------------------------------------------------------
    def notificateError(self):
        topic = "PoCAR/consultMeasuresNotificationResult/macDevice="+self.__message["message"]["data"]["macPatientDevice"]+"-sip="+self.__message["message"]["data"]["sip"]

        self.__message["message"]["error"] = "Error, vuelva a intentarlo en unos segundos"

        self.__sendMqttDataFunction(self.__message, topic)
        os.execv(sys.executable, ['python3'] + [sys.argv[0]])
        
    #--------------------------------------------------------------
    # string --> __getBloodPreassureFromDataFrame() --> float
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame, obtains the boold
    # preasure (systolic, diastolic and rate)
    #--------------------------------------------------------------
    def __getBloodPreassureFromDataFrame(self, value):
        systolic = self.__getSystolicNumber(value)
        
        diastolic = self.__getDiastolicNumber(value)

        rate = self.__getRateNumber(value)
        
        print("Presión final: ", systolic, " - ", diastolic, " - ", rate)
        
        return systolic, diastolic, rate

    #--------------------------------------------------------------
    # string --> __getSystolicNumber() --> int
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the systolic, obtains the byte
    # corresponding to the systolic (2)
    #--------------------------------------------------------------
    def __getSystolicNumber(self, value):
        systolicBytes = value[6:8]
                
        systolic = int(systolicBytes, 16)
        
        return systolic

    #--------------------------------------------------------------
    # string --> __getDiastolicNumber() --> int
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the diastolic, obtains the byte
    # corresponding to the diastolic (4)
    #--------------------------------------------------------------
    def __getDiastolicNumber(self, value):
        diastolicBytes = value[10:12]
                
        diastolic = int(diastolicBytes, 16)
        
        return diastolic

    #--------------------------------------------------------------
    # string --> __getRateNumber() --> int
    #--------------------------------------------------------------
    # Given a string of bytes (in hexadecimal format)
    # corresponding to the frame of the rate, obtains the byte
    # corresponding to the rate (8)
    #--------------------------------------------------------------
    def __getRateNumber(self, value):
        rateBytes = value[18:20]
                
        rate = int(rateBytes, 16)
        
        return rate