import { Component, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { mqtt } from 'aws-iot-device-sdk-v2';
import { Message, MessageService } from 'primeng/api';
import { AuthService } from './auth/services/auth.service';
import { ConsultNotification, PostResponse, PutResponse } from './protected/interfaces/interfaces';
import { ConsultsNotificationsService } from './protected/services/consults-notifications.service';
import { DoctorsService } from './protected/services/doctors.service';
import { MqttConsultNotification, MqttConsultCheckReceived } from './shared/interfaces/interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    MessageService
  ]
})
export class AppComponent {
  title = 'PoCAR';

  isLogged: boolean = false;

  connection: mqtt.MqttClientConnection | null = null;

  clientID: string = localStorage.getItem('id')!+"General";

  correctPost: Message = {key: 'notificationsAppKey', severity:'success', summary:'Éxito', detail:'Una nueva notificación de consulta ha sido almacenada con éxito'};
  errorPost: Message = {key: 'notificationsAppKey', severity:'error', summary:'Error', detail:'No se ha podido almacenar la nueva notificación de consulta'};
  correctPut: Message = {key: 'notificationsAppKey', severity:'success', summary:'Éxito', detail:'Una notificación de consulta ha sido modificada con éxito'};
  errorPut: Message = {key: 'notificationsAppKey', severity:'error', summary:'Error', detail:'No se ha podido modificar la notificación de consulta'};

  constructor(
    private service: AuthService,
    private doctorsService: DoctorsService,
    private consultsNotificationsService: ConsultsNotificationsService,
    private messageService: MessageService,
    private router: Router
  ){}

  ngOnInit(): void {
    this.router.events.subscribe(() => {
      this.service.isLogged().subscribe( (resp: boolean) => {
        this.isLogged = resp;

        if(this.isLogged){
          this.doctorsService.putDoctorOnlineState(true).subscribe( (resp: PutResponse) => {
            this.subscribeNotifications();
          });
        }
      });
    });
  }

  /**--------------------------------------------------------------
   * beforeUnloadHandler()
   * --------------------------------------------------------------
   * Function so that before closing the browser the status of 
   * the doctor is updated to not online
   --------------------------------------------------------------*/
  @HostListener('window:beforeunload', [ '$event' ])
  beforeUnloadHandler(event: any) {
    this.doctorsService.putDoctorOnlineState(false).subscribe( (resp: PutResponse) => {
      console.log(resp);
    });
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MQTT ----------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * subscribeNotifications()
   * --------------------------------------------------------------
   * Once the connection has been established, a subscription is 
   * created so that you can receive notifications anywhere on the 
   * page
   --------------------------------------------------------------*/
  subscribeNotifications(): void{
    this.connection?.subscribe('PoCAR/consultlNotifications/idDoctor='+localStorage.getItem('id'), mqtt.QoS.AtLeastOnce, (topic, payload) => {
      try {
        const decoder = new TextDecoder('utf8');
        const notification: MqttConsultNotification = JSON.parse(decoder.decode(payload));

        console.log(notification);
        if(notification.status == 200){
          this.addEventToList(notification);

          this.sendReceivedNotificationCheck(notification.message.mac);
        }
      } catch (error) {
        console.log(error);
      }
    });
  }

  /**--------------------------------------------------------------
   * string --> sendReceivedNotificationCheck()
   * --------------------------------------------------------------
   * Upon receipt of a consultation notification, the doctor 
   * sends you another as having received it
   --------------------------------------------------------------*/
  sendReceivedNotificationCheck(mac: string): void{
    const message: MqttConsultCheckReceived = {
      status: 200,
      message: {
        data: {
          action: "receivedNotification"
        }
      }
    }
    this.connection?.publish("PoCAR/consultlNotifications/macDevice="+mac, JSON.stringify(message), mqtt.QoS.AtLeastOnce);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------- EVENTS, PATIENTS, NOTIFICATIONS -----------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * MqttConsultNotification --> addEventToList()
   * --------------------------------------------------------------
   * Creates an event from the message received by MQTT, later, 
   * it searches if it is in the database, if it is not, it adds 
   * it, if not, it updates it
   --------------------------------------------------------------*/
  addEventToList(mqtt: MqttConsultNotification): void{
    const notification: ConsultNotification = {
      macDevice: mqtt.message.mac,
      idRoom: mqtt.message.data.idRoom,
      action: mqtt.message.data.action,
      sip: mqtt.message.data.sip,
      idDoctor: mqtt.message.data.idDoctor,
      idEvent: mqtt.message.data.idEvent
    }

    this.consultsNotificationsService.isNotificationInBBDD(notification.macDevice, notification.idRoom.toString(), notification.idDoctor.toString(), notification.sip).subscribe( (resp: Boolean) => {
      if(resp){
        this.updateNotificationInBBDD(mqtt);
      }else{
        this.createNotificationInBBDD(mqtt);
      }
    });
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * MqttConsultNotification --> createNotificationInBBDD()
   * --------------------------------------------------------------
   * Create a notification in the database
   --------------------------------------------------------------*/
  createNotificationInBBDD(notification: MqttConsultNotification): void{
    this.consultsNotificationsService.postConsultNotification(notification).subscribe( (resp: PostResponse) => {
      this.messageService.clear();

      if(resp.status == 200){
        this.messageService.add(this.correctPost);
      }else{
        this.messageService.add(this.errorPost);
      }
    });
  }

  /**--------------------------------------------------------------
   * MqttConsultNotification --> updateNotificationInBBDD()
   * --------------------------------------------------------------
   * Update a notification in the database
   --------------------------------------------------------------*/
  updateNotificationInBBDD(notification: MqttConsultNotification): void{
    this.consultsNotificationsService.putConsultsNotifications(notification).subscribe( (resp: PutResponse) => {
      this.messageService.clear();

      if(resp.status == 200){
        this.messageService.add(this.correctPut);
      }else{
        this.messageService.add(this.errorPut);
      }
    });
  }
}
