import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PatientsApplication, PatientsApplicationResponse, PostResponse, PutResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class PatientsApplicationsService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string --> getPatientApplicationInformation() 
   * --> Observable<PatientsApplication>
   * --------------------------------------------------------------
   * Get the data from the patient application given their MAC
   --------------------------------------------------------------*/
  getPatientApplicationInformation(mac: string): Observable<PatientsApplication>{
    return this.http.get<PatientsApplicationResponse>(environment.baseUrl+"patientsApplication?mac="+mac)
    .pipe(
      map((res: PatientsApplicationResponse) => {
        return res.message ? res.message : null!;
      })
    );
  }

  /**--------------------------------------------------------------
   * string --> getPatientApplicationInformationByIP() 
   * --> Observable<PatientsApplication>
   * --------------------------------------------------------------
   * Get the data from the patient application given their ip 
   * address
   --------------------------------------------------------------*/
   getPatientApplicationInformationByIP(ip: string): Observable<PatientsApplication>{
    return this.http.get<PatientsApplicationResponse>(environment.baseUrl+"patientsApplication?ip="+ip)
    .pipe(
      map((res: PatientsApplicationResponse) => {
        return res.message ? res.message : null!;
      })
    );
  }

  /**--------------------------------------------------------------
   * string --> getPatientApplicationInformationByRoom() 
   * --> Observable<PatientsApplication>
   * --------------------------------------------------------------
   * Get the data from the patient application given room id
   --------------------------------------------------------------*/
   getPatientApplicationInformationByRoom(idRoom: string): Observable<PatientsApplication>{
    return this.http.get<PatientsApplicationResponse>(environment.baseUrl+"patientsApplication?idRoom="+idRoom)
    .pipe(
      map((res: PatientsApplicationResponse) => {
        return res.message ? res.message : null!;
      })
    );
  }

  /**--------------------------------------------------------------
   * PatientsApplication --> postPatientsApplication() 
   * --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the patientsapplication table
   --------------------------------------------------------------*/
   postPatientsApplication(patientsApplication: PatientsApplication): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = patientsApplication;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"patientsApplication",body,{headers})
    .pipe(
      map(res=>{
        return res;
      })
    );
  }

  /**--------------------------------------------------------------
   * PatientsApplication --> putPatientsApplication() 
   * --> Observable<PutResponse>
   * --------------------------------------------------------------
   * Modify a database room
   --------------------------------------------------------------*/
   putPatientsApplication(patientsApplication: PatientsApplication): Observable<PutResponse>{
    let headers=new HttpHeaders();
    
    let body = patientsApplication;
    headers.append('Content-Type' , 'application/json');
    
    return this.http.put<PutResponse>(environment.baseUrl+"patientsApplication",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }
}
