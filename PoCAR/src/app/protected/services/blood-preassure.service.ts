import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BloodPreassure, BloodPreassureResponse, PostResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class BloodPreassureService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string, string, int --> getBloodPreassures() 
   * --> Observable<List[BloodPreassure]>
   * --------------------------------------------------------------
   * Obtains all the values ​​between a certain period of time of the 
   * blood pressure in for a patient
   --------------------------------------------------------------*/
  getBloodPreassures(start: string, end: string, idPatient: number): Observable<BloodPreassure[]>{
    return this.http.get<BloodPreassureResponse>(environment.baseUrl+"bloodPreassures?idPatient="+idPatient+"&startDate="+start+"&endDate="+end)
    .pipe(
      map((res: BloodPreassureResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * BloodPreassure --> postBloodPreassure() 
   * --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the blood pressures table
   --------------------------------------------------------------*/
  postBloodPreassure(bloodPreassure: BloodPreassure): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = bloodPreassure;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"bloodPreassures",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }
}
