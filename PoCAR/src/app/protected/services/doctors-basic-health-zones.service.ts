import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { DeleteResponse, PostResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DoctorsBasicHealthZonesService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string, string 
   * --> putNewRealtionBetweenBasicHealthZoneAndDoctor() 
   * --> Observable<List[BasicHealthZone]>
   * --------------------------------------------------------------
   * Gain all basic health zones from a doctor
   --------------------------------------------------------------*/
  putNewRealtionBetweenBasicHealthZoneAndDoctor(idDoctor: string, idZone: string): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = {
        idDoctor: idDoctor,
        idBasicHealthZone: idZone
    };
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"doctorsBasicHealthZones",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }

  /**--------------------------------------------------------------
   * string, string 
   * --> deleteRealtionBetweenBasicHealthZoneAndDoctor() 
   * --> Observable<DeleteResponse>
   * --------------------------------------------------------------
   * Delete a database event
   --------------------------------------------------------------*/
   deleteRealtionBetweenBasicHealthZoneAndDoctor(idDoctor: string, idZone: string): Observable<DeleteResponse>{
    return this.http.delete<DeleteResponse>(environment.baseUrl+"doctorsBasicHealthZones?idDoctor="+idDoctor+"&idBasicHealthZone="+idZone)
    .pipe(
      map((res: DeleteResponse) => {
        return res;
      })
    );
  }
}
