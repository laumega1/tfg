import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Doctor, DoctorResponse } from 'src/app/shared/interfaces/interfaces';
import { environment } from 'src/environments/environment';
import { PostResponse, PutResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DoctorsService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * getDoctorInfor() --> Observable<Doctor | null>
   * --------------------------------------------------------------
   * Get the data of the doctor given his id
   --------------------------------------------------------------*/
  getDoctorInfor(): Observable<Doctor | null>{
    const id = localStorage.getItem('id');

    return this.http.get<DoctorResponse>(environment.baseUrl+"doctors?id="+id)
    .pipe(
      map((res: DoctorResponse) => {
        return res.message ? res.message[0] : null;
      })
    );
  }

  /**--------------------------------------------------------------
   * string --> getDoctorsByBasicHealthZone() 
   * --> Observable<List[Doctor]>
   * --------------------------------------------------------------
   * Gets all the doctors that belong to a basic health zone
   --------------------------------------------------------------*/
  getDoctorsByBasicHealthZone(idZone: string): Observable<Doctor[]>{
    return this.http.get<DoctorResponse>(environment.baseUrl+"doctors?idBasicHealthZone="+idZone)
    .pipe(
      map((res: DoctorResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * Doctor --> putDoctor() --> Observable<PutResponse>
   * --------------------------------------------------------------
   * Update the information of the doctor
   --------------------------------------------------------------*/
   putDoctor(doctor: Doctor, isPasswordChanged: boolean): Observable<PutResponse>{
    let headers=new HttpHeaders();
    
    let body = {
      id: doctor.id,
      name: doctor.name,
      surname: doctor.surname,
      isAdmin: doctor.isAdmin,
      password: doctor.password,
      isPasswordChanged: isPasswordChanged
    };
    headers.append('Content-Type' , 'application/json');
    
    return this.http.put<PutResponse>(environment.baseUrl+"doctors",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }

  /**--------------------------------------------------------------
   * Doctor --> postDoctor() --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the doctors table
   --------------------------------------------------------------*/
   postDoctor(doctor: Doctor): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = doctor;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"doctors",body,{headers})
    .pipe(
      map(res=>{
        return res;
      })
    );
  }

  /**--------------------------------------------------------------
   * boolean --> putDoctorOnlineState() --> Observable<PutResponse>
   * --------------------------------------------------------------
   * Update the online status of the doctor
   --------------------------------------------------------------*/
  putDoctorOnlineState(isOnline: boolean): Observable<PutResponse>{
    const id = localStorage.getItem('id');

    let headers=new HttpHeaders();
    
    let body = {
      id: id,
      isOnline: isOnline
    }
    headers.append('Content-Type' , 'application/json');
    
    return this.http.put<PutResponse>(environment.baseUrl+"doctors",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }

  /**--------------------------------------------------------------
   * getAllDoctors() --> Observable<Doctor[]>
   * --------------------------------------------------------------
   * Get the data of the doctor given his id
   --------------------------------------------------------------*/
   getAllDoctors(): Observable<Doctor[]>{
    return this.http.get<DoctorResponse>(environment.baseUrl+"doctors?all="+true)
    .pipe(
      map((res: DoctorResponse) => {
        return res.message ? res.message : [];
      })
    );
  }
}
