import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PostResponse, Weight, WeightResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class WeightService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string, string, int --> getWeights() 
   * --> Observable<List[Weight]>
   * --------------------------------------------------------------
   * Obtains all the values ​​between a certain period of time of the 
   * weights in for a patient
   --------------------------------------------------------------*/
  getWeights(start: string, end: string, idPatient: number): Observable<Weight[]>{
    return this.http.get<WeightResponse>(environment.baseUrl+"weights?idPatient="+idPatient+"&startDate="+start+"&endDate="+end)
    .pipe(
      map((res: WeightResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * Weight --> postWeight() --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the weights table
   --------------------------------------------------------------*/
  postWeight(weight: Weight): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = weight;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"weights",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }
}
