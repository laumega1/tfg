import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BasicHealthZone, BasicHealthZoneResponse, PostResponse, PutResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class BasicHealthZonesService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string --> getBasicHealthZonesByDoctor() 
   * --> Observable<List[BasicHealthZone]>
   * --------------------------------------------------------------
   * Gain all basic health zones from a doctor
   --------------------------------------------------------------*/
  getBasicHealthZonesByDoctor(idDoctor: string): Observable<BasicHealthZone[]>{
    return this.http.get<BasicHealthZoneResponse>(environment.baseUrl+"basicHealthZones?idDoctor="+idDoctor)
    .pipe(
      map((res: BasicHealthZoneResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * string --> getBasicHealthZoneByID() 
   * --> Observable<List[BasicHealthZone]>
   * --------------------------------------------------------------
   * Gain all basic health zones from a center
   --------------------------------------------------------------*/
   getBasicHealthZoneByID(id: string): Observable<BasicHealthZone>{
    return this.http.get<BasicHealthZoneResponse>(environment.baseUrl+"basicHealthZones?id="+id)
    .pipe(
      map((res: BasicHealthZoneResponse) => {
        return res.message ? res.message[0] : null!;
      })
    );
  }

  /**--------------------------------------------------------------
   * string --> getAllHealthZones() 
   * --> Observable<List[BasicHealthZone]>
   * --------------------------------------------------------------
   * Gain all basic health zones from a doctor
   --------------------------------------------------------------*/
   getAllHealthZones(): Observable<BasicHealthZone[]>{
    return this.http.get<BasicHealthZoneResponse>(environment.baseUrl+"basicHealthZones?all="+true)
    .pipe(
      map((res: BasicHealthZoneResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * BasicHealthZone --> putBasicHealthZone() 
   * --> Observable<List[BasicHealthZone]>
   * --------------------------------------------------------------
   * Update the name of the basic health zone
   --------------------------------------------------------------*/
   putBasicHealthZone(basicHealthZone: BasicHealthZone): Observable<PutResponse>{
    let headers=new HttpHeaders();
    
    let body = basicHealthZone;
    headers.append('Content-Type' , 'application/json');
    
    return this.http.put<PutResponse>(environment.baseUrl+"basicHealthZones",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }

  /**--------------------------------------------------------------
   * BasicHealthZone --> postBasicHealthZone() 
   * --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the basichealthzones table
   --------------------------------------------------------------*/
   postBasicHealthZone(basicHealthZone: BasicHealthZone): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = basicHealthZone;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"basicHealthZones",body,{headers})
    .pipe(
      map(res=>{
        return res;
      })
    );
  }
}
