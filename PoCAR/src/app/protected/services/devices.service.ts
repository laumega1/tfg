import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Device, DeviceMultipleResponse, DeviceResponse, PostResponse, PutResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DevicesService {

  constructor(
    private http: HttpClient
  ) { }

  /**--------------------------------------------------------------
   * int, string --> getDeviceByTypeAndCoordinatorId() 
   * --> Observable<Device>
   * --------------------------------------------------------------
   * Gets the device we use to take the measurement taking into 
   * account what type of measurement it is and the id of the 
   * coordinator of the room to which it belongs
   --------------------------------------------------------------*/
  getDeviceByTypeAndCoordinatorId(id: number, type: string): Observable<Device>{
    return this.http.get<DeviceResponse>(environment.baseUrl+"devices?idCoordinator="+id+"&type="+type)
    .pipe(
      map((res: DeviceResponse) => {
        return res.message ? res.message : null!;
      })
    );
  }

  /**--------------------------------------------------------------
   * string --> getDeviceByMAC() 
   * --> Observable<Device>
   * --------------------------------------------------------------
   * Get a device based on its MAC address
   --------------------------------------------------------------*/
   getDeviceByMAC(mac: string): Observable<Device>{
    return this.http.get<DeviceResponse>(environment.baseUrl+"devices?mac="+mac)
    .pipe(
      map((res: DeviceResponse) => {
        return res.message ? res.message : null!;
      })
    );
  }

  /**--------------------------------------------------------------
   * getAllDevices() --> Observable<Device>
   * --------------------------------------------------------------
   * Get all of database devices
   --------------------------------------------------------------*/
   getAllDevices(): Observable<Device[]>{
    return this.http.get<DeviceMultipleResponse>(environment.baseUrl+"devices?all="+true)
    .pipe(
      map((res: DeviceMultipleResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * Device --> postDevice() --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the devices table
   --------------------------------------------------------------*/
   postDevice(device: Device): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = device;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"devices",body,{headers})
    .pipe(
      map(res=>{
        return res;
      })
    );
  }

  /**--------------------------------------------------------------
   * Device --> putDevice() --> Observable<PutResponse>
   * --------------------------------------------------------------
   * Modify a database device
   --------------------------------------------------------------*/
   putDevice(device: Device): Observable<PutResponse>{
    let headers=new HttpHeaders();
    
    let body = device;
    headers.append('Content-Type' , 'application/json');
    
    return this.http.put<PutResponse>(environment.baseUrl+"devices",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }
}
