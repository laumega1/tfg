import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MqttConsultNotification } from 'src/app/shared/interfaces/interfaces';
import { environment } from 'src/environments/environment';
import { ConsultNotification, ConsultNotificationResponse, PostResponse, PutResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ConsultsNotificationsService {

  constructor(
    private http: HttpClient
  ) { }

  /**--------------------------------------------------------------
   * MqttConsultNotification --> postConsultNotification() 
   * --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the consultsnotifications table
   --------------------------------------------------------------*/
  postConsultNotification(notification: MqttConsultNotification): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = notification;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"consultsNotifications",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }

  /**--------------------------------------------------------------
   * string, string --> getConsultsNotificationsFromTo() 
   * --> Observable<List[ConsultNotification]>
   * --------------------------------------------------------------
   * Get all query notifications from a doctor's database
   --------------------------------------------------------------*/
  getConsultsNotificationsFromTo(start: string, end: string): Observable<ConsultNotification[]>{
    const id = localStorage.getItem('id');

    return this.http.get<ConsultNotificationResponse>(environment.baseUrl+"consultsNotifications?startDate="+start+"&endDate="+end+"&idDoctor="+id)
    .pipe(
      map((res: ConsultNotificationResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * string, string, string, string --> isNotificationInBBDD() 
   * --> Observable<Boolean>
   * --------------------------------------------------------------
   * Check if a certain notification exists in the database or not
   --------------------------------------------------------------*/
  isNotificationInBBDD(mac: string, idRoom: string, idDoctor: string, sip: string): Observable<Boolean>{
    return this.http.get<ConsultNotificationResponse>(environment.baseUrl+"consultsNotifications?mac="+mac+"&idRoom="+idRoom+"&idDoctor="+idDoctor+"&sip="+sip)
    .pipe(
      map((res: ConsultNotificationResponse) => {
        if(res.message && res.message.length > 0){
          return true;
        }else{
          return false;
        }
      })
    );
  }

  /**--------------------------------------------------------------
   * MqttConsultNotification, ConsultNotification 
   * --> putConsultsNotifications() --> Observable<PutResponse>
   * --------------------------------------------------------------
   * Modify a notification in the database
   --------------------------------------------------------------*/
  putConsultsNotifications(notification: MqttConsultNotification | ConsultNotification): Observable<PutResponse>{
    let headers=new HttpHeaders();
    
    let body = notification;
    headers.append('Content-Type' , 'application/json');
    
    return this.http.put<PutResponse>(environment.baseUrl+"consultsNotifications",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }
}
