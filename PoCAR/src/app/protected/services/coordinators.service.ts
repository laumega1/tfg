import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Coordinator, CoordinatorMultipleResponse, CoordinatorResponse, PostResponse, PutResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class CoordinatorsService {

  constructor(
    private http: HttpClient
  ) { }

  /**--------------------------------------------------------------
   * int --> getCoordinatorByRoomID() --> Observable<Coordinator>
   * --------------------------------------------------------------
   * Get a coordinator given the id of the room to which it belongs
   --------------------------------------------------------------*/
  getCoordinatorByRoomID(id: number): Observable<Coordinator>{
    return this.http.get<CoordinatorResponse>(environment.baseUrl+"coordinators?idRoom="+id)
    .pipe(
      map((res: CoordinatorResponse) => {
        return res.message ? res.message : null!;
      })
    );
  }

  /**--------------------------------------------------------------
   * string --> getCoordinatorByMAC() --> Observable<Coordinator>
   * --------------------------------------------------------------
   * Get a coordinator given the mac of the device to which it 
   * belongs
   --------------------------------------------------------------*/
   getCoordinatorByMAC(mac: string): Observable<Coordinator>{
    return this.http.get<CoordinatorResponse>(environment.baseUrl+"coordinators?mac="+mac)
    .pipe(
      map((res: CoordinatorResponse) => {
        return res.message ? res.message : null!;
      })
    );
  }

  /**--------------------------------------------------------------
   * string --> getCoordinatorByID() --> Observable<Coordinator>
   * --------------------------------------------------------------
   * Get a coordinator given the id
   --------------------------------------------------------------*/
   getCoordinatorByID(id: string): Observable<Coordinator>{
    return this.http.get<CoordinatorResponse>(environment.baseUrl+"coordinators?id="+id)
    .pipe(
      map((res: CoordinatorResponse) => {
        return res.message ? res.message : null!;
      })
    );
  }

  /**--------------------------------------------------------------
   * getAllCoordinators() --> Observable<Coordinator[]>
   * --------------------------------------------------------------
   * Get all of coordinators
   --------------------------------------------------------------*/
   getAllCoordinators(): Observable<Coordinator[]>{
    return this.http.get<CoordinatorMultipleResponse>(environment.baseUrl+"coordinators?all="+true)
    .pipe(
      map((res: CoordinatorMultipleResponse) => {
        return res.message ? res.message : null!;
      })
    );
  }

  /**--------------------------------------------------------------
   * Coordinator --> postCoordinator() 
   * --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the coordinators table
   --------------------------------------------------------------*/
   postCoordinator(coordinator: Coordinator): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = coordinator;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"coordinators",body,{headers})
    .pipe(
      map(res=>{
        return res;
      })
    );
  }

  /**--------------------------------------------------------------
   * Coordinator --> putCoordinator() --> Observable<PutResponse>
   * --------------------------------------------------------------
   * Modify a database coordinator
   --------------------------------------------------------------*/
  putCoordinator(coordinator: Coordinator): Observable<PutResponse>{
    let headers=new HttpHeaders();
    
    let body = coordinator;
    headers.append('Content-Type' , 'application/json');
    
    return this.http.put<PutResponse>(environment.baseUrl+"coordinators",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }
}
