import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Center, CenterResponse, PostResponse, PutResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class CentersService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string --> getCenterByBasicHealthZone() 
   * --> Observable<List[Center]>
   * --------------------------------------------------------------
   * Gain all hubs in a basic health zone
   --------------------------------------------------------------*/
  getCenterByBasicHealthZone(idHealthZone: string): Observable<Center[]>{
    return this.http.get<CenterResponse>(environment.baseUrl+"centers?idBasicHealthZone="+idHealthZone)
    .pipe(
      map((res: CenterResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * string --> getCenterById() 
   * --> Observable<List[Center]>
   * --------------------------------------------------------------
   * Get center by id
   --------------------------------------------------------------*/
   getCenterById(id: string): Observable<Center>{
    return this.http.get<CenterResponse>(environment.baseUrl+"centers?id="+id)
    .pipe(
      map((res: CenterResponse) => {
        return res.message ? res.message[0] : null!;
      })
    );
  }

  /**--------------------------------------------------------------
   * getAllCenters() --> Observable<List[Center]>
   * --------------------------------------------------------------
   * Get all centers of the data base
   --------------------------------------------------------------*/
   getAllCenters(): Observable<Center[]>{
    return this.http.get<CenterResponse>(environment.baseUrl+"centers?all="+true)
    .pipe(
      map((res: CenterResponse) => {
        return res.message ? res.message : [];
      })
    );
  }
  
  /**--------------------------------------------------------------
   * Center --> postCenter() --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the centers table
   --------------------------------------------------------------*/
   postCenter(center: Center): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = center;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"centers",body,{headers})
    .pipe(
      map(res=>{
        return res;
      })
    );
  }

  /**--------------------------------------------------------------
   * Center --> putCenter() --> Observable<PutResponse>
   * --------------------------------------------------------------
   * Modify a database center
   --------------------------------------------------------------*/
  putCenter(center: Center): Observable<PutResponse>{
    let headers=new HttpHeaders();
    
    let body = center;
    headers.append('Content-Type' , 'application/json');
    
    return this.http.put<PutResponse>(environment.baseUrl+"centers",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }
}
