import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Patient, PatientResponse, PostResponse, PutResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * getAllPatients() --> Observable<List[Patient]>
   * --------------------------------------------------------------
   * Get all database patients
   --------------------------------------------------------------*/
  getAllPatients(): Observable<Patient[]>{
    return this.http.get<PatientResponse>(environment.baseUrl+"patients?all="+true)
    .pipe(
      map((res: PatientResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * int/null --> getDoctorPatients() --> Observable<List[Patient]>
   * --------------------------------------------------------------
   * Gets the patients of a doctor or the patients of a doctor 
   * and a hospital
   --------------------------------------------------------------*/
  getDoctorPatients(idHospital: number | null): Observable<Patient[]>{
    const id = localStorage.getItem('id');

    var params ="";
    if(idHospital != null){
      params = "&idHospital="+idHospital;
    }

    return this.http.get<PatientResponse>(environment.baseUrl+"patients?idDoctor="+id+params)
    .pipe(
      map((res: PatientResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * Patient --> putPatient() --> Observable<PutResponse>
   * --------------------------------------------------------------
   * Modify the patient from the databasel
   --------------------------------------------------------------*/
  putPatient(patient: Patient): Observable<PutResponse>{
    let headers=new HttpHeaders();
    
    let body = patient;
    headers.append('Content-Type' , 'application/json');
    
    return this.http.put<PutResponse>(environment.baseUrl+"patients",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }

  /**--------------------------------------------------------------
   * Patient --> postPatient() --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the patients table
   --------------------------------------------------------------*/
  postPatient(patient: Patient): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = patient;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"patients",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }
}
