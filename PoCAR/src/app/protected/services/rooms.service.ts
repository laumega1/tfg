import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PostResponse, PutResponse, Room, RoomResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class RoomsService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string --> getRoomsByCenter() --> Observable<List[Room]>
   * --------------------------------------------------------------
   * Get all rooms of a center
   --------------------------------------------------------------*/
  getRoomsByCenter(idCenter: string): Observable<Room[]>{
    return this.http.get<RoomResponse>(environment.baseUrl+"rooms?idCenter="+idCenter)
    .pipe(
      map((res: RoomResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * stringetAllRooms() --> Observable<List[Room]>
   * --------------------------------------------------------------
   * Get all rooms in the database
   --------------------------------------------------------------*/
  getAllRooms(): Observable<Room[]>{
    return this.http.get<RoomResponse>(environment.baseUrl+"rooms?all="+true)
    .pipe(
      map((res: RoomResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * Room --> postRoom() --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the rooms table
   --------------------------------------------------------------*/
  postRoom(room: Room): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = room;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"rooms",body,{headers})
    .pipe(
      map(res=>{
        return res;
      })
    );
  }

  /**--------------------------------------------------------------
   * Room --> putRoom() --> Observable<PutResponse>
   * --------------------------------------------------------------
   * Modify a database room
   --------------------------------------------------------------*/
   putRoom(room: Room): Observable<PutResponse>{
    let headers=new HttpHeaders();
    
    let body = room;
    headers.append('Content-Type' , 'application/json');
    
    return this.http.put<PutResponse>(environment.baseUrl+"rooms",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }
}
