import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { DeleteResponse, Event, EventResponse, PostResponse, PutResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * Event --> postEvent() --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the events table
   --------------------------------------------------------------*/
  postEvent(event: Event): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = event;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"events",body,{headers})
    .pipe(
      map(res=>{
        return res;
      })
    );
  }

  /**--------------------------------------------------------------
   * getAllEvents() --> Observable<List[Event]>
   * --------------------------------------------------------------
   * Get all doctor events
   --------------------------------------------------------------*/
  getAllEvents(): Observable<Event[]>{
    const id = localStorage.getItem('id');

    return this.http.get<EventResponse>(environment.baseUrl+"events?idDoctor="+id)
    .pipe(
      map((res: EventResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * int --> getPatientEvents() --> Observable<List[Event]>
   * --------------------------------------------------------------
   * Get all the events of a patient
   --------------------------------------------------------------*/
  getPatientEvents(idPatient: number): Observable<Event[]>{
    const id = localStorage.getItem('id');
    
    return this.http.get<EventResponse>(environment.baseUrl+"events?idDoctor="+id+"&idPatient="+idPatient)
    .pipe(
      map((res: EventResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * string, string --> getEventsFromTo() 
   * --> Observable<List[Event]>
   * --------------------------------------------------------------
   * Get all the events of a doctor between a certain period
   --------------------------------------------------------------*/
  getEventsFromTo(start: string, end: string): Observable<Event[]>{
    const id = localStorage.getItem('id');

    return this.http.get<EventResponse>(environment.baseUrl+"events?idDoctor="+id+"&startDate="+start+"&endDate="+end)
    .pipe(
      map((res: EventResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * Event --> putEvent() --> Observable<PutResponse>
   * --------------------------------------------------------------
   * Modify a database event
   --------------------------------------------------------------*/
  putEvent(event: Event): Observable<PutResponse>{
    let headers=new HttpHeaders();
    
    let body = event;
    headers.append('Content-Type' , 'application/json');
    
    return this.http.put<PutResponse>(environment.baseUrl+"events",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }

  /**--------------------------------------------------------------
   * int --> deleteEvent() --> Observable<DeleteResponse>
   * --------------------------------------------------------------
   * Delete a database event
   --------------------------------------------------------------*/
  deleteEvent(id: number): Observable<DeleteResponse>{
    return this.http.delete<DeleteResponse>(environment.baseUrl+"events?id="+id)
    .pipe(
      map((res: DeleteResponse) => {
        return res;
      })
    );
  }
}
