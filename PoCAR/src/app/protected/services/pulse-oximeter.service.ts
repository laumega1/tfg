import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PostResponse, PulseOximeter, PulseOximeterResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class PulseOximeterService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string, string, int --> getPulseOximeter() 
   * --> Observable<List[PulseOximeter]>
   * --------------------------------------------------------------
   * Obtains all the values ​​between a certain period of time of the 
   * pulse oximeter in for a patient
   --------------------------------------------------------------*/
  getPulseOximeter(start: string, end: string, idPatient: number): Observable<PulseOximeter[]>{
    return this.http.get<PulseOximeterResponse>(environment.baseUrl+"pulseOximeter?idPatient="+idPatient+"&startDate="+start+"&endDate="+end)
    .pipe(
      map((res: PulseOximeterResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * PulseOximeter --> postPulseOximeter() 
   * --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the pulseoximeter table
   --------------------------------------------------------------*/
  postPulseOximeter(pulseOximeter: PulseOximeter): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = pulseOximeter;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"pulseOximeter",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }
}
