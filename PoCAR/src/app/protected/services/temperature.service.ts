import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PostResponse, Temperature, TemperatureResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class TemperatureService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string, string, int --> getTemperatures() 
   * --> Observable<List[Temperature]>
   * --------------------------------------------------------------
   * Obtains all the values ​​between a certain period of time of the 
   * temperature in for a patient
   --------------------------------------------------------------*/
  getTemperatures(start: string, end: string, idPatient: number): Observable<Temperature[]>{
    return this.http.get<TemperatureResponse>(environment.baseUrl+"temperatures?idPatient="+idPatient+"&startDate="+start+"&endDate="+end)
    .pipe(
      map((res: TemperatureResponse) => {
        return res.message ? res.message : [];
      })
    );
  }

  /**--------------------------------------------------------------
   * Temperature --> postTemperature() 
   * --> Observable<PostResponse>
   * --------------------------------------------------------------
   * Create a new row in the temperatures table
   --------------------------------------------------------------*/
  postTemperature(temperature: Temperature): Observable<PostResponse>{
    let headers=new HttpHeaders();
    
    let body = temperature;
    
    headers.append('Content-Type' , 'application/json');
    
    return this.http.post<PostResponse>(environment.baseUrl+"temperatures",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }
}
