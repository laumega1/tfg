import { LOCALE_ID, Inject, Injectable } from '@angular/core';
import { CalendarEventTitleFormatter, CalendarEvent } from 'angular-calendar';
import { formatDate } from '@angular/common';

@Injectable()
export class CustomEventTitleFormatter extends CalendarEventTitleFormatter {
  constructor(@Inject(LOCALE_ID) private locale: string) {
    super();
  }

  /**--------------------------------------------------------------
   * CalendarEvent --> month() --> string
   * --------------------------------------------------------------
   * Generate an html for the month view to be able to have a 
   * custom event display from the calendar
   --------------------------------------------------------------*/
  month(event: CalendarEvent): string {
    const code = `
    <div class="calendarEventInformation">
        <div>
            <b>${new Date(event.start).getHours()}:${new Date(event.start).getMinutes()}</b>
            -
            ${event.meta.patient.showData}
        </div>
        <div>
            ${event.title} en:
            ${event.meta.center.name}
        </div>
    </div>
    `;

    return code;
  }

  /**--------------------------------------------------------------
   * CalendarEvent --> week() --> string
   * --------------------------------------------------------------
   * Generate an html for the week view to be able to have a 
   * custom event display from the calendar
   --------------------------------------------------------------*/
  week(event: CalendarEvent): string {
    const code = `
    <div class="calendarEventInformation">
        <div>
            <b>${new Date(event.start).getHours()}:${new Date(event.start).getMinutes()}</b>
            -
            ${event.meta.patient.showData}
        </div>
        <div>
          ${event.title} en:
          ${event.meta.center.name}
        </div>
    </div>
    `;

    return code;
  }

  /**--------------------------------------------------------------
   * CalendarEvent --> day() --> string
   * --------------------------------------------------------------
   * Generate an html for the day view to be able to have a 
   * custom event display from the calendar
   --------------------------------------------------------------*/
  day(event: CalendarEvent): string {
    const code = `
    <div class="calendarEventInformation">
        <div>
            <b>${new Date(event.start).getHours()}:${new Date(event.start).getMinutes()}</b>
            -
            ${event.meta.patient.showData}
        </div>
        <div>
          ${event.title} en:
          ${event.meta.center.name}
        </div>
    </div>
    `;

    return code;
  }
}
