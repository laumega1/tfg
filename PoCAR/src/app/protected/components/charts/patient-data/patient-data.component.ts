import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { MessageService } from 'primeng/api';
import { BloodPreassure, Patient, PulseOximeter, Weight } from 'src/app/protected/interfaces/interfaces';
import { TemperatureService } from 'src/app/protected/services/temperature.service';
import { Temperature } from '../../../interfaces/interfaces';
import { subHours, subDays, subWeeks, subMonths, subYears } from 'date-fns';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { WeightService } from 'src/app/protected/services/weight.service';
import { BloodPreassureService } from '../../../services/blood-preassure.service';
import { PulseOximeterService } from '../../../services/pulse-oximeter.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patient-data',
  templateUrl: './patient-data.component.html',
  styleUrls: ['./patient-data.component.scss'],
  providers: [
    MessageService
  ]
})
export class PatientDataComponent implements OnInit {

  @Input() patient: Patient = null!;

  @Input() updateData: boolean = false;
  @Output() updateDataChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  currentUrl: string = "";

  posibleCharts: string[] = ["Temperatura", "Peso", "Tensión", "Pulsaciones y oxígeno"];
  selectedCharts: string[] = this.posibleCharts;

  posibleTimes: string[] = ["Última hora", "Último día", "Última semana", "Último mes", "Último año", "Personalizado"];
  selectedTime: string = "Última hora";

  myForm: FormGroup = this.fb.group({
    selectedCharts: [this.posibleCharts, [Validators.required]],
    selectedTime: ["Última hora", [Validators.required]],
    startDate: [null , [Validators.required]],
    endDate: [null , [Validators.required]]
  });

  temperatures: Temperature[] = [];
  weights: Weight[] = [];
  bloodPreasures: BloodPreassure[] = [];
  pulseOximeter: PulseOximeter[] = []

  constructor(
    private temperatureService: TemperatureService,
    private weightService: WeightService,
    private bloodPreassureService: BloodPreassureService,
    private pulseOximeterService: PulseOximeterService,
    private router: Router,
    private fb: FormBuilder
  ) { }

  ngOnChanges(changes: SimpleChanges) {
   if(changes.patient && changes.patient.currentValue){
      this.patient = changes.patient.currentValue;
      this.showAllCharts();
    }

    if(changes.updateData && this.updateData == true){
      this.showAllCharts();

      this.updateData = false;
      this.updateDataChange.emit(this.updateData);
    }
  }

  ngOnInit(): void {
    this.subscribeChangesFilter();

    this.currentUrl = this.router.url;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- AUXILIAR ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * subscribeChangesFilter()
   * --------------------------------------------------------------
   * Subscribes to changes in the type of charts to display or the 
   * time at which to display them. These parameters can be 
   * modified from the icons in the upper right corner of the panel
   --------------------------------------------------------------*/
  subscribeChangesFilter(): void{
    this.myForm.get('selectedCharts')?.valueChanges.subscribe((value: string[]) => {
      this.selectedCharts = value;
      this.showAllCharts();
    });

    this.myForm.get('selectedTime')?.valueChanges.subscribe((value: string) => {
      this.selectedTime = value;
      this.showAllCharts();
    });

    this.myForm.get('startDate')?.valueChanges.subscribe((value: string) => {
      this.showAllCharts();
    });

    this.myForm.get('endDate')?.valueChanges.subscribe((value: string) => {
      this.showAllCharts();
    });
  }

  /**--------------------------------------------------------------
   * showAllCharts()
   * --------------------------------------------------------------
   * It requests the data and shows it for each type of graph 
   * selected in a certain period
   --------------------------------------------------------------*/
  showAllCharts(): void{
    this.selectedCharts.forEach(chart => {
      this.getChartData(chart);
    });
  }

  /**--------------------------------------------------------------
   * string --> getChartData()
   * --------------------------------------------------------------
   * Depending on the type of graph, it calls one function or 
   * another that will make the request to the database
   --------------------------------------------------------------*/
  getChartData(chart: string): void{
    switch(chart){
      case "Temperatura":
        this.getTemperaturesByDate(this.getStartTime(), this.getEndTime());
        break;
      case "Peso":
        this.getWeightsByDate(this.getStartTime(), this.getEndTime());
        break;
      case "Tensión":
        this.getBloodPreassuresByDate(this.getStartTime(), this.getEndTime());
        break;
      case "Pulsaciones y oxígeno":
        this.getpulsesOximeterByDate(this.getStartTime(), this.getEndTime());
        break;
    }
  }

  /**--------------------------------------------------------------
   * getStartTime() --> Date
   * --------------------------------------------------------------
   * Gets the specific start Date based on the selected interval
   --------------------------------------------------------------*/
  getStartTime(): Date{
    switch(this.selectedTime){
      case "Última hora":
        return subHours(new Date(), 1);
      case "Último día":
        return subDays(new Date(), 1);
      case "Última semana":
        return subWeeks(new Date(), 1);
      case "Último mes":
        return subMonths(new Date(), 1);
      case "Último año":
        return subYears(new Date(), 1);
      case "Personalizado":
        return new Date(this.myForm.get('startDate')?.value);
      default:
        return new Date();
    }
  }

  /**--------------------------------------------------------------
   * getEndTime() --> Date
   * --------------------------------------------------------------
   * Gets the specific end Date based on the selected interval
   --------------------------------------------------------------*/
  getEndTime(): Date{
    if(this.selectedTime == "Personalizado"){
      return new Date(this.myForm.get('endDate')?.value);
    }else{
      return new Date();
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * Date, Date --> getTemperaturesByDate()
   * --------------------------------------------------------------
   * Obtain all temperature data for a certain period for the 
   * selected patient
   --------------------------------------------------------------*/
  getTemperaturesByDate(start: Date, end: Date): void{
    this.temperatureService.getTemperatures(start.toUTCString(), end.toUTCString(), this.patient.id).subscribe( (resp: Temperature[]) => {
      this.temperatures = resp ? resp : [];
    });
  }

  /**--------------------------------------------------------------
   * Date, Date --> getWeightsByDate()
   * --------------------------------------------------------------
   * Obtain all weight data for a certain period for the 
   * selected patient
   --------------------------------------------------------------*/
  getWeightsByDate(start: Date, end: Date): void{
    this.weightService.getWeights(start.toUTCString(), end.toUTCString(), this.patient.id).subscribe( (resp: Weight[]) => {
      this.weights = resp ? resp : [];
    });
  }

  /**--------------------------------------------------------------
   * Date, Date --> getBloodPreassuresByDate()
   * --------------------------------------------------------------
   * Obtain all blood pressures data for a certain period for the 
   * selected patient
   --------------------------------------------------------------*/
  getBloodPreassuresByDate(start: Date, end: Date): void{
    this.bloodPreassureService.getBloodPreassures(start.toUTCString(), end.toUTCString(), this.patient.id).subscribe( (resp: BloodPreassure[]) => {
      this.bloodPreasures = resp ? resp : [];
    });
  }

  /**--------------------------------------------------------------
   * Date, Date --> getpulsesOximeterByDate()
   * --------------------------------------------------------------
   * Obtain all pulseoximeter data for a certain period for the 
   * selected patient
   --------------------------------------------------------------*/
  getpulsesOximeterByDate(start: Date, end: Date): void{
    this.pulseOximeterService.getPulseOximeter(start.toUTCString(), end.toUTCString(), this.patient.id).subscribe( (resp: PulseOximeter[]) => {
      this.pulseOximeter = resp ? resp : [];
    });
  }

}
