import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { LegendPosition } from '@swimlane/ngx-charts';
import { GraphData, GraphDataValue, Weight } from 'src/app/protected/interfaces/interfaces';
import { Temperature, BloodPreassure, PulseOximeter } from '../../../interfaces/interfaces';

@Component({
  selector: 'app-generic-chart',
  templateUrl: './generic-chart.component.html',
  styleUrls: ['./generic-chart.component.scss']
})
export class GenericChartComponent implements OnInit {

  @Input() elementType: string = '';
  @Input() selectedTime: string = '';
  @Input() receivedData: Temperature[] | Weight[] = [];
  @Input() receivedBloodPreassureData: BloodPreassure[] = [];
  @Input() receivedPulseOximeterData: PulseOximeter[] = [];

  data: GraphData[] = null!;

  colorScheme: any = { domain: ['#5995BA', '#258075', '#59BAAE']};

  xAxisLabel: string = '';

  yAxisLabel: string = '';

  showLegend: boolean = false;
  legendPosition: LegendPosition = LegendPosition.Below;

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.receivedData){
      switch(this.elementType){
        case "Temperatura":
          this.showSimpleData();
          break;
        case "Peso":
          this.showSimpleData();
          break;
      }
    }else if(changes.receivedBloodPreassureData){
      this.showLegend = true;
      this.showBloodPreassureData();
    }else if(changes.receivedPulseOximeterData){
      this.showLegend = true;
      this.showPulseOximeterData()
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //---------------------------------------------- SIMPLE LINE CHARTS ---------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * showSimpleData()
   * --------------------------------------------------------------
   * Create and display a single-item chart type, such as 
   * temperature or weight
   --------------------------------------------------------------*/
  showSimpleData(): void{
    if(this.receivedData.length > 0 ){
      this.yAxisLabel = this.receivedData[0].unit;
      this.data = [{
        name: this.elementType,
        series: []
      }];
  
      this.xAxisLabel = this.generateXLabel();
  
      this.receivedData.forEach(val => {
        var item: GraphDataValue = {
          name: this.generateNameBySelectedTime(new Date(val.timestamp)),
          value: val.value
        }
  
        this.data[0].series.push(item);
      });
    }else{
      this.data = null!;
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //---------------------------------------------- MULTIPLE LINES CHARTS ------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * showBloodPreassureData()
   * --------------------------------------------------------------
   * Create and display a type of blood pressure graph, which will 
   * have 3 different values, the systolic pressure, the diastolic 
   * pressure and the baud rate
   --------------------------------------------------------------*/
  showBloodPreassureData(): void{
    if(this.receivedBloodPreassureData.length > 0 ){
      this.yAxisLabel = this.receivedBloodPreassureData[0].diastolicUnit + " (Sist. y Diast.) · "+ this.receivedBloodPreassureData[0].rateUnit +" (Puls.)";
      this.data = [{
        name: "Sistólica",
        series: []
      },
      {
        name: "Diastólica",
        series: []
      },
      {
        name: "Pulsaciones",
        series: []
      }
      ];
  
      this.xAxisLabel = this.generateXLabel();
  
      this.receivedBloodPreassureData.forEach(val => {
        this.data[0].series.push({
          name: this.generateNameBySelectedTime(new Date(val.timestamp)),
          value: val.systolicValue
        });
  
        this.data[1].series.push({
          name: this.generateNameBySelectedTime(new Date(val.timestamp)),
          value: val.diastolicValue
        });
  
        this.data[2].series.push({
          name: this.generateNameBySelectedTime(new Date(val.timestamp)),
          value: val.rateValue
        });
      });
    }else{
      this.data = null!;
    }
  }

  /**--------------------------------------------------------------
   * showPulseOximeterData()
   * --------------------------------------------------------------
   * Create and display a type of pulse oximeter graph, which will 
   * have 2 different values, the oxigen, and the baud rate
   --------------------------------------------------------------*/
  showPulseOximeterData(): void{
    if(this.receivedPulseOximeterData.length > 0 ){
      this.yAxisLabel = this.receivedPulseOximeterData[0].oxygenUnit+" (Oxi.) · "+ this.receivedPulseOximeterData[0].beatsUnit +" (Puls.)";
      this.data = [{
        name: "Oxígeno",
        series: []
      },
      {
        name: "Pulsaciones",
        series: []
      }
      ];
  
      this.xAxisLabel = this.generateXLabel();
  
      this.receivedPulseOximeterData.forEach(val => {
        this.data[0].series.push({
          name: this.generateNameBySelectedTime(new Date(val.timestamp)),
          value: val.oxygenValue
        });
  
        this.data[1].series.push({
          name: this.generateNameBySelectedTime(new Date(val.timestamp)),
          value: val.beatsValue
        });
      });
    }else{
      this.data = null!;
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- AUXILIAR ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * generateXLabel()
   * --------------------------------------------------------------
   * Generates the text to show the label x so that it best adapts 
   * to each time interval
   --------------------------------------------------------------*/
  generateXLabel(): string{
    switch(this.selectedTime){
      case "Última hora":
        return "Hora";
      case "Último día":
        return "Hora";
      case "Última semana":
        return "Día y hora";
      case "Último mes":
        return "Día y hora";
      case "Último año":
        return "Fecha";
      default:
        return "Fecha";
    }
  }

  /**--------------------------------------------------------------
   * generateNameBySelectedTime()
   * --------------------------------------------------------------
   * Modify each text of each value that will be displayed on the 
   * x-axis in order to better adapt to the time interval
   --------------------------------------------------------------*/
  generateNameBySelectedTime(date: Date): string{
    switch(this.selectedTime){
      case "Última hora":
        return date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
      case "Último día":
        return date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
      case "Último mes":
        return date.getUTCDate()+", "+date.getHours()+":"+date.getMinutes();
      case "Última semana":
        return date.getUTCDate()+", "+date.getHours()+":"+date.getMinutes();
      case "Último año":
        return date.toLocaleString();
      default:
        return date.toLocaleString();
    }
  }

  /**--------------------------------------------------------------
   * generateNameBySelectedTime()
   * --------------------------------------------------------------
   * Calls a certain function that re-requests the data when the 
   * user manually clicks the reload arrow next to the title of 
   * each chart
   --------------------------------------------------------------*/
  updateChartData(): void{
    this.updateData.emit();
  }
}
