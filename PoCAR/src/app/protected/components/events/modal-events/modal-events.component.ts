import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { addHours } from 'date-fns';
import { CalendarEvent, CalendarEventAction } from 'angular-calendar';
import { Message, MessageService } from 'primeng/api';
import { Patient, Event, PostResponse, PutResponse, Center } from 'src/app/protected/interfaces/interfaces';
import { EventService } from 'src/app/protected/services/event.service';
import { CentersService } from '../../../services/centers.service';
import { DoctorsService } from 'src/app/protected/services/doctors.service';
import { BasicHealthZonesService } from 'src/app/protected/services/basic-healt-zones.service';
import { BasicHealthZone } from '../../../interfaces/interfaces';
import { Doctor } from 'src/app/shared/interfaces/interfaces';

@Component({
  selector: 'app-modal-events',
  templateUrl: './modal-events.component.html',
  styleUrls: ['./modal-events.component.scss'],
  providers: [
    MessageService
  ]
})
export class ModalEventsComponent implements OnInit {
  @Input() parentType: string = "";

  @Input() dialogHeader: string = "";
  @Input() buttonSummitTitle: string = "";

  @Input() resetModalValues: boolean = false;

  @Input() currentEvent: CalendarEvent = null!;
  @Input() currentDay: Date = null!;

  @Input() patients: Patient[] = [];
  
  centers: Center[] = [];
  currentDoctor: Doctor = null!;

  @Input() dialogMode: string = "";
  @Output() dialogModeChange: EventEmitter<string> = new EventEmitter<string>();

  @Input() displayModalEvent: boolean = false;
  @Output() displayModalEventChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() calendarEvents: CalendarEvent[] = [];
  @Output() calendarEventsChange: EventEmitter<CalendarEvent[]> = new EventEmitter<CalendarEvent[]>();

  @Input() upcommingEvents: Event[] = [];
  @Output() upcommingEventsChange: EventEmitter<Event[]> = new EventEmitter<Event[]>();

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();

  @Input() actions: CalendarEventAction[] = [];

  myForm: FormGroup = this.fb.group({
    selectedPatient: [null, [Validators.required]],
    selectedCenter: [null, [Validators.required]],
    selectedDoctor: [null, [Validators.required]],
    description: ['', [Validators.required]],
    primaryColor: ['#c9ff4d', [Validators.required]],
    secundaryColor: ['#ddfcb8', [Validators.required]],
    startDate: ['', [Validators.required]],
    endDate: ['', [Validators.required]]
  });

  correctPost: Message = {severity:'success', summary:'Éxito', detail:'El nuevo evento ha sido creado correctamente.'};
  errorPost: Message = {severity:'error', summary:'Error', detail:'No se ha podido crear un nuevo evento.'};
  correctUpdate: Message = {severity:'success', summary:'Éxito', detail:'El evento ha sido modificado correctamente.'};
  errorUpdate: Message = {severity:'error', summary:'Error', detail:'No se ha podido editar el evento.'};
  
  constructor(
    private fb: FormBuilder,
    private eventService: EventService,
    private messageService: MessageService,
    private centersService: CentersService,
    private basicHealthZonesSevice: BasicHealthZonesService,
    private doctorsService: DoctorsService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    switch(this.parentType){
      case "fullCalendar":
        if(changes.dialogMode && changes.displayModalEvent.currentValue == true){
          this.getDoctorsData();
          this.getDoctorCenters();

          if(changes.dialogMode.currentValue == 'put'){
            this.changeDialogInputsEdit();
          }else{
            this.resetFormToInitValues();
          }
        }
        break;
      case "upcommingEvents":
        if(changes.dialogMode && changes.displayModalEvent.currentValue == true){
          this.getDoctorsData();
          this.getDoctorCenters();
          
          this.resetFormToInitValues();
          this.myForm.patchValue({
            startDate: this.currentDay,
            endDate: addHours(this.currentDay, 1)
          });
        }
        break;
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------------- FORMS -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * any --> setPatientData()
   * --------------------------------------------------------------
   * Set patient data, including their default center
   --------------------------------------------------------------*/
  setPatientData(event: any): void{
    const patient = event.value;

    this.myForm.patchValue({
      selectedCenter: this.centers.find(x => x.id == patient.center!.id)
    });

    this.getDoctorsData();
  }

  /**--------------------------------------------------------------
   * getFormValues() --> Array[Event, Patient, Center, Doctor]
   * --------------------------------------------------------------
   * Get all the form data
   --------------------------------------------------------------*/
  getFormValues(): [Event, Patient, Center, Doctor]{
    const {selectedPatient, selectedCenter, description, primaryColor, secundaryColor, startDate, endDate} = this.myForm.value;

    var event: Event = {
      idDoctor: this.currentDoctor.id,
      idPatient: selectedPatient.id,
      idCenter: selectedCenter.id,
      description: description,
      primaryColor: primaryColor,
      secundaryColor: secundaryColor,
      startDate: startDate,
      endDate: endDate
    }

    return [event, selectedPatient, selectedCenter, this.currentDoctor];
  }

  /**--------------------------------------------------------------
   * resetFormToInitValues()
   * --------------------------------------------------------------
   * Reset the form to the initial values ​​for when you want to 
   * create a new event
   --------------------------------------------------------------*/
  resetFormToInitValues(): void{
    this.myForm.reset();

    this.myForm.patchValue({
      primaryColor: '#c9ff4d',
      secundaryColor: '#ddfcb8'
    });
  }
  
  /**--------------------------------------------------------------
   * changeDialogInputsEdit()
   * --------------------------------------------------------------
   * When you want to edit an event, you get all the values ​​in the 
   * corresponding fields
   --------------------------------------------------------------*/
  changeDialogInputsEdit(): void{
    this.myForm.patchValue({
      selectedDoctor: this.currentEvent.meta.doctor.showData,
      selectedPatient: this.currentEvent.meta.patient,
      description: this.currentEvent.title,
      primaryColor: this.currentEvent.color?.primary,
      secundaryColor: this.currentEvent.color?.secondary,
      startDate: this.currentEvent.start,
      endDate: this.currentEvent.end
    });
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- MODALS ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * Event, Patient, Center, Doctor --> updateEventCalendar()
   * --------------------------------------------------------------
   * Modifies the corresponding event in the list and emits it to 
   * be updated in the calendar
   --------------------------------------------------------------*/
  updateEventCalendar(event: Event, patient: Patient, center: Center, doctor: Doctor): void{
    var calendarEvent = this.calendarEvents.find(x => x.meta.id == event.id);

    calendarEvent!.start = event.startDate;
    calendarEvent!.end = event.endDate;
    calendarEvent!.title = event.description;
    calendarEvent!.color!.primary = event.primaryColor;
    calendarEvent!.color!.secondary = event.secundaryColor;
    calendarEvent!.meta.center = center;
    calendarEvent!.meta.doctor = doctor;
    calendarEvent!.meta.patient = patient;
    calendarEvent!.meta.id = event.id;
    
    this.calendarEventsChange.emit(this.calendarEvents);
    this.updateData.emit();
  }

  /**--------------------------------------------------------------
   * closeModal()
   * --------------------------------------------------------------
   * Closes the modal and casts it to be updated from the 
   * component that contains this
   --------------------------------------------------------------*/
  closeModal(): void{
    this.displayModalEvent = false;
    this.displayModalEventChange.emit(this.displayModalEvent);

    this.dialogMode = "";
    this.dialogModeChange.emit(this.dialogMode);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- AUXILIAR ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * Event, Patient, Center, Doctor --> generateCalendarEvent()
   * --------------------------------------------------------------
   * An object of type calendarEvent is generated, which is the one
   * that can read and use the calendar
   --------------------------------------------------------------*/
  generateCalendarEvent(event: Event, patient: Patient, center: Center, doctor: Doctor): CalendarEvent{
    const myEvent: CalendarEvent = {
      start: new Date(event.startDate),
      end: new Date(event.endDate),
      title: event.description,
      color: {
        primary: event.primaryColor,
        secondary: event.secundaryColor
      },
      actions: this.actions,
      allDay: false,
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
      draggable: true,
      meta: {
        center: center,
        doctor: doctor,
        patient: patient,
        id: event.id
      }
    }

    return myEvent;
  }

  /**--------------------------------------------------------------
   * Message --> showAlert()
   * --------------------------------------------------------------
   * Display alert message in popup
   --------------------------------------------------------------*/
  showAlert(alert: Message): void{
    if(this.messageService){
      this.messageService.clear();
    }
    this.messageService.add(alert);
  }

  /**--------------------------------------------------------------
   * Patient --> setCenterAfterDropdownUpdate()
   * --------------------------------------------------------------
   * When a patient is selected, the center dropdown is set, his by 
   * default. It is important to run a little sleep, otherwise the 
   * selected item is not shown, since it is selected before the 
   * total list of centers is available.
   --------------------------------------------------------------*/
  async setCenterAfterDropdownUpdate(patient: Patient) {
    await this.sleep(1000);

    if(this.dialogMode == "put"){
      this.myForm.patchValue({
        selectedCenter: this.centers.find(x => x.id == this.currentEvent.meta.center.id)
      });
    }else{
      this.myForm.patchValue({
        selectedCenter: this.centers.find(x => x.id == patient.center!.id)
      });
    }
  }
 
  /**--------------------------------------------------------------
   * sleep()
   * --------------------------------------------------------------
   * Await n milliseconds
   --------------------------------------------------------------*/
  sleep(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------
  
  /**--------------------------------------------------------------
   * createNewEvent()
   * --------------------------------------------------------------
   * When pressing the create button, the new event is created and 
   * inserted both in the database and in the calendar or in the
   * upcommingEvents
   --------------------------------------------------------------*/
  createNewEvent(): void{
    const [newEvent, selectedPatient, selectedCenter, selectedDoctor] = this.getFormValues();

    console.log(newEvent);

    this.eventService.postEvent(newEvent).subscribe( (resp: PostResponse) => {
      if(resp.status == 200){
        newEvent.id = resp.id;

        this.showAlert(this.correctPost);

        switch(this.parentType){
          case "fullCalendar":
            this.calendarEvents.push(this.generateCalendarEvent(newEvent, selectedPatient, selectedCenter, selectedDoctor));
            this.calendarEventsChange.emit(this.calendarEvents);
            this.updateData.emit();
            break;
          case "upcommingEvents":
            this.upcommingEvents.push(newEvent);
            this.upcommingEventsChange.emit(this.upcommingEvents);
            this.updateData.emit();
        }
      }else{
        this.showAlert(this.errorPost);
      }
    });
  }

  /**--------------------------------------------------------------
   * editEvent()
   * --------------------------------------------------------------
   * When pressing the edit button, the current event is modified 
   * and inserted both in the database and in the calendar or in 
   * the upcommingEvents
   --------------------------------------------------------------*/
  editEvent(): void{
    const [event, selectedPatient, selectedCenter, selectedDoctor] = this.getFormValues();

    event.id = this.currentEvent.meta.id;

    this.eventService.putEvent(event).subscribe( (resp: PutResponse) => {
      if(resp.status == 200){
        this.updateEventCalendar(event, selectedPatient, selectedCenter, selectedDoctor);

        this.showAlert(this.correctUpdate);
      }else{
        this.showAlert(this.errorUpdate);
      }
    });
  }

  /**--------------------------------------------------------------
   * string --> getCentersOfDoctorBasicHealthZone()
   * --------------------------------------------------------------
   * Gets all the centers of a basic health zones that the 
   * doctor can access
   --------------------------------------------------------------*/
  getCentersOfDoctorBasicHealthZone(id: string): void{
    const {selectedPatient} = this.myForm.value;

    this.centersService.getCenterByBasicHealthZone(id).subscribe((centers: Center[]) => {
      centers.forEach(center => {
        this.centers.push(center);
      });

      this.setCenterAfterDropdownUpdate(selectedPatient);
    });
  }

  /**--------------------------------------------------------------
   * string --> getDoctorCenters()
   * --------------------------------------------------------------
   * Gain all basic health zones from the medic
   --------------------------------------------------------------*/
  getDoctorCenters(): void{
    const id = localStorage.getItem('id');

    this.basicHealthZonesSevice.getBasicHealthZonesByDoctor(id!).subscribe((basicHealthZones: BasicHealthZone[]) => {
      this.centers = [];

      basicHealthZones.forEach(zone => {
        this.getCentersOfDoctorBasicHealthZone(zone.id!.toString());
      });
    });
  }

  /**--------------------------------------------------------------
   * getDoctorsData()
   * --------------------------------------------------------------
   * Get the current doctor information
   --------------------------------------------------------------*/
  getDoctorsData(): void{
    this.doctorsService.getDoctorInfor().subscribe((doctor: Doctor | null) => {
      this.currentDoctor = doctor!;

      this.myForm.patchValue({
        selectedDoctor: doctor?.showData
      });
    });
  }
}
