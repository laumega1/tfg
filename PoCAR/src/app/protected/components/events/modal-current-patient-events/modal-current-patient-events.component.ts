import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Message, MessageService } from 'primeng/api';
import { BasicHealthZone, Center, Event, Patient, PostResponse, PutResponse } from 'src/app/protected/interfaces/interfaces';
import { BasicHealthZonesService } from 'src/app/protected/services/basic-healt-zones.service';
import { CentersService } from 'src/app/protected/services/centers.service';
import { EventService } from 'src/app/protected/services/event.service';

@Component({
  selector: 'app-modal-current-patient-events',
  templateUrl: './modal-current-patient-events.component.html',
  styleUrls: ['./modal-current-patient-events.component.scss']
})
export class ModalCurrentPatientEventsComponent implements OnInit {
  @Input() resetModalValues: boolean = false;

  @Input() displayModalEvent: boolean = false;
  @Output() displayModalEventChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() dialogMode: string = "";
  @Output() dialogModeChange: EventEmitter<string> = new EventEmitter<string>();

  @Input() dialogHeader: string = "";
  @Input() buttonSummitTitle: string = "";

  @Input() patient: Patient = null!;
  
  centers: Center[] = [];

  @Input() currentEvent: Event = null!;

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();

  correctUpdate: Message = {severity:'success', summary:'Éxito', detail:'El evento ha sido modificado correctamente.'};
  errorUpdate: Message = {severity:'error', summary:'Error', detail:'No se ha podido editar el evento.'};
  correctPost: Message = {severity:'success', summary:'Éxito', detail:'El nuevo evento ha sido creado correctamente.'};
  errorPost: Message = {severity:'error', summary:'Error', detail:'No se ha podido crear un nuevo evento.'};

  myForm: FormGroup = this.fb.group({
    selectedPatient: [null, [Validators.required]],
    selectedCenter: [null, [Validators.required]],
    selectedDoctor: [null, [Validators.required]],
    description: ['', [Validators.required]],
    primaryColor: ['#c9ff4d', [Validators.required]],
    secundaryColor: ['#ddfcb8', [Validators.required]],
    startDate: ['', [Validators.required]],
    endDate: ['', [Validators.required]]
  });

  constructor(
    private fb: FormBuilder,
    private eventService: EventService,
    private messageService: MessageService,
    private centersService: CentersService,
    private basicHealthZonesSevice: BasicHealthZonesService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    if(changes.dialogMode && changes.displayModalEvent.currentValue == true){
      this.getDoctorCenters();

      if(changes.dialogMode.currentValue == 'put'){
        this.changeDialogInputsEdit();
      }else{
        this.resetFormToInitValues();
      }
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- AUXILIAR ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * editCurrentEvent()
   * --------------------------------------------------------------
   * Modifies the selected event taking into account the new 
   * parameters of the form
   --------------------------------------------------------------*/
  editCurrentEvent(): void{
    const { selectedCenter, description, primaryColor, secundaryColor, startDate, endDate} = this.myForm.value;

    this.currentEvent.idCenter = selectedCenter.id;
    this.currentEvent.idPatient = this.patient.id;
    this.currentEvent.description = description;
    this.currentEvent.primaryColor = primaryColor;
    this.currentEvent.secundaryColor = secundaryColor;
    this.currentEvent.startDate = startDate;
    this.currentEvent.endDate = endDate;
  }

  /**--------------------------------------------------------------
   * createEvent()
   * --------------------------------------------------------------
   * Create a new event taking into account the new parameters of 
   * the form
   --------------------------------------------------------------*/
  createEvent(): Event{
    const {selectedCenter, description, primaryColor, secundaryColor, startDate, endDate} = this.myForm.value;

    const newEvent: Event = {
      idDoctor: this.patient.idDoctor,
      idCenter: selectedCenter.id,
      idPatient: this.patient.id,
      description: description,
      primaryColor: primaryColor,
      secundaryColor: secundaryColor,
      startDate: startDate,
      endDate: endDate
    }

    return newEvent;
  }

  /**--------------------------------------------------------------
   * resetFormToInitValues()
   * --------------------------------------------------------------
   * Resets all the values ​​of the form to the initial value. This 
   * is used when you want to create a new event for a certain 
   * patient
   --------------------------------------------------------------*/
  resetFormToInitValues(): void{
    this.myForm.reset();

    this.myForm.patchValue({
      selectedPatient: this.patient.showData,
      primaryColor: '#c9ff4d',
      secundaryColor: '#ddfcb8',
      selectedDoctor: this.patient.doctor?.showData
    });
  }

  /**--------------------------------------------------------------
   * Message --> showAlert()
   * --------------------------------------------------------------
   * Display alert message in popup
   --------------------------------------------------------------*/
  showAlert(alert: Message): void{
    if(this.messageService){
      this.messageService.clear();
    }
    this.messageService.add(alert);
  }

   /**--------------------------------------------------------------
   * Patient --> setCenterAfterDropdownUpdate()
   * --------------------------------------------------------------
   * When a patient is selected, the center dropdown is set, his by 
   * default. It is important to run a little sleep, otherwise the 
   * selected item is not shown, since it is selected before the 
   * total list of centers is available.
   --------------------------------------------------------------*/
  async setCenterAfterDropdownUpdate(patient: Patient) {
    await this.sleep(1000);

    if(this.dialogMode == "put"){
      this.myForm.patchValue({
        selectedCenter: this.centers.find(x => x.id == this.currentEvent.idCenter)
      });
    }else{
      this.myForm.patchValue({
        selectedCenter: this.centers.find(x => x.id == patient.center!.id)
      });
    }
  }

  /**--------------------------------------------------------------
   * sleep()
   * --------------------------------------------------------------
   * Await n milliseconds
   --------------------------------------------------------------*/
  sleep(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- MODALS ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * closeModal()
   * --------------------------------------------------------------
   * Closes the modal and casts it to be updated from the 
   * component that contains this
   --------------------------------------------------------------*/
  closeModal(): void{
    this.displayModalEvent = false;
    this.displayModalEventChange.emit(this.displayModalEvent);

    this.dialogMode = "";
    this.dialogModeChange.emit(this.dialogMode);
  }

  /**--------------------------------------------------------------
   * changeDialogInputsEdit()
   * --------------------------------------------------------------
   * When you want to edit an event, you get all the values ​​in the 
   * corresponding fields
   --------------------------------------------------------------*/
  changeDialogInputsEdit(): void{
    this.myForm.patchValue({
      selectedCenter: this.patient.center,
      selectedDoctor: this.patient.doctor?.showData,
      selectedPatient: this.patient.showData,
      description: this.currentEvent.description,
      primaryColor: this.currentEvent.primaryColor,
      secundaryColor: this.currentEvent.secundaryColor,
      startDate: new Date(this.currentEvent.startDate),
      endDate: new Date(this.currentEvent.endDate)
    });
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * createNewEvent()
   * --------------------------------------------------------------
   * When pressing the create button, the new event is created and 
   * inserted both in the database and in the list
   --------------------------------------------------------------*/
  createNewEvent(): void{
    const newEvent = this.createEvent();

    this.eventService.postEvent(newEvent).subscribe( (resp: PostResponse) => {
      if(resp.status == 200){
        this.showAlert(this.correctPost);
        
        newEvent.id = resp.id;
        this.updateData.emit(newEvent);
      }else{
        this.showAlert(this.errorPost);
      }
    });
  }

  /**--------------------------------------------------------------
   * editEvent()
   * --------------------------------------------------------------
   * When pressing the edit button, the current event is modified 
   * and inserted both in the database and in the list
   --------------------------------------------------------------*/
  editEvent(): void{
    this.editCurrentEvent();

    this.eventService.putEvent(this.currentEvent).subscribe( (resp: PutResponse) => {
      if(resp.status == 200){
        this.showAlert(this.correctUpdate);
      }else{
        this.showAlert(this.errorUpdate);
      }
    });
  }

  /**--------------------------------------------------------------
   * string --> getCentersOfDoctorBasicHealthZone()
   * --------------------------------------------------------------
   * Gets all the centers of a basic health zones that the 
   * doctor can access
   --------------------------------------------------------------*/
  getCentersOfDoctorBasicHealthZone(id: string): void{
    this.centersService.getCenterByBasicHealthZone(id).subscribe((centers: Center[]) => {
      centers.forEach(center => {
        this.centers.push(center);
      });

      this.setCenterAfterDropdownUpdate(this.patient);
    });
  }

  /**--------------------------------------------------------------
   * string --> getDoctorCenters()
   * --------------------------------------------------------------
   * Gain all basic health zones from the medic
   --------------------------------------------------------------*/
  getDoctorCenters(): void{
    const id = localStorage.getItem('id');

    this.basicHealthZonesSevice.getBasicHealthZonesByDoctor(id!).subscribe((basicHealthZones: BasicHealthZone[]) => {
      this.centers = [];

      basicHealthZones.forEach(zone => {
        this.getCentersOfDoctorBasicHealthZone(zone.id!.toString());
      });
    });
  }

}
