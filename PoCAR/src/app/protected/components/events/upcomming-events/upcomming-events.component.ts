import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { EventService } from 'src/app/protected/services/event.service';
import { PatientService } from 'src/app/protected/services/patient.service';
import { EventResume, Patient } from '../../../interfaces/interfaces';
import { Event } from '../../../interfaces/interfaces';
import { Day } from '../../../interfaces/interfaces';
import { addDays, subDays } from 'date-fns';

@Component({
  selector: 'app-upcomming-events',
  templateUrl: './upcomming-events.component.html',
  styleUrls: ['./upcomming-events.component.scss'],
})
export class UpcommingEventsComponent implements OnInit {
  patients: Patient[] = [];

  dialogHeader: string = '';
  buttonSummitTitle: string = '';
  dialogMode: string = '';

  displayModalEvent: boolean = false;
  activeDayIsOpen: boolean = true;
  resetModalValues: boolean = false;

  days: Day[] = [];
  dayEvents: EventResume[] = [];

  events: Event[] = [];

  currentDay: number = 0;

  weekDays: Array<string> = ['Dom', 'Lun', 'Mar', 'Mié', 'Jue', 'Vier', 'Sáb'];
  months: Array<string> = [
    'Enero',
    'Febero',
    'Marzo',
    'Abril',
    'Mayo',
    'Junio',
    'Julio',
    'Agosto',
    'Septiembre',
    'Octube',
    'Noviembre',
    'Diciembre',
  ];

  constructor(
    private cRef: ChangeDetectorRef,
    private eventService: EventService,
    private patientsService: PatientService
  ) {}

  ngOnInit(): void {
    this.getWeek();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- WEEK ----------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getWeek()
   * --------------------------------------------------------------
   * Get the current week. It is very important to keep in mind 
   * that the first day is Sunday
   --------------------------------------------------------------*/
  getWeek(): void {
    const now = new Date();

    var firstDay = new Date(now.setDate(now.getUTCDate() - now.getDay()));
    var lastDay = new Date(now.setDate(now.getUTCDate() - now.getDay() + 6));

    this.currentDay = new Date().getUTCDate();

    var count = 0;

    for (let i = 0; i < 7; i++) {
      var current = new Date(now.setDate(now.getDate() - now.getDay() + count));

      const day: Day = {
        value: current.getUTCDate(),
        text: this.weekDays[current.getUTCDay()],
        isCurrentDay:
          current.getUTCDate() == new Date().getUTCDate() ? true : false,
        month: this.months[current.getMonth()],
        year: current.getFullYear(),
      };

      this.days.push(day);

      count++;
    }

    this.getSevenDaysEvents(
      this.convertDateToDayMonthYear(subDays(firstDay, 1)),
      this.convertDateToDayMonthYear(addDays(lastDay, 1))
    );
  }

  /**--------------------------------------------------------------
   * Day --> changeCurrentDay()
   * --------------------------------------------------------------
   * In the top bar, when you select one of the days, you switch 
   * to that day and the events are filtered so that only those 
   * days appear
   --------------------------------------------------------------*/
  changeCurrentDay(day: Day): void {
    this.days.forEach((element) => {
      if (element != day) {
        element.isCurrentDay = false;
      } else {
        element.isCurrentDay = true;

        this.searchEventDays(day);
      }
    });
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MODALS --------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * string, string, string --> showEventDialog()
   * --------------------------------------------------------------
   * Auxiliary function that assigns the necessary parameters to be 
   * able to open and use the modal correctly
   --------------------------------------------------------------*/
  showEventDialog(title: string, button: string, type: string): void {
    this.dialogHeader = title;
    this.buttonSummitTitle = button;
    this.dialogMode = type;

    this.resetModalValues = true;

    this.displayModalEvent = true;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- AUXILIAR ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * detectCalendarChanges()
   * --------------------------------------------------------------
   * Force the calendar to update
   --------------------------------------------------------------*/
  detectCalendarChanges(): void {
    this.events = [...this.events];
    this.cRef.detectChanges();

    this.searchEventDays(this.days.find((x) => x.isCurrentDay == true)!);
  }

  /**--------------------------------------------------------------
   * Date --> convertDateToDayMonthYear()
   * --------------------------------------------------------------
   * Converts an object of type Date into a string with the day, 
   * month and year
   --------------------------------------------------------------*/
  convertDateToDayMonthYear(date: Date): string {
    var month = date.getUTCMonth() + 1;
    var day = date.getUTCDate();
    var year = date.getUTCFullYear();

    return year + '-' + month + '-' + day;
  }

  /**--------------------------------------------------------------
   * Day --> searchEventDays()
   * --------------------------------------------------------------
   * Search for events for a specific day. This function is called 
   * when a day is selected in the top bar
   --------------------------------------------------------------*/
  searchEventDays(day: Day): void {
    this.dayEvents = [];

    this.events.forEach((event) => {
      if (new Date(event.startDate).getUTCDate() == day.value) {
        this.dayEvents.push(
          this.generateEventResume(
            event,
            this.patients.find((x) => x.id == event.idPatient)!
          )
        );
      }
    });
  }

  /**--------------------------------------------------------------
   * Event, Patient --> generateEventResume() --> EventResume
   * --------------------------------------------------------------
   * Generates a summary of the selected event. This object is 
   * used to display the data on the screen
   --------------------------------------------------------------*/
  generateEventResume(event: Event, patient: Patient): EventResume {
    var startDate = new Date(event.startDate);

    const eventResume: EventResume = {
      name: patient.name,
      surname: patient.surname,
      age: patient.age,
      resume: event.description,
      time: {
        hours: startDate.getHours(),
        minutes: startDate.getMinutes(),
      },
      month: startDate.getUTCMonth(),
      day: startDate.getUTCDate(),
      year: startDate.getFullYear(),
      id: event.id!,
    };

    return eventResume;
  }

  /**--------------------------------------------------------------
   * EventResume --> isTimeOff() --> boolean
   * --------------------------------------------------------------
   * Check which events have already happened and disable them, 
   * leave the others normal
   --------------------------------------------------------------*/
  isTimeOff(event: EventResume): boolean {
    const now = new Date();

    if (this.currentDay == event.day) {
      if (
        event.time.hours < now.getHours() ||
        (event.time.hours == now.getHours() &&
          event.time.minutes < now.getMinutes())
      ) {
        return true;
      }
    } else if (
      this.currentDay > event.day &&
      now.getUTCMonth() == event.month
    ) {
      return true;
    } else if (now.getUTCMonth() > event.month) {
      return true;
    }

    return false;
  }

  /**--------------------------------------------------------------
   * getSelectedDayDate() --> Date
   * --------------------------------------------------------------
   * Gets the date of the selected day
   --------------------------------------------------------------*/
  getSelectedDayDate(): Date {
    const day = this.days.find((x) => x.isCurrentDay)!;
    const actualTime = new Date().getHours() + ':' + new Date().getMinutes();
    return new Date(
      day.year + '-' + day.month + '-' + day.value + ' ' + actualTime
    );
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getAllDoctorBasicHealthZonePatients()
   * --------------------------------------------------------------
   * Obtains all patients from the basic health zones to which the 
   * doctor belongs
   --------------------------------------------------------------*/
  getAllDoctorBasicHealthZonePatients(): void {
    this.patientsService
      .getDoctorPatients(null)
      .subscribe((patients: Patient[]) => {
        this.patients = patients;

        this.searchEventDays(this.days.find((x) => x.isCurrentDay)!);
      });
  }

  /**--------------------------------------------------------------
   * string, string --> getSevenDaysEvents()
   * --------------------------------------------------------------
   * Get the events of the 7 days of the week
   --------------------------------------------------------------*/
  getSevenDaysEvents(start: string, end: string): void {
    this.eventService.getEventsFromTo(start, end).subscribe((resp: Event[]) => {
      this.events = resp;

      this.getAllDoctorBasicHealthZonePatients();
    });
  }
}
