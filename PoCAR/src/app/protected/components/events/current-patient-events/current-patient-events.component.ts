import { Component, Input, OnInit, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmationService, Message, MessageService } from 'primeng/api';
import { DeleteResponse, Patient } from 'src/app/protected/interfaces/interfaces';
import { EventService } from 'src/app/protected/services/event.service';
import { Event } from '../../../interfaces/interfaces';

@Component({
  selector: 'app-current-patient-events',
  templateUrl: './current-patient-events.component.html',
  styleUrls: ['./current-patient-events.component.scss'],
  providers: [
    MessageService
  ]
})
export class CurrentPatientEventsComponent implements OnInit {
  dialogHeader: string = "";
  buttonSummitTitle: string = "";
  dialogMode: string = "";

  displayModalEvent: boolean = false;
  resetModalValues: boolean = false;

  patientEvents: Event[] = [];
  savedPatientEvents: Event[] = [];
  currentEvent: Event = null!;

  @Input() patient: Patient = null!;

  myForm: FormGroup = this.fb.group({
    eventsSearch: ['', [Validators.required]]
  });

  correctDelete: Message = {key: 'currentPatientEventKey', severity:'success', summary:'Éxito', detail:'El evento ha sido eliminado correctamente.'};
  errorDelete: Message = {key: 'currentPatientEventKey', severity:'error', summary:'Error', detail:'No se ha podido eliminar el evento.'};

  weekDays: Array<string> = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
  months: Array<string> = ['Enero', 'Febero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

  constructor(
    private eventService: EventService,
    private fb: FormBuilder,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.subscribeChangesFilter();
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.patient && changes.patient.currentValue){
      this.patient = changes.patient.currentValue;
      this.getAllPatientEvents();
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- AUXILIAR ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------
  
  /**--------------------------------------------------------------
   * Event --> detectCalendarChanges()
   * --------------------------------------------------------------
   * Detect changes in the calendar, adding the new event
   --------------------------------------------------------------*/
  detectCalendarChanges(event: Event): void{
    this.savedPatientEvents.push(event);

    this.patientEvents = this.savedPatientEvents;
  }

  /**--------------------------------------------------------------
   * subscribeChangesFilter()
   * --------------------------------------------------------------
   * Wait for changes in the browser, so that the list of patient 
   * events is updated in real time
   --------------------------------------------------------------*/
  subscribeChangesFilter(): void{
    this.myForm.get('eventsSearch')?.valueChanges.subscribe((value: string) => {
      this.filterEvents(value.toLowerCase());
    });
  }

  /**--------------------------------------------------------------
   * string --> filterEvents()
   * --------------------------------------------------------------
   * Filter events based on the text entered in the search engine
   --------------------------------------------------------------*/
  filterEvents(newInfo: string): void{
    this.patientEvents = this.savedPatientEvents.filter(x => (
      this.weekDays[(new Date(x.startDate)).getDay()].toLowerCase().includes(newInfo) ||
      (new Date(x.startDate).getUTCDate()).toString().includes(newInfo) ||
      this.months[(new Date(x.startDate)).getMonth()].toLowerCase().includes(newInfo) ||
      (new Date(x.startDate).getFullYear()).toString().includes(newInfo) ||
      (new Date(x.startDate).getHours()+":"+new Date(x.startDate).getMinutes()).includes(newInfo) ||
      x.description.toLowerCase().includes(newInfo)
    ));
  }

  /**--------------------------------------------------------------
   * Event --> getDataFromDate() --> string
   * --------------------------------------------------------------
   * Gets the date of the event
   --------------------------------------------------------------*/
  getDataFromDate(event: Event): string{
    var startDate = new Date(event.startDate);

    return this.weekDays[startDate.getDay()] + " " + startDate.getUTCDate() + " de " + this.months[startDate.getMonth()] + " de " + startDate.getFullYear();
  }

  /**--------------------------------------------------------------
   * Event --> getDataFromDate() --> string
   * --------------------------------------------------------------
   * Get the hour and minute of the event
   --------------------------------------------------------------*/
  getTimeFromDate(event: Event): string{
    var startDate = new Date(event.startDate);

    return startDate.getHours() + ":" + startDate.getMinutes() + " H";
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //---------------------------------------------- MODALS AND CONFIRMATIONS ---------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * Event --> confirmDelete()
   * --------------------------------------------------------------
   * The modal that asks for confirmation before deleting an 
   * event is opened, since this action cannot be undone
   --------------------------------------------------------------*/
  confirmDelete(event: Event): void {
    this.confirmationService.confirm({
      message: '¿Está seguro de borrar este evento? La acción será irreversible.',
      accept: () => {
        this.deleteEvent(event);
      },
      acceptLabel: 'Si',
      rejectLabel: 'No',
      rejectButtonStyleClass: 'p-button cancelButton'
    });
  }

  /**--------------------------------------------------------------
   * Event --> showEditPanel()
   * --------------------------------------------------------------
   * Open the panel to modify an event
   --------------------------------------------------------------*/
  showEditPanel(event: Event): void{
    this.currentEvent = event;

    this.showEventDialog("Editar evento", "Modificar", 'put');
  }

  /**--------------------------------------------------------------
   * string, string, string --> showEventDialog()
   * --------------------------------------------------------------
   * Auxiliary function that assigns the necessary parameters to be 
   * able to open and use the modal correctly
   --------------------------------------------------------------*/
  showEventDialog(title: string, button: string, type: string): void{
    this.dialogHeader = title;
    this.buttonSummitTitle = button;
    this.dialogMode = type;

    this.resetModalValues = true;

    this.displayModalEvent = true;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getAllPatientEvents()
   * --------------------------------------------------------------
   * Obtains all the events of a patient through the request to the 
   * database
   --------------------------------------------------------------*/
  getAllPatientEvents(): void{
    this.savedPatientEvents = [];

    this.eventService.getPatientEvents(this.patient.id).subscribe( (resp: Event[]) => {
      this.savedPatientEvents = resp;

      this.patientEvents = this.savedPatientEvents;
    });
  }

  /**--------------------------------------------------------------
   * Event --> deleteEvent()
   * --------------------------------------------------------------
   * Deletes a patient event from the database after double 
   * confirmation
   --------------------------------------------------------------*/
  deleteEvent(event: Event): void{
    this.eventService.deleteEvent(event.id!).subscribe( (resp: DeleteResponse) => {
      if(resp.status == 200){
        this.savedPatientEvents = this.savedPatientEvents.filter((x) => x.id != event.id);
        this.patientEvents = this.savedPatientEvents;

        this.messageService.add(this.correctDelete);
      }else{
        this.messageService.add(this.errorDelete);
      }
    });
  }

}
