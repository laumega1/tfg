import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Event, Patient } from 'src/app/protected/interfaces/interfaces';

@Component({
  selector: 'app-patient-resume',
  templateUrl: './patient-resume.component.html',
  styleUrls: ['./patient-resume.component.scss']
})
export class PatientResumeComponent implements OnInit {
  @Input() patient: Patient = null!;
  @Input() event: Event = null!;
  @Input() viewType: string = "";

  @Output() closeConsultation: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  /**--------------------------------------------------------------
   * closeConsultationCall()
   * --------------------------------------------------------------
   * Notificate consultation calls is end
   --------------------------------------------------------------*/
  closeConsultationCall(): void{
    this.closeConsultation.emit();
  }

}
