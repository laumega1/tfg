import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Message, MessageService } from 'primeng/api';
import { Center, Patient } from 'src/app/protected/interfaces/interfaces';
import { PatientService } from 'src/app/protected/services/patient.service';
import { PutResponse, PostResponse, BasicHealthZone } from '../../../interfaces/interfaces';
import { DoctorsService } from 'src/app/protected/services/doctors.service';
import { CentersService } from 'src/app/protected/services/centers.service';
import { Doctor } from 'src/app/shared/interfaces/interfaces';

@Component({
  selector: 'app-modal-patients-list',
  templateUrl: './modal-patients-list.component.html',
  styleUrls: ['./modal-patients-list.component.scss'],
  providers: [
    MessageService
  ]
})
export class ModalPatientsListComponent implements OnInit {
  @Input() dialogHeader: string = "";
  @Input() buttonSummitTitle: string = "";

  @Input() displayModalPatient: boolean = false;
  @Output() displayModalPatientChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() dialogMode: string = "";
  @Output() dialogModeChange: EventEmitter<string> = new EventEmitter<string>();

  @Input() basicHealthZones: BasicHealthZone[] = [];

  isBasicHealthZoneSelected = false;

  centers: Center[] = [];
  doctors: Doctor[] = [];
  
  @Input() currentPatient: Patient = null!;

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();
  
  myForm: FormGroup = this.fb.group({
    patientName: ['', [Validators.required]],
    patientSurname: ['', [Validators.required]],
    patientAge: ['', [Validators.required]],
    patientSIP: ['', [Validators.required]],
    selectedBasicHealthZone: [null , [Validators.required]],
    selectedCenter: [null, [Validators.required]],
    selectedDoctor: [null, [Validators.required]]
  });

  correctUpdate: Message = {severity:'success', summary:'Éxito', detail:'El paciente ha sido modificado correctamente.'};
  errorUpdate: Message = {severity:'error', summary:'Error', detail:'No se ha podido editar el paciente.'};
  correctPost: Message = {severity:'success', summary:'Éxito', detail:'El nuevo paciente ha sido creado correctamente.'};
  errorPost: Message = {severity:'error', summary:'Error', detail:'No se ha podido crear un paciente nuevo.'};

  constructor(
    private fb: FormBuilder,
    private patientsService: PatientService,
    private messageService: MessageService,
    private doctorsService: DoctorsService,
    private centersService: CentersService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.displayModalPatient && changes.displayModalPatient.currentValue == true){
      this.isBasicHealthZoneSelected = false;
      this.subscribeChanges();

      if(this.dialogMode == 'put'){
        this.changeDialogInputsEdit();
      }else{
        this.resetFormToInitValues();
      }
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- FORMS -------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * resetFormToInitValues()
   * --------------------------------------------------------------
   * Reset/empty the form fields. This is useful when you want to 
   * create a new object
   --------------------------------------------------------------*/
  resetFormToInitValues(): void{
    this.myForm.reset();

    this.currentPatient = {
      id: 0,
      name: "",
      surname: "",
      age: 0,
      sip: "",
      idDoctor: 0,
      idCenter: 0
    }
  }

  /**--------------------------------------------------------------
   * changeDialogInputsEdit()
   * --------------------------------------------------------------
   * Insert the patient data in the form. Subsequently, all basic 
   * health areas and their corresponding centers are requested
   --------------------------------------------------------------*/
  changeDialogInputsEdit(): void{
    this.currentPatient.doctor!.isOnline = false;

    this.currentPatient.doctor ? this.doctors.push(this.currentPatient.doctor) : [];
    this.currentPatient.center ? this.centers.push(this.currentPatient.center) : [];

    this.myForm.patchValue({
      patientName: this.currentPatient.name,
      patientSurname: this.currentPatient.surname,
      patientAge: this.currentPatient.age,
      patientSIP: this.currentPatient.sip,
      selectedBasicHealthZone: this.currentPatient.basicHealthZone,
      selectedCenter: this.currentPatient.center,
      selectedDoctor: this.currentPatient.doctor
    });

    this.getDoctorsOfDoctorBasicHealthZone(this.currentPatient.basicHealthZone?.id!);
    this.getCentersOfDoctorBasicHealthZone(this.currentPatient.basicHealthZone?.id!);
  }

  /**--------------------------------------------------------------
   * subscribeChanges()
   * --------------------------------------------------------------
   * Subscription to the changes of the selector of the basic 
   * health zones. When changing, it re-requests the doctors and 
   * centers of said areas in order to update the list
   --------------------------------------------------------------*/
  subscribeChanges(): void{
    this.myForm.get('selectedBasicHealthZone')?.valueChanges.subscribe(value => {
      if(value != null){
        this.isBasicHealthZoneSelected = true;
        
        this.getDoctorsOfDoctorBasicHealthZone(value.id);
        this.getCentersOfDoctorBasicHealthZone(value.id);
      }else{
        this.isBasicHealthZoneSelected = false;
      }
    });
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- MODALS ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * closeModal()
   * --------------------------------------------------------------
   * Close modal and notificate the parent component
   --------------------------------------------------------------*/
  closeModal(): void{
    this.displayModalPatient = false;
    this.displayModalPatientChange.emit(this.displayModalPatient);

    this.dialogMode = "";
    this.dialogModeChange.emit(this.dialogMode);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- AUXILIAR ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * Message --> showAlert()
   * --------------------------------------------------------------
   * Display popup alert
   --------------------------------------------------------------*/
  showAlert(alert: Message): void{
    if(this.messageService){
      this.messageService.clear();
    }
    this.messageService.add(alert);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * createNewPatientInformation()
   * --------------------------------------------------------------
   * A new patient in database is created with all the information 
   * collected from the form
   --------------------------------------------------------------*/
  createNewPatientInformation(): void{
    const { patientName, patientSurname, patientAge, patientSIP, selectedBasicHealthZone, selectedCenter, selectedDoctor } = this.myForm.value;

    this.currentPatient.name = patientName;
    this.currentPatient.surname = patientSurname;
    this.currentPatient.age = patientAge;
    this.currentPatient.sip = patientSIP;
    this.currentPatient.idCenter = selectedCenter.id;
    this.currentPatient.center = selectedCenter;
    this.currentPatient.basicHealthZone = selectedBasicHealthZone;
    this.currentPatient.idDoctor = selectedDoctor.id;
    this.currentPatient.doctor = selectedDoctor;

    this.patientsService.postPatient(this.currentPatient).subscribe( (resp: PostResponse) => {
      if(resp.status == 200){
        this.showAlert(this.correctPost);

        this.currentPatient.id = resp.id ? resp.id : 0;
        if(this.currentPatient.id != 0){
          this.updateData.emit(this.currentPatient);
        }
      }else{
        this.showAlert(this.errorPost);
      }
    });
  }

  /**--------------------------------------------------------------
   * updatePatientInformation()
   * --------------------------------------------------------------
   * Update a patient in database with all the information collected 
   * from the form
   --------------------------------------------------------------*/
  updatePatientInformation(): void{
    const { patientName, patientSurname, patientAge, selectedBasicHealthZone, selectedCenter, selectedDoctor } = this.myForm.value;

    if(patientName != this.currentPatient.name || patientSurname != this.currentPatient.surname || patientAge != this.currentPatient.age || selectedBasicHealthZone != this.currentPatient.basicHealthZone || selectedCenter.id != this.currentPatient.idCenter || selectedDoctor.id != this.currentPatient.idDoctor){
      this.currentPatient.name = patientName;
      this.currentPatient.surname = patientSurname;
      this.currentPatient.age = patientAge;
      this.currentPatient.idCenter = selectedCenter.id;
      this.currentPatient.center = selectedCenter;
      this.currentPatient.basicHealthZone = selectedBasicHealthZone;
      this.currentPatient.idDoctor = selectedDoctor.id;
      this.currentPatient.doctor = selectedDoctor;

      this.patientsService.putPatient(this.currentPatient).subscribe( (resp: PutResponse) => {
        if(resp.status == 200){
          this.showAlert(this.correctUpdate);
        }else{
          this.showAlert(this.errorUpdate);
        }
      });
    }
  }

  /**--------------------------------------------------------------
   * int --> getDoctorsOfDoctorBasicHealthZone()
   * --------------------------------------------------------------
   * Get doctors of a basic health zone
   --------------------------------------------------------------*/
  getDoctorsOfDoctorBasicHealthZone(idZone: number): void{
    this.doctorsService.getDoctorsByBasicHealthZone(idZone.toString()).subscribe((doctors: Doctor[]) => {
      doctors.forEach(doct => {
        doct.isOnline = false;
      });
      
      this.doctors = doctors;
    });
  }

  /**--------------------------------------------------------------
   * int --> getCentersOfDoctorBasicHealthZone()
   * --------------------------------------------------------------
   * Get centers of a basic health zone
   --------------------------------------------------------------*/
  getCentersOfDoctorBasicHealthZone(idZone: number): void{
    this.centersService.getCenterByBasicHealthZone(idZone.toString()).subscribe((centers: Center[]) => {
      this.centers = centers;
    });
  }

}
