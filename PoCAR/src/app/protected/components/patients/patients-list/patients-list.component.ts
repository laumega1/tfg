import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BasicHealthZone, Patient } from 'src/app/protected/interfaces/interfaces';
import { PatientService } from 'src/app/protected/services/patient.service';
import { BasicHealthZonesService } from 'src/app/protected/services/basic-healt-zones.service';

@Component({
  selector: 'app-patients-list',
  templateUrl: './patients-list.component.html',
  styleUrls: ['./patients-list.component.scss']
})
export class PatientsListComponent implements OnInit {
  savedPatients: Patient[] = [];
  patients: Patient[] = [];

  basicHealthZones: BasicHealthZone[] = [];

  currentPatient: Patient = null!;

  dialogHeader: string = "";
  buttonSummitTitle: string = "";
  dialogMode: string = "post";

  displayModalPatient: boolean = false;

  @Output() changeCurrentPatientEmit: EventEmitter<Patient> = new EventEmitter<Patient>();

  orderAlphabeticClass: string = "fa-solid fa-arrow-up-a-z";

  myForm: FormGroup = this.fb.group({
    patientsSearch: ['', [Validators.required]]
  });

  constructor(
    private patientsService: PatientService,
    private basicHealthZonesService: BasicHealthZonesService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.getNecessaryData();

    this.subscribeChangesFilter();
  }
  
  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- AUXILIAR ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getNecessaryData()
   * --------------------------------------------------------------
   * Get all doctors patients and filter it by the doctor basic
   * health zones
   --------------------------------------------------------------*/
  getNecessaryData(): void{
    this.getPatients();
    this.getDoctorBasicHealtZones();
  }

  /**--------------------------------------------------------------
   * Patient --> detectPatientsChanges()
   * --------------------------------------------------------------
   * When a change is detected, the input is emptied and the 
   * patient is added to the list
   --------------------------------------------------------------*/
  detectPatientsChanges(patient: Patient): void{
    this.myForm.patchValue({
      patientsSearch: ""
    });

    this.savedPatients.push(patient);
    this.patients = this.savedPatients;

    this.changeCurrentPatient(patient);
  }

  /**--------------------------------------------------------------
   * Patient --> changeCurrentPatient()
   * --------------------------------------------------------------
   * Change the current patient and send it to parent
   --------------------------------------------------------------*/
  changeCurrentPatient(patient: Patient): void{
    this.currentPatient = patient;
    this.changeCurrentPatientEmit.emit(this.currentPatient);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MODALS --------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * string, string, string, Patient --> showPatientDialog()
   * --------------------------------------------------------------
   * When clicking on edit a patient, a modal opens given certain 
   * parameters for the configuration and the selected patient 
   * that you want to edit
   --------------------------------------------------------------*/
  showPatientDialog(title: string, button: string, type: string, patient: Patient): void{
    this.dialogHeader = title;
    this.buttonSummitTitle = button;
    this.dialogMode = type;

    this.displayModalPatient = true;

    this.currentPatient = patient;
  }
  
  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- SEARCH - FILTER -------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * subscribeChangesFilter()
   * --------------------------------------------------------------
   * Detects when changes occur in the search engine that helps us 
   * filter the list of patients
   --------------------------------------------------------------*/
  subscribeChangesFilter(): void{
    this.myForm.get('patientsSearch')?.valueChanges.subscribe((value: string) => {
      this.filterPatients(value.toLowerCase());
    });
  }

  /**--------------------------------------------------------------
   * string --> filterPatients()
   * --------------------------------------------------------------
   * Filter the list of patients taking into account numerous 
   * things such as name, surname, doctor...
   --------------------------------------------------------------*/
  filterPatients(newInfo: string): void{
    this.patients = this.savedPatients.filter(x => (
      x.name.toLowerCase().includes(newInfo) || 
      x.surname.toLowerCase().includes(newInfo) ||
      x.age.toString().includes(newInfo) ||
      x.sip.toLowerCase().includes(newInfo) ||
      x.basicHealthZone?.name.toLowerCase().includes(newInfo) ||
      x.center?.name.toLowerCase().includes(newInfo) ||
      x.doctor?.name.toLowerCase().includes(newInfo) ||
      x.doctor?.surname.toLowerCase().includes(newInfo)
    ));
  }

  /**--------------------------------------------------------------
   * orderPatientsByAlphabetic()
   * --------------------------------------------------------------
   * By clicking on the sort button, it allows you to sort the 
   * patients in alphabetical order or vice versa
   --------------------------------------------------------------*/
  orderPatientsByAlphabetic(): void{
    switch(this.orderAlphabeticClass){
      case "fa-solid fa-arrow-up-a-z":
        this.patients = this.savedPatients.sort((a, b) => (a.name+" "+a.surname).localeCompare(b.name+" "+b.surname));
        this.orderAlphabeticClass = "fa-solid fa-arrow-up-z-a";
        break;
      case "fa-solid fa-arrow-up-z-a":
        this.patients = this.savedPatients.sort((a, b) => (b.name+" "+b.surname).localeCompare(a.name+" "+a.surname));
        this.orderAlphabeticClass = "fa-solid fa-arrow-up-a-z";
        break;
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getPatients()
   * --------------------------------------------------------------
   * Get all patients in the database
   --------------------------------------------------------------*/
  getPatients(): void{
    this.patientsService.getDoctorPatients(null).subscribe((patients: Patient[]) => {
      this.savedPatients = patients;

      this.patients = this.savedPatients;
    });
  }

  /**--------------------------------------------------------------
   * getDoctorBasicHealtZones()
   * --------------------------------------------------------------
   * Gain all basic health zones from the medic
   --------------------------------------------------------------*/
  getDoctorBasicHealtZones(): void{
    const idDoctor = localStorage.getItem('id');

    this.basicHealthZonesService.getBasicHealthZonesByDoctor(idDoctor!).subscribe((basicHealthZones: BasicHealthZone[]) => {
      this.basicHealthZones = basicHealthZones;
    });
  }
}
