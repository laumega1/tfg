import { ChangeDetectorRef, Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { isSameDay, isSameMonth } from 'date-fns';
import { Subject } from 'rxjs';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarEventTitleFormatter, CalendarView } from 'angular-calendar';
import { Message, MessageService } from 'primeng/api';
import { Event, Patient, PutResponse, DeleteResponse } from '../../interfaces/interfaces';
import { PatientService } from '../../services/patient.service';
import { EventService } from '../../services/event.service';
import { CustomEventTitleFormatter } from '../../providers/custom-event-title-formatter.provider';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-full-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './full-calendar.component.html',
  styleUrls: ['./full-calendar.component.scss'],
  providers: [
    {
      provide: CalendarEventTitleFormatter,
      useClass: CustomEventTitleFormatter,
    },
    MessageService
  ]
})
export class FullCalendarComponent implements OnInit {
  patients: Patient[] = [];

  displayModalEvent: boolean = false;
  activeDayIsOpen: boolean = true;
  resetModalValues: boolean = false;

  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  refresh = new Subject<void>();
  
  events: CalendarEvent[] = [];
  currentEvent: CalendarEvent = null!;

  dialogHeader: string = "";
  buttonSummitTitle: string = "";
  dialogMode: string = "";

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa-solid fa-trash"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.currentEvent = event;
        this.confirmDelete();
      }
    },
    {
      label: '<i class="fa-solid fa-pen"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.showEditPanel(event);
      }
    }
  ];
  
  correctUpdateTime: Message = {key: 'calendarKey', severity:'success', summary:'Éxito', detail:'El evento ha sido modificado correctamente.'};
  errorUpdateTime: Message = {key: 'calendarKey', severity:'error', summary:'Error', detail:'No se ha podido editar el evento.'};
  correctDelete: Message = {key: 'calendarKey', severity:'success', summary:'Éxito', detail:'El evento ha sido eliminado correctamente.'};
  errorDelete: Message = {key: 'calendarKey', severity:'error', summary:'Error', detail:'No se ha podido eliminar el evento.'};
  
  constructor(
    private cRef: ChangeDetectorRef,
    private patientsService: PatientService,
    private eventService: EventService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {}
  
  ngOnInit(): void {
    this.getStartData();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- AUXILIAR ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getStartData()
   * --------------------------------------------------------------
   * Get initial data to show calendar events
   --------------------------------------------------------------*/
  getStartData(): void{
    this.getAllDoctorBasicHealthZonePatients();
  }

  /**--------------------------------------------------------------
   * detectCalendarChanges()
   * --------------------------------------------------------------
   * Detect changes in the calendar, adding the new event
   --------------------------------------------------------------*/
  detectCalendarChanges(): void{
    this.events = [...this.events];
    this.cRef.detectChanges();
  }

  /**--------------------------------------------------------------
   * Event, Patient --> generateCalendarEvent() --> CalendarEvent
   * --------------------------------------------------------------
   * Create a calendar event given the event stored in the 
   * database and the corresponding patient
   --------------------------------------------------------------*/
  generateCalendarEvent(event: Event, patient: Patient): CalendarEvent{
    const myEvent: CalendarEvent = {
      start: new Date(event.startDate),
      end: new Date(event.endDate),
      title: event.description,
      color: {
        primary: event.primaryColor,
        secondary: event.secundaryColor
      },
      actions: this.actions,
      allDay: false,
      resizable: {
        beforeStart: true,
        afterEnd: true,
      },
      draggable: true,
      meta: {
        patient: patient,
        center: event.center,
        doctor: event.doctor,
        id: event.id
      }
    }

    return myEvent;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- CALENDAR ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * CalendarView --> setView()
   * --------------------------------------------------------------
   * Set the calendar view
   --------------------------------------------------------------*/
  setView(view: CalendarView) {
    this.view = view;
  }

  /**--------------------------------------------------------------
   * closeOpenMonthViewDay()
   * --------------------------------------------------------------
   * Set activeDay to false
   --------------------------------------------------------------*/
  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  /**--------------------------------------------------------------
   * Date, List<CalendarEvent> --> dayClicked()
   * --------------------------------------------------------------
   * Default function of the calendar, where when you click to 
   * change a day, it opens its view and therefore its events
   --------------------------------------------------------------*/
  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  /**--------------------------------------------------------------
   * CalendarEventTimesChangedEvent --> eventTimesChanged()
   * --------------------------------------------------------------
   * Change the times, for example to change the view from day to 
   * week or month
   --------------------------------------------------------------*/
  eventTimesChanged({event, newStart, newEnd}: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd!;

    this.currentEvent = event;

    this.detectCalendarChanges();

    this.editTimeEvent(event);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //---------------------------------------------- MODALS AND CONFIRMATIONS ---------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * confirmDelete()
   * --------------------------------------------------------------
   * Open a modal that asks the user to confirm if they want to 
   * delete the event since the action cannot be reversed
   --------------------------------------------------------------*/
  confirmDelete() {
    this.confirmationService.confirm({
      message: '¿Está seguro de borrar este evento? La acción será irreversible.',
      accept: () => {
        this.deleteEvent();
      },
      acceptLabel: 'Si',
      rejectLabel: 'No',
      rejectButtonStyleClass: 'p-button cancelButton'
    });
  }

  /**--------------------------------------------------------------
   * string, string, string --> showEventDialog()
   * --------------------------------------------------------------
   * Auxiliary function that assigns the necessary parameters to be 
   * able to open and use the modal correctly
   --------------------------------------------------------------*/
  showEventDialog(title: string, button: string, type: string): void{
    this.dialogHeader = title;
    this.buttonSummitTitle = button;
    this.dialogMode = type;

    this.resetModalValues = true;

    this.displayModalEvent = true;
  }

  /**--------------------------------------------------------------
   * CalendarEvent --> showEditPanel()
   * --------------------------------------------------------------
   * Open the panel to modify an event
   --------------------------------------------------------------*/
  showEditPanel(event: CalendarEvent): void{
    this.showEventDialog("Editar evento", "Modificar", 'put');

    this.currentEvent = event;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getAllDoctorBasicHealthZonePatients()
   * --------------------------------------------------------------
   * Obtains all patients from the basic health zones to which the 
   * doctor belongs
   --------------------------------------------------------------*/
  getAllDoctorBasicHealthZonePatients(): void{
    this.patientsService.getDoctorPatients(null).subscribe((patients: Patient[]) => {
      this.patients = patients;

      this.getEvents();
    })
  }

  /**--------------------------------------------------------------
   * getEvents()
   * --------------------------------------------------------------
   * Get all doctor events and add to the list to display it
   --------------------------------------------------------------*/
  getEvents(): void{
    this.eventService.getAllEvents().subscribe( (resp: Event[]) => {
      resp.forEach(event => {
        this.events.push(this.generateCalendarEvent(event, this.patients.find(x => x.id == event.idPatient)!));
      });
      this.detectCalendarChanges();
    });
  }
  
  /**--------------------------------------------------------------
   * CalendarEvent --> editTimeEvent()
   * --------------------------------------------------------------
   * Modify a calendar event. This function is called when for 
   * example an event is dragged from one day to another
   --------------------------------------------------------------*/
  editTimeEvent(event: CalendarEvent): void{
    const eventEdited: Event = {
      id: event.meta.id,
      idDoctor: event.meta.doctor.id,
      idPatient: event.meta.patient.id,
      idCenter: event.meta.center.id,
      description: event.title,
      primaryColor: event.color?.primary!,
      secundaryColor: event.color?.secondary!,
      startDate: event.start,
      endDate: event.end!
    }

    this.eventService.putEvent(eventEdited).subscribe( (resp: PutResponse) => {
      if(resp.status == 200){
        this.messageService.add(this.correctUpdateTime);
      }else{
        this.messageService.add(this.errorUpdateTime);
      }
    });
  }

  /**--------------------------------------------------------------
   * deleteEvent()
   * --------------------------------------------------------------
   * After confirming that the user wants to delete the event, it 
   * is deleted from the database
   --------------------------------------------------------------*/
  deleteEvent(): void{
    const id = this.currentEvent.meta.id;

    this.eventService.deleteEvent(id).subscribe( (resp: DeleteResponse) => {
      if(resp.status == 200){
        this.events = this.events.filter((event) => event !== this.currentEvent);
        this.detectCalendarChanges();

        this.messageService.add(this.correctDelete);
      }else{
        this.messageService.add(this.errorDelete);
      }
    });
  }
}
