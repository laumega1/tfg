import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConsultNotification, Device, MeasurementInstruction, MeasurementResult, PostResponse, Weight } from 'src/app/protected/interfaces/interfaces';
import { CoordinatorsService } from '../../../services/coordinators.service';
import { Coordinator, PulseOximeter, Temperature, BloodPreassure } from '../../../interfaces/interfaces';
import { DevicesService } from 'src/app/protected/services/devices.service';
import { mqtt } from 'aws-iot-device-sdk-v2';
import { TemperatureService } from '../../../services/temperature.service';
import { WeightService } from 'src/app/protected/services/weight.service';
import { BloodPreassureService } from 'src/app/protected/services/blood-preassure.service';
import { PulseOximeterService } from 'src/app/protected/services/pulse-oximeter.service';

@Component({
  selector: 'app-take-measures',
  templateUrl: './take-measures.component.html',
  styleUrls: ['./take-measures.component.scss']
})
export class TakeMeasuresComponent implements OnInit {

  connection: mqtt.MqttClientConnection | null = null;

  clientID: string = localStorage.getItem('id')!+"TakeMeasures";

  @Input() notification: ConsultNotification = null!;

  measuresType: string[] = ['Temperatura', 'Peso', 'Tensión', 'Pulsaciones y oxígeno'];
  measuresTypeBBDD: string[] = ['temperature', 'weight', 'bloodPreassure', 'pulseOximeter'];
  measuresTypeIconClass: string[] = ['fa-solid fa-temperature-high', 'fa-solid fa-weight-scale', 'fa-solid fa-droplet', 'fa-solid fa-heart-pulse'];
  measuresTypeDescription: string[] = ['Por favor, pulse 1 vez el botón. Cuando aparezca en la pantalla el título de: For ºC, acerqueselo a la cabeza. Vuelva a pulsar hasta notar una o varias vibraciones.', 'Por favor, subase encima de la báscula y espere hasta que los números parpadeen intermitentemente.', 'Por favor, póngase el tensiómetro en la parte superior de su brazo. Fíjese en que parte va arriba y cual va abajo. Posteriormente, pulse y espere a que se deshinche por completo.', 'Por favor, póngase el pulsioxímetro en el dedo índice y pulse el botón.'];

  currentIconClass: string = '';

  myForm: FormGroup = this.fb.group({
    measureType: ['', [Validators.required]],
    time: [new Date(), [Validators.required]],
    description: ['', [Validators.required]]
  });

  currentMeasureTake: MeasurementInstruction = null!
  measuresList: MeasurementInstruction[] = [];

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private fb: FormBuilder,
    private coordinatorService: CoordinatorsService,
    private devicesService: DevicesService,
    private temperatureService: TemperatureService,
    private weightService: WeightService,
    private bloodPreassureService: BloodPreassureService,
    private pulseOximeterService: PulseOximeterService
  ) { }

  ngOnInit(): void {
    this.subscribeToMeasureTypeChanges();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //-------------------------------------------------------- MQTT -------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * sendTakeMeasurementNotification()
   * --------------------------------------------------------------
   * It sends a message requesting to read data from a certain 
   * device both to the patient's application and to the 
   * coordinator. Then wait for the response from the coordinator
   --------------------------------------------------------------*/
  sendTakeMeasurementNotification(): void{
    this.connection?.publish("PoCAR/consultMeasuresNotification/macDevice="+this.currentMeasureTake.message.data.macPatientDevice+"-sip="+this.currentMeasureTake.message.data.sip, JSON.stringify(this.currentMeasureTake), mqtt.QoS.AtLeastOnce);
    this.connection?.publish("PoCAR/consultMeasuresNotification/coordinatorMAC="+this.currentMeasureTake.message.data.coordinator.mac, JSON.stringify(this.currentMeasureTake), mqtt.QoS.AtLeastOnce);
  
    this.subscribeTakeMeasurementResponse();
  }

  /**--------------------------------------------------------------
   * subscribeTakeMeasurementResponse()
   * --------------------------------------------------------------
   * It awaits the arrival of the response to the measurement 
   * request. In case of receiving it, check if there is data or an 
   * error has occurred. If there is data, it uploads it to the 
   * database and adds the order to the list. Just in case, there 
   * was no connection, the cancel subscription function is called, 
   * which checks that if no response has been received in a given 
   * time, it is allowed to take the data again
   --------------------------------------------------------------*/
  subscribeTakeMeasurementResponse(): void{
    var topic = "PoCAR/consultMeasuresNotificationResult/macDevice="+this.currentMeasureTake.message.data.macPatientDevice+"-sip="+this.currentMeasureTake.message.data.sip

    this.cancelSubscription(topic);
    
    this.connection?.subscribe(topic, mqtt.QoS.AtLeastOnce, (topic, payload) => {
      try {
        const decoder = new TextDecoder('utf8');
        const notification: MeasurementInstruction = JSON.parse(decoder.decode(payload));

        if(notification.status == 200){
          notification.message.data.time = new Date(notification.message.data.time);

          this.currentMeasureTake = notification;

          this.postResultToBBDD();
          if(this.currentMeasureTake != null && this.currentMeasureTake.message.error == '' && this.currentMeasureTake.message.data.measurementResult![0].value != undefined){
            this.measuresList.unshift(this.currentMeasureTake);
            this.currentMeasureTake = null!;
          }

          this.connection?.unsubscribe(topic);
        }
      } catch (error) {
        console.log(error);
      }
    });
  }
  
  /**--------------------------------------------------------------
   * string --> cancelSubscription()
   * --------------------------------------------------------------
   * If a minute and a half passes and there is no response, 
   * unsubscribe
   --------------------------------------------------------------*/
  async cancelSubscription(topic: string) {
    await this.sleep(15000);

    if(this.currentMeasureTake.message.data.measurementResult![0].value == undefined && this.currentMeasureTake.message.error == ''){
      this.connection?.unsubscribe(topic);
      this.currentMeasureTake.message.error = "No hay conexión";
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------ AUXILIAR -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * setCurrentTimeToInput()
   * --------------------------------------------------------------
   * When changing the type of sensor to read, it changes the 
   * value of the time to the current one
   --------------------------------------------------------------*/
  setCurrentTimeToInput(): void{
    this.myForm.controls['time'].setValue(new Date());
  }

  /**--------------------------------------------------------------
   * subscribeToMeasureTypeChanges()
   * --------------------------------------------------------------
   * Subscription to changes, so that when the type of measurement 
   * changes, the information field and the time field must be 
   * updated
   --------------------------------------------------------------*/
  subscribeToMeasureTypeChanges(): void{
    this.myForm.controls['measureType'].valueChanges.subscribe(value => {
      if(value != null){
        this.measuresType.forEach(element => {
          if(element == value){
            const itemPosition = this.measuresType.findIndex(x => x == element);
            this.currentIconClass = this.measuresTypeIconClass[itemPosition];
            this.myForm.controls['description'].setValue(this.measuresTypeDescription[itemPosition]);
            this.setCurrentTimeToInput();
          }
        });
      }else{
        this.currentIconClass = "";
        this.myForm.controls['description'].setValue("");
        this.setCurrentTimeToInput();
      }
   })
  }

  /**--------------------------------------------------------------
   * sendInstruction()
   * --------------------------------------------------------------
   * Create an instruction with the basic objects and then request 
   * those to obtain the missing information such as the device
   --------------------------------------------------------------*/
  sendInstruction(): void{
    const {measureType, time, description} = this.myForm.value;

    var newInstruction: MeasurementInstruction = {
      status: 200,
      message: {
        data: {
          type: measureType,
          typeBBDD: '',
          time: new Date(time),
          description: description,
          idRoom: this.notification.idRoom,
          macPatientDevice: this.notification.macDevice,
          coordinator: null!,
          sip: this.notification.sip,
          device: null!,
          iconClass: this.currentIconClass
        },
        error: ''
      }
    }

    this.getInstrucctionCoordinatorAndDevice(newInstruction);
  }

  /**--------------------------------------------------------------
   * string --> generateMeasurementMeasure()
   * --------------------------------------------------------------
   * Create an object of type MeasurementResult based on the type 
   * of measurement we want to take, since for example for 
   * temperature we only need one field but for blood pressure we 
   * need three
   --------------------------------------------------------------*/
  generateMeasurementMeasure(type: string): void{
    var measurementResult: MeasurementResult[] = [];

    switch(type){
      case 'temperature':
        measurementResult.push({
          name: "Temperatura",
          units: "ºC"
        });
        break;
      case 'weight':
        measurementResult.push({
          name: "Peso",
          units: "Kg"
        });
        break;
      case 'bloodPreassure':
        measurementResult.push({
          name: "Sistólica",
          units: "mmHg"
        });
        measurementResult.push({
          name: "Diastólica",
          units: "mmHg"
        });
        measurementResult.push({
          name: "Pulso",
          units: "ppm"
        });
        break;
      case 'pulseOximeter':
        measurementResult.push({
          name: "Oxígeno",
          units: "SpO2%"
        });
        measurementResult.push({
          name: "Pulso",
          units: "ppm%"
        });
        break;
    }

    this.currentMeasureTake.message.data.measurementResult = measurementResult;
  }

  /**--------------------------------------------------------------
   * postResultToBBDD()
   * --------------------------------------------------------------
   * It checks if, depending on the type of measurement to be 
   * taken, if all its values ​​are different from undefined and if 
   * so, it uploads them to the database
   --------------------------------------------------------------*/
  postResultToBBDD(): void{
    var data = this.currentMeasureTake.message.data;

    switch(data.typeBBDD){
      case 'temperature':
        if(data.measurementResult![0].value != undefined){
          this.createNewTemperature();
        }
        break;
      case 'weight':
        if(data.measurementResult![0].value != undefined){
          this.createNewWeight();
        }
        break;
      case 'bloodPreassure':
        if(data.measurementResult![0].value != undefined && data.measurementResult![1].value != undefined && data.measurementResult![2].value != undefined){
          this.createNewBloodPreassures();
        }
        break;
      case 'pulseOximeter':
        if(data.measurementResult![0].value != undefined && data.measurementResult![1].value != undefined){
          this.createNewPulseOximeter();
        }
        break;
    }
  }

  /**--------------------------------------------------------------
   * resendMqttToGetMeasure()
   * --------------------------------------------------------------
   * By pressing the retry button, the error field is emptied and 
   * the measurement is requested again
   --------------------------------------------------------------*/
  resendMqttToGetMeasure(): void{
    this.currentMeasureTake.message.error = "";
    this.sendTakeMeasurementNotification();
  }

  /**--------------------------------------------------------------
   * sleep()
   * --------------------------------------------------------------
   * Await n milliseconds
   --------------------------------------------------------------*/
  sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * MeasurementInstruction 
   * --> getInstrucctionCoordinatorAndDevice()
   * --------------------------------------------------------------
   * Given the main values ​​it obtains its coordinator and its 
   * device
   --------------------------------------------------------------*/
  getInstrucctionCoordinatorAndDevice(instruction: MeasurementInstruction): void{
    this.coordinatorService.getCoordinatorByRoomID(this.notification.idRoom).subscribe( (coordinator: Coordinator) => {
      if(coordinator.id){
        instruction.message.data.coordinator = coordinator;

        const itemPosition = this.measuresType.findIndex(x => x == instruction.message.data.type);
        instruction.message.data.typeBBDD = this.measuresTypeBBDD[itemPosition];

        this.devicesService.getDeviceByTypeAndCoordinatorId(coordinator.id, instruction.message.data.typeBBDD).subscribe( (device: Device) => {
          if(device.id){
            instruction.message.data.device = device;

            this.currentMeasureTake = instruction;
            this.generateMeasurementMeasure(instruction.message.data.typeBBDD);
            this.sendTakeMeasurementNotification();
          }
        });
      }
    });
  }

  /**--------------------------------------------------------------
   * createNewTemperature()
   * --------------------------------------------------------------
   * Create a new temperature of a given patient in the database
   --------------------------------------------------------------*/
  createNewTemperature(): void{
    const temperature: Temperature = {
      idPatient: this.notification.patient!.id,
      value: this.currentMeasureTake.message.data.measurementResult![0].value!,
      unit: this.currentMeasureTake.message.data.measurementResult![0].units,
      timestamp: this.currentMeasureTake.message.data.time
    }

    this.temperatureService.postTemperature(temperature).subscribe( (resp: PostResponse) => {
      console.log("Temperatura subida correctamente");
      this.updateData.emit();
    });
  }

  /**--------------------------------------------------------------
   * createNewWeight()
   * --------------------------------------------------------------
   * Create a new weight of a given patient in the database
   --------------------------------------------------------------*/
  createNewWeight(): void{
    const weight: Weight = {
      idPatient: this.notification.patient!.id,
      value: this.currentMeasureTake.message.data.measurementResult![0].value!,
      unit: this.currentMeasureTake.message.data.measurementResult![0].units,
      timestamp: this.currentMeasureTake.message.data.time
    }

    this.weightService.postWeight(weight).subscribe( (resp: PostResponse) => {
      console.log("Peso subido correctamente");
      this.updateData.emit();
    });
  }

  /**--------------------------------------------------------------
   * createNewBloodPreassures()
   * --------------------------------------------------------------
   * Create a new blood preassure of a given patient in the 
   * database
   --------------------------------------------------------------*/
  createNewBloodPreassures(): void{
    const bloodPreassure: BloodPreassure = {
      idPatient: this.notification.patient!.id,
      systolicValue: this.currentMeasureTake.message.data.measurementResult![0].value!,
      systolicUnit: this.currentMeasureTake.message.data.measurementResult![0].units,
      diastolicValue: this.currentMeasureTake.message.data.measurementResult![1].value!,
      diastolicUnit: this.currentMeasureTake.message.data.measurementResult![1].units,
      rateValue: this.currentMeasureTake.message.data.measurementResult![2].value!,
      rateUnit: this.currentMeasureTake.message.data.measurementResult![2].units,
      timestamp: this.currentMeasureTake.message.data.time
    }

    this.bloodPreassureService.postBloodPreassure(bloodPreassure).subscribe( (resp: PostResponse) => {
      console.log("Presión subida correctamente");
      this.updateData.emit();
    });
  }

  /**--------------------------------------------------------------
   * createNewPulseOximeter()
   * --------------------------------------------------------------
   * Create a new pulse oximeter of a given patient in the 
   * database
   --------------------------------------------------------------*/
  createNewPulseOximeter(): void{
    const pulseOximeter: PulseOximeter = {
      idPatient: this.notification.patient!.id,
      oxygenValue: this.currentMeasureTake.message.data.measurementResult![0].value!,
      oxygenUnit: this.currentMeasureTake.message.data.measurementResult![0].units,
      beatsValue: this.currentMeasureTake.message.data.measurementResult![1].value!,
      beatsUnit: this.currentMeasureTake.message.data.measurementResult![1].units,
      timestamp: this.currentMeasureTake.message.data.time
    }

    this.pulseOximeterService.postPulseOximeter(pulseOximeter).subscribe( (resp: PostResponse) => {
      console.log("Presión subida correctamente");
      this.updateData.emit();
    });
  }
}
