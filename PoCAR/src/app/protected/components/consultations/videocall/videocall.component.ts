import { Component, Input, OnInit } from '@angular/core';
import { VideoSDKMeeting } from '@videosdk.live/rtc-js-prebuilt';
import { ConsultNotification } from 'src/app/protected/interfaces/interfaces';
import { Doctor } from 'src/app/shared/interfaces/interfaces';
import { environment } from 'src/environments/environment';
import { DoctorsService } from '../../../services/doctors.service';

@Component({
  selector: 'app-videocall',
  templateUrl: './videocall.component.html',
  styleUrls: ['./videocall.component.scss']
})
export class VideocallComponent implements OnInit {

  @Input() notification: ConsultNotification = null!;

  doctor: Doctor = null!;

  isVideocallShowed = false;

  constructor(
    private doctorsService: DoctorsService
  ) { }

  ngOnInit(): void {
    this.getDoctorData();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- VIDEOCALL -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * startVideocall()
   * --------------------------------------------------------------
   * Start a video call giving it all the parameters and 
   * permissions it needs
   --------------------------------------------------------------*/
  startVideocall(): void{
    const apiKey = environment.videosdkApiKey;
    const meetingId = this.notification.idRoom+"-"+this.notification.sip+"-"+this.notification.macDevice;
    const name = this.doctor ? this.doctor.name + " " + this.doctor.surname : "";

    const config = {
      name: name,
      meetingId: meetingId,
      apiKey: apiKey,

      region: "sg001", // region for new meeting

      containerId: "myVideoCall",
      //redirectOnLeave: "https://www.videosdk.live/",

      micEnabled: true,
      webcamEnabled: true,
      participantCanToggleSelfWebcam: true,
      participantCanToggleSelfMic: true,
      participantCanLeave: true, // if false, leave button won't be visible

      chatEnabled: true,
      screenShareEnabled: true,
      pollEnabled: true,
      whiteboardEnabled: true,
      raiseHandEnabled: true,

      recording: {
        autoStart: false, // auto start recording on participant joined
        enabled: false,
        webhookUrl: "https://www.videosdk.live/callback",
        awsDirPath: `/meeting-recordings/${meetingId}/`, // automatically save recording in this s3 path
      },

      livestream: {
        autoStart: true,
        enabled: true,
      },

      layout: {
        type: "SIDEBAR", // "SPOTLIGHT" | "SIDEBAR" | "GRID"
        priority: "PIN", // "SPEAKER" | "PIN",
        // gridSize: 3,
      },

      branding: {
        enabled: false,
        logoURL:
          "https://static.zujonow.com/videosdk.live/videosdk_logo_circle_big.png",
        name: "Prebuilt",
        poweredBy: false,
      },

      permissions: {
        pin: true,
        askToJoin: false, // Ask joined participants for entry in meeting
        toggleParticipantMic: true, // Can toggle other participant's mic
        toggleParticipantWebcam: true, // Can toggle other participant's webcam
        drawOnWhiteboard: true, // Can draw on whiteboard
        toggleWhiteboard: true, // Can toggle whiteboard
        toggleRecording: true, // Can toggle meeting recording
        toggleLivestream: true, //can toggle live stream
        removeParticipant: true, // Can remove participant
        endMeeting: true, // Can end meeting
        changeLayout: true, //can change layout
      },

      joinScreen: {
        visible: true, // Show the join screen ?
        title: this.notification.event ? this.notification.event.description : "Visita médica", // Meeting title
        meetingUrl: window.location.href, // Meeting joining url
      },

      leftScreen: {
        // visible when redirect on leave not provieded
        actionButton: {
          // optional action button
          label: "Video SDK Live", // action button label
          href: "https://videosdk.live/", // action button href
        },
      },

      notificationSoundEnabled: true,

      debug: true, // pop up error during invalid config or netwrok error

      maxResolution: "sd", // "hd" or "sd"

    };

    const meeting = new VideoSDKMeeting();
    meeting.init(config);
    
    this.redimensionCanvas();
  }

  /**--------------------------------------------------------------
   * redimensionCanvas()
   * --------------------------------------------------------------
   * Resize the frame to fit the size we want
   --------------------------------------------------------------*/
  async redimensionCanvas(){
    while(!this.isVideocallShowed){
      try {
        const frame = document.getElementById("videosdk-frame");
        frame!.setAttribute("height", "inherit");
        frame!.style.minHeight = "inherit";

        const videocall = document.getElementById("myVideoCall");
        if(videocall!.childElementCount > 1){
          videocall?.removeChild(videocall!.lastChild!);
        }

        this.isVideocallShowed = true;
      } catch (error) {}

      await this.delay(1000);
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------ AUXILIAR -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * delay()
   * --------------------------------------------------------------
   * Await n milliseconds
   --------------------------------------------------------------*/
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getDoctorData()
   * --------------------------------------------------------------
   * Que the doctor information, to personalice the videocall
   --------------------------------------------------------------*/
  getDoctorData(): void{
    this.doctorsService.getDoctorInfor().subscribe( (resp: Doctor | null) => {
      if(resp != null){
        this.doctor = resp;
      }

      this.startVideocall();
    });
  }

}
