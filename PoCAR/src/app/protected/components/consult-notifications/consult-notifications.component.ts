import { ChangeDetectorRef, Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ConsultNotification, FilterOptions, Patient, PatientsApplication } from '../../interfaces/interfaces';
import { mqtt } from 'aws-iot-device-sdk-v2';
import { ConsultsNotificationsService } from '../../services/consults-notifications.service';
import { EventService } from '../../services/event.service';
import { PatientService } from '../../services/patient.service';
import { forkJoin } from 'rxjs';
import { addDays } from 'date-fns';
import { Event } from '../../interfaces/interfaces';
import { PatientsApplicationsService } from '../../services/patients-applications.service';
import { Message, MessageService } from 'primeng/api';
import { Router } from '@angular/router';
import { MqttConsultCheckReceived, MqttConsultNotification } from 'src/app/shared/interfaces/interfaces';

@Component({
  selector: 'app-consult-notifications',
  templateUrl: './consult-notifications.component.html',
  styleUrls: ['./consult-notifications.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
  providers: [
    MessageService
  ]
})
export class ConsultNotificationsComponent implements OnInit {
  connection: mqtt.MqttClientConnection | null = null;

  clientID: string = localStorage.getItem('id')!+"ConsultNotificationComponent";

  patients: Patient[] = [];
  events: Event[] = [];
  savedNotifications: ConsultNotification[] = [];
  notifications: ConsultNotification[] = [];

  filters: FilterOptions[] = [];

  errorRoomIsInUse: Message = {key: 'notificationsKey', severity:'error', summary:'Error', detail:'Ya hay una consulta empezada, en dicha sala, vuelva a probar más tarde'};

  constructor(
    private consultsNotificationsService: ConsultsNotificationsService,
    private eventService: EventService,
    private patientsService: PatientService,
    private patientsApplicationsService: PatientsApplicationsService,
    private messageService: MessageService,
    private router: Router,
    private cRef: ChangeDetectorRef
  ) { }

  ngOnInit(): void {
    this.getNecessaryData();

    this.displayFilterOptions();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------- EVENTS, PATIENTS, NOTIFICATIONS -----------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * MqttConsultNotification --> addEventToList()
   * --------------------------------------------------------------
   * Given an object from the response received by MQTT, it finds 
   * the corresponding patient and their appointment. Later it is 
   * stored in the list of notifications and the list is updated
   --------------------------------------------------------------*/
  addEventToList(mqtt: MqttConsultNotification): void{
    const notification: ConsultNotification = {
      macDevice: mqtt.message.mac,
      idRoom: mqtt.message.data.idRoom,
      action: mqtt.message.data.action,
      sip: mqtt.message.data.sip,
      idDoctor: mqtt.message.data.idDoctor,
      idEvent: mqtt.message.data.idEvent
    }

    this.identifyPatient(notification);
    this.identifyDate(notification);

    this.savedNotifications.push(notification);
    
    this.notifications = this.savedNotifications;

    console.log(this.notifications);

    document.getElementById("Todas")?.click();

    this.detectListChanges();
  }

  /**--------------------------------------------------------------
   * MqttConsultNotification --> updateEventToList()
   * --------------------------------------------------------------
   * Update a list event as it could change its state to left or 
   * end
   --------------------------------------------------------------*/
  updateEventToList(mqtt: MqttConsultNotification): void{
    const notification = this.savedNotifications.find(x => x.macDevice == mqtt.message.mac && x.idRoom == mqtt.message.data.idRoom && x.sip == mqtt.message.data.sip);

    notification!.action = mqtt.message.data.action;

    this.notifications = this.savedNotifications;
    this.changeCurrentFilter(this.filters.find(x => x.isSelected)!);
  }

  /**--------------------------------------------------------------
   * ConsultNotification --> identifyPatient()
   * --------------------------------------------------------------
   * Given the doctor's list of patients, it searches for the 
   * patient that corresponds to the sip received in the 
   * notification
   --------------------------------------------------------------*/
  identifyPatient(notification: ConsultNotification): void{
    const patient = this.patients.find(x => x.sip == notification.sip);

    if(patient != undefined){
      notification.patient = patient;
    }
  }

  /**--------------------------------------------------------------
   * ConsultNotification --> identifyDate()
   * --------------------------------------------------------------
   * Given the list of today's doctor's appointments, look in it 
   * for the event that corresponds to the event id received in 
   * the notification
   --------------------------------------------------------------*/
  identifyDate(notification: ConsultNotification): void{
    const event = this.events.find(x => (x.idPatient == notification.patient?.id) && (x.id == notification.idEvent));

    if(event != undefined){
      notification.event = event;
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- AUXILIAR ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * detectListChanges()
   * --------------------------------------------------------------
   * Force change detection
   --------------------------------------------------------------*/
  detectListChanges(): void{
    this.notifications = [...this.notifications];
    this.cRef.detectChanges();
  }

  /**--------------------------------------------------------------
   * Date --> convertDateToDayMonthYear() --> string
   * --------------------------------------------------------------
   * Convert the date to text containing only the day, month, and 
   * year
   --------------------------------------------------------------*/
  convertDateToDayMonthYear(date: Date): string{
    var month = date.getUTCMonth() + 1;
    var day = date.getUTCDate();
    var year = date.getUTCFullYear();

    return year + "-" + month + "-" + day;
  }

  /**--------------------------------------------------------------
   * displayFilterOptions()
   * --------------------------------------------------------------
   * Shows the options on the top bar that allow you to filter 
   * notifications by completed, pending or all. Sets all option 
   * by default to True
   --------------------------------------------------------------*/
  displayFilterOptions(): void{
    this.filters.push({ value: "Pendientes", isSelected: false });
    this.filters.push({ value: "Finalizadas", isSelected: false });
    this.filters.push({ value: "Todas", isSelected: true });
  }

  /**--------------------------------------------------------------
   * ConsultNotification --> getTimeString() --> string
   * --------------------------------------------------------------
   * Gets the time the query should start (it is the same time 
   * the event is scheduled for)
   --------------------------------------------------------------*/
  getTimeString(notification: ConsultNotification): string{
    const startDate = new Date(notification.event?.startDate!);
    return startDate.getHours() + ":" + startDate.getMinutes()+" H";
  }

  /**--------------------------------------------------------------
   * FilterOptions --> changeCurrentFilter()
   * --------------------------------------------------------------
   * Clicking on one of the options on the top bar changes the 
   * status of the selected one to True and the rest to False
   --------------------------------------------------------------*/
  changeCurrentFilter(filter: FilterOptions): void{
    this.filters.forEach(element => {
      if(element != filter){
        element.isSelected = false;
      }else{
        element.isSelected = true;

        switch(element.value){
          case "Finalizadas":
            this.notifications = this.savedNotifications.length > 0 ? this.savedNotifications.filter(x => x.action == 'end') : [];
            break;
          case "Pendientes":
            this.notifications = this.savedNotifications.length > 0 ? this.savedNotifications.filter(x => x.action == 'arrive') : [];
            break;
          case "Todas":
            this.notifications = this.savedNotifications.length > 0 ? this.savedNotifications : [];
        }
      }
    });
  }

  /**--------------------------------------------------------------
   * ConsultNotification --> isNotificationNotAccessible() --> T/F
   * --------------------------------------------------------------
   * Checks if a notification is not accessible, that is, for some 
   * reason, the call cannot be accessed
   --------------------------------------------------------------*/
  isNotificationNotAccessible(notification: ConsultNotification): boolean{
   switch(notification.action){
    case "left":
      return true;
    case "end":
      return true;
    case "arrive":
      return false;
    default:
      return false;
   }
  }

  /*isTimeOff(notification: ConsultNotification): boolean{
    if(!notification.event){
      return false;
    }else{
      const endDate = new Date(notification.event?.endDate!);

      const now = new Date();

      if(endDate.getHours() < now.getHours() || (endDate.getHours() == now.getHours() && endDate.getMinutes() < now.getMinutes())){
        return true;
      }
    }

    return false;
  }*/

  /**--------------------------------------------------------------
   * displaySavedNotifications()
   * --------------------------------------------------------------
   * Shows the stored notifications by previously calling other 
   * functions that allow obtaining the data of the patient and of 
   * the event that has been programmed
   --------------------------------------------------------------*/
  displaySavedNotifications(): void{
    this.savedNotifications.forEach(notification => {
      this.identifyPatient(notification);
      this.identifyDate(notification);
    });

    this.notifications = this.savedNotifications;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MQTT ----------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * subscribeNotifications()
   * --------------------------------------------------------------
   * Create an MQTT subscription when the connection is established 
   * in order to receive notifications of patient arrivals in the 
   * visiting room. When a notification is received, it is stored 
   * in the list, previously looking for the patient to whom it is 
   * addressed and the appointment
   --------------------------------------------------------------*/
  subscribeNotifications(): void{
    this.connection?.subscribe('PoCAR/consultlNotifications/idDoctor='+localStorage.getItem('id'), mqtt.QoS.AtLeastOnce, (topic, payload) => {
      try {
        const decoder = new TextDecoder('utf8');
        const notification: MqttConsultNotification = JSON.parse(decoder.decode(payload));

        if(notification.status == 200){
          this.addEventToList(notification);
        }
      } catch (error) {
        console.log(error);
      }
    });
  }

  /**--------------------------------------------------------------
   * ConsultNotification --> sendStartNotification()
   * --------------------------------------------------------------
   * Publish an object through MQTT that allows the query to be 
   * opened automatically to the patient
   --------------------------------------------------------------*/
  sendStartNotification(notification: ConsultNotification): void{
    if(localStorage.getItem('currentNotificationID') != notification.id?.toString()){
      this.patientsApplicationsService.getPatientApplicationInformation(notification.macDevice).subscribe((information: PatientsApplication) => {
        if(information.isEnabled == 1){
          this.setNotificationAndNavegate(notification);
  
          localStorage.setItem('currentNotificationID', notification.id!.toString());
        }else{
          this.messageService.add(this.errorRoomIsInUse);
        }
      });
    }else{
      this.setNotificationAndNavegate(notification);
    }
  }

  /**--------------------------------------------------------------
   * ConsultNotification --> setNotificationAndNavegate()
   * --------------------------------------------------------------
   * Sends via MQTT a notification to the patient's device, so that 
   * the video call begins and the doctor is redirected to the 
   * consultation screen
   --------------------------------------------------------------*/
  setNotificationAndNavegate(notification: ConsultNotification): void{
    const message: MqttConsultCheckReceived = {
      status: 200,
      message: {
        data: {
          action: "start"
        }
      }
    }
    this.connection?.publish('PoCAR/consultlNotifications/startConsultation/macDevice='+notification.macDevice+"-sip="+notification.sip, JSON.stringify(message), mqtt.QoS.AtLeastOnce);
    this.router.navigate([ '/consulta' ], {state: { notification: notification }});
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getNecessaryData()
   * --------------------------------------------------------------
   * Get all the doctor's patients, all the doctor's scheduled 
   * events for today and the list of stored notifications
   --------------------------------------------------------------*/
  getNecessaryData(): void{
    const start = this.convertDateToDayMonthYear(new Date());
    const end = this.convertDateToDayMonthYear(addDays(new Date(), 1));
    forkJoin([this.eventService.getEventsFromTo(start, end), this.patientsService.getDoctorPatients(null), this.consultsNotificationsService.getConsultsNotificationsFromTo(start, end)])
    .subscribe(([events, patients, savedNotifications]) => {
      this.events = events;
      this.patients = patients;

      this.savedNotifications = savedNotifications;
      this.displaySavedNotifications();
    })
  }

}
