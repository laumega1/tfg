import { Component, OnInit } from '@angular/core';
import { Doctor } from 'src/app/shared/interfaces/interfaces';
import { DoctorsService } from '../../../services/doctors.service';
import { AuthService } from '../../../../auth/services/auth.service';
import { BasicHealthZone } from '../../../interfaces/interfaces';
import { BasicHealthZonesService } from 'src/app/protected/services/basic-healt-zones.service';

@Component({
  selector: 'app-doctor-resume',
  templateUrl: './doctor-resume.component.html',
  styleUrls: ['./doctor-resume.component.scss']
})
export class DoctorResumeComponent implements OnInit {

  dialogHeader: string = "";
  buttonSummitTitle: string = "";
  dialogMode: string = "put";

  displayModalDoctor: boolean = false;

  doctor: Doctor = null!;

  basicHealthZones: BasicHealthZone[] = [];

  constructor(
    private doctorsService: DoctorsService,
    private authService: AuthService,
    private basicHealthZonesService: BasicHealthZonesService
  ) { }

  ngOnInit(): void {
    this.getDoctorInformation();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MODALS --------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * string, string, string --> showDoctorDialog()
   * --------------------------------------------------------------
   * When clicking on edit a doctor, a modal opens given certain 
   * parameters for the configuration and the selected doctor 
   * that you want to edit
   --------------------------------------------------------------*/
   showDoctorDialog(title: string, button: string, type: string): void{
    this.dialogHeader = title;
    this.buttonSummitTitle = button;
    this.dialogMode = type;

    this.displayModalDoctor = true;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------ AUXILIAR -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * closeSession()
   * --------------------------------------------------------------
   * Delete the localstorage id and token and reload the website
   --------------------------------------------------------------*/
  closeSession(): void{
    this.authService.deleteBasicDataLocalStorage();
    location.reload();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getDoctorInformation()
   * --------------------------------------------------------------
   * Get the information about current doctor
   --------------------------------------------------------------*/
   getDoctorInformation(): void{
    this.doctorsService.getDoctorInfor().subscribe((doctor: Doctor | null) => {
      this.doctor = doctor!;

      this.getDoctorBasicHealthZones(doctor!);
    });
  }

  /**--------------------------------------------------------------
   * Doctor --> getDoctorBasicHealthZones()
   * --------------------------------------------------------------
   * Get all basic doctor health zones
   --------------------------------------------------------------*/
  getDoctorBasicHealthZones(doctor: Doctor): void{
    this.basicHealthZonesService.getBasicHealthZonesByDoctor(doctor.id.toString()).subscribe((basicHealthZones: BasicHealthZone[]) => {
      this.basicHealthZones = basicHealthZones;
    });
  }

}
