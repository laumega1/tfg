import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { isThisMinute } from 'date-fns';
import { Message, MessageService } from 'primeng/api';
import { BasicHealthZone, PutResponse } from 'src/app/protected/interfaces/interfaces';
import { BasicHealthZonesService } from 'src/app/protected/services/basic-healt-zones.service';
import { Doctor } from 'src/app/shared/interfaces/interfaces';
import { DoctorsService } from '../../../services/doctors.service';
import { PostResponse, DeleteResponse } from '../../../interfaces/interfaces';
import { DoctorsBasicHealthZonesService } from 'src/app/protected/services/doctors-basic-health-zones.service';

@Component({
  selector: 'app-modal-doctors',
  templateUrl: './modal-doctors.component.html',
  styleUrls: ['./modal-doctors.component.scss'],
  providers: [
    MessageService
  ]
})
export class ModalDoctorsComponent implements OnInit {
  @Input() dialogHeader: string = "";
  @Input() buttonSummitTitle: string = "";

  @Input() displayModalDoctor: boolean = false;
  @Output() displayModalDoctorChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() dialogMode: string = "";
  @Output() dialogModeChange: EventEmitter<string> = new EventEmitter<string>();

  @Input() currentDoctor: Doctor = null!;

  @Input() basicHealthZones: BasicHealthZone[] = [];
  savedDoctorBasicHealthZones: BasicHealthZone[] = [];
  allBasicHealthZones: BasicHealthZone[] = [];
  allBasicHealthZonesCopy: BasicHealthZone[] = [];
  removedBasicHealthZones: BasicHealthZone[] = [];

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();

  correctUpdate: Message = {severity:'success', summary:'Éxito', detail:'El médico ha sido modificado correctamente.'};
  errorUpdate: Message = {severity:'error', summary:'Error', detail:'No se ha podido editar el médico.'};
  correctPost: Message = {severity:'success', summary:'Éxito', detail:'El nuevo médico ha sido creado correctamente.'};
  errorPost: Message = {severity:'error', summary:'Error', detail:'No se ha podido crear un médico nuevo.'};
  errorPasswords: Message = {severity:'error', summary:'Error', detail:'Ambas contraseñas no son iguales, porfavor, vuelvalas a escribir.'};

  myForm: FormGroup = this.fb.group({
    doctorName: ['', [Validators.required]],
    doctorSurname: ['', [Validators.required]],
    doctorEmail: ['', [Validators.required]],
    doctorPassword: ['', [Validators.required]],
    doctorRepeatPassword: ['', [Validators.required]],
    doctorIsAdmin: [false , [Validators.required]],
    selectedZBS: [null]
  });

  sessionDoctor: Doctor = null!;

  constructor(
    private fb: FormBuilder,
    private basicHealthZonesService: BasicHealthZonesService,
    private messageService: MessageService,
    private doctorsService: DoctorsService,
    private doctorsBasicHealthZonesService: DoctorsBasicHealthZonesService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.displayModalDoctor && changes.displayModalDoctor.currentValue == true){
      this.getDoctorSessionDoctorInformation();
      this.getAllBasicHealthZones();

      if(this.dialogMode == 'put'){
        this.changeDialogInputsEdit();
      }else{
        this.resetFormToInitValues();
      }
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- FORMS -------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * resetFormToInitValues()
   * --------------------------------------------------------------
   * Reset/empty the form fields. This is useful when you want to 
   * create a new object
   --------------------------------------------------------------*/
   resetFormToInitValues(): void{
    this.myForm.reset();

    this.myForm.patchValue({
      doctorIsAdmin: false
    });

    this.currentDoctor = {
      id: 0,
      name: "",
      surname: "",
      email: "",
      password: "",
      isAdmin: false,
      isOnline: true
    }
  }

  /**--------------------------------------------------------------
   * changeDialogInputsEdit()
   * --------------------------------------------------------------
   * Insert the doctor data in the form. Subsequently, all basic 
   * health areas and their corresponding centers are requested
   --------------------------------------------------------------*/
  changeDialogInputsEdit(): void{
    this.savedDoctorBasicHealthZones = [...this.basicHealthZones];

    this.myForm.patchValue({
      doctorName: this.currentDoctor.name,
      doctorSurname: this.currentDoctor.surname,
      doctorEmail: this.currentDoctor.email,
      doctorPassword: "********",
      doctorRepeatPassword: "********",
      doctorIsAdmin: false,
      selectedZBS: null
    });

    if(this.currentDoctor.isAdmin == 1){
      this.setClickToCheckbox();
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- MODALS ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * closeModal()
   * --------------------------------------------------------------
   * Close modal and notificate the parent component
   --------------------------------------------------------------*/
  closeModal(): void{
    this.displayModalDoctor = false;
    this.displayModalDoctorChange.emit(this.displayModalDoctor);

    this.dialogMode = "";
    this.dialogModeChange.emit(this.dialogMode);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- AUXILIAR ----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * sleep()
   * --------------------------------------------------------------
   * Await n milliseconds
   --------------------------------------------------------------*/
  sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  /**--------------------------------------------------------------
   * updateRemovedList()
   * --------------------------------------------------------------
   * Add item to removed list to later delete it from the database
   --------------------------------------------------------------*/
  updateRemovedList(bhz: BasicHealthZone): void{
    this.removedBasicHealthZones.push(bhz);
    this.basicHealthZones.splice(this.basicHealthZones.indexOf(bhz), 1);
    
    this.filtreDropdown();
  }

  /**--------------------------------------------------------------
   * setClickToCheckbox()
   * --------------------------------------------------------------
   * If a minute and a half passes and there is no response, 
   * unsubscribe
   --------------------------------------------------------------*/
  async setClickToCheckbox() {
    await this.sleep(1000);

    let element: HTMLElement = document.getElementsByClassName('p-checkbox-box')[0] as HTMLElement;
    element.click();
  }

  /**--------------------------------------------------------------
   * filtreDropdown()
   * --------------------------------------------------------------
   * Filter the list of basic health zones from the drop-down by 
   * removing those zones that are added
   --------------------------------------------------------------*/
   filtreDropdown() {
    this.allBasicHealthZones = this.allBasicHealthZonesCopy.filter(a => this.basicHealthZones.findIndex((y: BasicHealthZone) => y.id == a.id) == -1);
  }

  /**--------------------------------------------------------------
   * setDropdownChangeListener()
   * --------------------------------------------------------------
   * Listen when the dropdown value changes and add the selected 
   * item to the list
   --------------------------------------------------------------*/
   setDropdownChangeListener() {
    this.myForm.get('selectedZBS')!.valueChanges.subscribe(value => {
      if(value != null){
        this.basicHealthZones.push(value);

        this.filtreDropdown();
      }
    })
  }

  /**--------------------------------------------------------------
   * Message --> showAlert()
   * --------------------------------------------------------------
   * Display popup alert
   --------------------------------------------------------------*/
   showAlert(alert: Message): void{
    if(this.messageService){
      this.messageService.clear();
    }
    this.messageService.add(alert);
  }

  /**--------------------------------------------------------------
   * generateOrDeleteBasicHealthZones()
   * --------------------------------------------------------------
   * Go through the list of basic health zones and if it is a new 
   * zone, add it, on the contrary, if one that was there is not 
   * now, delete it
   --------------------------------------------------------------*/
  generateOrDeleteBasicHealthZones(): void{
    this.basicHealthZones.forEach(bhz => {
      if(this.savedDoctorBasicHealthZones.findIndex(x => x.id == bhz.id) == -1){
        this.createNewBasicHealthZoneRelation(this.currentDoctor.id.toString(), bhz.id!.toString());
      }
    });

    this.deleteDuplicateBasicHealthZones();

    this.removedBasicHealthZones.forEach(bhz => {
      if(this.basicHealthZones.findIndex(x => x.id == bhz.id) == -1 && this.savedDoctorBasicHealthZones.findIndex(x => x.id == bhz.id) != -1){
        this.deleteBasicHealthZoneRelation(this.currentDoctor.id.toString(), bhz.id!.toString());
      }
    });
  }

  /**--------------------------------------------------------------
   * deleteDuplicateBasicHealthZones()
   * --------------------------------------------------------------
   * Delete the duplicated basic health zones to to avoid causing 
   * problems by deleting an item more than once
   --------------------------------------------------------------*/
  deleteDuplicateBasicHealthZones(): void{
    this.removedBasicHealthZones = this.removedBasicHealthZones.filter((value, index, self) => index === self.findIndex((t) => (t.id === value.id)));
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getDoctorSessionDoctorInformation()
   * --------------------------------------------------------------
   * Get the information about current doctor
   --------------------------------------------------------------*/
   getDoctorSessionDoctorInformation(): void{
    this.doctorsService.getDoctorInfor().subscribe((doctor: Doctor | null) => {
      this.sessionDoctor = doctor!;
    });
  }

  /**--------------------------------------------------------------
   * createNewDoctorInformation()
   * --------------------------------------------------------------
   * A new doctor in database is created with all the information 
   * collected from the form
   --------------------------------------------------------------*/
  createNewDoctorInformation(): void{
    const { doctorName, doctorSurname, doctorEmail, doctorPassword, doctorRepeatPassword, doctorIsAdmin } = this.myForm.value;

    var isOkay = true;

    if(doctorPassword == doctorRepeatPassword){
      this.currentDoctor.password = doctorPassword;
    }else{
      isOkay = false;
      this.showAlert(this.errorPasswords);
    }

    if(isOkay){
      this.currentDoctor.name = doctorName;
      this.currentDoctor.surname = doctorSurname;
      this.currentDoctor.email = doctorEmail;
      this.currentDoctor.isAdmin = doctorIsAdmin;
      this.currentDoctor.basicHealthZones = this.basicHealthZones;
  
      this.doctorsService.postDoctor(this.currentDoctor).subscribe( (resp: PostResponse) => {
        if(resp.status == 200){
          this.showAlert(this.correctPost);

          this.currentDoctor.id = resp.id!;

          this.generateOrDeleteBasicHealthZones();
          this.updateData.emit(this.currentDoctor);
        }else{
          this.showAlert(this.errorPost);
        }
      });
    }
  }

  /**--------------------------------------------------------------
   * updateDoctorInformation()
   * --------------------------------------------------------------
   * Update a doctor in database with all the information collected 
   * from the form
   --------------------------------------------------------------*/
  updateDoctorInformation(): void{
    const { doctorName, doctorSurname, doctorPassword, doctorRepeatPassword, doctorIsAdmin } = this.myForm.value;

    var isOkay = true;
    var isPasswordChanged = false;

    if(doctorPassword != "********" || doctorRepeatPassword != "********"){
      if(doctorPassword == doctorRepeatPassword){
        this.currentDoctor.password = doctorPassword;
        isPasswordChanged = true;
      }else{
        isOkay = false;
        this.showAlert(this.errorPasswords);
      }
    }

    if(isOkay){
      this.currentDoctor.name = doctorName;
      this.currentDoctor.surname = doctorSurname;
      this.currentDoctor.isAdmin = doctorIsAdmin;
  
      this.doctorsService.putDoctor(this.currentDoctor, isPasswordChanged).subscribe( (resp: PutResponse) => {
        if(resp.status == 200){
          this.showAlert(this.correctUpdate);
        }else{
          this.showAlert(this.errorUpdate);
        }
      });
    }

    this.generateOrDeleteBasicHealthZones();
  }

  /**--------------------------------------------------------------
   * getAllBasicHealthZones()
   * --------------------------------------------------------------
   * Get all basic health zones to data base so that the doctor 
   * can be added to them
   --------------------------------------------------------------*/
  getAllBasicHealthZones(): void{
    this.basicHealthZonesService.getAllHealthZones().subscribe((basicHealthZones: BasicHealthZone[]) => {
      this.allBasicHealthZones = basicHealthZones;
      this.allBasicHealthZonesCopy = basicHealthZones;

      this.filtreDropdown();
      this.setDropdownChangeListener();
    });
  }

  /**--------------------------------------------------------------
   * string, string --> createNewBasicHealthZoneRelation()
   * --------------------------------------------------------------
   * It creates a basic relationship between a basic health area 
   * and a doctor so that the doctor can have access to their 
   * devices, rooms, etc.
   --------------------------------------------------------------*/
  createNewBasicHealthZoneRelation(idDoctor: string, idBHZ: string): void{
    this.doctorsBasicHealthZonesService.putNewRealtionBetweenBasicHealthZoneAndDoctor(idDoctor, idBHZ).subscribe((resp: PostResponse) => {
      if(resp.status == 200){
        this.showAlert(this.correctPost);
      }else{
        this.showAlert(this.errorPost);
      }
    });
  }

  /**--------------------------------------------------------------
   * string, string --> deleteBasicHealthZoneRelation()
   * --------------------------------------------------------------
   * Removes a basic relationship between a basic health zone and 
   * a doctor. He will no longer be able to see those patients
   --------------------------------------------------------------*/
   deleteBasicHealthZoneRelation(idDoctor: string, idBHZ: string): void{
    this.doctorsBasicHealthZonesService.deleteRealtionBetweenBasicHealthZoneAndDoctor(idDoctor, idBHZ).subscribe((resp: DeleteResponse) => {
      if(resp.status == 200){
        this.showAlert(this.correctPost);
      }else{
        this.showAlert(this.errorPost);
      }
    });
  }
}
