import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmationService } from 'primeng/api';
import { BasicHealthZone } from 'src/app/protected/interfaces/interfaces';
import { BasicHealthZonesService } from 'src/app/protected/services/basic-healt-zones.service';
import { Doctor } from 'src/app/shared/interfaces/interfaces';
import { DoctorsService } from '../../../services/doctors.service';

@Component({
  selector: 'app-doctors-list',
  templateUrl: './doctors-list.component.html',
  styleUrls: ['./doctors-list.component.scss']
})
export class DoctorsListComponent implements OnInit {

  myForm: FormGroup = this.fb.group({
    doctorsSearch: ['', [Validators.required]]
  });

  savedDoctors: Doctor[] = [];
  doctors: Doctor[] = [];

  currentDoctor: Doctor = null!;

  isAdmin: boolean = false;

  orderAlphabeticClass: string = "fa-solid fa-arrow-up-a-z";

  dialogHeader: string = "";
  buttonSummitTitle: string = "";
  dialogMode: string = "";

  displayModalDoctor: boolean = false;

  constructor(
    private fb: FormBuilder,
    private doctorsService: DoctorsService,
    private basicHealthZonesService: BasicHealthZonesService
  ) { }

  ngOnInit(): void {
    this.getDoctorInformation();

    this.getAllDoctors();
    this.subscribeChangesFilter();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------ AUXILIAR -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * Doctor --> detectDoctorsChanges()
   * --------------------------------------------------------------
   * When a change is detected, the input is emptied and the 
   * doctor is added to the list
   --------------------------------------------------------------*/
   detectDoctorsChanges(doctor: Doctor): void{
    this.myForm.patchValue({
      doctorsSearch: ""
    });

    this.savedDoctors.push(doctor);
    this.doctors = this.savedDoctors;

    this.currentDoctor = doctor;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- SEARCH - FILTER -------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * subscribeChangesFilter()
   * --------------------------------------------------------------
   * Detects when changes occur in the search engine that helps us 
   * filter the list of doctors
   --------------------------------------------------------------*/
   subscribeChangesFilter(): void{
    this.myForm.get('doctorsSearch')?.valueChanges.subscribe((value: string) => {
      this.filterDoctors(value.toLowerCase());
    });
  }

  /**--------------------------------------------------------------
   * string --> filterDoctors()
   * --------------------------------------------------------------
   * Filter the list of doctors taking into account numerous 
   * things such as name, surname, basic health zones...
   --------------------------------------------------------------*/
   filterDoctors(newInfo: string): void{
    this.doctors = this.savedDoctors.filter(x => (
      x.name.toLowerCase().includes(newInfo) || 
      x.surname.toLowerCase().includes(newInfo) ||
      x.email.toString().includes(newInfo) ||
      (x.isAdmin ? "si" : "no").includes(newInfo) ||
      x.basicHealthZones?.findIndex(x => x.name.toLowerCase().includes(newInfo)) != -1
    ));
  }

  /**--------------------------------------------------------------
   * orderDoctorsByAlphabetic()
   * --------------------------------------------------------------
   * By clicking on the sort button, it allows you to sort the 
   * doctors in alphabetical order or vice versa
   --------------------------------------------------------------*/
   orderDoctorsByAlphabetic(): void{
    switch(this.orderAlphabeticClass){
      case "fa-solid fa-arrow-up-a-z":
        this.doctors = this.savedDoctors.sort((a, b) => (a.name+" "+a.surname).localeCompare(b.name+" "+b.surname));
        this.orderAlphabeticClass = "fa-solid fa-arrow-up-z-a";
        break;
      case "fa-solid fa-arrow-up-z-a":
        this.doctors = this.savedDoctors.sort((a, b) => (b.name+" "+b.surname).localeCompare(a.name+" "+a.surname));
        this.orderAlphabeticClass = "fa-solid fa-arrow-up-a-z";
        break;
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MODALS --------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * string, string, string, Doctor --> showDoctorDialog()
   * --------------------------------------------------------------
   * When clicking on edit a doctor, a modal opens given certain 
   * parameters for the configuration and the selected doctor 
   * that you want to edit
   --------------------------------------------------------------*/
   showDoctorDialog(title: string, button: string, type: string, doctor: Doctor): void{
    this.dialogHeader = title;
    this.buttonSummitTitle = button;
    this.dialogMode = type;

    this.displayModalDoctor = true;

    this.currentDoctor = doctor;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getDoctorInformation()
   * --------------------------------------------------------------
   * Get the information about current doctor
   --------------------------------------------------------------*/
   getDoctorInformation(): void{
    this.doctorsService.getDoctorInfor().subscribe((doctor: Doctor | null) => {
      this.isAdmin = doctor!.isAdmin == 1 ? true : false;
    });
  }

  /**--------------------------------------------------------------
   * getAllDoctors()
   * --------------------------------------------------------------
   * Get all doctors from database
   --------------------------------------------------------------*/
  getAllDoctors(): void{
    this.doctorsService.getAllDoctors().subscribe((doctors: Doctor[]) => {
      this.savedDoctors = doctors;
      this.doctors = doctors;

      this.savedDoctors.forEach(element => {
        this.getDoctorBasicHealthZones(element);
      });
    });
  }

  /**--------------------------------------------------------------
   * Doctor --> getDoctorBasicHealthZones()
   * --------------------------------------------------------------
   * Get all basic doctor health zones
   --------------------------------------------------------------*/
  getDoctorBasicHealthZones(doctor: Doctor): void{
    this.basicHealthZonesService.getBasicHealthZonesByDoctor(doctor.id.toString()).subscribe((basicHealthZones: BasicHealthZone[]) => {
      console.log(basicHealthZones);
      doctor.basicHealthZones = basicHealthZones;

      this.doctors = this.savedDoctors;
    });
  }
}
