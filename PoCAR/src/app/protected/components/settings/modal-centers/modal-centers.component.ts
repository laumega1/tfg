import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Message, MessageService } from 'primeng/api';
import { Center, PutResponse } from 'src/app/protected/interfaces/interfaces';
import { BasicHealthZonesService } from 'src/app/protected/services/basic-healt-zones.service';
import { BasicHealthZone, PostResponse } from '../../../interfaces/interfaces';
import { CentersService } from '../../../services/centers.service';

@Component({
  selector: 'app-modal-centers',
  templateUrl: './modal-centers.component.html',
  styleUrls: ['./modal-centers.component.scss'],
  providers: [
    MessageService
  ]
})
export class ModalCentersComponent implements OnInit {
  @Input() dialogHeader: string = "";
  @Input() buttonSummitTitle: string = "";

  @Input() displayModalCenters: boolean = false;
  @Output() displayModalCentersChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() dialogMode: string = "";
  @Output() dialogModeChange: EventEmitter<string> = new EventEmitter<string>();

  @Input() currentCenter: Center = null!;

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();

  basicHealthZones: BasicHealthZone[] = [];

  myForm: FormGroup = this.fb.group({
    nameCenter: ['', [Validators.required]],
    phoneCenter: ['', [Validators.required]],
    selectedZBS: [null, [Validators.required]]
  });

  correctUpdate: Message = {severity:'success', summary:'Éxito', detail:'La zona básica de salud ha sido modificado correctamente.'};
  errorUpdate: Message = {severity:'error', summary:'Error', detail:'No se ha podido editar la zona básica de salud.'};
  correctPost: Message = {severity:'success', summary:'Éxito', detail:'La nueva zona básica de salud ha sido creado correctamente.'};
  errorPost: Message = {severity:'error', summary:'Error', detail:'No se ha podido crear una zona básica de salud.'};

  constructor(
    private fb: FormBuilder,
    private messageService: MessageService,
    private basicHealthZonesService: BasicHealthZonesService,
    private centersService: CentersService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.displayModalCenters && changes.displayModalCenters.currentValue == true){
      this.getAllBasicHealthZones();

      if(this.dialogMode == 'put'){
        this.changeDialogInputsEdit();
      }else{
        this.resetFormToInitValues();
      }
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- FORMS -------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * resetFormToInitValues()
   * --------------------------------------------------------------
   * Reset/empty the form fields. This is useful when you want to 
   * create a new object
   --------------------------------------------------------------*/
   resetFormToInitValues(): void{
    this.myForm.reset();

    this.currentCenter = {
      id: 0,
      name: "",
      phone: 0,
      idBasicHealthZone: 0,
      basicHealthZone: null!,
      rooms: []
    }
  }

  /**--------------------------------------------------------------
   * changeDialogInputsEdit()
   * --------------------------------------------------------------
   * Show the basicHealthZone data in the form
   --------------------------------------------------------------*/
   async changeDialogInputsEdit(){
    await this.sleep(1000);
    
    this.myForm.patchValue({
      nameCenter: this.currentCenter.name,
      phoneCenter: this.currentCenter.phone,
      selectedZBS: this.currentCenter.basicHealthZone
    });
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- AUXILIAR ----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * Message --> showAlert()
   * --------------------------------------------------------------
   * Display popup alert
   --------------------------------------------------------------*/
   showAlert(alert: Message): void{
    if(this.messageService){
      this.messageService.clear();
    }
    this.messageService.add(alert);
  }

  /**--------------------------------------------------------------
   * sleep()
   * --------------------------------------------------------------
   * Await n milliseconds
   --------------------------------------------------------------*/
   sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- MODALS ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * closeModal()
   * --------------------------------------------------------------
   * Close modal and notificate the parent component
   --------------------------------------------------------------*/
   closeModal(): void{
    this.displayModalCenters = false;
    this.displayModalCentersChange.emit(this.displayModalCenters);

    this.dialogMode = "";
    this.dialogModeChange.emit(this.dialogMode);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * createNewCenter()
   * --------------------------------------------------------------
   * A new center in database is created with all the 
   * information collected from the form
   --------------------------------------------------------------*/
   createNewCenter(): void{
    const { nameCenter, phoneCenter, selectedZBS } = this.myForm.value;

    this.currentCenter.name = nameCenter;
    this.currentCenter.phone = phoneCenter;
    this.currentCenter.basicHealthZone = selectedZBS;
    this.currentCenter.idBasicHealthZone = selectedZBS.id;

    this.centersService.postCenter(this.currentCenter).subscribe((resp: PostResponse) => {
      if(resp.status == 200){
        this.currentCenter.id = resp.id!;

        this.showAlert(this.correctPost);
        this.updateData.emit(this.currentCenter);
      }else{
        this.showAlert(this.errorPost);
      }
    });
  }

  /**--------------------------------------------------------------
   * updateCenterInformation()
   * --------------------------------------------------------------
   * Update a center in database with all the information collected 
   * from the form
   --------------------------------------------------------------*/
   updateCenterInformation(): void{
    const { nameCenter, phoneCenter, selectedZBS } = this.myForm.value;

    this.currentCenter.name = nameCenter;
    this.currentCenter.phone = phoneCenter;
    this.currentCenter.basicHealthZone = selectedZBS;
    this.currentCenter.idBasicHealthZone = selectedZBS.id;

    this.centersService.putCenter(this.currentCenter).subscribe((resp: PutResponse) => {
      if(resp.status == 200){
        this.showAlert(this.correctUpdate);
        this.updateData.emit(null);
      }else{
        this.showAlert(this.errorUpdate);
      }
    });
  }

  /**--------------------------------------------------------------
   * getAllBasicHealthZones()
   * --------------------------------------------------------------
   * Get all basic health zones to data base so that the doctor 
   * can be added to them
   --------------------------------------------------------------*/
   getAllBasicHealthZones(): void{
    this.basicHealthZonesService.getAllHealthZones().subscribe((basicHealthZones: BasicHealthZone[]) => {
      this.basicHealthZones = basicHealthZones;
    });
  }

}
