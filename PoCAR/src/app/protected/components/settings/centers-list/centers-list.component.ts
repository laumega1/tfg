import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Center, Room } from 'src/app/protected/interfaces/interfaces';
import { DoctorsService } from 'src/app/protected/services/doctors.service';
import { Doctor } from 'src/app/shared/interfaces/interfaces';
import { CentersService } from '../../../services/centers.service';
import { BasicHealthZonesService } from '../../../services/basic-healt-zones.service';
import { BasicHealthZone } from '../../../interfaces/interfaces';
import { RoomsService } from 'src/app/protected/services/rooms.service';

@Component({
  selector: 'app-centers-list',
  templateUrl: './centers-list.component.html',
  styleUrls: ['./centers-list.component.scss']
})
export class CentersListComponent implements OnInit {

  dialogHeader: string = "";
  buttonSummitTitle: string = "";
  dialogMode: string = "";

  displayModalCenters: boolean = false;

  myForm: FormGroup = this.fb.group({
    centerSearch: ['', [Validators.required]]
  });

  isAdmin: boolean = false;

  savedCenters: Center[] = [];
  centers: Center[] = [];

  currentCenter: Center = null!;

  orderAlphabeticClass: string = "fa-solid fa-arrow-up-a-z";

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();

  @Input() updateDataRooms: boolean = false;
  @Output() updateDataRoomsChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private fb: FormBuilder,
    private doctorsService: DoctorsService,
    private centersService: CentersService,
    private basicHealthZonesService: BasicHealthZonesService,
    private roomsService: RoomsService
  ) { }

  ngOnInit(): void {
    this.getDoctorInformation();

    this.getAllCenters();
    this.subscribeChangesFilter();
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.updateDataRooms && this.updateDataRooms == true){
      this.getAllCenters();

      this.updateDataRooms = false;
      this.updateDataRoomsChange.emit(this.updateDataRooms);
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------ AUXILIAR -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * Center --> detectCenterChanges()
   * --------------------------------------------------------------
   * When a change is detected, the input is emptied and the 
   * center is added to the list
   --------------------------------------------------------------*/
   detectCenterChanges(center: Center): void{
    if(center != null){
      this.myForm.patchValue({
        centerSearch: ""
      });
  
      this.savedCenters.push(center);
      this.centers = this.savedCenters;
  
      this.currentCenter = center;
    }

    this.updateData.emit();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MODALS --------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * string, string, string, Center --> showCenterDialog()
   * --------------------------------------------------------------
   * When clicking on edit a center, a modal opens given certain 
   * parameters for the configuration and the selected center 
   * that you want to edit
   --------------------------------------------------------------*/
   showCenterDialog(title: string, button: string, type: string, center: Center): void{
    this.dialogHeader = title;
    this.buttonSummitTitle = button;
    this.dialogMode = type;

    this.displayModalCenters = true;

    this.currentCenter = center;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- SEARCH - FILTER -------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * subscribeChangesFilter()
   * --------------------------------------------------------------
   * Detects when changes occur in the search engine that helps us 
   * filter the list of centers
   --------------------------------------------------------------*/
   subscribeChangesFilter(): void{
    this.myForm.get('centerSearch')?.valueChanges.subscribe((value: string) => {
      this.filterCenters(value.toLowerCase());
    });
  }

  /**--------------------------------------------------------------
   * string --> filterCenters()
   * --------------------------------------------------------------
   * Filter the list of centers taking into account numerous 
   * things such as name, phone...
   --------------------------------------------------------------*/
   filterCenters(newInfo: string): void{
    this.centers = this.savedCenters.filter(x => (
      x.name.toLowerCase().includes(newInfo) ||
      x.phone.toString().includes(newInfo) ||
      x.rooms?.findIndex(x => x.name.toLowerCase().includes(newInfo)) != -1 ||
      x.basicHealthZone?.name.toLowerCase().includes(newInfo)
    ));
  }

  /**--------------------------------------------------------------
   * orderCentersByAlphabetic()
   * --------------------------------------------------------------
   * By clicking on the sort button, it allows you to sort the 
   * centers in alphabetical order or vice versa
   --------------------------------------------------------------*/
   orderCentersByAlphabetic(): void{
    switch(this.orderAlphabeticClass){
      case "fa-solid fa-arrow-up-a-z":
        this.centers = this.savedCenters.sort((a, b) => (a.name).localeCompare(b.name));
        this.orderAlphabeticClass = "fa-solid fa-arrow-up-z-a";
        break;
      case "fa-solid fa-arrow-up-z-a":
        this.centers = this.savedCenters.sort((a, b) => (b.name).localeCompare(a.name));
        this.orderAlphabeticClass = "fa-solid fa-arrow-up-a-z";
        break;
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getDoctorInformation()
   * --------------------------------------------------------------
   * Get the information about current doctor
   --------------------------------------------------------------*/
   getDoctorInformation(): void{
    this.doctorsService.getDoctorInfor().subscribe((doctor: Doctor | null) => {
      this.isAdmin = doctor!.isAdmin == 1 ? true : false;
    });
  }

  /**--------------------------------------------------------------
   * getAllCenters()
   * --------------------------------------------------------------
   * Get all centers
   --------------------------------------------------------------*/
   getAllCenters(): void{
    this.centersService.getAllCenters().subscribe((centers: Center[]) => {
      this.savedCenters = centers;
      this.centers = centers;

      this.savedCenters.forEach(center => {
        this.getBasicHealtZoneByCenter(center);
        this.getRoomsByCenter(center);
      });
    });
  }

  /**--------------------------------------------------------------
   * getRoomsByCenter()
   * --------------------------------------------------------------
   * Get the rooms of the center
   --------------------------------------------------------------*/
   getRoomsByCenter(center: Center): void{
    this.roomsService.getRoomsByCenter(center.id!.toString()).subscribe((rooms: Room[]) => {
      center.rooms = rooms;

      this.centers = this.savedCenters;
    });
  }

  /**--------------------------------------------------------------
   * getBasicHealtZoneByCenter()
   * --------------------------------------------------------------
   * Get the basic health zone of the center
   --------------------------------------------------------------*/
   getBasicHealtZoneByCenter(center: Center): void{
    this.basicHealthZonesService.getBasicHealthZoneByID(center.idBasicHealthZone.toString()).subscribe((bhz: BasicHealthZone) => {
      center.basicHealthZone = bhz;

      this.centers = this.savedCenters;
    });
  }
}
