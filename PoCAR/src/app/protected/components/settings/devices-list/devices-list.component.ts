import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Coordinator, Device } from 'src/app/protected/interfaces/interfaces';
import { DoctorsService } from 'src/app/protected/services/doctors.service';
import { Doctor } from 'src/app/shared/interfaces/interfaces';
import { DevicesService } from '../../../services/devices.service';
import { CoordinatorsService } from '../../../services/coordinators.service';

@Component({
  selector: 'app-devices-list',
  templateUrl: './devices-list.component.html',
  styleUrls: ['./devices-list.component.scss']
})
export class DevicesListComponent implements OnInit {
  dialogHeader: string = "";
  buttonSummitTitle: string = "";
  dialogMode: string = "";

  displayModalDevices: boolean = false;

  myForm: FormGroup = this.fb.group({
    devicesSearch: ['', [Validators.required]]
  });

  savedDevices: Device[] = [];
  devices: Device[] = [];

  isAdmin: boolean = false;

  currentDevice: Device = null!;

  orderAlphabeticClass: string = "fa-solid fa-arrow-up-a-z";

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private fb: FormBuilder,
    private doctorsService: DoctorsService,
    private devicesService: DevicesService,
    private coordinatorsService: CoordinatorsService
  ) {}

  ngOnInit(): void {
    this.getDoctorInformation();

    this.getAllDevices();
    this.subscribeChangesFilter();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------ AUXILIAR -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * Device --> detectDeviceChanges()
   * --------------------------------------------------------------
   * When a change is detected, the input is emptied and the 
   * device is added to the list
   --------------------------------------------------------------*/
   detectDeviceChanges(device: Device): void{
    if(device != null){
      this.myForm.patchValue({
        devicesSearch: ""
      });
  
      this.savedDevices.push(device);
      this.devices = this.savedDevices;
  
      this.currentDevice = device;
    }

    this.updateData.emit();
  }

  /**--------------------------------------------------------------
   * string --> getSpanishTranslation()
   * --------------------------------------------------------------
   * When a change is detected, the input is emptied and the 
   * device is added to the list
   --------------------------------------------------------------*/
   getSpanishTranslation(type: string): string{
    switch(type){
      case "weight":
        return "Peso";
      case "bloodPreassure":
        return "Tensión";
      case "temperature":
        return "Temperatura";
      case "pulseOximeter":
        return "PulsiOxímetro";
      default:
        return "";
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MODALS --------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * string, string, string, Device --> showDeviceDialog()
   * --------------------------------------------------------------
   * When clicking on edit a device, a modal opens given certain 
   * parameters for the configuration and the selected device 
   * that you want to edit
   --------------------------------------------------------------*/
   showDeviceDialog(title: string, button: string, type: string, device: Device): void{
    this.dialogHeader = title;
    this.buttonSummitTitle = button;
    this.dialogMode = type;

    this.displayModalDevices = true;

    this.currentDevice = device;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- SEARCH - FILTER -------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * subscribeChangesFilter()
   * --------------------------------------------------------------
   * Detects when changes occur in the search engine that helps us 
   * filter the list of devices
   --------------------------------------------------------------*/
   subscribeChangesFilter(): void{
    this.myForm.get('devicesSearch')?.valueChanges.subscribe((value: string) => {
      this.filterDevices(value.toLowerCase());
    });
  }

  /**--------------------------------------------------------------
   * string --> filterDevices()
   * --------------------------------------------------------------
   * Filter the list of devices taking into account numerous 
   * things such as coordinator, mac...
   --------------------------------------------------------------*/
   filterDevices(newInfo: string): void{
    this.devices = this.savedDevices.filter(x => (
      x.mac.toLowerCase().includes(newInfo) ||
      x.service.toLowerCase().includes(newInfo) ||
      x.characteristic.toLowerCase().includes(newInfo) ||
      x.description.toLowerCase().includes(newInfo) ||
      this.getSpanishTranslation(x.type).toLowerCase().includes(newInfo) ||
      x.coordinator?.mac.toLowerCase().includes(newInfo)
    ));
  }

  /**--------------------------------------------------------------
   * orderDevicesByAlphabetic()
   * --------------------------------------------------------------
   * By clicking on the sort button, it allows you to sort the 
   * devices in alphabetical order or vice versa
   --------------------------------------------------------------*/
   orderDevicesByAlphabetic(): void{
    switch(this.orderAlphabeticClass){
      case "fa-solid fa-arrow-up-a-z":
        this.devices = this.savedDevices.sort((a, b) => (a.type).localeCompare(b.type));
        this.orderAlphabeticClass = "fa-solid fa-arrow-up-z-a";
        break;
      case "fa-solid fa-arrow-up-z-a":
        this.devices = this.savedDevices.sort((a, b) => (b.type).localeCompare(a.type));
        this.orderAlphabeticClass = "fa-solid fa-arrow-up-a-z";
        break;
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getDoctorInformation()
   * --------------------------------------------------------------
   * Get the information about current doctor
   --------------------------------------------------------------*/
  getDoctorInformation(): void{
    this.doctorsService.getDoctorInfor().subscribe((doctor: Doctor | null) => {
      this.isAdmin = doctor!.isAdmin == 1 ? true : false;
    });
  }

  /**--------------------------------------------------------------
   * getAllDevices()
   * --------------------------------------------------------------
   * Get all devices
   --------------------------------------------------------------*/
  getAllDevices(): void{
    this.devicesService.getAllDevices().subscribe((devices: Device[]) => {
      this.savedDevices = devices;
      this.devices = devices;

      this.savedDevices.forEach(device => {
        this.getCoordinatorByID(device);
      });
    });
  }

  /**--------------------------------------------------------------
   * Device --> getCoordinatorByID()
   * --------------------------------------------------------------
   * Get coordinator by id
   --------------------------------------------------------------*/
   getCoordinatorByID(device: Device): void{
    this.coordinatorsService.getCoordinatorByID(device.idCoordinator.toString()).subscribe((coordinator: Coordinator) => {
      device.coordinator = coordinator;

      this.devices = this.savedDevices;
    });
  }
}
