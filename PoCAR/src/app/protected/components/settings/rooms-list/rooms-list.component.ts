import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ro } from 'date-fns/locale';
import { Center, Room } from 'src/app/protected/interfaces/interfaces';
import { CentersService } from 'src/app/protected/services/centers.service';
import { CoordinatorsService } from 'src/app/protected/services/coordinators.service';
import { DoctorsService } from 'src/app/protected/services/doctors.service';
import { PatientsApplicationsService } from 'src/app/protected/services/patients-applications.service';
import { RoomsService } from 'src/app/protected/services/rooms.service';
import { Doctor } from 'src/app/shared/interfaces/interfaces';
import { Coordinator, PatientsApplication } from '../../../interfaces/interfaces';

@Component({
  selector: 'app-rooms-list',
  templateUrl: './rooms-list.component.html',
  styleUrls: ['./rooms-list.component.scss']
})
export class RoomsListComponent implements OnInit {

  dialogHeader: string = "";
  buttonSummitTitle: string = "";
  dialogMode: string = "";

  displayModalRooms: boolean = false;

  myForm: FormGroup = this.fb.group({
    roomsSearch: ['', [Validators.required]]
  });

  isAdmin: boolean = false;

  savedRooms: Room[] = [];
  rooms: Room[] = [];

  currentRoom: Room = null!;

  orderAlphabeticClass: string = "fa-solid fa-arrow-up-a-z";

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private fb: FormBuilder,
    private doctorsService: DoctorsService,
    private centersService: CentersService,
    private roomsService: RoomsService,
    private coordinatorsService: CoordinatorsService,
    private patientsApplicationService: PatientsApplicationsService
  ) { }

  ngOnInit(): void {
    this.getDoctorInformation();

    this.getAllRooms();
    this.subscribeChangesFilter();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------ AUXILIAR -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * BasicHealthZone --> detectRoomChanges()
   * --------------------------------------------------------------
   * When a change is detected, the input is emptied and the 
   * room is added to the list
   --------------------------------------------------------------*/
   detectRoomChanges(room: Room): void{
    if(room != null){
      this.myForm.patchValue({
        bhzSearch: ""
      });
  
      this.savedRooms.push(room);
      this.rooms = this.savedRooms;
  
      this.currentRoom = room;
    }

    //TODO: HACER EL EMIT
    this.updateData.emit();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MODALS --------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * string, string, string, Room 
   * --> showRoomDialog()
   * --------------------------------------------------------------
   * When clicking on edit a room, a modal opens given certain 
   * parameters for the configuration and the selected room 
   * that you want to edit
   --------------------------------------------------------------*/
   showRoomDialog(title: string, button: string, type: string, room: Room): void{
    this.dialogHeader = title;
    this.buttonSummitTitle = button;
    this.dialogMode = type;

    this.displayModalRooms = true;

    this.currentRoom = room;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- SEARCH - FILTER -------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * subscribeChangesFilter()
   * --------------------------------------------------------------
   * Detects when changes occur in the search engine that helps us 
   * filter the list of rooms
   --------------------------------------------------------------*/
   subscribeChangesFilter(): void{
    this.myForm.get('roomsSearch')?.valueChanges.subscribe((value: string) => {
      this.filterRooms(value.toLowerCase());
    });
  }

  /**--------------------------------------------------------------
   * string --> filterRooms()
   * --------------------------------------------------------------
   * Filter the list of rooms taking into account numerous 
   * things such as name or centers...
   --------------------------------------------------------------*/
   filterRooms(newInfo: string): void{
    this.rooms = this.savedRooms.filter(x => (
      x.name.toLowerCase().includes(newInfo) ||
      x.center?.name.toLowerCase().includes(newInfo) ||
      x.coordinator?.mac.toLowerCase().includes(newInfo) ||
      x.patientsApplication?.ip.toLowerCase().includes(newInfo) || 
      x.patientsApplication?.mac.toLowerCase().includes(newInfo)
    ));
  }

  /**--------------------------------------------------------------
   * orderRoomsByAlphabetic()
   * --------------------------------------------------------------
   * By clicking on the sort button, it allows you to sort the 
   * rooms in alphabetical order or vice versa
   --------------------------------------------------------------*/
   orderRoomsByAlphabetic(): void{
    switch(this.orderAlphabeticClass){
      case "fa-solid fa-arrow-up-a-z":
        this.rooms = this.savedRooms.sort((a, b) => (a.name).localeCompare(b.name));
        this.orderAlphabeticClass = "fa-solid fa-arrow-up-z-a";
        break;
      case "fa-solid fa-arrow-up-z-a":
        this.rooms = this.savedRooms.sort((a, b) => (b.name).localeCompare(a.name));
        this.orderAlphabeticClass = "fa-solid fa-arrow-up-a-z";
        break;
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getDoctorInformation()
   * --------------------------------------------------------------
   * Get the information about current doctor
   --------------------------------------------------------------*/
  getDoctorInformation(): void{
    this.doctorsService.getDoctorInfor().subscribe((doctor: Doctor | null) => {
      this.isAdmin = doctor!.isAdmin == 1 ? true : false;
    });
  }

  /**--------------------------------------------------------------
   * getAllRooms()
   * --------------------------------------------------------------
   * Get all rooms of data base
   --------------------------------------------------------------*/
  getAllRooms(): void{
    this.roomsService.getAllRooms().subscribe((rooms: Room[]) => {
      this.savedRooms = rooms;
      this.rooms = rooms;

      this.savedRooms.forEach(element => {
        this.getCenterById(element);
        this.getCoordinatorByRoom(element);
        this.getPatientsApplicationByRoom(element);
      });
    });
  }

  /**--------------------------------------------------------------
   * Room --> getCenterById()
   * --------------------------------------------------------------
   * Get the centers of the basic health zones
   --------------------------------------------------------------*/
   getCenterById(room: Room): void{
    this.centersService.getCenterById(room.idCenter!.toString()).subscribe((center: Center) => {
      room.center = center;

      this.rooms = this.savedRooms;
    });
  }

  /**--------------------------------------------------------------
   * Room --> getCoordinatorByRoom()
   * --------------------------------------------------------------
   * Get the the coordinator of the room
   --------------------------------------------------------------*/
   getCoordinatorByRoom(room: Room): void{
    this.coordinatorsService.getCoordinatorByRoomID(room.id!).subscribe((coordinator: Coordinator) => {
      room.coordinator = coordinator;

      this.rooms = this.savedRooms;
    });
  }

  /**--------------------------------------------------------------
   * Room --> getPatientsApplicationByRoom()
   * --------------------------------------------------------------
   * Get the the data of the patient application of the room
   --------------------------------------------------------------*/
   getPatientsApplicationByRoom(room: Room): void{
    this.patientsApplicationService.getPatientApplicationInformationByRoom(room.id!.toString()).subscribe((app: PatientsApplication) => {
      room.patientsApplication = app;

      this.rooms = this.savedRooms;
    });
  }
}
