import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Message, MessageService } from 'primeng/api';
import { BasicHealthZone, Center, PutResponse, PostResponse } from '../../../interfaces/interfaces';
import { BasicHealthZonesService } from '../../../services/basic-healt-zones.service';

@Component({
  selector: 'app-modal-basic-health-zones',
  templateUrl: './modal-basic-health-zones.component.html',
  styleUrls: ['./modal-basic-health-zones.component.scss'],
  providers: [
    MessageService
  ]
})
export class ModalBasicHealthZonesComponent implements OnInit {
  @Input() dialogHeader: string = "";
  @Input() buttonSummitTitle: string = "";

  @Input() displayModalBhz: boolean = false;
  @Output() displayModalBhzChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() dialogMode: string = "";
  @Output() dialogModeChange: EventEmitter<string> = new EventEmitter<string>();

  @Input() currentBHZ: BasicHealthZone = null!;

  @Input() centers: Center[] = [];

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();

  correctUpdate: Message = {severity:'success', summary:'Éxito', detail:'La zona básica de salud ha sido modificado correctamente.'};
  errorUpdate: Message = {severity:'error', summary:'Error', detail:'No se ha podido editar la zona básica de salud.'};
  correctPost: Message = {severity:'success', summary:'Éxito', detail:'La nueva zona básica de salud ha sido creado correctamente.'};
  errorPost: Message = {severity:'error', summary:'Error', detail:'No se ha podido crear una zona básica de salud.'};

  myForm: FormGroup = this.fb.group({
    name: ['', [Validators.required]],
    selectedCenter: [null]
  });

  constructor(
    private fb: FormBuilder,
    private messageService: MessageService,
    private basicHealthZonesService: BasicHealthZonesService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.displayModalBhz && changes.displayModalBhz.currentValue == true){
      if(this.dialogMode == 'put'){
        this.changeDialogInputsEdit();
      }else{
        this.resetFormToInitValues();
      }
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- AUXILIAR ----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * Message --> showAlert()
   * --------------------------------------------------------------
   * Display popup alert
   --------------------------------------------------------------*/
   showAlert(alert: Message): void{
    if(this.messageService){
      this.messageService.clear();
    }
    this.messageService.add(alert);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- FORMS -------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * resetFormToInitValues()
   * --------------------------------------------------------------
   * Reset/empty the form fields. This is useful when you want to 
   * create a new object
   --------------------------------------------------------------*/
   resetFormToInitValues(): void{
    this.myForm.reset();

    this.currentBHZ = {
      id: 0,
      name: "",
      centers: []
    }
  }

  /**--------------------------------------------------------------
   * changeDialogInputsEdit()
   * --------------------------------------------------------------
   * Show the basicHealthZone data in the form
   --------------------------------------------------------------*/
   changeDialogInputsEdit(): void{
    this.myForm.patchValue({
      name: this.currentBHZ.name
    });
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- MODALS ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * closeModal()
   * --------------------------------------------------------------
   * Close modal and notificate the parent component
   --------------------------------------------------------------*/
   closeModal(): void{
    this.displayModalBhz = false;
    this.displayModalBhzChange.emit(this.displayModalBhz);

    this.dialogMode = "";
    this.dialogModeChange.emit(this.dialogMode);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * createNewBasicHealthZoneInformation()
   * --------------------------------------------------------------
   * A new basic health zone in database is created with all the 
   * information collected from the form
   --------------------------------------------------------------*/
   createNewBasicHealthZoneInformation(): void{
    const { name } = this.myForm.value;

    this.currentBHZ.name = name;

    this.basicHealthZonesService.postBasicHealthZone(this.currentBHZ).subscribe((resp: PostResponse) => {
      if(resp.status == 200){
        this.currentBHZ.id = resp.id;

        this.showAlert(this.correctPost);
        this.updateData.emit(this.currentBHZ);
      }else{
        this.showAlert(this.errorPost);
      }
    });
  }

  /**--------------------------------------------------------------
   * updateBasicHealthZoneInformation()
   * --------------------------------------------------------------
   * Update a bhz in database with all the information collected 
   * from the form
   --------------------------------------------------------------*/
  updateBasicHealthZoneInformation(): void{
    const { name } = this.myForm.value;

    this.currentBHZ.name = name;

    this.basicHealthZonesService.putBasicHealthZone(this.currentBHZ).subscribe((resp: PutResponse) => {
      if(resp.status == 200){
        this.showAlert(this.correctUpdate);
      }else{
        this.showAlert(this.errorUpdate);
      }
    });
  }

}
