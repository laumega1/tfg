import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Message, MessageService } from 'primeng/api';
import { Device, PostResponse, PutResponse } from 'src/app/protected/interfaces/interfaces';
import { Coordinator } from '../../../interfaces/interfaces';
import { CoordinatorsService } from '../../../services/coordinators.service';
import { DevicesService } from '../../../services/devices.service';

@Component({
  selector: 'app-modal-devices',
  templateUrl: './modal-devices.component.html',
  styleUrls: ['./modal-devices.component.scss'],
  providers: [
    MessageService
  ]
})
export class ModalDevicesComponent implements OnInit {
  @Input() dialogHeader: string = "";
  @Input() buttonSummitTitle: string = "";

  @Input() displayModalDevice: boolean = false;
  @Output() displayModalDeviceChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() dialogMode: string = "";
  @Output() dialogModeChange: EventEmitter<string> = new EventEmitter<string>();

  @Input() currentDevice: Device = null!;

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();

  correctUpdate: Message = {severity:'success', summary:'Éxito', detail:'El dispositivo ha sido modificado correctamente.'};
  errorUpdate: Message = {severity:'error', summary:'Error', detail:'No se ha podido editar el dispositivo.'};
  correctPost: Message = {severity:'success', summary:'Éxito', detail:'El nuevo dispositivo ha sido creado correctamente.'};
  errorPost: Message = {severity:'error', summary:'Error', detail:'No se ha podido crear un dispositivo.'};
  errorCoordinatorType: Message = {severity:'error', summary:'Error', detail:'El coordinador seleccionado ya dispone de un dispositivo de ese tipo.'};
  errorMAC: Message = {severity:'error', summary:'Error', detail:'Esa dirección MAC ya está en uso, no puede asignar más de un dispositivo a una sala.'};

  coordinators: Coordinator[] = [];

  typesList: string[] = ["Temperatura", "Peso", "Tensión", "PulsiOximetro"];
  defaultServices: string[] = ["0000fff0-0000-1000-8000-00805f9b34fb", "", "0000fff0-0000-1000-8000-00805f9b34fb", "0000ffe0-0000-1000-8000-00805f9b34fb"];
  defaultCharacteristics: string[] = ["0000fff4-0000-1000-8000-00805f9b34fb", "", "0000fff4-0000-1000-8000-00805f9b34fb", "0000ffe1-0000-1000-8000-00805f9b34fb"];
  defaultDescriptions: string[] = ["", "Manufacturer", "", ""];

  myForm: FormGroup = this.fb.group({
    selectedCoordinator: [null, [Validators.required]],
    mac: ['', [Validators.required]],
    type: ['', [Validators.required]],
    service: ['', [Validators.required]],
    characteristic: ['', [Validators.required]],
    description: ['', [Validators.required]]
  });

  constructor(
    private fb: FormBuilder,
    private messageService: MessageService,
    private coordinatorsService: CoordinatorsService,
    private devicesService: DevicesService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.displayModalDevice && changes.displayModalDevice.currentValue == true){
      this.getAllCoordinators();

      if(this.dialogMode == 'put'){
        this.changeDialogInputsEdit();
      }else{
        this.resetFormToInitValues();
      }

      this.subscribeChangesFilter();
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- FORMS -------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * resetFormToInitValues()
   * --------------------------------------------------------------
   * Reset/empty the form fields. This is useful when you want to 
   * create a new object
   --------------------------------------------------------------*/
   resetFormToInitValues(): void{
    this.myForm.reset();

    this.currentDevice = {
      id: 0,
      idCoordinator: 0,
      mac: "",
      service: "",
      characteristic: "",
      description: "",
      type: ""
    }
  }

  /**--------------------------------------------------------------
   * changeDialogInputsEdit()
   * --------------------------------------------------------------
   * Show the basicHealthZone data in the form
   --------------------------------------------------------------*/
  async changeDialogInputsEdit(){
    await this.sleep(1000);

    this.myForm.patchValue({
      selectedCoordinator: this.currentDevice.coordinator,
      mac: this.currentDevice.mac,
      type: this.getSpanishTranslation(this.currentDevice.type),
      service: this.currentDevice.service,
      characteristic: this.currentDevice.characteristic,
      description: this.currentDevice.description
    });

    this.setValidators(this.getSpanishTranslation(this.currentDevice.type));
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- AUXILIAR ----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * subscribeChangesFilter()
   * --------------------------------------------------------------
   * Detects when changes occur in the search engine that helps us 
   * filter the list of devices
   --------------------------------------------------------------*/
   subscribeChangesFilter(): void{
    this.myForm.get('type')?.valueChanges.subscribe((value: string) => {
      this.typesList.forEach(type => {
        if(type == value){
          var index = this.typesList.findIndex(x => x == type);

          this.myForm.patchValue({
            service: this.defaultServices[index],
            characteristic: this.defaultCharacteristics[index],
            description: this.defaultDescriptions[index]
          });
        }

        this.setValidators(value);
      });
    });
  }

  /**--------------------------------------------------------------
   * string --> setValidators()
   * --------------------------------------------------------------
   * Delete or add a required validator in function of device type
   --------------------------------------------------------------*/
  setValidators(value: string): void{
    if(value == "Peso"){
      this.myForm.get("characteristic")!.clearValidators();
      this.myForm.get("characteristic")!.updateValueAndValidity();

      this.myForm.get("service")!.clearValidators();
      this.myForm.get("service")!.updateValueAndValidity();

      this.myForm.get('description')!.setValidators([Validators.required]);
      this.myForm.get('description')!.updateValueAndValidity();
    }else{
      this.myForm.get("characteristic")!.setValidators([Validators.required]);
      this.myForm.get("characteristic")!.updateValueAndValidity();

      this.myForm.get("service")!.setValidators([Validators.required]);
      this.myForm.get("service")!.updateValueAndValidity();

      this.myForm.get('description')!.clearValidators();
      this.myForm.get('description')!.updateValueAndValidity();
    }
  }

  /**--------------------------------------------------------------
   * Message --> showAlert()
   * --------------------------------------------------------------
   * Display popup alert
   --------------------------------------------------------------*/
   showAlert(alert: Message): void{
    if(this.messageService){
      this.messageService.clear();
    }
    this.messageService.add(alert);
  }

  /**--------------------------------------------------------------
   * sleep()
   * --------------------------------------------------------------
   * Await n milliseconds
   --------------------------------------------------------------*/
   sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  /**--------------------------------------------------------------
   * string --> getSpanishTranslation()
   * --------------------------------------------------------------
   * Translate an english string to spanish string
   --------------------------------------------------------------*/
   getSpanishTranslation(type: string): string{
    switch(type){
      case "weight":
        return "Peso";
      case "bloodPreassure":
        return "Tensión";
      case "temperature":
        return "Temperatura";
      case "pulseOximeter":
        return "PulsiOxímetro";
      default:
        return "";
    }
  }

  /**--------------------------------------------------------------
   * string --> getEnglishTranslation()
   * --------------------------------------------------------------
   * Translate a spanish string to english string
   --------------------------------------------------------------*/
   getEnglishTranslation(type: string): string{
    switch(type){
      case "Peso":
        return "weight";
      case "Tensión":
        return "bloodPreassure";
      case "Temperatura":
        return "temperature";
      case "PulsiOxímetro":
        return "pulseOximeter";
      default:
        return "";
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- MODALS ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * closeModal()
   * --------------------------------------------------------------
   * Close modal and notificate the parent component
   --------------------------------------------------------------*/
   closeModal(): void{
    this.displayModalDevice = false;
    this.displayModalDeviceChange.emit(this.displayModalDevice);

    this.dialogMode = "";
    this.dialogModeChange.emit(this.dialogMode);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * checkConditions()
   * --------------------------------------------------------------
   * Makes sure that there is no other device with the same MAC 
   * and that the selected coordinator does not have more than one 
   * device of each type
   --------------------------------------------------------------*/
   checkConditions(): void{
    const { selectedCoordinator, type, mac } = this.myForm.value;

    this.devicesService.getDeviceByTypeAndCoordinatorId(selectedCoordinator.id, this.getEnglishTranslation(type)).subscribe((device: Device) => {
      if(device == null || (this.currentDevice.idCoordinator == selectedCoordinator.id && this.currentDevice.type == this.getEnglishTranslation(type))){
        this.devicesService.getDeviceByMAC(mac).subscribe((device2: Device) => {
          if(device2 == null || this.currentDevice.mac == mac){
            if(this.dialogMode == 'put'){
              this.updateDeviceInformation();
            }else{
              this.createNewDevice();
            }
          }else{
            this.messageService.add(this.errorMAC);
          }
        });
      }else{
        this.messageService.add(this.errorCoordinatorType);
      }
    });
  }

  /**--------------------------------------------------------------
   * getAllCoordinators()
   * --------------------------------------------------------------
   * Get all coordinatos
   --------------------------------------------------------------*/
   getAllCoordinators(): void{
    this.coordinatorsService.getAllCoordinators().subscribe((coordinators: Coordinator[]) => {
      this.coordinators = coordinators;
    });
  }

  /**--------------------------------------------------------------
   * createNewDevice()
   * --------------------------------------------------------------
   * A new device in database is created with all the 
   * information collected from the form
   --------------------------------------------------------------*/
   createNewDevice(): void{
    const { selectedCoordinator, type, mac, service, characteristic, description } = this.myForm.value;

    this.currentDevice.idCoordinator = selectedCoordinator.id;
    this.currentDevice.coordinator = selectedCoordinator;
    this.currentDevice.type = this.getEnglishTranslation(type);
    this.currentDevice.mac = mac;
    this.currentDevice.service = service;
    this.currentDevice.characteristic = characteristic;
    this.currentDevice.description = description;

    this.devicesService.postDevice(this.currentDevice).subscribe((resp: PostResponse) => {
      if(resp.status == 200 && resp.id){
        this.currentDevice.id = resp.id;

        this.showAlert(this.correctPost);
        this.updateData.emit(this.currentDevice);
      }else{
        this.showAlert(this.errorPost);
      }
    });
  }

  /**--------------------------------------------------------------
   * updateDeviceInformation()
   * --------------------------------------------------------------
   * Update a device in database with all the information collected 
   * from the form
   --------------------------------------------------------------*/
   updateDeviceInformation(): void{
    const { selectedCoordinator, type, mac, service, characteristic, description } = this.myForm.value;

    this.currentDevice.idCoordinator = selectedCoordinator.id;
    this.currentDevice.coordinator = selectedCoordinator;
    this.currentDevice.type = this.getEnglishTranslation(type);
    this.currentDevice.mac = mac;
    this.currentDevice.service = service;
    this.currentDevice.characteristic = characteristic;
    this.currentDevice.description = description;

    this.devicesService.putDevice(this.currentDevice).subscribe((resp: PutResponse) => {
      if(resp.status == 200){
        this.showAlert(this.correctUpdate);
      }else{
        this.showAlert(this.errorUpdate);
      }
    });
  }

}
