import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Message, MessageService } from 'primeng/api';
import { Center, PostResponse, Room } from 'src/app/protected/interfaces/interfaces';
import { Coordinator, PatientsApplication, PutResponse } from '../../../interfaces/interfaces';
import { CentersService } from 'src/app/protected/services/centers.service';
import { CoordinatorsService } from '../../../services/coordinators.service';
import { PatientsApplicationsService } from '../../../services/patients-applications.service';
import { RoomsService } from 'src/app/protected/services/rooms.service';

@Component({
  selector: 'app-modal-rooms',
  templateUrl: './modal-rooms.component.html',
  styleUrls: ['./modal-rooms.component.scss'],
  providers: [
    MessageService
  ]
})
export class ModalRoomsComponent implements OnInit {
  @Input() dialogHeader: string = "";
  @Input() buttonSummitTitle: string = "";

  @Input() displayModalRooms: boolean = false;
  @Output() displayModalRoomsChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input() dialogMode: string = "";
  @Output() dialogModeChange: EventEmitter<string> = new EventEmitter<string>();

  @Input() currentRoom: Room = null!;

  centers: Center[] = [];

  @Output() updateData: EventEmitter<any> = new EventEmitter<any>();

  myForm: FormGroup = this.fb.group({
    nameRoom: ['', [Validators.required]],
    selectedCenter: [null, [Validators.required]],
    macCoordinator: ['', [Validators.required]],
    ipApp: ['', [Validators.required]],
    macApp: ['', [Validators.required]]
  });

  correctUpdate: Message = {severity:'success', summary:'Éxito', detail:'La sala ha sido modificado correctamente.'};
  errorUpdate: Message = {severity:'error', summary:'Error', detail:'No se ha podido editar la sala.'};
  correctPost: Message = {severity:'success', summary:'Éxito', detail:'La nueva sala ha sido creada correctamente.'};
  errorPost: Message = {severity:'error', summary:'Error', detail:'No se ha podido crear la sala.'};
  errorMacCoordinator: Message = {severity:'error', summary:'Error', detail:'Ese coordinador ya está asignado a otra sala.'};
  errorIpApp: Message = {severity:'error', summary:'Error', detail:'Esa dirección IP ya está asignado a otra sala.'};
  errorMacApp: Message = {severity:'error', summary:'Error', detail:'Esa MAC ya está asignado a otra sala.'};

  constructor(
    private fb: FormBuilder,
    private messageService: MessageService,
    private centersService: CentersService,
    private coordinatorsService: CoordinatorsService,
    private patientsApplicationService: PatientsApplicationsService,
    private roomsService: RoomsService
  ) { }

  ngOnInit(): void {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.displayModalRooms && changes.displayModalRooms.currentValue == true){
      this.getAllCenters();

      if(this.dialogMode == 'put'){
        this.changeDialogInputsEdit();
      }else{
        this.resetFormToInitValues();
      }
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- AUXILIAR ----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * Message --> showAlert()
   * --------------------------------------------------------------
   * Display popup alert
   --------------------------------------------------------------*/
   showAlert(alert: Message): void{
    if(this.messageService){
      this.messageService.clear();
    }
    this.messageService.add(alert);
  }

  /**--------------------------------------------------------------
   * sleep()
   * --------------------------------------------------------------
   * Await n milliseconds
   --------------------------------------------------------------*/
   sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- FORMS -------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * resetFormToInitValues()
   * --------------------------------------------------------------
   * Reset/empty the form fields. This is useful when you want to 
   * create a new object
   --------------------------------------------------------------*/
   resetFormToInitValues(): void{
    this.myForm.reset();

    this.currentRoom = {
      id: 0,
      name: "",
      idCenter: 0
    }
  }

  /**--------------------------------------------------------------
   * changeDialogInputsEdit()
   * --------------------------------------------------------------
   * Show the basicHealthZone data in the form
   --------------------------------------------------------------*/
   async changeDialogInputsEdit(){
    await this.sleep(1000);
    
    this.myForm.patchValue({
      nameRoom: this.currentRoom.name,
      selectedCenter: this.currentRoom.center,
      macCoordinator: this.currentRoom.coordinator?.mac,
      ipApp: this.currentRoom.patientsApplication?.ip,
      macApp: this.currentRoom.patientsApplication?.mac
    });
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- MODALS ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * closeModal()
   * --------------------------------------------------------------
   * Close modal and notificate the parent component
   --------------------------------------------------------------*/
   closeModal(): void{
    this.displayModalRooms = false;
    this.displayModalRoomsChange.emit(this.displayModalRooms);

    this.dialogMode = "";
    this.dialogModeChange.emit(this.dialogMode);
  }

    //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * createNewRoom()
   * --------------------------------------------------------------
   * A new room in database is created with all the 
   * information collected from the form
   --------------------------------------------------------------*/
   createNewRoom(): void{
    const { nameRoom, selectedCenter, macCoordinator, ipApp, macApp } = this.myForm.value;

    var coordinator: Coordinator = {
      mac: macCoordinator,
      idRoom: 0
    }

    var patientsApplication: PatientsApplication = {
      ip: ipApp,
      mac: macApp,
      idRoom: 0,
      room: null!,
      idCenter: selectedCenter.id,
      center: selectedCenter
    }

    this.currentRoom.name = nameRoom;
    this.currentRoom.center = selectedCenter;
    this.currentRoom.idCenter = selectedCenter.id;
    this.currentRoom.coordinator = coordinator;
    this.currentRoom.patientsApplication = patientsApplication;

    this.roomsService.postRoom(this.currentRoom).subscribe((resp: PostResponse) => {
      if(resp.status == 200 && resp.id){
        this.currentRoom.id = resp.id!;
        this.currentRoom.coordinator!.idRoom = resp.id!;
        this.currentRoom.patientsApplication!.idRoom = resp.id!;

        this.createNewCoordinator();

        this.showAlert(this.correctPost);
      }else{
        this.showAlert(this.errorPost);
      }
    });
  }

  /**--------------------------------------------------------------
   * checkNonRepeatingValues()
   * --------------------------------------------------------------
   * Check that the ip and mac are not already in the database, 
   * since they must be unique
   --------------------------------------------------------------*/
  checkNonRepeatingValues(): void{
    const { macCoordinator, ipApp, macApp } = this.myForm.value;

    this.coordinatorsService.getCoordinatorByMAC(macCoordinator).subscribe((coordinator: Coordinator) => {
      if(coordinator == null || macCoordinator == this.currentRoom.coordinator?.mac){
        this.patientsApplicationService.getPatientApplicationInformationByIP(ipApp).subscribe((app: PatientsApplication) => {
          if(app == null || ipApp == this.currentRoom.patientsApplication?.ip){
            this.patientsApplicationService.getPatientApplicationInformation(macApp).subscribe((app2: PatientsApplication) => {
              if(app2 == null || macApp == this.currentRoom.patientsApplication?.mac){
                if(this.dialogMode == 'put'){
                  this.updateRoomInformation();
                }else{
                  this.createNewRoom();
                }
              }else{
                this.showAlert(this.errorMacApp);
              }
            });
          }else{
            this.showAlert(this.errorIpApp);
          }
        });
      }else{
        this.showAlert(this.errorMacCoordinator);
      }
    });
  }

  /**--------------------------------------------------------------
   * updateRoomInformation()
   * --------------------------------------------------------------
   * Update a room in database with all the information collected 
   * from the form
   --------------------------------------------------------------*/
   updateRoomInformation(): void{
    const { nameRoom, selectedCenter, macCoordinator, ipApp, macApp } = this.myForm.value;

    var coordinator: Coordinator = {
      id: 0,
      mac: macCoordinator,
      idRoom: this.currentRoom.id
    }

    var patientsApplication: PatientsApplication = {
      id: 0,
      ip: ipApp,
      mac: macApp,
      idRoom: this.currentRoom.id,
      room: null!,
      idCenter: selectedCenter.id,
      center: selectedCenter
    }

    this.currentRoom.name = nameRoom;
    this.currentRoom.center = selectedCenter;
    this.currentRoom.idCenter = selectedCenter.id;
    this.currentRoom.coordinator ? this.currentRoom.coordinator.mac = macCoordinator : this.currentRoom.coordinator = coordinator;
    if(this.currentRoom.patientsApplication){
      this.currentRoom.patientsApplication.ip = ipApp;
      this.currentRoom.patientsApplication.mac = macApp;
    }else{
      this.currentRoom.patientsApplication = patientsApplication;
    }

    this.roomsService.putRoom(this.currentRoom).subscribe((resp: PostResponse) => {
      if(resp.status == 200){
        if(this.currentRoom.coordinator!.id == 0){
          this.createNewCoordinator();
        }else{
          this.updateCoordinator();
        }

        this.showAlert(this.correctUpdate);
      }else{
        this.showAlert(this.errorUpdate);
      }
    });
  }

  /**--------------------------------------------------------------
   * getAllCenters()
   * --------------------------------------------------------------
   * Get all centers
   --------------------------------------------------------------*/
   getAllCenters(): void{
    this.centersService.getAllCenters().subscribe((centers: Center[]) => {
      this.centers = centers;
    });
  }

  /**--------------------------------------------------------------
   * createNewCoordinator()
   * --------------------------------------------------------------
   * Create a coordinator into database
   --------------------------------------------------------------*/
  createNewCoordinator(): void{
    this.coordinatorsService.postCoordinator(this.currentRoom.coordinator!).subscribe((resp: PostResponse) => {
      if(resp.status == 200 && resp.id){
        if(this.dialogMode == 'put'){
          if(this.currentRoom.patientsApplication!.id == 0){
            this.createNewPatientsApplication();
          }else{
            this.updatePatientsApplication();
          }
        }else{
          this.currentRoom.coordinator!.id = resp.id!;

          this.createNewPatientsApplication();
        }

        this.showAlert(this.correctPost);
      }else{
        this.showAlert(this.errorPost);
      }
    });
  }

  /**--------------------------------------------------------------
   * updateCoordinator()
   * --------------------------------------------------------------
   * Edit a coordinator into database
   --------------------------------------------------------------*/
   updateCoordinator(): void{
    this.coordinatorsService.putCoordinator(this.currentRoom.coordinator!).subscribe((resp: PutResponse) => {
      if(resp.status == 200){
        if(this.currentRoom.patientsApplication!.id == 0){
            this.createNewPatientsApplication();
        }else{
          this.updatePatientsApplication();
        }

        this.showAlert(this.correctPost);
      }else{
        this.showAlert(this.errorPost);
      }
    });
  }

  /**--------------------------------------------------------------
   * createNewPatientsApplication()
   * --------------------------------------------------------------
   * Create a patients application into database
   --------------------------------------------------------------*/
   createNewPatientsApplication(): void{
    this.patientsApplicationService.postPatientsApplication(this.currentRoom.patientsApplication!).subscribe((resp: PostResponse) => {
      if(resp.status == 200 && resp.id){
        this.currentRoom.patientsApplication!.id = resp.id!;

        if(this.dialogMode == 'post'){
          this.updateData.emit(this.currentRoom);
        }else{
          this.updateData.emit(null!);
        }

        this.showAlert(this.correctPost);
      }else{
        this.showAlert(this.errorPost);
      }
    });
  }

  /**--------------------------------------------------------------
   * updatePatientsApplication()
   * --------------------------------------------------------------
   * Update a patients application into database
   --------------------------------------------------------------*/
   updatePatientsApplication(): void{
    this.currentRoom.patientsApplication!.isEnabled = null!;
    this.patientsApplicationService.putPatientsApplication(this.currentRoom.patientsApplication!).subscribe((resp: PostResponse) => {
      if(resp.status == 200){
        if(this.dialogMode == 'post'){
          this.updateData.emit(this.currentRoom);
        }else{
          this.updateData.emit(null!);
        }

        this.showAlert(this.correctUpdate);
      }else{
        this.showAlert(this.errorUpdate);
      }
    });
  }
}
