import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DoctorsService } from 'src/app/protected/services/doctors.service';
import { Doctor } from 'src/app/shared/interfaces/interfaces';
import { BasicHealthZone, Center } from '../../../interfaces/interfaces';
import { BasicHealthZonesService } from 'src/app/protected/services/basic-healt-zones.service';
import { CentersService } from '../../../services/centers.service';

@Component({
  selector: 'app-basic-health-zones-list',
  templateUrl: './basic-health-zones-list.component.html',
  styleUrls: ['./basic-health-zones-list.component.scss']
})
export class BasicHealthZonesListComponent implements OnInit {

  dialogHeader: string = "";
  buttonSummitTitle: string = "";
  dialogMode: string = "";

  displayModalBasicHealthZone: boolean = false;

  myForm: FormGroup = this.fb.group({
    bhzSearch: ['', [Validators.required]]
  });

  isAdmin: boolean = false;

  savedBasicHealthZone: BasicHealthZone[] = [];
  basicHealthZones: BasicHealthZone[] = [];

  currentBasicHealthZone: BasicHealthZone = null!;

  orderAlphabeticClass: string = "fa-solid fa-arrow-up-a-z";

  @Input() updateData: boolean = false;
  @Output() updateDataChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(
    private fb: FormBuilder,
    private doctorsService: DoctorsService,
    private basicHealthZonesService: BasicHealthZonesService,
    private centersService: CentersService
  ) { }

  ngOnInit(): void {
    this.getDoctorInformation();

    this.getAllBHZ();
    this.subscribeChangesFilter();
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.updateData && this.updateData == true){
      this.getAllBHZ();

      this.updateData = false;
      this.updateDataChange.emit(this.updateData);
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------ AUXILIAR -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * BasicHealthZone --> detectBasicHealthZoneChanges()
   * --------------------------------------------------------------
   * When a change is detected, the input is emptied and the 
   * basic health zone is added to the list
   --------------------------------------------------------------*/
   detectBasicHealthZoneChanges(bhz: BasicHealthZone): void{
    this.myForm.patchValue({
      bhzSearch: ""
    });

    this.savedBasicHealthZone.push(bhz);
    this.basicHealthZones = this.savedBasicHealthZone;

    this.currentBasicHealthZone = bhz;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MODALS --------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * string, string, string, BasicHealthZone 
   * --> showBasicHealthZoneDialog()
   * --------------------------------------------------------------
   * When clicking on edit a bhz, a modal opens given certain 
   * parameters for the configuration and the selected bhz 
   * that you want to edit
   --------------------------------------------------------------*/
   showBasicHealthZoneDialog(title: string, button: string, type: string, bhz: BasicHealthZone): void{
    this.dialogHeader = title;
    this.buttonSummitTitle = button;
    this.dialogMode = type;

    this.displayModalBasicHealthZone = true;

    this.currentBasicHealthZone = bhz;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- SEARCH - FILTER -------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * subscribeChangesFilter()
   * --------------------------------------------------------------
   * Detects when changes occur in the search engine that helps us 
   * filter the list of bhz
   --------------------------------------------------------------*/
   subscribeChangesFilter(): void{
    this.myForm.get('bhzSearch')?.valueChanges.subscribe((value: string) => {
      this.filterBasicHealthZones(value.toLowerCase());
    });
  }

  /**--------------------------------------------------------------
   * string --> filterBasicHealthZones()
   * --------------------------------------------------------------
   * Filter the list of doctors taking into account numerous 
   * things such as name or centers...
   --------------------------------------------------------------*/
   filterBasicHealthZones(newInfo: string): void{
    this.basicHealthZones = this.savedBasicHealthZone.filter(x => (
      x.name.toLowerCase().includes(newInfo) ||
      x.centers?.findIndex(x => x.name.toLowerCase().includes(newInfo)) != -1
    ));
  }

  /**--------------------------------------------------------------
   * orderBasicHealthZonesByAlphabetic()
   * --------------------------------------------------------------
   * By clicking on the sort button, it allows you to sort the 
   * bhz in alphabetical order or vice versa
   --------------------------------------------------------------*/
   orderBasicHealthZonesByAlphabetic(): void{
    switch(this.orderAlphabeticClass){
      case "fa-solid fa-arrow-up-a-z":
        this.basicHealthZones = this.savedBasicHealthZone.sort((a, b) => (a.name).localeCompare(b.name));
        this.orderAlphabeticClass = "fa-solid fa-arrow-up-z-a";
        break;
      case "fa-solid fa-arrow-up-z-a":
        this.basicHealthZones = this.savedBasicHealthZone.sort((a, b) => (b.name).localeCompare(a.name));
        this.orderAlphabeticClass = "fa-solid fa-arrow-up-a-z";
        break;
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getDoctorInformation()
   * --------------------------------------------------------------
   * Get the information about current doctor
   --------------------------------------------------------------*/
  getDoctorInformation(): void{
    this.doctorsService.getDoctorInfor().subscribe((doctor: Doctor | null) => {
      this.isAdmin = doctor!.isAdmin == 1 ? true : false;
    });
  }

  /**--------------------------------------------------------------
   * getAllBHZ()
   * --------------------------------------------------------------
   * Get all basic health zones of data base
   --------------------------------------------------------------*/
  getAllBHZ(): void{
    this.basicHealthZonesService.getAllHealthZones().subscribe((basicHealthZones: BasicHealthZone[]) => {
      this.savedBasicHealthZone = basicHealthZones;
      this.basicHealthZones = basicHealthZones;

      this.savedBasicHealthZone.forEach(element => {
        this.getAllCentersOfBasicHealthZone(element);
      });
    });
  }

  /**--------------------------------------------------------------
   * BasicHealthZone --> getAllCentersOfBasicHealthZone()
   * --------------------------------------------------------------
   * Get the centers of the basic health zones
   --------------------------------------------------------------*/
   getAllCentersOfBasicHealthZone(bhz: BasicHealthZone): void{
    this.centersService.getCenterByBasicHealthZone(bhz.id!.toString()).subscribe((centers: Center[]) => {
      bhz.centers = centers;

      this.basicHealthZones = this.savedBasicHealthZone;
    });
  }

}
