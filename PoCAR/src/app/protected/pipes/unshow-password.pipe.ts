import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'unshowPassword'
})
export class UnshowPasswordPipe implements PipeTransform {

  transform(value: string): string {
    var text = "";
    for (let i = 0; i < value.length; i++) {
      text += "*";
    }
    return text;
  }

}
