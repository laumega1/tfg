import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { CalendarComponent } from './pages/calendar/calendar.component';
import { PatientsComponent } from './pages/patients/patients.component';
import { ConsultationComponent } from './pages/consultation/consultation.component';
import { DoctorsComponent } from './pages/doctors/doctors.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'inicio', component: HomeComponent },
      { path: 'calendario', component: CalendarComponent },
      { path: 'pacientes', component: PatientsComponent },
      { path: 'consulta', component: ConsultationComponent },
      { path: 'ajustes', component: DoctorsComponent },
      { path:'**', redirectTo: 'inicio' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProtectedRoutingModule { }
