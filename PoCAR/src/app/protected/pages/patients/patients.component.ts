import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Patient } from '../../interfaces/interfaces';

@Component({
  selector: 'app-patients',
  templateUrl: './patients.component.html',
  styleUrls: ['./patients.component.scss']
})
export class PatientsComponent implements OnInit {
  currentPatient: Patient = null!;

  constructor(
    private router: Router
  ) {
    try {
      const json = this.router.getCurrentNavigation()?.extras.state;
      this.currentPatient = json!.patient;
    } catch (error) {
      console.log(error, "Not patient selected");
    }
  }

  ngOnInit(): void {
  }


  /**--------------------------------------------------------------
   * Patient --> detectChanges()
   * --------------------------------------------------------------
   * Detect changes when selecting a patient from the list or when 
   * selecting a worse patient from the home page
   --------------------------------------------------------------*/
  detectChanges(patient: Patient): void{
    this.currentPatient = patient;
  }

}
