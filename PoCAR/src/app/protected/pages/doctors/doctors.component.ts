import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.scss']
})
export class DoctorsComponent implements OnInit {

  updateBhzIsNecessary: boolean = false;
  updateCentersIsNecessary: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  /**--------------------------------------------------------------
   * notificateUpdateBhzData()
   * --------------------------------------------------------------
   * This function is called when a center is created or modified 
   * so that the list of bhz is updated
   --------------------------------------------------------------*/
  notificateUpdateBhzData(): void{
    this.updateBhzIsNecessary = true;
  }

  /**--------------------------------------------------------------
   * notificateUpdateCentersData()
   * --------------------------------------------------------------
   * This function is called when a room is created or modified 
   * so that the list of centers is updated
   --------------------------------------------------------------*/
   notificateUpdateCentersData(): void{
    this.updateCentersIsNecessary = true;
  }

}
