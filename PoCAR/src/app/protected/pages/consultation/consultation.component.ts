import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConsultNotification, PutResponse } from '../../interfaces/interfaces';
import { mqtt } from 'aws-iot-device-sdk-v2';
import { Message, MessageService } from 'primeng/api';
import { ConsultsNotificationsService } from '../../services/consults-notifications.service';
import { MqttConsultCheckReceived } from 'src/app/shared/interfaces/interfaces';

@Component({
  selector: 'app-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.scss'],
  providers: [
    MessageService
  ]
})
export class ConsultationComponent implements OnInit {
  clientID: string = localStorage.getItem('id')!+"Consultation";

  connection: mqtt.MqttClientConnection | null = null;

  notification: ConsultNotification = null!;

  updateChartsDataIsNecessary: boolean = false;

  correctAWSConnection: Message = {key: 'consultationKey', severity:'success', summary:'Éxito', detail:'La conexión con AWS se ha establecido correctamente'};
  errorAWSConnection: Message = {key: 'consultationKey', severity:'error', summary:'Error', detail:'No se ha podido establecer la conexión con AWS'};
  errorAWSDisconnect: Message = {key: 'consultationKey', severity:'error', summary:'Error', detail:'Se ha perdido la conexión con AWS, recarge la página para reintentarlo'};
  correctPut: Message = {key: 'consultationKey', severity:'success', summary:'Éxito', detail:'La notificación de consulta ha sido modificada con éxito'};
  errorPut: Message = {key: 'consultationKey', severity:'error', summary:'Error', detail:'No se ha podido modificar la notificación de consulta'};

  constructor(
    private router: Router,
    private messageService: MessageService,
    private consultsNotificationsService: ConsultsNotificationsService
  ) {
    try {
      const json = this.router.getCurrentNavigation()?.extras.state;
      this.notification = json!.notification;
    } catch (error) {
      console.log(error, "Notification not selected");
    }
  }

  ngOnInit(): void {
    if(this.notification == null){
      this.router.navigateByUrl('/');
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- AUXILIAR --------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * notificateUpdateChartData()
   * --------------------------------------------------------------
   * Function so that when a measure is read from the take-measures 
   * component it can be displayed in the graphs
   --------------------------------------------------------------*/
  notificateUpdateChartData(): void{
    this.updateChartsDataIsNecessary = true;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------- MQTT --------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * closeConsultation()
   * --------------------------------------------------------------
   * Updates the query status to end and posts to log out from the 
   * patient app
   --------------------------------------------------------------*/
  closeConsultation(): void{
    const message: MqttConsultCheckReceived = {
      status: 200,
      message: {
        data: {
          action: "end"
        }
      }
    }

    this.connection?.publish('PoCAR/endConsultation/macDevice='+this.notification.macDevice+"-sip="+this.notification.sip, JSON.stringify(message), mqtt.QoS.AtLeastOnce);
    this.notification.action = "end";
    this.updateNotificationInBBDD(this.notification);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * ConsultNotification --> updateNotificationInBBDD()
   * --------------------------------------------------------------
   * It is used for when sending the notification of finished 
   * session that is updated in the database
   --------------------------------------------------------------*/
  updateNotificationInBBDD(notification: ConsultNotification): void{
    this.consultsNotificationsService.putConsultsNotifications(notification).subscribe( (resp: PutResponse) => {
      if(resp.status == 200){
        this.messageService.add(this.correctPut);
        this.router.navigateByUrl('/inicio');
      }else{
        this.messageService.add(this.errorPut);
      }
    });
  }

}
