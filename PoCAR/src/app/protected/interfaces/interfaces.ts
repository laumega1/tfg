import { Time } from "@angular/common"
import { Doctor } from "src/app/shared/interfaces/interfaces"

//---------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------- EVENTS ------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface Day{
    value: number,
    text: string,
    isCurrentDay: boolean,
    month: string,
    year: number
}

export interface EventResume{
    name: string,
    surname: string,
    age: number,
    resume: string,
    time: Time,
    day: number,
    year: number,
    month: number,
    id: number
}

export interface EventResponse{
    status: number,
    method: string,
    resource: string,
    message?: Event[]
}

export interface Event{
    id?: number,
    idPatient: number,
    idCenter: number,
    idDoctor: number,
    description: string,
    primaryColor: string,
    secundaryColor: string,
    startDate: Date,
    endDate: Date,
    center?: Center,
    doctor?: Doctor
}

//---------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------- PATIENTS ------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface PatientResponse{
    status: number,
    method: string,
    resource: string,
    message?: Patient[]
}

export interface Patient{
    id: number,
    name: string,
    surname: string,
    showData?: string
    sip: string,
    age: number,
    idDoctor: number,
    idCenter: number,
    center?: Center,
    basicHealthZone?: BasicHealthZone,
    doctor?: Doctor
}

//---------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------- CENTERS -------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface CenterResponse{
    status: number,
    method: string,
    resource: string,
    message?: Center[]
}

export interface Center{
    id?: number,
    name: string,
    phone: number,
    idBasicHealthZone: number,
    basicHealthZone?: BasicHealthZone,
    rooms?: Room[]
}

//---------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------- BASIC HEALTH ZONES --------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface BasicHealthZoneResponse{
    status: number,
    method: string,
    resource: string,
    message?: BasicHealthZone[]
}

export interface BasicHealthZone{
    id?: number,
    name: string,
    centers?: Center[]
}

//---------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------- POST, PUT, DELETE ---------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface PostResponse{
    status: number,
    method: string,
    resource: string,
    id?: number
}

export interface PutResponse{
    status: number,
    method: string,
    resource: string
}

export interface DeleteResponse{
    status: number,
    method: string,
    resource: string
}

//---------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------- CONSULT NOTIFICATIONS --------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface FilterOptions{
    value: string,
    isSelected: boolean
}

export interface ConsultNotificationResponse{
    status: number,
    method: string,
    resource: string,
    message?: ConsultNotification[]
}

export interface ConsultNotification{
    id?: number,
    macDevice: string,
    idRoom: number,
    idDoctor: number,
    idEvent: number,
    action: string,
    sip: string,
    timestamp?: Date,
    patient?: Patient,
    event?: Event
}

//---------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------- TEMPERATURES ------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface TemperatureResponse{
    status: number,
    method: string,
    resource: string,
    message?: Temperature[]
}

export interface Temperature{
    id?: number,
    idPatient: number,
    value: number,
    unit: string,
    timestamp: Date
}

//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------- WEIGHTS --------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface WeightResponse{
    status: number,
    method: string,
    resource: string,
    message?: Weight[]
}

export interface Weight{
    id?: number,
    idPatient: number,
    value: number,
    unit: string,
    timestamp: Date
}

//---------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------ BLOOD PRESSURES ----------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface BloodPreassure{
    id?: number,
    idPatient: number,
    systolicValue: number,
    diastolicValue: number,
    rateValue: number,
    systolicUnit: string,
    diastolicUnit: string,
    rateUnit: string,
    timestamp: Date
}

export interface BloodPreassureResponse{
    status: number,
    method: string,
    resource: string,
    message?: BloodPreassure[]
}

//---------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------ PULSEOXIMETER ------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface PulseOximeter{
    id?: number,
    idPatient: number,
    oxygenValue: number,
    beatsValue: number,
    oxygenUnit: string,
    beatsUnit: string,
    timestamp: Date
}

export interface PulseOximeterResponse{
    status: number,
    method: string,
    resource: string,
    message?: PulseOximeter[]
}

//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------- CHARTS ---------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface GraphData{
    name: string,
    series: GraphDataValue[]
}

export interface GraphDataValue{
    name: string,
    value: number
}

//---------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------- COORDINATOR -----------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface CoordinatorResponse{
    status: number,
    method: string,
    resource: string,
    message?: Coordinator
}

export interface CoordinatorMultipleResponse{
    status: number,
    method: string,
    resource: string,
    message?: Coordinator[]
}

export interface Coordinator{
    id?: number,
    mac: string,
    idRoom: number
}

//---------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------ DEVICE -------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface DeviceResponse{
    status: number,
    method: string,
    resource: string,
    message?: Device
}

export interface DeviceMultipleResponse{
    status: number,
    method: string,
    resource: string,
    message?: Device[]
}

export interface Device{
    id?: number,
    idCoordinator: number,
    mac: string,
    service: string,
    characteristic: string,
    description: string,
    type: string,
    coordinator?: Coordinator
}

//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------- PATIENTS APPLICATION -------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface PatientsApplicationResponse{
    status: number,
    method: string,
    resource: string,
    message: PatientsApplication
}

export interface PatientsApplication{
    id?: number,
    ip: string,
    mac: string,
    idRoom: number,
    idCenter: number,
    isEnabled?: number,
    center: Center,
    room: Room
}

//---------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------- ROOMS ------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface RoomResponse{
    status: number,
    method: string,
    resource: string,
    message: Room[]
}

export interface Room{
    id: number,
    name: string,
    idCenter: number,
    center?: Center,
    coordinator?: Coordinator,
    patientsApplication?: PatientsApplication
}

//---------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------- TAKE MEASUREMENTS -------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface MeasurementInstruction{
    status: number,
    message: {
        data: {
            type: string,
            typeBBDD: string,
            time: Date,
            description: string,
            idRoom: number,
            macPatientDevice: string,
            coordinator: Coordinator
            sip: string,
            device: Device,
            iconClass?: string,
            measurementResult?: MeasurementResult[]
        },
        error: string
    }
}

export interface MeasurementResult{
    name: string,
    value?: number,
    units: string
}