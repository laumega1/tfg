import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CommonModule } from '@angular/common';
import { ProtectedRoutingModule } from './protected-routing.module';

import { HomeComponent } from './pages/home/home.component';

import { UpcommingEventsComponent } from './components/events/upcomming-events/upcomming-events.component';
import { PrimeNgModule } from '../prime-ng/prime-ng.module';
import { ConsultNotificationsComponent } from './components/consult-notifications/consult-notifications.component';
import { CalendarComponent } from './pages/calendar/calendar.component';
import { FullCalendarComponent } from './components/full-calendar/full-calendar.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalEventsComponent } from './components/events/modal-events/modal-events.component';
import { PatientsListComponent } from './components/patients/patients-list/patients-list.component';
import { ModalPatientsListComponent } from './components/patients/modal-patients-list/modal-patients-list.component';
import { PatientsComponent } from './pages/patients/patients.component';
import { PatientResumeComponent } from './components/patients/patient-resume/patient-resume.component';
import { CurrentPatientEventsComponent } from './components/events/current-patient-events/current-patient-events.component';
import { ModalCurrentPatientEventsComponent } from './components/events/modal-current-patient-events/modal-current-patient-events.component';
import { PatientDataComponent } from './components/charts/patient-data/patient-data.component';
import { GenericChartComponent } from './components/charts/generic-chart/generic-chart.component';
import { ConsultationComponent } from './pages/consultation/consultation.component';
import { VideocallComponent } from './components/consultations/videocall/videocall.component';
import { TakeMeasuresComponent } from './components/consultations/take-measures/take-measures.component';
import { SharedModule } from '../shared/shared.module';
import { DoctorResumeComponent } from './components/settings/doctor-resume/doctor-resume.component';
import { DoctorsComponent } from './pages/doctors/doctors.component';
import { UnshowPasswordPipe } from './pipes/unshow-password.pipe';
import { ModalDoctorsComponent } from './components/settings/modal-doctors/modal-doctors.component';
import { DoctorsListComponent } from './components/settings/doctors-list/doctors-list.component';
import { BasicHealthZonesListComponent } from './components/settings/basic-health-zones-list/basic-health-zones-list.component';
import { ModalBasicHealthZonesComponent } from './components/settings/modal-basic-health-zones/modal-basic-health-zones.component';
import { CentersListComponent } from './components/settings/centers-list/centers-list.component';
import { ModalCentersComponent } from './components/settings/modal-centers/modal-centers.component';
import { RoomsListComponent } from './components/settings/rooms-list/rooms-list.component';
import { ModalRoomsComponent } from './components/settings/modal-rooms/modal-rooms.component';
import { DevicesListComponent } from './components/settings/devices-list/devices-list.component';
import { ModalDevicesComponent } from './components/settings/modal-devices/modal-devices.component';

@NgModule({
  declarations: [
    HomeComponent,
    UpcommingEventsComponent,
    ConsultNotificationsComponent,
    CalendarComponent,
    FullCalendarComponent,
    ModalEventsComponent,
    PatientsListComponent,
    ModalPatientsListComponent,
    PatientsComponent,
    PatientResumeComponent,
    CurrentPatientEventsComponent,
    ModalCurrentPatientEventsComponent,
    PatientDataComponent,
    GenericChartComponent,
    ConsultationComponent,
    VideocallComponent,
    TakeMeasuresComponent,
    DoctorResumeComponent,
    DoctorsComponent,
    UnshowPasswordPipe,
    ModalDoctorsComponent,
    DoctorsListComponent,
    BasicHealthZonesListComponent,
    ModalBasicHealthZonesComponent,
    CentersListComponent,
    ModalCentersComponent,
    RoomsListComponent,
    ModalRoomsComponent,
    DevicesListComponent,
    ModalDevicesComponent
  ],
  imports: [
    CommonModule,
    ProtectedRoutingModule,
    PrimeNgModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    ReactiveFormsModule,
    NgxChartsModule,
    SharedModule
  ]
})
export class ProtectedModule { }
