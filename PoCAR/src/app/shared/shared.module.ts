import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PrimeNgModule } from '../prime-ng/prime-ng.module';
import { AwsMqttComponent } from './aws-mqtt/aws-mqtt.component';
import { LeftBarComponent } from './left-bar/left-bar.component';


@NgModule({
  declarations: [
    AwsMqttComponent,
    LeftBarComponent
  ],
  exports: [
    AwsMqttComponent,
    LeftBarComponent
  ],
  imports: [
    CommonModule,
    PrimeNgModule,
    RouterModule
  ]
})
export class SharedModule { }
