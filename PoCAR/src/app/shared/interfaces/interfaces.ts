import { BasicHealthZone } from '../../protected/interfaces/interfaces';
//---------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------- DOCTORS ----------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface Doctor{
    id: number,
    name: string,
    surname: string,
    email: string,
    password: string,
    isAdmin: boolean | number,
    showData?: string,
    isOnline?: boolean
    basicHealthZones?: BasicHealthZone[]
}

export interface DoctorResponse{
    status: number,
    method: string,
    resource: string,
    message: Doctor[]
}

//---------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------- CONSULT NOTIFICATIONS --------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

export interface MqttConsultNotification{
    status: number,
    message: {
        mac: string,
        data: {
            idRoom: number,
            action: string,
            sip: string,
            idDoctor: number,
            idEvent: number
        }
    }
}

export interface MqttConsultCheckReceived{
    status: number,
    message: {
        data: {
            action: string
        }
    }
}