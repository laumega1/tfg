import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ValidateUserGuard implements CanActivate, CanLoad {

  constructor(
    private service: AuthService,
    private router: Router
  ){}

  /**--------------------------------------------------------------
   * canActivate() --> Observable<boolean> | boolean
   * --------------------------------------------------------------
   * Check if doctor is logged or not
   --------------------------------------------------------------*/
  canActivate(): Observable<boolean> | boolean{
    this.service.isLogged().subscribe( (resp: boolean) => {
      if(!resp){
        this.router.navigateByUrl('/login');
        return false;
      }

      return resp;
    });

    return true;
  }

  /**--------------------------------------------------------------
   * canLoad() --> Observable<boolean> | boolean
   * --------------------------------------------------------------
   * Check if doctor is logged or not
   --------------------------------------------------------------*/
  canLoad(): Observable<boolean> | boolean{
    this.service.isLogged().subscribe( (resp: boolean) => {
      if(!resp){
        this.router.navigateByUrl('/login');
        return false;
      }

      return resp;
    });

    return true;
  }
}
