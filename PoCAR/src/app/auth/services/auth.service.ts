import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthResponse, TokenResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string, string --> login() --> Observable<AuthResponse>
   * --------------------------------------------------------------
   * Makes a request to the database, if it finds the user and its 
   * password is correct, it stores a token and the doctor's id in 
   * the localstorage
   --------------------------------------------------------------*/
  login(email: string, password: string): Observable<AuthResponse>{
    return this.http.get<AuthResponse>(environment.baseUrl+"login?email="+email+"&password="+password)
    .pipe(
      tap((res: AuthResponse) => {
        if(res.status == 200 && res.message.result == true){
          this.saveBasicDataLocalStorage(res.message.token!, res.message.id!);
        }
      }),
      map((res) => {
        return res;
      })
    );
  }

  /**--------------------------------------------------------------
   * string, int --> login()
   * --------------------------------------------------------------
   * Stores the doctor's id and a token in localstorage
   --------------------------------------------------------------*/
  saveBasicDataLocalStorage(token: string, id: number): void{
    localStorage.setItem('token', token);
    localStorage.setItem('id', String(id));
  }

  /**--------------------------------------------------------------
   * deleteBasicDataLocalStorage()
   * --------------------------------------------------------------
   * Deletes the doctor's id and token from localstorage
   --------------------------------------------------------------*/
  deleteBasicDataLocalStorage(): void{
    localStorage.removeItem('token');
    localStorage.removeItem('id');
  }

  /**--------------------------------------------------------------
   * isLogged() --> T/F
   * --------------------------------------------------------------
   * It checks if there is a token stored and if there is one, 
   * if it matches the one in the database. this way without 
   * the token you will not be able to access the data
   --------------------------------------------------------------*/
  isLogged(): Observable<boolean>{
    const token = localStorage.getItem('token');

    return this.http.get<TokenResponse>(environment.baseUrl+"login?token="+token)
    .pipe(
      map((res: TokenResponse) => {
        if(res.status == 200 && res.message.result == true){
          return true;
        }else{
          return false;
        }
      })
    );
  }
}
