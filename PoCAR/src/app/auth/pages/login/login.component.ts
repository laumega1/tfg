import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { Message, MessageService } from 'primeng/api';
import { AuthResponse } from '../../interfaces/interfaces';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {
  errorLogin: Message = {severity:'error', summary:'Error', detail:'El correo o contraseña son erroneos.'};

  myForm: FormGroup = this.fb.group({
    email: ['', [Validators.required]],
    password: ['', [Validators.required]]
  });

  constructor(
    private fb: FormBuilder,
    private messageService: MessageService,
    private service: AuthService, 
    private router: Router
  ) { }

  ngOnInit(): void {
    this.service.isLogged().subscribe( (resp: boolean) => {
      if(resp){
        this.router.navigateByUrl('/');
      }
    });
  }

  /**--------------------------------------------------------------
   * initSession()
   * --------------------------------------------------------------
   * Check if the username and password entered in the form are 
   * correct, if they are correct, it redirects the user to the 
   * home page, if not, it displays an error message
   --------------------------------------------------------------*/
  initSession(): void{
    const {email, password} = this.myForm.value;

    this.service.login(email,password).subscribe( (resp: AuthResponse) => {
      if(resp.status == 200 && resp.message.result){
        this.router.navigateByUrl('/');
      }else{
        this.showAlert();
      }
    });
  }

  /**--------------------------------------------------------------
   * showAlert()
   * --------------------------------------------------------------
   * Shows a popup alert
   --------------------------------------------------------------*/
  showAlert(): void{
    if(this.messageService){
      this.messageService.clear();
    }
    this.messageService.add(this.errorLogin);
  }

}
