import { Doctor } from "src/app/shared/interfaces/interfaces"

export interface AuthResponse{
    status: number,
    method: string,
    resource: string,
    message: AuthMessage
}

export interface AuthMessage{
    result: boolean,
    id?: number,
    token?: string
}

export interface TokenResponse{
    status: number,
    method: string,
    resource: string,
    message: TokenData
}

export interface TokenData{
    result: boolean,
    data?: Doctor
}