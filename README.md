# TFG

***

## Nombre
PoCAR: puntos de atención sanitaria en entornos rurales y remotos

## Descripción
Esto es el repositorio que incluye el trabajo realizado para el TFG (Trabajo Final de Grado) de Laura Melero Garrigós para GTI (Grado en Tecnologías Interactivas) en la UPV (Universidad Politécnica de Valencia). El proyecto está financiado por la fundación Fisabio, dentro del programa Polisabio, en colaboración con médicos del centro de salud de Morella (departamento de salud de Vinarós).

## Documentación
Toda la documentación perteneciente al proyecto se encuentra en la carpeta de _Documentación_. En ella se puede encontrar tanto el documento del TFG, como el de Anexos y los distintos manuales de instrucciones.

## Anexos
En este documento, se profundiza en el testeo completo de la aplicación web, además de mostrar todo el proceso de diseño de las páginas webs (pacientes y medicos). También se incluyen todas las instalaciones, tanto para la parte de hardware como la de software. Para la parte de hardware, se recomienda consultar de manera adicional el Manual Técnico.

## Manuales
Se presentan distintos manuales, los del paciente y médico se basan en aprender a usar las respectivas plataformas. El manual técnico incluye información transcendental sobre como añadir nuevos coordinadores y/o dispositivos.

## Support
Para soporte contactar con la desarroladora del proyecto: laumega1@epsg.upv.es; o con la UPV.

## Autora
Laura Melero Garrigos.

## Estado proyecto
El proyecto está en proceso de evaluación y de testeo completo probando su implementación en un entorno real (un hospital).
