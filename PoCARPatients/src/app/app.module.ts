import { LOCALE_ID, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import localeEs from '@angular/common/locales/es';
import { HashLocationStrategy, LocationStrategy, registerLocaleData } from '@angular/common';
import { PrimeNgModule } from './prime-ng/prime-ng.module';
import { SharedModule } from './shared/shared.module';
registerLocaleData(localeEs, 'es');

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    PrimeNgModule,
    AuthModule,
    SharedModule
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'es' },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy            
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
