import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ValidatePatientGuard } from './guards/validate-patient.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: '',
    loadChildren: () => import('./protected/protected.module').then(m => m.ProtectedModule),
    canActivate: [ValidatePatientGuard],
    canLoad: [ValidatePatientGuard]
  },
  {
    path: '**',
    redirectTo: 'login'
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
