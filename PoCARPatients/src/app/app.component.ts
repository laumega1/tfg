import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './auth/services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'PoCAR Pacientes';

  isLogged: boolean = false;

  constructor(
    private service: AuthService,
    private router: Router
  ){}

  ngOnInit(): void {
    this.router.events.subscribe(() => {
      this.isLogged = this.service.isLogged();
    });
  }
}
