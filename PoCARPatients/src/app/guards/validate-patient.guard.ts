import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ValidatePatientGuard implements CanActivate, CanLoad {

  constructor(
    private service: AuthService,
    private router: Router
  ){}

  canActivate(): Observable<boolean> | boolean{
    if(!this.service.isLogged()){
        this.router.navigateByUrl('/login');
        return false;
    }else{
        return true;
    }
  }

  canLoad(): Observable<boolean> | boolean{
    if(!this.service.isLogged()){
        this.router.navigateByUrl('/login');
        return false;
    }else{
        return true;
    }
  }
}