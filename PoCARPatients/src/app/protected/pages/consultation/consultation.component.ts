import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { io, iot, mqtt } from 'aws-iot-device-sdk-v2';
import { Message, MessageService } from 'primeng/api';
import { MqttConsultCheckReceived } from 'src/app/auth/interfaces/interfaces';
import { environment } from 'src/environments/environment';
import { AuthService } from '../../../auth/services/auth.service';

@Component({
  selector: 'app-consultation',
  templateUrl: './consultation.component.html',
  styleUrls: ['./consultation.component.scss'],
  providers: [
    MessageService
  ]
})
export class ConsultationComponent implements OnInit {

  connection: mqtt.MqttClientConnection | null = null;

  clientID: string = localStorage.getItem('mac')!+"Consult";

  correctAWSConnection: Message = {key: 'notificationsKey', severity:'success', summary:'Éxito', detail:'La conexión con AWS se ha establecido correctamente'};
  errorAWSConnection: Message = {key: 'notificationsKey', severity:'error', summary:'Error', detail:'No se ha podido establecer la conexión con AWS'};
  errorAWSDisconnect: Message = {key: 'notificationsKey', severity:'error', summary:'Error', detail:'Se ha perdido la conexión con AWS, en caso de que no reconecte, llamar al teléfono inferior'};

  constructor(
    private messageService: MessageService,
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MQTT ----------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * subscribeEndConsultation()
   * --------------------------------------------------------------
   * MQTT subscription for when the doctor wants to end the 
   * consultation. This removes the localstorage and returns the 
   * application to login
   --------------------------------------------------------------*/
  subscribeEndConsultation(): void{
    this.connection?.subscribe('PoCAR/endConsultation/macDevice='+localStorage.getItem('mac')+"-sip="+localStorage.getItem('sip'), mqtt.QoS.AtLeastOnce, (topic, payload) => {
      try {
        const decoder = new TextDecoder('utf8');
        const notification: MqttConsultCheckReceived = JSON.parse(decoder.decode(payload));

        if(notification.status == 200 && notification.message.data.action == "end"){
          this.authService.deleteBasicDataLocalStorage();
          this.router.navigateByUrl('/login');
        }
      } catch (error) {
        console.log(error);
      }
    });
  }
}
