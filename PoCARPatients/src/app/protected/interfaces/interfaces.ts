export interface MeasurementInstruction{
    status: number,
    message: {
        data: {
            type: string,
            typeBBDD: string,
            time: Date,
            description: string,
            idRoom: number,
            macPatientDevice: string,
            coordinator: Coordinator
            sip: string,
            device: Device,
            iconClass?: string,
            measurementResult?: MeasurementResult[]
        },
        error: string
    }
}

export interface Coordinator{
    id?: number,
    mac: string,
    idRoom: number
}

export interface Device{
    id?: number,
    idCoordinator: number,
    mac: string,
    service: string,
    characteristic: string,
    description: string,
    type: string
}

export interface MeasurementResult{
    name: string,
    value?: number,
    units: string
}