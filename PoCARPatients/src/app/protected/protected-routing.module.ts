import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultationComponent } from './pages/consultation/consultation.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'consulta', component: ConsultationComponent },
      { path:'**', redirectTo: 'consulta' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProtectedRoutingModule { }