import { Component, OnInit } from '@angular/core';
import { Patient } from 'src/app/auth/interfaces/interfaces';
import { environment } from 'src/environments/environment';
import { VideoSDKMeeting } from '@videosdk.live/rtc-js-prebuilt';
import { PatientsService } from 'src/app/shared/services/patients.service';

@Component({
  selector: 'app-videocall',
  templateUrl: './videocall.component.html',
  styleUrls: ['./videocall.component.scss']
})
export class VideocallComponent implements OnInit {
  patient: Patient = null!;

  isVideocallShowed = false;

  constructor(
    private patientsService: PatientsService
  ) { }

  ngOnInit(): void {
    this.getPatientData();
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- VIDEOCALL -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * startVideocall()
   * --------------------------------------------------------------
   * Start a video call giving it all the parameters and 
   * permissions it needs
   --------------------------------------------------------------*/
  startVideocall(): void{
    const apiKey = environment.videosdkApiKey;
    const meetingId = localStorage.getItem('idRoom')+"-"+localStorage.getItem('sip')+"-"+localStorage.getItem('mac');
    const name = this.patient ? this.patient.name + " " + this.patient.surname : "";

    const config = {
      name: name,
      meetingId: meetingId,
      apiKey: apiKey,

      region: "sg001", // region for new meeting

      containerId: "myVideoCall",
      //redirectOnLeave: "https://www.videosdk.live/",

      micEnabled: true,
      webcamEnabled: true,
      participantCanToggleSelfWebcam: false,
      participantCanToggleSelfMic: false,
      participantCanLeave: false, // if false, leave button won't be visible

      chatEnabled: true,
      screenShareEnabled: false,
      pollEnabled: false,
      whiteboardEnabled: false,
      raiseHandEnabled: true,

      recording: {
        autoStart: false, // auto start recording on participant joined
        enabled: false,
        webhookUrl: "https://www.videosdk.live/callback",
        awsDirPath: `/meeting-recordings/${meetingId}/`, // automatically save recording in this s3 path
      },

      livestream: {
        autoStart: true,
        enabled: true,
      },

      layout: {
        type: "SIDEBAR", // "SPOTLIGHT" | "SIDEBAR" | "GRID"
        priority: "PIN", // "SPEAKER" | "PIN",
        // gridSize: 3,
      },

      branding: {
        enabled: false,
        logoURL:
          "https://static.zujonow.com/videosdk.live/videosdk_logo_circle_big.png",
        name: "Prebuilt",
        poweredBy: false,
      },

      permissions: {
        pin: true,
        askToJoin: false, // Ask joined participants for entry in meeting
        toggleParticipantMic: false, // Can toggle other participant's mic
        toggleParticipantWebcam: false, // Can toggle other participant's webcam
        drawOnWhiteboard: true, // Can draw on whiteboard
        toggleWhiteboard: false, // Can toggle whiteboard
        toggleRecording: false, // Can toggle meeting recording
        toggleLivestream: false, //can toggle live stream
        removeParticipant: false, // Can remove participant
        endMeeting: false, // Can end meeting
        changeLayout: false, //can change layout
      },

      joinScreen: {
        visible: true, // Show the join screen ?
        title: "Visita médica", // Meeting title
        meetingUrl: window.location.href, // Meeting joining url
      },

      /*leftScreen: {
        // visible when redirect on leave not provieded
        actionButton: {
          // optional action button
          label: "Video SDK Live", // action button label
          href: "https://videosdk.live/", // action button href
        },
      },*/

      notificationSoundEnabled: true,

      debug: true, // pop up error during invalid config or netwrok error

      maxResolution: "sd", // "hd" or "sd"

    };

    const meeting = new VideoSDKMeeting();
    meeting.init(config);
    
    this.redimensionCanvas();
  }

  /**--------------------------------------------------------------
   * redimensionCanvas()
   * --------------------------------------------------------------
   * Resize the frame to fit the size we want
   --------------------------------------------------------------*/
  async redimensionCanvas(){
    while(!this.isVideocallShowed){
      try {
        const frame = document.getElementById("videosdk-frame");
        frame!.setAttribute("height", "inherit");
        frame!.style.minHeight = "inherit";

        const videocall = document.getElementById("myVideoCall");
        if(videocall!.childElementCount > 1){
          videocall?.removeChild(videocall!.lastChild!);
        }

        this.isVideocallShowed = true;
      } catch (error) {}

      await this.delay(1000);
    }
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------ AUXILIAR -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * delay()
   * --------------------------------------------------------------
   * Await n milliseconds
   --------------------------------------------------------------*/
  delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getPatientData()
   * --------------------------------------------------------------
   * Get patient data by SIP to personalice te UX
   --------------------------------------------------------------*/
  getPatientData(): void{
    const sip = localStorage.getItem('sip');
    if(sip){
      this.patientsService.getPatientBySIP(sip).subscribe((patient: Patient) => {
        if(patient != null){
          this.patient = patient;
        }
  
        this.startVideocall();
      });
    }
  }

}
