import { Component, OnInit } from '@angular/core';
import { mqtt } from 'aws-iot-device-sdk-v2';
import { MeasurementInstruction, MeasurementResult } from 'src/app/protected/interfaces/interfaces';

@Component({
  selector: 'app-take-measures',
  templateUrl: './take-measures.component.html',
  styleUrls: ['./take-measures.component.scss']
})
export class TakeMeasuresComponent implements OnInit {

  connection: mqtt.MqttClientConnection | null = null;

  clientID: string = localStorage.getItem('mac')!+"TakeMeasures";

  currentMeasureTake: MeasurementInstruction = null!
  measuresList: MeasurementInstruction[] = [];

  constructor() { }

  ngOnInit(): void {
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------------------ AUXILIAR -----------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * int --> sleep()
   * --------------------------------------------------------------
   * Await n milliseconds
   --------------------------------------------------------------*/
  sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MQTT ----------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * subscribeTakeMeasures()
   * --------------------------------------------------------------
   * Subscription to an MQTT topic that allows receiving 
   * notifications of action taking just as the coordinator will. 
   * These notifications will be displayed and another subscriber 
   * will be started to wait for the coordinator's response
   --------------------------------------------------------------*/
  subscribeTakeMeasures(): void{
    this.connection?.subscribe('PoCAR/consultMeasuresNotification/macDevice='+localStorage.getItem('mac')+"-sip="+localStorage.getItem('sip'), mqtt.QoS.AtLeastOnce, (topic, payload) => {
      try {
        const decoder = new TextDecoder('utf8');
        const notification: MeasurementInstruction = JSON.parse(decoder.decode(payload));

        if(notification.status == 200){
          this.currentMeasureTake = notification;
          this.currentMeasureTake.message.data.time = new Date(this.currentMeasureTake.message.data.time);
          this.subscribeTakeMeasurementResponse();
        }
      } catch (error) {
        console.log(error);
      }
    });
  }

  /**--------------------------------------------------------------
   * subscribeTakeMeasurementResponse()
   * --------------------------------------------------------------
   * It waits for the response to the request to obtain a certain 
   * measurement, if everything has gone well, it adds it to the 
   * history
   --------------------------------------------------------------*/
  subscribeTakeMeasurementResponse(): void{
    var topic = "PoCAR/consultMeasuresNotificationResult/macDevice="+this.currentMeasureTake.message.data.macPatientDevice+"-sip="+this.currentMeasureTake.message.data.sip

    this.cancelSubscription(topic);
    
    this.connection?.subscribe(topic, mqtt.QoS.AtLeastOnce, (topic, payload) => {
      try {
        const decoder = new TextDecoder('utf8');
        const notification: MeasurementInstruction = JSON.parse(decoder.decode(payload));

        if(notification.status == 200){
          notification.message.data.time = new Date(notification.message.data.time);

          this.currentMeasureTake = notification;
          
          if(this.currentMeasureTake != null && this.currentMeasureTake.message.error == '' && this.currentMeasureTake.message.data.measurementResult![0].value != undefined){
            this.measuresList.unshift(this.currentMeasureTake);
            this.currentMeasureTake = null!;
          }

          this.connection?.unsubscribe(topic);
        }
      } catch (error) {
        console.log(error);
      }
    });
  }

  /**--------------------------------------------------------------
   * string --> cancelSubscription()
   * --------------------------------------------------------------
   * Asynchronous function that allows canceling a subscription 
   * in case no response is received in x time from the coordinator
   --------------------------------------------------------------*/
  async cancelSubscription(topic: string) {
    await this.sleep(15000);
    
    if(this.currentMeasureTake.message.data.measurementResult![0].value == undefined && this.currentMeasureTake.message.error == ''){
      this.connection?.unsubscribe(topic);
      this.currentMeasureTake.message.error = "No hay conexión";
    }
  }

}
