import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimeNgModule } from '../prime-ng/prime-ng.module';
import { ProtectedRoutingModule } from './protected-routing.module';
import { ConsultationComponent } from './pages/consultation/consultation.component';
import { VideocallComponent } from './components/consultations/videocall/videocall.component';
import { TakeMeasuresComponent } from './components/consultations/take-measures/take-measures.component';
import { SharedModule } from '../shared/shared.module';



@NgModule({
  declarations: [
    ConsultationComponent,
    VideocallComponent,
    TakeMeasuresComponent
  ],
  imports: [
    CommonModule,
    ProtectedRoutingModule,
    PrimeNgModule,
    SharedModule
  ]
})
export class ProtectedModule { }
