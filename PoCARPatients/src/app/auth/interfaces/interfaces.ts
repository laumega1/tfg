import { MenuItem } from 'primeng/api';
export interface PatientsApplicationResponse{
    status: number,
    method: string,
    resource: string,
    message: PatientsApplication
}

export interface PatientsApplication{
    id: number,
    ip: string,
    mac: string,
    idRoom: number,
    idCenter: number,
    isEnabled?: boolean,
    center: Center,
    room: Room
}

export interface Room{
    id: number,
    name: string,
    idCenter: number
}

export interface Center{
    id: number,
    name: string,
    phone: number,
    idBasicHealthZone: number
}

export interface EventResponse{
    status: number,
    method: string,
    resource: string,
    message?: Event[]
}

export interface Event{
    id?: number,
    idPatient: number,
    idCenter: number,
    idDoctor: number,
    description: string,
    primaryColor: string,
    secundaryColor: string,
    startDate: Date,
    endDate: Date,
    centerName: string
}

export interface Doctor{
    id: number,
    name: string,
    surname: string,
    email: string,
    password: string,
    isAdmin: boolean,
    showData?: string,
    isOnline?: boolean
}

export interface DoctorResponse{
    status: number,
    method: string,
    resource: string,
    message: Doctor[]
}

export interface MqttConsultNotification{
    status: number,
    message: {
        mac: string,
        data: {
            idRoom: number,
            action: string,
            sip: string,
            idDoctor: number,
            idEvent: number
        }
    }
}

export interface WaitingItem{
    notification: MqttConsultNotification,
    event: Event,
    menuItem: MenuItem
}

export interface MqttConsultCheckReceived{
    status: number,
    message: {
        data: {
            action: string,
        }
    }
}

export interface PatientResponse{
    status: number,
    method: string,
    resource: string,
    message?: Patient[]
}


export interface Patient{
    id: number,
    name: string,
    surname: string,
    showData?: string
    sip: string,
    age: number,
    hospitals?: Hospital[]
}

export interface Hospital{
    id: number,
    name: string,
    phone: number
}

export interface ConsultNotificationResponse{
    status: number,
    method: string,
    resource: string,
    message?: ConsultNotification[]
}

export interface ConsultNotification{
    id?: number,
    macDevice: string,
    idRoom: number,
    idDoctor: number,
    idEvent: number,
    action: string,
    sip: string,
    timestamp?: Date,
    patient?: Patient,
    event?: Event
}

export interface PutResponse{
    status: number,
    method: string,
    resource: string
}
