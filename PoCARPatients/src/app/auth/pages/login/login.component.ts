import { Component, HostListener, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';

import { HttpClient  } from '@angular/common/http';
import { ConsultNotification, Doctor, Event, MqttConsultCheckReceived, MqttConsultNotification, Patient, PatientsApplication, PutResponse, WaitingItem } from '../../interfaces/interfaces';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { mqtt } from 'aws-iot-device-sdk-v2';
import { Message, MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { DoctorsService } from '../../services/doctors.service';
import { EventsService } from '../../services/events.service';
import { PatientsService } from '../../../shared/services/patients.service';
import { ConsultNotificationsService } from '../../services/consult-notifications.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [
    MessageService,
    ConfirmationService
  ]
})
export class LoginComponent implements OnInit {
  ipAddress: string = "";
  applicationInformation: PatientsApplication = null!;

  myForm: FormGroup = this.fb.group({
    sip: ['', [Validators.required]]
  });

  connection: mqtt.MqttClientConnection | null = null;

  clientID: string = "";

  displayAwaiting: boolean = false;

  currentAction: string = "";

  waitingList: WaitingItem[] = [];
  waitingPatients: MenuItem[] = [];

  correctAWSConnection: Message = {key: 'notificationsKey', severity:'success', summary:'Éxito', detail:'La conexión con AWS se ha establecido correctamente'};
  errorAWSConnection: Message = {key: 'notificationsKey', severity:'error', summary:'Error', detail:'No se ha podido establecer la conexión con AWS'};
  errorAWSDisconnect: Message = {key: 'notificationsKey', severity:'error', summary:'Error', detail:'Se ha perdido la conexión con AWS, en caso de que no reconecte, llamar al teléfono inferior'};
  errorPatientSIP: Message = {key: 'notificationsKey', severity:'error', summary:'Error', detail:'El SIP o identificador no existe en la BBDD, si necesita ayuda notifíquelo'};
  errorDoctorNotOnline: Message = {key: 'notificationsKey', severity:'error', summary:'Error', detail:'El médico no está disponible en este momento, intenteló en unos minutos'};
  errorNotEvent: Message = {key: 'notificationsKey', severity:'error', summary:'Error', detail:'No hay ninguna cita programada para este SIP, porfavor, llame para que le asignen una'};
  errorNotificationAllreadySend: Message = {key: 'notificationsKey', severity:'error', summary:'Error', detail:'El SIP ya ha sido introducido y se han obtenido las citas. Por favor, espere su cita'};

  constructor(
    private authService: AuthService,
    private doctorsService: DoctorsService,
    private eventsService: EventsService,
    private patientsService: PatientsService,
    private consultsNotificionsService: ConsultNotificationsService,
    private router: Router,
    private http: HttpClient,
    private fb: FormBuilder,
    private messageService: MessageService
  ) {
  }

  ngOnInit(): void {
    if(this.authService.isLogged()){
      this.router.navigateByUrl('/');
    }

    this.getIPAddress();
  }

  ngOnDestroy(): void {
    if(this.displayAwaiting == true){
      this.sendNotificationMessage('left');
    }
  }

  @HostListener('window:beforeunload', [ '$event' ])
  beforeUnloadHandler(event: any) {
    this.changeApplicationState(false);
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //------------------------------------------ BASIC NEW NOTIFICATION ---------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * string --> sendNotificationMessage()
   * --------------------------------------------------------------
   * It is in charge of when you want to send an arrival 
   * notification to make sure that the patient exists, 
   * an event and that it is not repeated
   --------------------------------------------------------------*/
  sendNotificationMessage(action: string): void{
    this.currentAction = action;

    this.checkIfPatientExsist();
  }

  /**--------------------------------------------------------------
   * checkIfPatientExsist()
   * --------------------------------------------------------------
   * Given the sip entered in the form, it checks if the patient 
   * exists in the database. If you do, look for your today's 
   * events
   --------------------------------------------------------------*/
  checkIfPatientExsist(): void{
    const {sip} = this.myForm.value;

    this.patientsService.getPatientBySIP(sip).subscribe((patient: Patient) => {
      if(patient != null){
        this.getCurrentDayPatientEvents(patient);
      }else{
        this.messageService.clear();
        this.messageService.add(this.errorPatientSIP);
      }
    });
  }

  /**--------------------------------------------------------------
   * Patient --> getCurrentDayPatientEvents()
   * --------------------------------------------------------------
   * Gets the events of the day for a certain patient. For each 
   * one of them, it first checks if they are from this center 
   * and if they are, if the doctor is online, it sends an arrival 
   * notification
   --------------------------------------------------------------*/
  getCurrentDayPatientEvents(patient: Patient): void{
    this.eventsService.getCurrentDayEventsByPatient(patient.id.toString()).subscribe((events: Event[]) => {
      if(events && events.length > 0){
        var isEventIdentify = false;

        var centersInList = 0;
        var centersList = "";

        events.forEach(event => {
          if(this.applicationInformation.center.id == event.idCenter){
            isEventIdentify = true;

            this.checkIfDoctorIsOnline(event);
          }else{
            centersInList++;
            centersList = (centersList == "") ? event.centerName : (centersList + " ," + event.centerName);
          }
        });

        if(!isEventIdentify && centersInList != 0){
          this.messageService.clear();

          if(centersInList == 1){
            this.messageService.add({key: 'notificationsKey', severity:'error', summary:'Error', detail:'La cita que usted tiene programada es para el centro: '+centersList})
          }else{
            this.messageService.add({key: 'notificationsKey', severity:'error', summary:'Error', detail:'Las citas que usted tiene programadas son para los centros: '+centersList})
          }
        }

      }else{
        this.messageService.clear();
        this.messageService.add(this.errorNotEvent);

        //this.checkIfWantEmergencyAppointment();
      }
    });
  }

  /*checkIfWantEmergencyAppointment(): void{
    const {sip} = this.myForm.value;

    this.confirmationService.confirm({
      message: "No tiene ninguna cita programada para hoy, ¿quiere una cita por urgencias?",
      accept: () => {
        //TODO: GESTIONAR CITA POR URGENCIAS
      },
      acceptLabel: 'Si',
      rejectLabel: 'No',
      rejectButtonStyleClass: 'p-button cancelButton'
  });
  }*/

  /**--------------------------------------------------------------
   * Event --> checkIfDoctorIsOnline()
   * --------------------------------------------------------------
   * Check if the doctor is online and if he is, send him a 
   * notification
   --------------------------------------------------------------*/
  checkIfDoctorIsOnline(event: Event): void{
    this.doctorsService.getDoctorInfor(event.idDoctor.toString()).subscribe((doctor: Doctor | null) => {
      if(doctor != null && doctor.isOnline == true){
        this.sendNotificationToDoctor(doctor, event);
      }else{
        this.messageService.clear();
        this.messageService.add(this.errorDoctorNotOnline)
      }
    });
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- MQTT ----------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * Doctor, Event --> sendNotificationToDoctor()
   * --------------------------------------------------------------
   * If the message has not already been sent, it sends an MQTT 
   * message to notify the arrival of the doctor's appointment
   --------------------------------------------------------------*/
  sendNotificationToDoctor(doctor: Doctor, event: Event): void{
    const {sip} = this.myForm.value;

    const message: MqttConsultNotification = {
      status: 200,
      message: {
        mac: this.applicationInformation.mac,
        data: {
          idRoom: this.applicationInformation.idRoom,
          action: this.currentAction,
          sip: sip,
          idDoctor: doctor.id,
          idEvent: event.id!
        }
      }
    }

    if(!this.isNotificationAllreadySend(message)){
      this.connection?.publish("PoCAR/consultlNotifications/idDoctor="+doctor.id, JSON.stringify(message), mqtt.QoS.AtLeastOnce);
      
      this.awaitConfirmationReceiveNotification(sip, message, event);
    }else{
      this.messageService.clear();
      this.messageService.add(this.errorNotificationAllreadySend);
    }
  }

  /**--------------------------------------------------------------
   * MqttConsultNotification --> isNotificationAllreadySend()
   * --> T/F
   * --------------------------------------------------------------
   * Check if any of the notifications you want to send have 
   * already been sent
   --------------------------------------------------------------*/
  isNotificationAllreadySend(message: MqttConsultNotification): boolean{
    var isDetected = false;

    this.waitingList.forEach(element => {
      if(JSON.stringify(element.notification) == JSON.stringify(message)){
        isDetected = true;
      }
    });

    return isDetected;
  }

  /**--------------------------------------------------------------
   * string, MqttConsultNotification, Event 
   * --> awaitConfirmationReceiveNotification() --> T/F
   * --------------------------------------------------------------
   * Generates a subscription to an aws topic in which it waits 
   * to receive a notification confirmation response
   --------------------------------------------------------------*/
  awaitConfirmationReceiveNotification(sip: string, message: MqttConsultNotification, event: Event): void{
    this.connection?.subscribe('PoCAR/consultlNotifications/macDevice='+this.applicationInformation.mac, mqtt.QoS.AtLeastOnce, (topic, payload) => {
      try {
        const decoder = new TextDecoder('utf8');
        const notification: MqttConsultCheckReceived = JSON.parse(decoder.decode(payload));

        if(notification.status == 200 && notification.message.data.action == "receivedNotification"){
          this.displayAwaiting = true;
          
          this.unshowDisplayWaiting();

          this.setWaitingList(sip, message, event);

          this.awaitConsultation(sip);
        }
      } catch (error) {
        console.log(error);
      }
    });
  }

  /**--------------------------------------------------------------
   * string--> awaitConsultation()
   * --------------------------------------------------------------
   * Create a subscription to an MQTT topic to wait for the doctor 
   * to start the call
   --------------------------------------------------------------*/
  awaitConsultation(sip: string): void{
    this.connection?.subscribe('PoCAR/consultlNotifications/startConsultation/macDevice='+this.applicationInformation.mac+"-sip="+sip, mqtt.QoS.AtLeastOnce, (topic, payload) => {
      try {
        const decoder = new TextDecoder('utf8');
        const notification: MqttConsultCheckReceived = JSON.parse(decoder.decode(payload));

        if(notification.status == 200 && notification.message.data.action == "start"){
          this.changeApplicationState(false);

          this.authService.saveBasicDataLocalStorage(this.applicationInformation.mac, sip, this.applicationInformation.idRoom);

          this.router.navigateByUrl('/');
        }
      } catch (error) {
        console.log(error);
      }
    });
  }

  /**--------------------------------------------------------------
   * subscribeNotifications()
   * --------------------------------------------------------------
   * Function carried out so that when the web page is restarted, 
   * for each element of the list of pending queries, a listener 
   * is created to be able to start the query
   --------------------------------------------------------------*/
  subscribeNotifications(): void{
    this.waitingList.forEach(element => {
      this.awaitConsultation(element.notification.message.data.sip);
    });
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //----------------------------------------------------- AUXILIAR ------------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getIPAddress()
   * --------------------------------------------------------------
   * Gets the IP address to which the device connects. THE USE 
   * OF STATIC IP IS ENCOURAGED. When it finds the IP, it requests 
   * the rest of the information from the application. In this 
   * way we can locate which room which center is
   --------------------------------------------------------------*/
  getIPAddress(): void{
    this.http.get("https://api.ipify.org/?format=json").subscribe((res:any)=>{
      this.ipAddress = res.ip;

      this.getPatientsApplicationInformation()
    });
  }

  /**--------------------------------------------------------------
   * getWelcomeMessage() --> string
   * --------------------------------------------------------------
   * To provide a better user experience, depending on the time 
   * of day it generates a greeting
   --------------------------------------------------------------*/
  getWelcomeMessage(): string{
    var date = new Date;
    var hour = date.getUTCHours();

    if(hour > 6 && hour < 13){
      return "¡Buenos días!";
    }else if(hour > 13 && hour < 20){
      return "¡Buenas tardes!"
    }else{
      return "¡Buenas noches!";
    }
  }

  /**--------------------------------------------------------------
   * int --> sleep()
   * --------------------------------------------------------------
   * Await n milliseconds
   --------------------------------------------------------------*/
  sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  /**--------------------------------------------------------------
   * string, MqttConsultNotification, Event --> setWaitingList()
   * --------------------------------------------------------------
   * Generates the waiting list, when for example the page is 
   * reloaded
   --------------------------------------------------------------*/
  setWaitingList(sip: string, message: MqttConsultNotification, event: Event): void{
    const newItem = {
      notification: message,
      event: event,
      menuItem: {label: "SIP: ..." + sip.substring(sip.length - 3)}
    }

    this.waitingList.push(newItem);
    this.waitingList = this.waitingList.sort((a, b) => new Date(a.event.startDate).getTime() - new Date(b.event.startDate).getTime());

    this.waitingPatients = [];
    this.waitingList.forEach(element => {
      this.waitingPatients.push(element.menuItem);
    });

    var waitingPatients = this.waitingPatients;

    this.waitingPatients = [];
    this.awaitToShowSteeps(waitingPatients);
  }

  /**--------------------------------------------------------------
   * string, MqttConsultNotification, Event --> setWaitingList()
   * --------------------------------------------------------------
   * After receiving the confirmation notification that our 
   * notification has reached the doctor, remove the modal after 
   * 3 seconds
   --------------------------------------------------------------*/
  async unshowDisplayWaiting() {
    await this.sleep(3000);
    this.displayAwaiting = false;
  }

  /**--------------------------------------------------------------
   * awaitToShowSteeps --> setWaitingList()
   * --------------------------------------------------------------
   * Wait a second before showing the waiting list. This is 
   * important because otherwise, it is not updated
   --------------------------------------------------------------*/
  async awaitToShowSteeps(value: MenuItem[]) {
    await this.sleep(1000);
    this.waitingPatients = value;
  }

  //---------------------------------------------------------------------------------------------------------------------------
  //--------------------------------------------------- HTTP PETITIONS --------------------------------------------------------
  //---------------------------------------------------------------------------------------------------------------------------

  /**--------------------------------------------------------------
   * getPatientsApplicationInformation()
   * --------------------------------------------------------------
   * Get the application information
   --------------------------------------------------------------*/
  getPatientsApplicationInformation(): void{
    console.log(this.ipAddress);
    this.authService.getPatientApplicationInformation(this.ipAddress).subscribe((information: PatientsApplication) => {
      this.applicationInformation = information;
      console.log(this.applicationInformation);
      this.clientID = this.applicationInformation.mac+"Login";

      this.changeApplicationState(true);

      this.getWaitingNotificationsList();
    });
  }

  /**--------------------------------------------------------------
   * T/F --> changeApplicationState()
   * --------------------------------------------------------------
   * Change the status of the application from online to not 
   * online and vice versa. It is important to send occuped room
   * when a patient is in consultation too.
   --------------------------------------------------------------*/
  changeApplicationState(value: boolean): void{
    this.authService.putAppState(value, this.applicationInformation.id.toString()).subscribe( (resp: PutResponse) => {
      console.log(resp);
    });
  }

  /**--------------------------------------------------------------
   * T/F --> getWaitingNotificationsList()
   * --------------------------------------------------------------
   * Wait for a SIP to be entered and carry out the entire process 
   * of pertinent checks
   --------------------------------------------------------------*/
  getWaitingNotificationsList(): void{
    // See if there are notifications for today, for this mac and this room
    this.consultsNotificionsService.getCurrentDayWaitingVisits(this.applicationInformation.mac, this.applicationInformation.idRoom.toString()).subscribe((consultNotifications: ConsultNotification[]) => {
      consultNotifications.forEach(consultNotification => {
        // Gets the patient of each notification
        this.patientsService.getPatientBySIP(consultNotification.sip).subscribe((patient: Patient) => {
          if(patient){
            // Gets today's events for that patient
            this.eventsService.getCurrentDayEventsByPatient(patient.id!.toString()).subscribe((events: Event[]) => {
              events.forEach(event => {
                // Check if the event is from this center...if it is, create the notification
                if(event.id == consultNotification.idEvent){
                  const message: MqttConsultNotification = {
                    status: 200,
                    message: {
                      mac: this.applicationInformation.mac,
                      data: {
                        idRoom: consultNotification.idRoom,
                        action: consultNotification.action,
                        sip: consultNotification.sip,
                        idDoctor: consultNotification.idDoctor,
                        idEvent: consultNotification.idEvent
                      }
                    }
                  }

                  this.setWaitingList(consultNotification.sip, message, event);
                }
              });
            });
          }
        });
      });
    });
  }
}
