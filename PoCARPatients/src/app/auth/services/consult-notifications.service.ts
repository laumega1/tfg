import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ConsultNotificationResponse, ConsultNotification } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ConsultNotificationsService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string, string --> getCurrentDayWaitingVisits()
   * --------------------------------------------------------------
   * Get visitor notifications from a device in a room
   --------------------------------------------------------------*/
  getCurrentDayWaitingVisits(mac: string, idRoom: string): Observable<ConsultNotification[]>{
    return this.http.get<ConsultNotificationResponse>(environment.baseUrl+"consultsNotifications?mac="+mac+"&idRoom="+idRoom)
    .pipe(
      map((res: ConsultNotificationResponse) => {
        return res.message ? res.message : [];
      })
    );
  }
}
