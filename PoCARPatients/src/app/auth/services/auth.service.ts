import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { PatientsApplication, PatientsApplicationResponse, PutResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string --> getPatientApplicationInformation()
   * --------------------------------------------------------------
   * Get the patients application information
   --------------------------------------------------------------*/
  getPatientApplicationInformation(ip: string): Observable<PatientsApplication>{
    return this.http.get<PatientsApplicationResponse>(environment.baseUrl+"patientsApplication?ip="+ip)
    .pipe(
      map((res: PatientsApplicationResponse) => {
        return res.message ? res.message : null!;
      })
    );
  }

  /**--------------------------------------------------------------
   * boolean, string --> putAppState()
   * --------------------------------------------------------------
   * Change the status of the application to online or offline
   --------------------------------------------------------------*/
  putAppState(isEnabled: boolean, id: string): Observable<PutResponse>{
    let headers=new HttpHeaders();
    
    let body = {
      id: id,
      isEnabled: isEnabled,
      onlyEnabled: true
    }
    headers.append('Content-Type' , 'application/json');
    
    return this.http.put<PutResponse>(environment.baseUrl+"patientsApplication",body,{headers})
      .pipe(
        map(res=>{
          return res;
        })
      );
  }

  /**--------------------------------------------------------------
   * string, string, number --> saveBasicDataLocalStorage()
   * --------------------------------------------------------------
   * When you enter a video call, store some necessary parameters 
   * in localstorage so that if you have to restart the system, 
   * you stay inside the call
   --------------------------------------------------------------*/
  saveBasicDataLocalStorage(mac: string, sip: string, idRoom: number): void{
    localStorage.setItem('mac', mac);
    localStorage.setItem('sip', sip);
    localStorage.setItem('idRoom', idRoom.toString());
  }

  /**--------------------------------------------------------------
   * deleteBasicDataLocalStorage()
   * --------------------------------------------------------------
   * When a query completes, it deletes the data from the 
   * localstorage in order to exit the application
   --------------------------------------------------------------*/
  deleteBasicDataLocalStorage(): void{
    localStorage.removeItem('mac');
    localStorage.removeItem('sip');
    localStorage.removeItem('idRoom');
  }

  /**--------------------------------------------------------------
   * isLogged()
   * --------------------------------------------------------------
   * Check if a patient is in a video call
   --------------------------------------------------------------*/
  isLogged(): boolean{
    if(localStorage.getItem('mac') && localStorage.getItem('sip') && localStorage.getItem('idRoom')){
        return true;
    }else{
        return false;
    }
  }
}
