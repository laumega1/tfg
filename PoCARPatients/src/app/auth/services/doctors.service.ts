import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Doctor, DoctorResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DoctorsService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string --> getDoctorInfor()
   * --------------------------------------------------------------
   * Obtains all the data of the doctor to know if he is online 
   * at that moment or not
   --------------------------------------------------------------*/
  getDoctorInfor(idDoctor: string): Observable<Doctor | null>{
    return this.http.get<DoctorResponse>(environment.baseUrl+"doctors?id="+idDoctor)
    .pipe(
      map((res: DoctorResponse) => {
        return res.message ? res.message[0] : null;
      })
    );
  }
}
