import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Event, EventResponse } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string --> getCurrentDayEventsByPatient()
   * --------------------------------------------------------------
   * Get the events of a patient for the current day
   --------------------------------------------------------------*/
  getCurrentDayEventsByPatient(id: string): Observable<Event[]>{
    return this.http.get<EventResponse>(environment.baseUrl+"events?idPatient="+id)
    .pipe(
      map((res: EventResponse) => {
        return res.message ? res.message : [];
      })
    );
  }
}
