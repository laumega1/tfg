import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AwsMqttComponent } from './aws-mqtt/aws-mqtt.component';
import { PrimeNgModule } from '../prime-ng/prime-ng.module';


@NgModule({
  declarations: [
    AwsMqttComponent
  ],
  exports: [
    AwsMqttComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    PrimeNgModule
  ]
})
export class SharedModule { }