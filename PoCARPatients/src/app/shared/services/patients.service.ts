import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Patient, PatientResponse } from '../../auth/interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class PatientsService {

  constructor(private http: HttpClient) { }

  /**--------------------------------------------------------------
   * string --> getPatientBySIP() --> Observable<Patient>
   * --------------------------------------------------------------
   * Get a patient information by SIP
   --------------------------------------------------------------*/
  getPatientBySIP(sip: string): Observable<Patient>{
    return this.http.get<PatientResponse>(environment.baseUrl+"patients?sip="+sip)
    .pipe(
      map((res: PatientResponse) => {
        return (res.message && res.message.length > 0) ? res.message[0] : null!;
      })
    );
  }
}
