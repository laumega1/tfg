import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { io, iot, mqtt } from 'aws-iot-device-sdk-v2';
import { Message, MessageService } from 'primeng/api';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-aws-mqtt',
  templateUrl: './aws-mqtt.component.html',
  styleUrls: ['./aws-mqtt.component.scss'],
  providers: [
    MessageService
  ]
})
export class AwsMqttComponent implements OnInit {

  @Input() clientID: string = "";

  @Input() connection: mqtt.MqttClientConnection | null = null;
  @Output() connectionChange: EventEmitter<mqtt.MqttClientConnection | null> = new EventEmitter<mqtt.MqttClientConnection | null>();

  @Output() isConnectionCorrectly: EventEmitter<any> = new EventEmitter<any>();

  correctAWSConnection: Message = {key: 'awsMqttKey', severity:'success', summary:'Éxito', detail:'La conexión con AWS se ha establecido correctamente'};
  errorAWSConnection: Message = {key: 'awsMqttKey', severity:'error', summary:'Error', detail:'No se ha podido establecer la conexión con AWS'};
  errorAWSDisconnect: Message = {key: 'awsMqttKey', severity:'error', summary:'Error', detail:'Se ha perdido la conexión con AWS, recarge la página para reintentarlo'};

  constructor(
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.initAWS();
  }

  /**--------------------------------------------------------------
   * initAWS()
   * --------------------------------------------------------------
   * Manage, configure and create the connection to AWS. Also, 
   * create alerts to notify when the connection has been 
   * established or not
   --------------------------------------------------------------*/
  initAWS(): void{
    this.manageAWSClientAndConnection();

    this.controlConnection();

    this.connection?.connect();
  }

  /**--------------------------------------------------------------
   * manageAWSClientAndConnection()
   * --------------------------------------------------------------
   * Create the configuration to later establish the connection 
   * with AWS. For example, you are given certain credentials
   --------------------------------------------------------------*/
  manageAWSClientAndConnection(): void{
    const client_bootstrap = new io.ClientBootstrap();
    const myClient = iot.AwsIotMqttConnectionConfigBuilder
      .new_with_websockets({
        region: environment.region,
        credentials_provider: undefined!, //Credential provider is part of auth which isn't available for browser.
        proxy_options: undefined
    });

    var config = myClient.with_client_id(this.clientID) // unique ClientId for each connection.
    .with_credentials(environment.region, environment.accessKey, environment.secretKey, environment.sessionToken) //because we had no credentials provider set, we have to manually add them, I recommend using cognito.
    .with_endpoint(environment.endPoint)
    .build();

    const client = new mqtt.MqttClient(client_bootstrap);

    this.connection = client.new_connection(config);
  }

  /**--------------------------------------------------------------
   * controlConnection()
   * --------------------------------------------------------------
   * Set different listeners for when the connection succeeds, 
   * disconnects, or fails
   --------------------------------------------------------------*/
  controlConnection(): void{
    this.connection?.on('connect', (session_present) => {
      console.log(`Connection correct: ${session_present}`);

      this.connectionChange.emit(this.connection);
      this.isConnectionCorrectly.emit();
      
      this.messageService.clear();
      this.messageService.add(this.correctAWSConnection);
    });

    this.connection?.on('disconnect', () => {
      this.messageService.clear();
      this.messageService.add(this.errorAWSDisconnect);
    });

    this.connection?.on('error', (error) => {
      this.messageService.clear();
      this.messageService.add(this.errorAWSConnection);
    });
  }
}
