<?php
    $output['status'] = http_response_code();

    if($output['method'] == 'get'){
        $output['message'] = $consultNotification;
    }else{
        $output['result'] = $result;
    }

    echo json_encode($output);
?>