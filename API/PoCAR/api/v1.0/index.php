<?php
    require_once('includes/conexion.php');

    $method = strtolower($_SERVER['REQUEST_METHOD']);

    $uri = explode('v1.0/', parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH))[1];

    $uri_array = explode('/', $uri);
    $resource = array_shift($uri_array);

    $query_params= $_GET;

    $uri_params= array();

    for($i=0; $i < count($uri_array); $i++){
        
        if($uri_array[$i] != "")$uri_params[$uri_array[$i]]= $uri_array[++$i];
    };

    $body_params= json_decode(file_get_contents('php://input'), true);

    $form_params= $_POST;

    $output = array();
    $output['method'] = $method;
    $output['resource'] = $resource;

    require('controllers/'.$resource.'_controller.php');
?>