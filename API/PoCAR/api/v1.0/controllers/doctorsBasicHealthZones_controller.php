<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: GET,POST,PUT,DELETE");
    header("Access-Control-Max-Age: 3600");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require('models/doctorsBasicHealthZones/doctorsBasicHealthZones_model_'.$method.'.php');

    require('views/doctorsBasicHealthZones_view_json.php');
?>