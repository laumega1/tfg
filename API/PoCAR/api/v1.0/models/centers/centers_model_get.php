<?php
    $centers = null;

    if(isset($query_params['id'])){
        $response = sqlGetCenterById($query_params, $conexion);

        $centers = generateResponse($response);

        $result = generateResult($centers);
    }
    if(isset($query_params['idBasicHealthZone'])){
        $response = sqlGetCenterByBasicHealthZone($query_params, $conexion);

        $centers = generateResponse($response);

        $result = generateResult($centers);
    }else if(isset($query_params['all'])){
        $response = sqlAllCenters($conexion);

        $centers = generateResponse($response);

        $result = generateResult($centers);
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlAllCenters($conexion){
        $sql = 'SELECT `centers`.*
        FROM `centers`';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetCenterById($query_params, $conexion){
        $sql = 'SELECT `centers`.*
        FROM `centers`
        WHERE `centers`.id = "'.$query_params['id'].'"';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetCenterByBasicHealthZone($query_params, $conexion){
        $sql = 'SELECT `centers`.*
        FROM `centers`
        WHERE `centers`.idBasicHealthZone = "'.$query_params['idBasicHealthZone'].'"';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function generateResponse($response){
        $centers = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                array_push($centers,$row);
            }
        }

        return $centers;
    }

    function generateResult($centers){
        if($centers != null){
            return true;
        }
        else{
            return false;
        }
    }
?>