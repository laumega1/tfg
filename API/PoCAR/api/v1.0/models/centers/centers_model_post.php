<?php
    $centers = null;

    $response = sqlPostNewCenter($body_params, $conexion);
    
    if($response != null){
        $output["id"] = mysqli_insert_id($conexion);
        $result = true;
    }
    else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPostNewCenter($body_params, $conexion){
        $sql = 'INSERT INTO `centers` (`name`, `phone`, `idBasicHealthZone`)
        VALUES (
        "'.$body_params['name'].'",
        "'.$body_params['phone'].'",
        "'.$body_params['idBasicHealthZone'].'")';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>