<?php
    $weights = null;

    if(isset($query_params['idPatient']) && isset($query_params['startDate']) && isset($query_params['endDate'])){
        $response = sqlGetWeightsBetweenTwoDates($query_params, $conexion);

        $weights = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                array_push($weights, $row);
            }
        }

        if($weights != null){
            $result = true;
        }
        else{
            $result = false;
        }
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetWeightsBetweenTwoDates($query_params, $conexion){
        $start = date('Y/m/d H:i:s', strtotime($query_params['startDate']));
        $end = date('Y/m/d H:i:s', strtotime($query_params['endDate']));

        $sql = 'SELECT *
        FROM `weights` 
        WHERE `weights`.idPatient = "'.$query_params['idPatient'].'"
        AND `weights`.timestamp BETWEEN "'.$start.'" AND "'.$end.'"
        ORDER BY `weights`.timestamp';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>