<?php
    $temperatures = null;

    if(isset($query_params['idPatient']) && isset($query_params['startDate']) && isset($query_params['endDate'])){
        $response = sqlGetTemperaturesBetweenTwoDates($query_params, $conexion);

        $temperatures = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                array_push($temperatures, $row);
            }
        }

        if($temperatures != null){
            $result = true;
        }
        else{
            $result = false;
        }
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetTemperaturesBetweenTwoDates($query_params, $conexion){
        $start = date('Y/m/d H:i:s', strtotime($query_params['startDate']));
        $end = date('Y/m/d H:i:s', strtotime($query_params['endDate']));

        $sql = 'SELECT *
        FROM `temperatures` 
        WHERE `temperatures`.idPatient = "'.$query_params['idPatient'].'"
        AND `temperatures`.timestamp BETWEEN "'.$start.'" AND "'.$end.'"
        ORDER BY `temperatures`.timestamp';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>