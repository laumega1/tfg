<?php
    $temperatures = null;

    $response = sqlPostNewTemperature($body_params, $conexion);
    
    if($response != null){
        $output["id"] = mysqli_insert_id($conexion);
        $result = true;
    }
    else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPostNewTemperature($body_params, $conexion){
        $sql = 'INSERT INTO `temperatures` (`idPatient`, `value`, `unit`)
        VALUES (
        "'.$body_params['idPatient'].'",
        "'.$body_params['value'].'",
        "'.$body_params['unit'].'")';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>