<?php
    $rooms = null;

    if(isset($query_params['idCenter'])){
        $response = sqlGetRoomsByCenter($query_params, $conexion);

        $rooms = generateResponse($response);

        $result = generateResult($rooms);
    }else if(isset($query_params['all'])){
        $response = sqlGetAllRooms($conexion);

        $rooms = generateResponse($response);

        $result = generateResult($rooms);
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetRoomsByCenter($query_params, $conexion){
        $sql = 'SELECT `rooms`.*
        FROM `rooms`
        WHERE `rooms`.idCenter = "'.$query_params['idCenter'].'"';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetAllRooms($conexion){
        $sql = 'SELECT *
        FROM `rooms`';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function generateResponse($response){
        $rooms = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                array_push($rooms,$row);
            }
        }

        return $rooms;
    }

    function generateResult($rooms){
        if($rooms != null){
            return true;
        }
        else{
            return false;
        }
    }
?>