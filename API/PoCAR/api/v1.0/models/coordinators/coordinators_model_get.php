<?php
    $coordinators = null;

    if(isset($query_params['id'])){
        $response = sqlGetCoordinatorById($query_params, $conexion);

        $coordinators = generateResponse($response);

        $result = generateResult($coordinators);
    }
    else if(isset($query_params['idRoom'])){
        $response = sqlGetCoordinatorByRoom($query_params, $conexion);

        $coordinators = generateResponse($response);

        $result = generateResult($coordinators);
    }else if(isset($query_params['mac'])){
        $response = sqlGetCoordinatorByMAC($query_params, $conexion);

        $coordinators = generateResponse($response);

        $result = generateResult($coordinators);
    }else if(isset($query_params['all'])){
        $response = sqlGetAllCoordinators($conexion);

        $coordinators = generateMultipleResponse($response);

        $result = generateResult($coordinators);
    }
    else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetAllCoordinators($conexion){
        $sql = 'SELECT *
        FROM `coordinators`';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetCoordinatorById($query_params, $conexion){
        $sql = 'SELECT *
        FROM `coordinators` 
        WHERE `coordinators`.id = "'.$query_params['id'].'"';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetCoordinatorByRoom($query_params, $conexion){
        $sql = 'SELECT *
        FROM `coordinators` 
        WHERE `coordinators`.idRoom = "'.$query_params['idRoom'].'"';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetCoordinatorByMAC($query_params, $conexion){
        $sql = 'SELECT *
        FROM `coordinators` 
        WHERE `coordinators`.mac = "'.$query_params['mac'].'"';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function generateResponse($response){
        $coordinators = null;
        
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                $coordinators = $row;
            }
        }

        return $coordinators;
    }

    function generateMultipleResponse($response){
        $coordinators = array();
        
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                array_push($coordinators,$row);
            }
        }

        return $coordinators;
    }

    function generateResult($coordinators){
        if($coordinators != null){
            return true;
        }
        else{
            return false;
        }
    }
?>