<?php
    $pulseOximeter = null;

    $response = sqlPostNewPulseOximeter($body_params, $conexion);
    
    if($response != null){
        $output["id"] = mysqli_insert_id($conexion);
        $result = true;
    }
    else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPostNewPulseOximeter($body_params, $conexion){
        $sql = 'INSERT INTO `pulseoximeter` (`idPatient`, `oxygenValue`, `beatsValue`, `oxygenUnit`, `beatsUnit`)
        VALUES (
        "'.$body_params['idPatient'].'",
        "'.$body_params['oxygenValue'].'",
        "'.$body_params['beatsValue'].'",
        "'.$body_params['oxygenUnit'].'",
        "'.$body_params['beatsUnit'].'")';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>