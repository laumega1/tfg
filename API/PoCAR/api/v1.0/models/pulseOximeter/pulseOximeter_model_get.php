<?php
    $pulseOximeter = null;

    if(isset($query_params['idPatient']) && isset($query_params['startDate']) && isset($query_params['endDate'])){
        $response = sqlGetPulseOximeterBetweenTwoDates($query_params, $conexion);

        $pulseOximeter = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                array_push($pulseOximeter, $row);
            }
        }

        if($pulseOximeter != null){
            $result = true;
        }
        else{
            $result = false;
        }
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetPulseOximeterBetweenTwoDates($query_params, $conexion){
        $start = date('Y/m/d H:i:s', strtotime($query_params['startDate']));
        $end = date('Y/m/d H:i:s', strtotime($query_params['endDate']));

        $sql = 'SELECT *
        FROM `pulseoximeter` 
        WHERE `pulseoximeter`.idPatient = "'.$query_params['idPatient'].'"
        AND `pulseoximeter`.timestamp BETWEEN "'.$start.'" AND "'.$end.'"
        ORDER BY `pulseoximeter`.timestamp';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>