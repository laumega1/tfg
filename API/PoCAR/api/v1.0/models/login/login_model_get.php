<?php
    $doctor = null;

    if(isset($query_params['email']) && isset($query_params['password'])){
        $hash = password_hash($query_params['password'], PASSWORD_DEFAULT);

        $sql = 'SELECT *
                FROM `doctors` 
                WHERE `email`="'.$query_params['email'].'"';
                
        $doctor = array();
        $response = mysqli_query($conexion, $sql);
        if($response){
            while($row = mysqli_fetch_assoc($response)) {
                if(password_verify($query_params['password'], $row['password'])){
                    array_push ($doctor,$row);
                }
            }
        }
        
        if(sizeof($doctor) > 0){
            $result = true;

            $output['message']['token'] = password_hash($doctor[0]['id'], PASSWORD_DEFAULT);
            $output['message']['id'] = $doctor[0]['id'];
        }
        else{
            $result = false;
        }

    }else if(isset($query_params['token'])){
        $sql = 'SELECT *
                FROM `doctors`';

        $response = mysqli_query($conexion, $sql);
        if($response){
            $result = false;

            while($row = mysqli_fetch_assoc($response)) {
                if(password_verify($row['id'], $query_params['token'])){
                    $output['message']['data'] = $row;
                    $result = true;
                }
            }
        }

    }else{
        $result = false;
    }
?>