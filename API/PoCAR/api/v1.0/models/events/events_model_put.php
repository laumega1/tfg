<?php
    $event = null;

    if(isset($body_params['id'])){
        $response = sqlPutEvent($body_params, $conexion);
    
        if($response != null){
            $result = true;
        }
        else{
            $result = false;
        }

    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPutEvent($body_params, $conexion){
        $start = date('Y/m/d H:i:s', strtotime($body_params['startDate']));
        $end = date('Y/m/d H:i:s', strtotime($body_params['endDate']));

        $sql = 'UPDATE `events` 
        SET `idPatient`="'.$body_params['idPatient'].'",`idCenter`="'.$body_params['idCenter'].'",`idDoctor`="'.$body_params['idDoctor'].'",
        `description`="'.$body_params['description'].'",`primaryColor`="'.$body_params['primaryColor'].'",`secundaryColor`="'.$body_params['secundaryColor'].'",
        `startDate`="'.$start.'",`endDate`="'.$end.'"
        WHERE `id`='.$body_params['id'].'';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>