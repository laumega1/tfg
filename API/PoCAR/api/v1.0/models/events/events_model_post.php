<?php
    $event = null;

    $response = sqlPostNewEvent($body_params, $conexion);
    
    if($response != null){
        $output["id"] = mysqli_insert_id($conexion);
        $result = true;
    }
    else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPostNewEvent($body_params, $conexion){
        $start = date('Y/m/d H:i:s', strtotime($body_params['startDate']));
        $end = date('Y/m/d H:i:s', strtotime($body_params['endDate']));

        $sql = 'INSERT INTO `events` (`idPatient`, `idCenter`, `idDoctor`, `description`, `primaryColor`, `secundaryColor`, `startDate`, `endDate`)
        VALUES (
        "'.$body_params['idPatient'].'",
        "'.$body_params['idCenter'].'",
        "'.$body_params['idDoctor'].'",
        "'.$body_params['description'].'",
        "'.$body_params['primaryColor'].'",
        "'.$body_params['secundaryColor'].'",
        "'.$start.'",
        "'.$end.'")';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>