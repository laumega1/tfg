<?php
    $events = null;

    if(isset($query_params['idDoctor'])){
        if(isset($query_params['startDate']) && isset($query_params['endDate'])){
            $response = sqlGetAllDoctorEventsBetweenTwoDates($query_params, $conexion);

            $events = generateObjectResponse($response);

            $result = generateResult($events);
        }else if(isset($query_params['idPatient'])){
            $response = sqlGetAllFuturePatientEvents($query_params, $conexion);

            $events = generateObjectResponse($response);

            $result = generateResult($events);
        }else{
            $response = sqlGetAllDoctorEvents($query_params, $conexion);

            $events = generateObjectResponse($response);

            $result = generateResult($events);
        }
    }else if(isset($query_params['idPatient'])){
            $response = sqlGetCurrentDayPatientEvents($query_params, $conexion);

            $events = generateResponse($response);

            $result = generateResult($events);
    }
    else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetAllDoctorEvents($query_params, $conexion){
        $sql = 'SELECT `events`.*, 
        `centers`.name AS centerName, `centers`.phone AS centerPhone, `centers`.idBasicHealthZone AS idBasicHealthZone,
        `doctors`.name AS nameDoctor, `doctors`.surname AS surnameDoctor, `doctors`.email AS emailDoctor, `doctors`.password AS passwordDoctor, `doctors`.isAdmin AS isAdminDoctor, `doctors`.isOnline AS isOnlineDoctor
        FROM `events`, `doctors`, `doctorsbasichealthzones`, `centers`
        WHERE `events`.idDoctor = "'.$query_params['idDoctor'].'"
        AND `centers`.id = `events`.`idCenter`
        AND `doctorsbasichealthzones`.idBasicHealthZone = `centers`.idBasicHealthZone
        AND `doctors`.id = `events`.idDoctor
        ORDER BY `events`.startDate';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetAllDoctorEventsBetweenTwoDates($query_params, $conexion){
        $sql = 'SELECT `events`.*, 
        `centers`.name AS centerName, `centers`.phone AS centerPhone, `centers`.idBasicHealthZone AS idBasicHealthZone,
        `doctors`.name AS nameDoctor, `doctors`.surname AS surnameDoctor, `doctors`.email AS emailDoctor, `doctors`.password AS passwordDoctor, `doctors`.isAdmin AS isAdminDoctor, `doctors`.isOnline AS isOnlineDoctor
        FROM `events`, `doctors`, `doctorsbasichealthzones`, `centers` 
        WHERE `events`.idDoctor = "'.$query_params['idDoctor'].'"
        AND `centers`.id = `events`.`idCenter`
        AND `doctorsbasichealthzones`.idBasicHealthZone = `centers`.idBasicHealthZone
        AND `doctors`.id = `events`.idDoctor
        AND `events`.startDate BETWEEN "'.$query_params['startDate'].'" AND "'.$query_params['endDate'].'"
        ORDER BY `events`.startDate';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetAllFuturePatientEvents($query_params, $conexion){
        $sql = 'SELECT `events`.*, 
        `centers`.name AS centerName, `centers`.phone AS centerPhone, `centers`.idBasicHealthZone AS idBasicHealthZone,
        `doctors`.name AS nameDoctor, `doctors`.surname AS surnameDoctor, `doctors`.email AS emailDoctor, `doctors`.password AS passwordDoctor, `doctors`.isAdmin AS isAdminDoctor, `doctors`.isOnline AS isOnlineDoctor
        FROM `events`, `doctors`, `doctorsbasichealthzones`, `centers`, `patients` 
        WHERE `events`.idDoctor = "'.$query_params['idDoctor'].'"
        AND `patients`.id = "'.$query_params['idPatient'].'"
        AND `events`.idPatient = `patients`.id
        AND `centers`.id = `events`.`idCenter`
        AND `doctorsbasichealthzones`.idBasicHealthZone = `centers`.idBasicHealthZone
        AND `doctors`.id = `events`.idDoctor
        AND `events`.startDate >= CURRENT_TIMESTAMP
        ORDER BY `events`.startDate';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetCurrentDayPatientEvents($query_params, $conexion){
        $sql = 'SELECT `events`.*, `centers`.name AS centerName
        FROM `events`, `centers` 
        WHERE `events`.idPatient = "'.$query_params['idPatient'].'"
        AND (DATE(`events`.startDate) = CURDATE() OR DATE(`events`.endDate) = CURDATE())
        AND `centers`.id = `events`.idCenter
        ORDER BY `events`.startDate';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function generateObjectResponse($response){
        $patients = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                $patient = array(
                    "id" => $row['id'],
                    "idPatient" =>  $row['idPatient'],
                    "idCenter" => $row['idCenter'],
                    "idDoctor" => $row['idDoctor'],
                    "description" =>  $row['description'],
                    "primaryColor" => $row['primaryColor'],
                    "secundaryColor" => $row['secundaryColor'],
                    "startDate" =>  $row['startDate'],
                    "endDate" =>  $row['endDate'],
                    "center" => array(
                        "id" =>  $row['idCenter'],
                        "name" => $row['centerName'],
                        "phone" => $row['centerPhone'],
                        "idBasicHealthZone" => $row['idBasicHealthZone']
                    ),
                    "doctor" => array(
                        "id" =>  $row['idDoctor'],
                        "name" =>  $row['nameDoctor'],
                        "surname" =>  $row['surnameDoctor'],
                        "showData" => $row['nameDoctor']." ".$row['surnameDoctor'],
                        "email" =>  $row['emailDoctor'],
                        "password" =>  $row['passwordDoctor'],
                        "isAdmin" =>  $row['isAdminDoctor'],
                        "isOnline" =>  $row['isOnlineDoctor']
                    )
                );
                array_push($patients,$patient);
            }
        }

        return $patients;
    }

    function generateResponse($response){
        $events = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                array_push($events,$row);
            }
        }

        return $events;
    }

    function generateResult($patients){
        if($patients != null){
            return true;
        }
        else{
            return false;
        }
    }
?>