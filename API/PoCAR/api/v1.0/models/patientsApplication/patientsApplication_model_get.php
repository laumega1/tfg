<?php
    $patientsApplication = null;

    if(isset($query_params['ip'])){
        $response = sqlGetPatientsApplicationByIP($query_params, $conexion);

        $patientsApplication = generateObjectResponse($response);

        $result = generateResult($patientsApplication);
    }else if(isset($query_params['mac'])){
        $response = sqlGetPatientsApplicationByMAC($query_params, $conexion);

        $patientsApplication = generateObjectResponse($response);

        $result = generateResult($patientsApplication);
    }else if(isset($query_params['idRoom'])){
        $response = sqlGetPatientsApplicationByIdRoom($query_params, $conexion);

        $patientsApplication = generateResponse($response);

        $result = generateResult($patientsApplication);
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetPatientsApplicationByIdRoom($query_params, $conexion){
        $sql = 'SELECT `patientsapplication`.*
        FROM `patientsapplication`
        WHERE `patientsapplication`.idRoom = "'.$query_params['idRoom'].'"';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetPatientsApplicationByIP($query_params, $conexion){
        $sql = 'SELECT `patientsapplication`.*, 
        `centers`.name AS centerName, `centers`.phone AS centerPhone, `centers`.idBasicHealthZone AS idBasicHealthZone,
        `rooms`.name AS roomName, `rooms`.idCenter AS idCenter
        FROM `patientsapplication`, `centers`, `rooms`
        WHERE `patientsapplication`.ip = "'.$query_params['ip'].'"
        AND `patientsapplication`.idRoom = `rooms`.id
        AND `rooms`.idCenter = `centers`.id';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetPatientsApplicationByMAC($query_params, $conexion){
        $sql = 'SELECT `patientsapplication`.*, 
        `centers`.name AS centerName, `centers`.phone AS centerPhone, `centers`.idBasicHealthZone AS idBasicHealthZone,
        `rooms`.name AS roomName, `rooms`.idCenter AS idCenter
        FROM `patientsapplication`, `centers`, `rooms`
        WHERE `patientsapplication`.mac = "'.$query_params['mac'].'"
        AND `patientsapplication`.idRoom = `rooms`.id
        AND `rooms`.idCenter = `centers`.id';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function generateObjectResponse($response){
        $patientsApplication = null;
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                $patient = array(
                    "id" => $row['id'],
                    "ip" =>  $row['ip'],
                    "mac" => $row['mac'],
                    "idRoom" => $row['idRoom'],
                    "isEnabled" => $row['isEnabled'],
                    "center" => array(
                        "id" =>  $row['idCenter'],
                        "name" => $row['centerName'],
                        "phone" => $row['centerPhone'],
                        "idBasicHealthZone" => $row['idBasicHealthZone']
                    ),
                    "room" => array(
                        "id" =>  $row['idRoom'],
                        "name" => $row['roomName'],
                        "idCenter" => $row['idCenter']
                    )
                );
                $patientsApplication = $patient;
            }
        }

        return $patientsApplication;
    }

    function generateResponse($response){
        $patientsApplication = null;

        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                $patientsApplication = $row;
            }
        }

        return $patientsApplication;
    }

    function generateResult($patientsApplication){
        if($patientsApplication != null){
            return true;
        }
        else{
            return false;
        }
    }
?>