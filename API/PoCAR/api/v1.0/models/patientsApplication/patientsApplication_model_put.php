<?php
    $patientsApplication = null;

    if(isset($body_params['id']) && isset($body_params['isEnabled']) && !isset($body_params['onlyEnabled'])){
        $response = sqlPutPatientApplication($body_params, $conexion);

        $result = generateResult($response);
    }else if(isset($body_params['id']) && isset($body_params['isEnabled']) && isset($body_params['onlyEnabled'])){
        $response = sqlPutCurrentState($body_params, $conexion);
    
        $result = generateResult($response);
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPutCurrentState($body_params, $conexion){
        $sql = 'UPDATE `patientsapplication` 
        SET `isEnabled`="'.$body_params['isEnabled'].'"
        WHERE `id`='.$body_params['id'].'';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlPutPatientApplication($body_params, $conexion){
        $sql = 'UPDATE `patientsapplication` 
        SET `ip`="'.$body_params['ip'].'",
        `mac`="'.$body_params['mac'].'"
        WHERE `id`='.$body_params['id'].'';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function generateResult($response){
        if($response != null){
            return true;
        }
        else{
            return false;
        }
    }
?>