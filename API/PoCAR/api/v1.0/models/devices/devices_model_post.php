<?php
    $devices = null;

    $response = sqlPostNewDevice($body_params, $conexion);
    
    if($response != null){
        $output["id"] = mysqli_insert_id($conexion);
        $result = true;
    }
    else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPostNewDevice($body_params, $conexion){
        $sql = 'INSERT INTO `devices` (`idCoordinator`, `mac`, `service`, `characteristic`, `description`, `type`)
        VALUES (
        "'.$body_params['idCoordinator'].'",
        "'.$body_params['mac'].'",
        "'.$body_params['service'].'",
        "'.$body_params['characteristic'].'",
        "'.$body_params['description'].'",
        "'.$body_params['type'].'")';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>