<?php
    $devices = null;

    if(isset($query_params['idCoordinator']) && isset($query_params['type'])){
        $response = sqlGetDevicesByCoordinatorAndType($query_params, $conexion);

        $devices = generateResponse($response);

        $result = generateResult($devices);
    }else if(isset($query_params['all'])){
        $response = sqlGetAllDevices($conexion);

        $devices = generateMultipleResponse($response);

        $result = generateResult($devices);
    }else if(isset($query_params['mac'])){
        $response = sqlGetDevicesByMAC($query_params, $conexion);

        $devices = generateResponse($response);

        $result = generateResult($devices);
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetAllDevices($conexion){
        $sql = 'SELECT `devices`.*
        FROM `devices`';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetDevicesByMAC($query_params, $conexion){
        $sql = 'SELECT `devices`.*
        FROM `devices` 
        WHERE `devices`.mac = "'.$query_params['mac'].'"';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetDevicesByCoordinatorAndType($query_params, $conexion){
        $sql = 'SELECT `devices`.*
        FROM `devices` 
        WHERE `devices`.idCoordinator = "'.$query_params['idCoordinator'].'"
        AND `devices`.type = "'.$query_params['type'].'"';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function generateResponse($response){
        $devices = null;
        
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                $devices = $row;
            }
        }

        return $devices;
    }

    function generateMultipleResponse($response){
        $devices = array();
        
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                array_push($devices,$row);
            }
        }

        return $devices;
    }

    function generateResult($devices){
        if($devices != null){
            return true;
        }
        else{
            return false;
        }
    }
?>