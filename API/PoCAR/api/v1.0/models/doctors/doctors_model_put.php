<?php
    $doctor = null;

    if(isset($body_params['id'])){
        if(isset($body_params['isOnline']) && !isset($body_params['name']) && !isset($body_params['surname'])){
            $response = sqlPutOnlineDoctorState($body_params, $conexion);
        }else{
            $response = sqlPutDoctor($body_params, $conexion);
        }
        
        if($response != null){
            $result = true;
        }
        else{
            $result = false;
        }

    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPutDoctor($body_params, $conexion){
        if($body_params['isPasswordChanged'] == true){
            $hash = password_hash($body_params['password'], PASSWORD_DEFAULT);

            $sql = 'UPDATE `doctors` 
            SET `name`="'.$body_params['name'].'",`surname`="'.$body_params['surname'].'",`password`="'.$hash.'",`isAdmin`="'.$body_params['isAdmin'].'"
            WHERE `id`='.$body_params['id'].'';
        }else{
            $sql = 'UPDATE `doctors` 
            SET `name`="'.$body_params['name'].'",`surname`="'.$body_params['surname'].'",`isAdmin`="'.$body_params['isAdmin'].'"
            WHERE `id`='.$body_params['id'].'';
        }

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlPutOnlineDoctorState($body_params, $conexion){
        $sql = 'UPDATE `doctors` 
        SET `isOnline`="'.$body_params['isOnline'].'"
        WHERE `id`='.$body_params['id'].'';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>