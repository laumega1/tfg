<?php
    $doctor = null;

    if(isset($body_params['name']) && isset($body_params['email']) && isset($body_params['password']) && isset($body_params['isAdmin'])){
        $response = sqlPostNewDoctor($body_params, $conexion);
        
        if($response != null){
            $output['message']['token'] = password_hash(mysqli_insert_id($conexion), PASSWORD_DEFAULT);
            $output["id"] = mysqli_insert_id($conexion);

            $result = true;
        }
        else{
            $result = false;
        }

    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPostNewDoctor($body_params, $conexion){
        $hash = password_hash($body_params['password'], PASSWORD_DEFAULT);

        $sql = 'INSERT INTO `doctors` (`name`, `surname`, `email`, `password`, `isAdmin`, `isOnline`)
        VALUES (
        "'.$body_params['name'].'",
        "'.$body_params['surname'].'",
        "'.$body_params['email'].'",
        "'.$hash.'",
        "'.$body_params['isAdmin'].'",
        "false")';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>