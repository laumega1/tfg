<?php
    $doctors = null;

    if(isset($query_params['id'])){
        $response = sqlGetDoctorById($query_params, $conexion);

        $doctors = generateResponse($response);
        
        $result = generateResult($doctors);
    }else if(isset($query_params['idBasicHealthZone'])){
        $response = sqlGetDoctorsByBasicHealthZone($query_params, $conexion);

        $doctors = generateResponse($response);

        $result = generateResult($doctors);
    }else if(isset($query_params['all'])){
        $response = sqlGetAllDoctors($conexion);

        $doctors = generateResponse($response);

        $result = generateResult($doctors);
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetAllDoctors($conexion){
        $sql = 'SELECT * FROM `doctors`';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetDoctorById($query_params, $conexion){
        $sql = 'SELECT * 
        FROM `doctors` 
        WHERE `id`="'.$query_params['id'].'"';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetDoctorsByBasicHealthZone($query_params, $conexion){
        $sql = 'SELECT `doctors`.* 
        FROM `doctors`, `doctorsbasichealthzones`
        WHERE `doctorsbasichealthzones`.`idBasicHealthZone`="'.$query_params['idBasicHealthZone'].'"
        AND `doctorsbasichealthzones`.idDoctor = `doctors`.`id`';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function generateResponse($response){
        $doctors = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                $row['showData'] = $row['name']." ".$row['surname'];
                array_push($doctors,$row);
            }
        }

        return $doctors;
    }

    function generateResult($doctors){
        if($doctors != null){
            return true;
        }
        else{
            return false;
        }
    }
?>