<?php
    $hospitals = null;

    if(isset($query_params['id'])){
        if(isset($query_params['idPatient'])){
            $response = sqlGetAllPatientAndDoctorHospitals($query_params, $conexion);
        }else{
            $response = sqlGetAllDoctorHospitals($query_params, $conexion);
        }

        $hospitals = generateResponse($response);
        $result = generateResult($hospitals);
    }else if(isset($query_params['idPatient'])){
        $response = sqlGetAllPatientHospitals($query_params, $conexion);

        $hospitals = generateResponse($response);
        $result = generateResult($hospitals);
    }
    else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetAllPatientAndDoctorHospitals($query_params, $conexion){
        $sql = 'SELECT `hospitals`.*
        FROM `hospitals`, `usershospitals`, `patientshospitals`
        WHERE `usershospitals`.idUser = "'.$query_params['id'].'" 
        AND `usershospitals`.idHospital = `hospitals`.id
        AND `patientshospitals`.idPatient = "'.$query_params['idPatient'].'"
        AND `patientshospitals`.idHospital = `hospitals`.id
        GROUP BY `hospitals`.id';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetAllDoctorHospitals($query_params, $conexion){
        $sql = 'SELECT `hospitals`.* 
        FROM `hospitals`, `usershospitals` 
        WHERE `usershospitals`.idUser = "'.$query_params['id'].'" 
        AND `usershospitals`.idHospital = `hospitals`.id';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetAllPatientHospitals($query_params, $conexion){
        $sql = 'SELECT `hospitals`.* 
        FROM `hospitals`, `usershospitals`, `patientshospitals`
        WHERE `patientshospitals`.idPatient = "'.$query_params['idPatient'].'"
        AND `patientshospitals`.idHospital = `hospitals`.id
        GROUP BY `hospitals`.id';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function generateResponse($response){
        $hospitals = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                array_push($hospitals,$row);
            }
        }

        return $hospitals;
    }

    function generateResult($hospitals){
        if($hospitals != null){
            return true;
        }
        else{
            return false;
        }
    }
?>