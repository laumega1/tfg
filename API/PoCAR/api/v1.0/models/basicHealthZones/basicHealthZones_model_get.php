<?php
    $basicHealthZones = null;

    if(isset($query_params['idDoctor'])){
        $response = sqlGetAllDoctorBasicHealthZones($query_params, $conexion);

        $basicHealthZones = generateResponse($response);

        $result = generateResult($basicHealthZones);
    }else if(isset($query_params['all'])){
        $response = sqlGetAllBasicHealthZones($conexion);

        $basicHealthZones = generateResponse($response);

        $result = generateResult($basicHealthZones);
    }else if(isset($query_params['id'])){
        $response = sqlGetBasicHealthZonesById($query_params, $conexion);

        $basicHealthZones = generateResponse($response);

        $result = generateResult($basicHealthZones);
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetAllDoctorBasicHealthZones($query_params, $conexion){
        $sql = 'SELECT `basichealthzones`.*
        FROM `basichealthzones`, `doctorsbasichealthzones`
        WHERE `doctorsbasichealthzones`.idDoctor = "'.$query_params['idDoctor'].'"
        AND `basichealthzones`.id = `doctorsbasichealthzones`.idBasicHealthZone';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetBasicHealthZonesById($query_params, $conexion){
        $sql = 'SELECT `basichealthzones`.*
        FROM `basichealthzones`
        WHERE `basichealthzones`.id = "'.$query_params['id'].'"';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetAllBasicHealthZones($conexion){
        $sql = 'SELECT `basichealthzones`.*
        FROM `basichealthzones`';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function generateResponse($response){
        $basicHealthZones = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                array_push($basicHealthZones,$row);
            }
        }

        return $basicHealthZones;
    }

    function generateResult($basicHealthZones){
        if($basicHealthZones != null){
            return true;
        }
        else{
            return false;
        }
    }
?>