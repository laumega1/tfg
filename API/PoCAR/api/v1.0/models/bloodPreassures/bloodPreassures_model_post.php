<?php
    $bloodPreassures = null;

    $response = sqlPostNewBloodPreassures($body_params, $conexion);
    
    if($response != null){
        $output["id"] = mysqli_insert_id($conexion);
        $result = true;
    }
    else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPostNewBloodPreassures($body_params, $conexion){
        $sql = 'INSERT INTO `bloodpreassures` (`idPatient`, `systolicValue`, `diastolicValue`, `rateValue`, `systolicUnit`, `diastolicUnit`, `rateUnit`)
        VALUES (
        "'.$body_params['idPatient'].'",
        "'.$body_params['systolicValue'].'",
        "'.$body_params['diastolicValue'].'",
        "'.$body_params['rateValue'].'",
        "'.$body_params['systolicUnit'].'",
        "'.$body_params['diastolicUnit'].'",
        "'.$body_params['rateUnit'].'")';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>