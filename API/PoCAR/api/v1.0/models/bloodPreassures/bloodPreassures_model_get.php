<?php
    $bloodPreassures = null;

    if(isset($query_params['idPatient']) && isset($query_params['startDate']) && isset($query_params['endDate'])){
        $response = sqlGetBloodPreassuresBetweenTwoDates($query_params, $conexion);

        $bloodPreassures = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                array_push($bloodPreassures, $row);
            }
        }

        if($bloodPreassures != null){
            $result = true;
        }
        else{
            $result = false;
        }
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetBloodPreassuresBetweenTwoDates($query_params, $conexion){
        $start = date('Y/m/d H:i:s', strtotime($query_params['startDate']));
        $end = date('Y/m/d H:i:s', strtotime($query_params['endDate']));

        $sql = 'SELECT *
        FROM `bloodpreassures` 
        WHERE `bloodpreassures`.idPatient = "'.$query_params['idPatient'].'"
        AND `bloodpreassures`.timestamp BETWEEN "'.$start.'" AND "'.$end.'"
        ORDER BY `bloodpreassures`.timestamp';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>