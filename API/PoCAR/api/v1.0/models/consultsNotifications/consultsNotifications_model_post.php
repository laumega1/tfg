<?php
    $consultNotification = null;

    $response = sqlPostNewNotification($body_params, $conexion);
    
    if($response != null){
        $output["id"] = mysqli_insert_id($conexion);
        $result = true;
    }
    else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPostNewNotification($body_params, $conexion){
        $sql = 'INSERT INTO `consultsnotifications` (`macDevice`, `idRoom`, `action`, `sip`, `idDoctor`, `idEvent`)
        VALUES (
        "'.$body_params['message']['mac'].'",
        "'.$body_params['message']['data']['idRoom'].'",
        "'.$body_params['message']['data']['action'].'",
        "'.$body_params['message']['data']['sip'].'",
        "'.$body_params['message']['data']['idDoctor'].'",
        "'.$body_params['message']['data']['idEvent'].'")';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>