<?php
    $consultNotification = null;

    if(isset($query_params['startDate']) && isset($query_params['endDate']) && isset($query_params['idDoctor'])){
        $response = sqlGetAllNotifications($query_params, $conexion);

        $consultNotification = generateResponse($response);

        $result = generateResult($consultNotification);
    }else if(isset($query_params['mac']) && isset($query_params['idRoom']) && isset($query_params['sip']) && isset($query_params['idDoctor'])){
        $response = sqlGetSpecificNotification($query_params, $conexion);
        
        $consultNotification = generateResponse($response);

        $result = generateResult($consultNotification);
    }else if(isset($query_params['mac']) && isset($query_params['idRoom'])){
        $response = sqlGetCurrentDayArriveNotificationsByDevice($query_params, $conexion);
        
        $consultNotification = generateResponse($response);

        $result = generateResult($consultNotification);
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetAllNotifications($query_params, $conexion){
        $sql = 'SELECT `consultsnotifications`.*
        FROM `consultsnotifications`
        WHERE `consultsnotifications`.timestamp BETWEEN "'.$query_params['startDate'].'" AND "'.$query_params['endDate'].'"
        AND `consultsnotifications`.idDoctor = "'.$query_params['idDoctor'].'"
        ORDER BY `consultsnotifications`.timestamp';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetSpecificNotification($query_params, $conexion){
        $sql = 'SELECT `consultsnotifications`.*
        FROM `consultsnotifications`
        WHERE `consultsnotifications`.macDevice = "'.$query_params['mac'].'"
        AND `consultsnotifications`.idRoom = "'.$query_params['idRoom'].'"
        AND `consultsnotifications`.sip = "'.$query_params['sip'].'"
        AND `consultsnotifications`.idDoctor = "'.$query_params['idDoctor'].'"
        AND DATE(`consultsnotifications`.timestamp) = CURDATE()
        ORDER BY `consultsnotifications`.timestamp';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetCurrentDayArriveNotificationsByDevice($query_params, $conexion){
        $sql = 'SELECT `consultsnotifications`.*
        FROM `consultsnotifications`
        WHERE `consultsnotifications`.macDevice = "'.$query_params['mac'].'"
        AND `consultsnotifications`.idRoom = "'.$query_params['idRoom'].'"
        AND `consultsnotifications`.action = "arrive"
        AND DATE(`consultsnotifications`.timestamp) = CURDATE()
        ORDER BY `consultsnotifications`.timestamp';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function generateResponse($response){
        $consultNotification = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                array_push($consultNotification,$row);
            }
        }

        return $consultNotification;
    }

    function generateResult($consultNotification){
        if($consultNotification != null){
            return true;
        }
        else{
            return false;
        }
    }
?>