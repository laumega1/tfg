<?php
    $patients = null;

    $response = sqlPostNewPatient($body_params, $conexion);
    
    if($response != null){
        $output["id"] = mysqli_insert_id($conexion);
        $result = true;
    }
    else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPostNewPatient($body_params, $conexion){
        $sql = 'INSERT INTO `patients` (`name`, `surname`, `sip`, `age`, `idDoctor`, `idCenter`)
        VALUES (
        "'.$body_params['name'].'",
        "'.$body_params['surname'].'",
        "'.$body_params['sip'].'",
        "'.$body_params['age'].'",
        "'.$body_params['idDoctor'].'",
        "'.$body_params['idCenter'].'")';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>