<?php
    $patients = null;

    if(isset($query_params['idDoctor'])){
        if(isset($query_params['idHospital'])){
            $response = sqlGetAllDoctorAndHospitalPatients($query_params, $conexion);

            $patients = generateResponse($response);
        }else{
            $response = sqlGetAllDoctorPatients($query_params, $conexion);

            $patients = generateObjectResponse($response);
        }

        $result = generateResult($patients);
    }else if(isset($query_params['sip'])){
        $response = sqlGetPatientBySIP($query_params, $conexion);

        $patients = generateResponse($response);

        $result = generateResult($patients);
    }else if(isset($query_params['all'])){
        $response = sqlGetAllPatients($conexion);

        $patients = generateResponse($response);

        $result = generateResult($patients);
    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlGetPatientBySIP($query_params, $conexion){
        $sql = 'SELECT * 
        FROM `patients` 
        WHERE `patients`.sip = "'.$query_params['sip'].'"';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetAllDoctorAndHospitalPatients($query_params, $conexion){
        $sql = 'SELECT `patients`.id, `patients`.name, `patients`.surname, `patients`.sip, `patients`.age 
        FROM `patients`, `patientshospitals`, `users`, `usershospitals` 
        WHERE `usershospitals`.idUser = "'.$query_params['idDoctor'].'"
        AND `patientshospitals`.idHospital = "'.$query_params['idHospital'].'"
        AND `patientshospitals`.idPatient = `patients`.id
        AND `usershospitals`.idHospital = `patientshospitals`.idHospital
        GROUP BY `patients`.id';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetAllDoctorPatients($query_params, $conexion){
        $sql = 'SELECT `patients`.*, 
        `centers`.name AS centerName, `centers`.phone AS centerPhone, `centers`.idBasicHealthZone AS idBasicHealthZone,
        `basichealthzones`.name AS basicHealthZoneName,
        `doctors`.name AS nameDoctor, `doctors`.surname AS surnameDoctor, `doctors`.email AS emailDoctor, `doctors`.password AS passwordDoctor, `doctors`.isAdmin AS isAdminDoctor, `doctors`.isOnline AS isOnline
        FROM `patients`, `basichealthzones`, `doctors`, `doctorsbasichealthzones`, `centers`
        WHERE `patients`.`idCenter` = `centers`.id
        AND `centers`.idBasicHealthZone = `basichealthzones`.`id`
        AND `centers`.idBasicHealthZone = `doctorsbasichealthzones`.idBasicHealthZone
        AND `doctorsbasichealthzones`.idBasicHealthZone = `basichealthzones`.`id`
        AND `doctorsbasichealthzones`.idDoctor = "'.$query_params['idDoctor'].'"
        AND `doctors`.id = `patients`.idDoctor';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function sqlGetAllPatients($conexion){
        $sql = 'SELECT *  FROM `patients`';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }

    function generateResponse($response){
        $patients = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                $row['showData'] = $row['name']." ".$row['surname']." · ".$row['sip'];
                array_push($patients,$row);
            }
        }

        return $patients;
    }

    function generateObjectResponse($response){
        $patients = array();
        if($response){
            while ($row = mysqli_fetch_assoc($response)) {
                $patient = array(
                    "id" => $row['id'],
                    "name" =>  $row['name'],
                    "surname" => $row['surname'],
                    "sip" => $row['sip'],
                    "age" =>  $row['age'],
                    "idDoctor" => $row['idDoctor'],
                    "idCenter" =>  $row['idCenter'],
                    "showData" => $row['name']." ".$row['surname']." · ".$row['sip'],
                    "center" => array(
                        "id" =>  $row['idCenter'],
                        "name" =>  $row['centerName'],
                        "phone" =>  $row['centerPhone'],
                        "idBasicHealthZone" =>  $row['idBasicHealthZone']
                    ),
                    "basicHealthZone" => array(
                        "id" =>  $row['idBasicHealthZone'],
                        "name" => $row['basicHealthZoneName']
                    ),
                    "doctor" => array(
                        "id" =>  $row['idDoctor'],
                        "name" =>  $row['nameDoctor'],
                        "surname" =>  $row['surnameDoctor'],
                        "showData" => $row['nameDoctor']." ".$row['surnameDoctor'],
                        "email" =>  $row['emailDoctor'],
                        "password" =>  $row['passwordDoctor'],
                        "isAdmin" =>  $row['isAdminDoctor'],
                        "isOnline" =>  $row['isOnline']
                    )
                );
                array_push($patients,$patient);
            }
        }

        return $patients;
    }

    function generateResult($patients){
        if($patients != null){
            return true;
        }
        else{
            return false;
        }
    }
?>