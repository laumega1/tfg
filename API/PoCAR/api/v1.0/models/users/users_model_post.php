<?php
    $user = null;

    if(isset($form_params['name']) && isset($form_params['email']) && isset($form_params['password']) && isset($form_params['isAdmin'])){
        $response = sqlPostNewUser($form_params, $conexion);
        
        if($response != null){
            $output['message']['token'] = password_hash(mysqli_insert_id($conexion), PASSWORD_DEFAULT);

            $result = true;
        }
        else{
            $result = false;
        }

    }else{
        $result = false;
    }

    //-----------------------------------------------------------------------------------------------
    //-------------------------------------- FUNCTIONS ----------------------------------------------
    //-----------------------------------------------------------------------------------------------

    function sqlPostNewUser($form_params, $conexion){
        $hash = password_hash($form_params['password'], PASSWORD_DEFAULT);

        $sql = 'INSERT INTO `users` (`name`, `surname`, `email`, `password`, `isAdmin`)
        VALUES (
        "'.$form_params['name'].'",
        "'.$form_params['surname'].'",
        "'.$form_params['email'].'",
        "'.$hash.'",
        "'.$form_params['isAdmin'].'")';

        $response = mysqli_query($conexion, $sql);

        return $response;
    }
?>