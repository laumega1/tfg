-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-07-2022 a las 21:24:03
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pocar`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `basichealthzones`
--

CREATE TABLE `basichealthzones` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bloodpreassures`
--

CREATE TABLE `bloodpreassures` (
  `id` int(11) NOT NULL,
  `idPatient` int(11) NOT NULL,
  `systolicValue` float NOT NULL,
  `diastolicValue` float NOT NULL,
  `rateValue` float NOT NULL,
  `systolicUnit` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `diastolicUnit` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `rateUnit` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `centers`
--

CREATE TABLE `centers` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `idBasicHealthZone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `consultsnotifications`
--

CREATE TABLE `consultsnotifications` (
  `id` int(11) NOT NULL,
  `macDevice` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `idRoom` int(11) NOT NULL,
  `idEvent` int(11) NOT NULL,
  `idDoctor` int(11) NOT NULL,
  `action` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `sip` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinators`
--

CREATE TABLE `coordinators` (
  `id` int(11) NOT NULL,
  `mac` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `idRoom` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `devices`
--

CREATE TABLE `devices` (
  `id` int(11) NOT NULL,
  `idCoordinator` int(11) NOT NULL,
  `mac` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `service` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `characteristic` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `description` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `type` text COLLATE utf8mb4_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `surname` text COLLATE utf8mb4_spanish_ci DEFAULT NULL,
  `email` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `password` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `isAdmin` tinyint(1) NOT NULL,
  `isOnline` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doctorsbasichealthzones`
--

CREATE TABLE `doctorsbasichealthzones` (
  `id` int(11) NOT NULL,
  `idDoctor` int(11) NOT NULL,
  `idBasicHealthZone` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `idPatient` int(11) NOT NULL,
  `idCenter` int(11) NOT NULL,
  `idDoctor` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `primaryColor` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `secundaryColor` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `startDate` datetime NOT NULL,
  `endDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patients`
--

CREATE TABLE `patients` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `surname` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `sip` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `age` int(11) NOT NULL,
  `idDoctor` int(11) NOT NULL,
  `idCenter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patientsapplication`
--

CREATE TABLE `patientsapplication` (
  `id` int(11) NOT NULL,
  `ip` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `mac` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `idRoom` int(11) NOT NULL,
  `isEnabled` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pulseoximeter`
--

CREATE TABLE `pulseoximeter` (
  `id` int(11) NOT NULL,
  `idPatient` int(11) NOT NULL,
  `oxygenValue` float NOT NULL,
  `beatsValue` float NOT NULL,
  `oxygenUnit` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `beatsUnit` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `idCenter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `temperatures`
--

CREATE TABLE `temperatures` (
  `id` int(11) NOT NULL,
  `idPatient` int(11) NOT NULL,
  `value` float NOT NULL,
  `unit` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `weights`
--

CREATE TABLE `weights` (
  `id` int(11) NOT NULL,
  `idPatient` int(11) NOT NULL,
  `value` float NOT NULL,
  `unit` text COLLATE utf8mb4_spanish_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `basichealthzones`
--
ALTER TABLE `basichealthzones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `bloodpreassures`
--
ALTER TABLE `bloodpreassures`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `centers`
--
ALTER TABLE `centers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `consultsnotifications`
--
ALTER TABLE `consultsnotifications`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `coordinators`
--
ALTER TABLE `coordinators`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `devices`
--
ALTER TABLE `devices`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `doctorsbasichealthzones`
--
ALTER TABLE `doctorsbasichealthzones`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `patientsapplication`
--
ALTER TABLE `patientsapplication`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `pulseoximeter`
--
ALTER TABLE `pulseoximeter`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `temperatures`
--
ALTER TABLE `temperatures`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `weights`
--
ALTER TABLE `weights`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `basichealthzones`
--
ALTER TABLE `basichealthzones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `bloodpreassures`
--
ALTER TABLE `bloodpreassures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `centers`
--
ALTER TABLE `centers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `consultsnotifications`
--
ALTER TABLE `consultsnotifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `coordinators`
--
ALTER TABLE `coordinators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `devices`
--
ALTER TABLE `devices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `doctorsbasichealthzones`
--
ALTER TABLE `doctorsbasichealthzones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `patients`
--
ALTER TABLE `patients`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `patientsapplication`
--
ALTER TABLE `patientsapplication`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `pulseoximeter`
--
ALTER TABLE `pulseoximeter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `temperatures`
--
ALTER TABLE `temperatures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `weights`
--
ALTER TABLE `weights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
